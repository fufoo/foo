package zeroclient

import (
	"context"
	"crypto/x509"
	"encoding/base64"
	"os"
	"sync"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/foo/internal/services/platform"
	"bitbucket.org/fufoo/foo/internal/tls"
	"bitbucket.org/fufoo/qrpc"
	"github.com/golang-jwt/jwt/v4"
)

var log = logging.New("zeroclient")

type Certificate struct {
	CertFile  string
	KeyFile   string
	TrustFile string
}

type Config struct {
	Server    string
	CertFile  string
	KeyFile   string
	TrustFile string
	Advertise string
	ID        string
	Name      string
}

func ConfigFromEnv(ctx context.Context) *Config {
	ok := true

	advertise := os.Getenv("FOO_NODE_ADVERTISE")
	if advertise == "" {
		log.Errorf(ctx, "No node advertisement address specified (FOO_NODE_ADVERTISE)")
		ok = false

	}

	certfile := os.Getenv("FOO_NODE_CERT")
	if certfile == "" {
		log.Errorf(ctx, "No node certificate file specified (FOO_NODE_CERT)")
		ok = false
	}

	keyfile := os.Getenv("FOO_NODE_KEY")
	if keyfile == "" {
		log.Errorf(ctx, "No node certificate key file specified (FOO_NODE_KEY)")
		ok = false
	}

	trust := os.Getenv("FOO_CA_TRUST")
	if trust == "" {
		log.Errorf(ctx, "No trust file specified (FOO_CA_TRUST)")
		ok = false
	}

	name := os.Getenv("FOO_NODE_NAME")
	if name == "" {
		log.Errorf(ctx, "No node name specified (FOO_NODE_NAME)")
		ok = false
	}

	id := os.Getenv("FOO_NODE_ID")
	if id == "" {
		log.Errorf(ctx, "No node id specified (FOO_NODE_ID)")
		ok = false
	}

	server := os.Getenv("FOO_SERVER")
	if id == "" {
		log.Errorf(ctx, "No zero server specified (FOO_SERVER)")
		ok = false
	}

	if !ok {
		return nil
	}
	return &Config{
		Advertise: advertise,
		ID:        id,
		Name:      name,
		Server:    server,
		CertFile:  certfile,
		KeyFile:   keyfile,
		TrustFile: trust,
	}
}

type XConfig struct {
	TrustFile string
	Server    string
	Keepalive bool
}

/*

** we no longer let external services talk to the zero server; they
** should get their bootstrap info during token exchange with the auth
** server, which is designed to scale horizontally

func NewExternal(ctx context.Context, cfg *XConfig) (platform.ZeroClient, error) {
	t, err := tls.PlatformClient(tls.Trust(cfg.TrustFile))
	if err != nil {
		return nil, err
	}
	c, err := qrpc.Dial(cfg.Server, qrpc.DialWithTLS(t))
	if err != nil {
		return nil, err
	}
	if cfg.Keepalive {
		go c.Keepalive(ctx, 15*time.Second)
	}
	return platform.NewZeroClient(c), nil
}
*/

// New creates a zero client using mutual TLS (when that becomes
// possible) to the zero server, as used by platform services
func New(ctx context.Context, cfg *Config) (platform.ZeroQClient, error) {

	t, err := tls.PlatformClientMutual(cfg.CertFile, cfg.KeyFile, tls.Trust(cfg.TrustFile))
	if err != nil {
		return nil, err
	}
	c, err := qrpc.Dial(ctx, cfg.Server, qrpc.DialWithTLS(t))
	if err != nil {
		return nil, err
	}
	go c.Keepalive(ctx, 15*time.Second)
	return platform.NewZeroQClient(c), nil
}

// Bootstrap is a helper function to obtain the initial bootstrap
// information and keep a watch on it
func Bootstrap(ctx context.Context, kind string, zc platform.ZeroQClient, cfg *Config) *SystemModel {
	sm := &SystemModel{}
	sm.bump = sync.NewCond(&sm.lock)
	go sm.maintain(ctx, kind, zc, cfg)
	return sm
}

type State struct {
	info      *platform.BootstrapNodeResponse
	err       error
	connected bool
	seq       uint64
}

type SystemModel struct {
	current *State
	lock    sync.Mutex
	bump    *sync.Cond
	seq     uint64
}

func (s *State) Deployment() *platform.Deployment {
	if s.info == nil {
		return nil
	}
	return s.info.Deployment
}

func (s *State) Keys() *platform.KeySet {
	if s.info == nil {
		return nil
	}
	return s.info.Keys
}

func (s *State) Token() string {
	if s.info == nil {
		return ""
	}
	return s.info.Token.Token
}

func (s *State) Connected() bool {
	return s.connected
}

func (s *State) Error() error {
	return s.err
}

func (sys *SystemModel) Next(after *State) *State {
	sys.lock.Lock()
	// note that the first time (after==nil) we will wait for the first info
	for sys.current == after {
		sys.bump.Wait()
	}
	state := sys.current
	sys.lock.Unlock()
	return state
}

func (sys *SystemModel) maintain(ctx context.Context, kind string, zc platform.ZeroQClient, cfg *Config) {

retryloop:

	for {
		log.Debugf(ctx, "bootstrapping this node (seq=%d)", sys.seq)

		info, err := zc.BootstrapNode(ctx, &platform.BootstrapNodeRequest{
			Kind: kind,
			Node: &platform.Node{
				Name: cfg.Name,
				Addr: cfg.Advertise,
				Seed: cfg.ID,
				// Weight: int32(cc.Int("weight")),
			},
		})
		if err != nil {
			log.Warningf(ctx, "initial error: %s", err)
			sys.lock.Lock()
			sys.seq++
			sys.current = &State{
				err:       err,
				seq:       sys.seq,
				connected: false,
			}
			sys.bump.Broadcast()
			sys.lock.Unlock()
			time.Sleep(100 * time.Millisecond)
			continue retryloop
		}
		for {
			item, err := info.Recv()
			if err != nil {
				log.Warningf(ctx, "subseq error: %s", err)
				sys.lock.Lock()
				sys.seq++
				sys.current = &State{
					err:       err,
					seq:       sys.seq,
					connected: false,
				}
				sys.bump.Broadcast()
				sys.lock.Unlock()
				continue retryloop
			}
			log.Infof(ctx, "config: {%s}", item)

			sys.lock.Lock()
			sys.seq++
			sys.current = &State{
				info:      item,
				seq:       sys.seq,
				connected: true,
			}
			sys.bump.Broadcast()
			sys.lock.Unlock()
		}
	}
}

func (sys *SystemModel) keyfn(dst **platform.PublicKey) func(tok *jwt.Token) (any, error) {
	return func(tok *jwt.Token) (any, error) {
		sys.lock.Lock()
		defer sys.lock.Unlock()

		if sys.current == nil {
			return nil, errlib.Newf("FAZ-8401 system state not obtained")
		}

		iss := tok.Claims.(*jwt.RegisteredClaims).Issuer
		entry, ok := sys.current.info.Keys.Index[iss]
		if !ok {
			return nil, errlib.Newf("FAZ-8400 issuer %q is not known", iss)
		}
		dt := time.Since(entry.Expires.AsTime())
		if dt > 0 {
			return nil, errlib.Newf("FAZ-8403 issuer %q expired %s ago", iss, dt)
		}

		pub, err := base64.StdEncoding.DecodeString(entry.Public)
		if err != nil {
			return nil, err
		}

		*dst = entry
		return x509.ParsePKIXPublicKey(pub)
	}
}

func (s *SystemModel) VerifyToken(ctx context.Context, token string) (string, error) {
	var jwtparser = jwt.NewParser(jwt.WithValidMethods([]string{"ES384"}))

	var claims jwt.RegisteredClaims
	var pub *platform.PublicKey

	_, err := jwtparser.ParseWithClaims(token, &claims, s.keyfn(&pub))
	if err != nil {
		return "", err
	}
	log.Debugf(ctx, "authority is %s", pub.Authority)
	return claims.Subject, nil
}

// generated
package platform

import (
	"bufio"
	"context"

	"bitbucket.org/fufoo/qrpc"
	"google.golang.org/protobuf/proto"
)

type ZeroQServer interface {
	BootstrapNode(context.Context, *BootstrapNodeRequest) (Zero_BootstrapNodeQClient, error)
	AddExternalAuthKey(context.Context, *AddExternalAuthKeyRequest) (*AddExternalAuthKeyResponse, error)
}

type Zero_BootstrapNodeQClient interface {
	Recv() (*BootstrapNodeResponse, error)
}

type ZeroQClient interface {
	BootstrapNode(context.Context, *BootstrapNodeRequest) (Zero_BootstrapNodeQClient, error)
	AddExternalAuthKey(context.Context, *AddExternalAuthKeyRequest) (*AddExternalAuthKeyResponse, error)
}

type simpleZeroImpl struct {
	rpc *qrpc.Client
}

func NewZeroQClient(rpc *qrpc.Client) ZeroQClient {
	return &simpleZeroImpl{
		rpc: rpc,
	}
}

func (c *simpleZeroImpl) BootstrapNode(ctx context.Context, msg *BootstrapNodeRequest) (Zero_BootstrapNodeQClient, error) {
	var ret BootstrapNodeResponse
	ch := make(chan interface{}, 10)
	err := c.rpc.RPCServerStream(ctx, "qrpc://foo.internal.services.platform/Zero/BootstrapNode", msg, &ret, ch)
	if err != nil {
		return nil, err
	}
	return simpleZeroBootstrapNodeStreamer{ch}, nil
}

type simpleZeroBootstrapNodeStreamer struct {
	src <-chan interface{}
}

func (s simpleZeroBootstrapNodeStreamer) Recv() (*BootstrapNodeResponse, error) {
	item, ok := <-s.src
	if !ok {
		return nil, qrpc.EOF
	}
	if err, ok := item.(*qrpc.RPCError); ok {
		return nil, err
	}
	return item.(*BootstrapNodeResponse), nil
}

type rxZero_BootstrapNode struct {
	next ZeroQServer
}

func (b rxZero_BootstrapNode) Handle(ctx context.Context, _ qrpc.Stream, dst *bufio.Writer, recv []byte) error {
	var req BootstrapNodeRequest

	err := proto.Unmarshal(recv, &req)
	if err != nil {
		return err
	}
	resp, err := b.next.BootstrapNode(ctx, &req)

	if err != nil {
		return qrpc.WriteResponse(ctx, dst, nil, err)
	}
	for {
		item, err := resp.Recv()
		if err != nil {
			if !qrpc.IsEOF(err) {
				return qrpc.WriteResponse(ctx, dst, nil, err)
			}
			return nil
		}
		qrpc.WriteResponse(ctx, dst, item, nil)
	}

}

func (c *simpleZeroImpl) AddExternalAuthKey(ctx context.Context, msg *AddExternalAuthKeyRequest) (*AddExternalAuthKeyResponse, error) {
	var ret AddExternalAuthKeyResponse
	err := c.rpc.RPC(ctx, "qrpc://foo.internal.services.platform/Zero/AddExternalAuthKey", msg, &ret)
	if err != nil {
		return nil, err
	}
	return &ret, nil
}

type rxZero_AddExternalAuthKey struct {
	next ZeroQServer
}

func (b rxZero_AddExternalAuthKey) Handle(ctx context.Context, _ qrpc.Stream, dst *bufio.Writer, recv []byte) error {
	var req AddExternalAuthKeyRequest

	err := proto.Unmarshal(recv, &req)
	if err != nil {
		return err
	}
	resp, err := b.next.AddExternalAuthKey(ctx, &req)

	return qrpc.WriteResponse(ctx, dst, resp, err)

}

func RegisterZeroQServer(s *qrpc.Server, srv ZeroQServer) {
	s.RegisterServiceMethod(srv, "qrpc://foo.internal.services.platform/Zero/BootstrapNode", rxZero_BootstrapNode{srv})
	s.RegisterServiceMethod(srv, "qrpc://foo.internal.services.platform/Zero/AddExternalAuthKey", rxZero_AddExternalAuthKey{srv})
}

type AuthQServer interface {
	Salt(context.Context, *SaltRequest) (*SaltResponse, error)
	Exchange(context.Context, *ExchangeRequest) (*ExchangeResponse, error)
	Enroll(context.Context, *EnrollRequest) (*EnrollResponse, error)
}

type AuthQClient interface {
	Salt(context.Context, *SaltRequest) (*SaltResponse, error)
	Exchange(context.Context, *ExchangeRequest) (*ExchangeResponse, error)
	Enroll(context.Context, *EnrollRequest) (*EnrollResponse, error)
}

type simpleAuthImpl struct {
	rpc *qrpc.Client
}

func NewAuthQClient(rpc *qrpc.Client) AuthQClient {
	return &simpleAuthImpl{
		rpc: rpc,
	}
}

func (c *simpleAuthImpl) Salt(ctx context.Context, msg *SaltRequest) (*SaltResponse, error) {
	var ret SaltResponse
	err := c.rpc.RPC(ctx, "qrpc://foo.internal.services.platform/Auth/Salt", msg, &ret)
	if err != nil {
		return nil, err
	}
	return &ret, nil
}

type rxAuth_Salt struct {
	next AuthQServer
}

func (b rxAuth_Salt) Handle(ctx context.Context, _ qrpc.Stream, dst *bufio.Writer, recv []byte) error {
	var req SaltRequest

	err := proto.Unmarshal(recv, &req)
	if err != nil {
		return err
	}
	resp, err := b.next.Salt(ctx, &req)

	return qrpc.WriteResponse(ctx, dst, resp, err)

}

func (c *simpleAuthImpl) Exchange(ctx context.Context, msg *ExchangeRequest) (*ExchangeResponse, error) {
	var ret ExchangeResponse
	err := c.rpc.RPC(ctx, "qrpc://foo.internal.services.platform/Auth/Exchange", msg, &ret)
	if err != nil {
		return nil, err
	}
	return &ret, nil
}

type rxAuth_Exchange struct {
	next AuthQServer
}

func (b rxAuth_Exchange) Handle(ctx context.Context, _ qrpc.Stream, dst *bufio.Writer, recv []byte) error {
	var req ExchangeRequest

	err := proto.Unmarshal(recv, &req)
	if err != nil {
		return err
	}
	resp, err := b.next.Exchange(ctx, &req)

	return qrpc.WriteResponse(ctx, dst, resp, err)

}

func (c *simpleAuthImpl) Enroll(ctx context.Context, msg *EnrollRequest) (*EnrollResponse, error) {
	var ret EnrollResponse
	err := c.rpc.RPC(ctx, "qrpc://foo.internal.services.platform/Auth/Enroll", msg, &ret)
	if err != nil {
		return nil, err
	}
	return &ret, nil
}

type rxAuth_Enroll struct {
	next AuthQServer
}

func (b rxAuth_Enroll) Handle(ctx context.Context, _ qrpc.Stream, dst *bufio.Writer, recv []byte) error {
	var req EnrollRequest

	err := proto.Unmarshal(recv, &req)
	if err != nil {
		return err
	}
	resp, err := b.next.Enroll(ctx, &req)

	return qrpc.WriteResponse(ctx, dst, resp, err)

}

func RegisterAuthQServer(s *qrpc.Server, srv AuthQServer) {
	s.RegisterServiceMethod(srv, "qrpc://foo.internal.services.platform/Auth/Salt", rxAuth_Salt{srv})
	s.RegisterServiceMethod(srv, "qrpc://foo.internal.services.platform/Auth/Exchange", rxAuth_Exchange{srv})
	s.RegisterServiceMethod(srv, "qrpc://foo.internal.services.platform/Auth/Enroll", rxAuth_Enroll{srv})
}

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v4.24.3
// source: zero.proto

package platform

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	timestamppb "google.golang.org/protobuf/types/known/timestamppb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type PublicKey_Authority int32

const (
	PublicKey_ZERO  PublicKey_Authority = 0
	PublicKey_XAUTH PublicKey_Authority = 1
)

// Enum value maps for PublicKey_Authority.
var (
	PublicKey_Authority_name = map[int32]string{
		0: "ZERO",
		1: "XAUTH",
	}
	PublicKey_Authority_value = map[string]int32{
		"ZERO":  0,
		"XAUTH": 1,
	}
)

func (x PublicKey_Authority) Enum() *PublicKey_Authority {
	p := new(PublicKey_Authority)
	*p = x
	return p
}

func (x PublicKey_Authority) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (PublicKey_Authority) Descriptor() protoreflect.EnumDescriptor {
	return file_zero_proto_enumTypes[0].Descriptor()
}

func (PublicKey_Authority) Type() protoreflect.EnumType {
	return &file_zero_proto_enumTypes[0]
}

func (x PublicKey_Authority) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use PublicKey_Authority.Descriptor instead.
func (PublicKey_Authority) EnumDescriptor() ([]byte, []int) {
	return file_zero_proto_rawDescGZIP(), []int{5, 0}
}

type AddExternalAuthKeyRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Key *PublicKey `protobuf:"bytes,1,opt,name=key,proto3" json:"key,omitempty"`
}

func (x *AddExternalAuthKeyRequest) Reset() {
	*x = AddExternalAuthKeyRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_zero_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AddExternalAuthKeyRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AddExternalAuthKeyRequest) ProtoMessage() {}

func (x *AddExternalAuthKeyRequest) ProtoReflect() protoreflect.Message {
	mi := &file_zero_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AddExternalAuthKeyRequest.ProtoReflect.Descriptor instead.
func (*AddExternalAuthKeyRequest) Descriptor() ([]byte, []int) {
	return file_zero_proto_rawDescGZIP(), []int{0}
}

func (x *AddExternalAuthKeyRequest) GetKey() *PublicKey {
	if x != nil {
		return x.Key
	}
	return nil
}

type AddExternalAuthKeyResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *AddExternalAuthKeyResponse) Reset() {
	*x = AddExternalAuthKeyResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_zero_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AddExternalAuthKeyResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AddExternalAuthKeyResponse) ProtoMessage() {}

func (x *AddExternalAuthKeyResponse) ProtoReflect() protoreflect.Message {
	mi := &file_zero_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AddExternalAuthKeyResponse.ProtoReflect.Descriptor instead.
func (*AddExternalAuthKeyResponse) Descriptor() ([]byte, []int) {
	return file_zero_proto_rawDescGZIP(), []int{1}
}

type BootstrapNodeRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Kind string `protobuf:"bytes,1,opt,name=kind,proto3" json:"kind,omitempty"` // e.g. "cam", "state", or "xauth"
	Node *Node  `protobuf:"bytes,2,opt,name=node,proto3" json:"node,omitempty"`
}

func (x *BootstrapNodeRequest) Reset() {
	*x = BootstrapNodeRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_zero_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BootstrapNodeRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BootstrapNodeRequest) ProtoMessage() {}

func (x *BootstrapNodeRequest) ProtoReflect() protoreflect.Message {
	mi := &file_zero_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BootstrapNodeRequest.ProtoReflect.Descriptor instead.
func (*BootstrapNodeRequest) Descriptor() ([]byte, []int) {
	return file_zero_proto_rawDescGZIP(), []int{2}
}

func (x *BootstrapNodeRequest) GetKind() string {
	if x != nil {
		return x.Kind
	}
	return ""
}

func (x *BootstrapNodeRequest) GetNode() *Node {
	if x != nil {
		return x.Node
	}
	return nil
}

type BootstrapNodeResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Token      *IssuedToken `protobuf:"bytes,1,opt,name=token,proto3" json:"token,omitempty"`
	Deployment *Deployment  `protobuf:"bytes,2,opt,name=deployment,proto3" json:"deployment,omitempty"`
	Keys       *KeySet      `protobuf:"bytes,3,opt,name=keys,proto3" json:"keys,omitempty"`
	Seq        uint64       `protobuf:"varint,4,opt,name=seq,proto3" json:"seq,omitempty"`
}

func (x *BootstrapNodeResponse) Reset() {
	*x = BootstrapNodeResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_zero_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BootstrapNodeResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BootstrapNodeResponse) ProtoMessage() {}

func (x *BootstrapNodeResponse) ProtoReflect() protoreflect.Message {
	mi := &file_zero_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BootstrapNodeResponse.ProtoReflect.Descriptor instead.
func (*BootstrapNodeResponse) Descriptor() ([]byte, []int) {
	return file_zero_proto_rawDescGZIP(), []int{3}
}

func (x *BootstrapNodeResponse) GetToken() *IssuedToken {
	if x != nil {
		return x.Token
	}
	return nil
}

func (x *BootstrapNodeResponse) GetDeployment() *Deployment {
	if x != nil {
		return x.Deployment
	}
	return nil
}

func (x *BootstrapNodeResponse) GetKeys() *KeySet {
	if x != nil {
		return x.Keys
	}
	return nil
}

func (x *BootstrapNodeResponse) GetSeq() uint64 {
	if x != nil {
		return x.Seq
	}
	return 0
}

type KeySet struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Index map[string]*PublicKey `protobuf:"bytes,3,rep,name=index,proto3" json:"index,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
}

func (x *KeySet) Reset() {
	*x = KeySet{}
	if protoimpl.UnsafeEnabled {
		mi := &file_zero_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *KeySet) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*KeySet) ProtoMessage() {}

func (x *KeySet) ProtoReflect() protoreflect.Message {
	mi := &file_zero_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use KeySet.ProtoReflect.Descriptor instead.
func (*KeySet) Descriptor() ([]byte, []int) {
	return file_zero_proto_rawDescGZIP(), []int{4}
}

func (x *KeySet) GetIndex() map[string]*PublicKey {
	if x != nil {
		return x.Index
	}
	return nil
}

type PublicKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Issuer    string                 `protobuf:"bytes,1,opt,name=issuer,proto3" json:"issuer,omitempty"`
	Public    string                 `protobuf:"bytes,2,opt,name=public,proto3" json:"public,omitempty"`
	Expires   *timestamppb.Timestamp `protobuf:"bytes,3,opt,name=expires,proto3" json:"expires,omitempty"`
	Authority PublicKey_Authority    `protobuf:"varint,4,opt,name=authority,proto3,enum=foo.internal.services.platform.PublicKey_Authority" json:"authority,omitempty"`
}

func (x *PublicKey) Reset() {
	*x = PublicKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_zero_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PublicKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PublicKey) ProtoMessage() {}

func (x *PublicKey) ProtoReflect() protoreflect.Message {
	mi := &file_zero_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PublicKey.ProtoReflect.Descriptor instead.
func (*PublicKey) Descriptor() ([]byte, []int) {
	return file_zero_proto_rawDescGZIP(), []int{5}
}

func (x *PublicKey) GetIssuer() string {
	if x != nil {
		return x.Issuer
	}
	return ""
}

func (x *PublicKey) GetPublic() string {
	if x != nil {
		return x.Public
	}
	return ""
}

func (x *PublicKey) GetExpires() *timestamppb.Timestamp {
	if x != nil {
		return x.Expires
	}
	return nil
}

func (x *PublicKey) GetAuthority() PublicKey_Authority {
	if x != nil {
		return x.Authority
	}
	return PublicKey_ZERO
}

type RefreshTokenRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Kind string `protobuf:"bytes,1,opt,name=kind,proto3" json:"kind,omitempty"`
	Id   string `protobuf:"bytes,2,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *RefreshTokenRequest) Reset() {
	*x = RefreshTokenRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_zero_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RefreshTokenRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RefreshTokenRequest) ProtoMessage() {}

func (x *RefreshTokenRequest) ProtoReflect() protoreflect.Message {
	mi := &file_zero_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RefreshTokenRequest.ProtoReflect.Descriptor instead.
func (*RefreshTokenRequest) Descriptor() ([]byte, []int) {
	return file_zero_proto_rawDescGZIP(), []int{6}
}

func (x *RefreshTokenRequest) GetKind() string {
	if x != nil {
		return x.Kind
	}
	return ""
}

func (x *RefreshTokenRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type RefreshTokenResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Token *IssuedToken `protobuf:"bytes,1,opt,name=token,proto3" json:"token,omitempty"`
}

func (x *RefreshTokenResponse) Reset() {
	*x = RefreshTokenResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_zero_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RefreshTokenResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RefreshTokenResponse) ProtoMessage() {}

func (x *RefreshTokenResponse) ProtoReflect() protoreflect.Message {
	mi := &file_zero_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RefreshTokenResponse.ProtoReflect.Descriptor instead.
func (*RefreshTokenResponse) Descriptor() ([]byte, []int) {
	return file_zero_proto_rawDescGZIP(), []int{7}
}

func (x *RefreshTokenResponse) GetToken() *IssuedToken {
	if x != nil {
		return x.Token
	}
	return nil
}

type IssuedToken struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Token   string                 `protobuf:"bytes,1,opt,name=token,proto3" json:"token,omitempty"`
	Expires *timestamppb.Timestamp `protobuf:"bytes,2,opt,name=expires,proto3" json:"expires,omitempty"`
}

func (x *IssuedToken) Reset() {
	*x = IssuedToken{}
	if protoimpl.UnsafeEnabled {
		mi := &file_zero_proto_msgTypes[8]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *IssuedToken) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*IssuedToken) ProtoMessage() {}

func (x *IssuedToken) ProtoReflect() protoreflect.Message {
	mi := &file_zero_proto_msgTypes[8]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use IssuedToken.ProtoReflect.Descriptor instead.
func (*IssuedToken) Descriptor() ([]byte, []int) {
	return file_zero_proto_rawDescGZIP(), []int{8}
}

func (x *IssuedToken) GetToken() string {
	if x != nil {
		return x.Token
	}
	return ""
}

func (x *IssuedToken) GetExpires() *timestamppb.Timestamp {
	if x != nil {
		return x.Expires
	}
	return nil
}

var File_zero_proto protoreflect.FileDescriptor

var file_zero_proto_rawDesc = []byte{
	0x0a, 0x0a, 0x7a, 0x65, 0x72, 0x6f, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x1e, 0x66, 0x6f,
	0x6f, 0x2e, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x73, 0x2e, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x1a, 0x1f, 0x67, 0x6f,
	0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x74, 0x69,
	0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x0c, 0x72,
	0x65, 0x67, 0x69, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x58, 0x0a, 0x19, 0x41,
	0x64, 0x64, 0x45, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x41, 0x75, 0x74, 0x68, 0x4b, 0x65,
	0x79, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x3b, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x29, 0x2e, 0x66, 0x6f, 0x6f, 0x2e, 0x69, 0x6e, 0x74, 0x65,
	0x72, 0x6e, 0x61, 0x6c, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x70, 0x6c,
	0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x50, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x4b, 0x65, 0x79,
	0x52, 0x03, 0x6b, 0x65, 0x79, 0x22, 0x1c, 0x0a, 0x1a, 0x41, 0x64, 0x64, 0x45, 0x78, 0x74, 0x65,
	0x72, 0x6e, 0x61, 0x6c, 0x41, 0x75, 0x74, 0x68, 0x4b, 0x65, 0x79, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x22, 0x64, 0x0a, 0x14, 0x42, 0x6f, 0x6f, 0x74, 0x73, 0x74, 0x72, 0x61, 0x70,
	0x4e, 0x6f, 0x64, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x6b,
	0x69, 0x6e, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6b, 0x69, 0x6e, 0x64, 0x12,
	0x38, 0x0a, 0x04, 0x6e, 0x6f, 0x64, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x24, 0x2e,
	0x66, 0x6f, 0x6f, 0x2e, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x2e, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x4e,
	0x6f, 0x64, 0x65, 0x52, 0x04, 0x6e, 0x6f, 0x64, 0x65, 0x22, 0xf4, 0x01, 0x0a, 0x15, 0x42, 0x6f,
	0x6f, 0x74, 0x73, 0x74, 0x72, 0x61, 0x70, 0x4e, 0x6f, 0x64, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x12, 0x41, 0x0a, 0x05, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x2b, 0x2e, 0x66, 0x6f, 0x6f, 0x2e, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x61,
	0x6c, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x70, 0x6c, 0x61, 0x74, 0x66,
	0x6f, 0x72, 0x6d, 0x2e, 0x49, 0x73, 0x73, 0x75, 0x65, 0x64, 0x54, 0x6f, 0x6b, 0x65, 0x6e, 0x52,
	0x05, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x12, 0x4a, 0x0a, 0x0a, 0x64, 0x65, 0x70, 0x6c, 0x6f, 0x79,
	0x6d, 0x65, 0x6e, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x2a, 0x2e, 0x66, 0x6f, 0x6f,
	0x2e, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x73, 0x2e, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x44, 0x65, 0x70, 0x6c,
	0x6f, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x52, 0x0a, 0x64, 0x65, 0x70, 0x6c, 0x6f, 0x79, 0x6d, 0x65,
	0x6e, 0x74, 0x12, 0x3a, 0x0a, 0x04, 0x6b, 0x65, 0x79, 0x73, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x26, 0x2e, 0x66, 0x6f, 0x6f, 0x2e, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x2e,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72,
	0x6d, 0x2e, 0x4b, 0x65, 0x79, 0x53, 0x65, 0x74, 0x52, 0x04, 0x6b, 0x65, 0x79, 0x73, 0x12, 0x10,
	0x0a, 0x03, 0x73, 0x65, 0x71, 0x18, 0x04, 0x20, 0x01, 0x28, 0x04, 0x52, 0x03, 0x73, 0x65, 0x71,
	0x22, 0xb6, 0x01, 0x0a, 0x06, 0x4b, 0x65, 0x79, 0x53, 0x65, 0x74, 0x12, 0x47, 0x0a, 0x05, 0x69,
	0x6e, 0x64, 0x65, 0x78, 0x18, 0x03, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x31, 0x2e, 0x66, 0x6f, 0x6f,
	0x2e, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x73, 0x2e, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x4b, 0x65, 0x79, 0x53,
	0x65, 0x74, 0x2e, 0x49, 0x6e, 0x64, 0x65, 0x78, 0x45, 0x6e, 0x74, 0x72, 0x79, 0x52, 0x05, 0x69,
	0x6e, 0x64, 0x65, 0x78, 0x1a, 0x63, 0x0a, 0x0a, 0x49, 0x6e, 0x64, 0x65, 0x78, 0x45, 0x6e, 0x74,
	0x72, 0x79, 0x12, 0x10, 0x0a, 0x03, 0x6b, 0x65, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x03, 0x6b, 0x65, 0x79, 0x12, 0x3f, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x29, 0x2e, 0x66, 0x6f, 0x6f, 0x2e, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x6e,
	0x61, 0x6c, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x70, 0x6c, 0x61, 0x74,
	0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x50, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x4b, 0x65, 0x79, 0x52, 0x05,
	0x76, 0x61, 0x6c, 0x75, 0x65, 0x3a, 0x02, 0x38, 0x01, 0x22, 0xe6, 0x01, 0x0a, 0x09, 0x50, 0x75,
	0x62, 0x6c, 0x69, 0x63, 0x4b, 0x65, 0x79, 0x12, 0x16, 0x0a, 0x06, 0x69, 0x73, 0x73, 0x75, 0x65,
	0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x69, 0x73, 0x73, 0x75, 0x65, 0x72, 0x12,
	0x16, 0x0a, 0x06, 0x70, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x06, 0x70, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x12, 0x34, 0x0a, 0x07, 0x65, 0x78, 0x70, 0x69, 0x72,
	0x65, 0x73, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73,
	0x74, 0x61, 0x6d, 0x70, 0x52, 0x07, 0x65, 0x78, 0x70, 0x69, 0x72, 0x65, 0x73, 0x12, 0x51, 0x0a,
	0x09, 0x61, 0x75, 0x74, 0x68, 0x6f, 0x72, 0x69, 0x74, 0x79, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0e,
	0x32, 0x33, 0x2e, 0x66, 0x6f, 0x6f, 0x2e, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x2e,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72,
	0x6d, 0x2e, 0x50, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x4b, 0x65, 0x79, 0x2e, 0x41, 0x75, 0x74, 0x68,
	0x6f, 0x72, 0x69, 0x74, 0x79, 0x52, 0x09, 0x61, 0x75, 0x74, 0x68, 0x6f, 0x72, 0x69, 0x74, 0x79,
	0x22, 0x20, 0x0a, 0x09, 0x41, 0x75, 0x74, 0x68, 0x6f, 0x72, 0x69, 0x74, 0x79, 0x12, 0x08, 0x0a,
	0x04, 0x5a, 0x45, 0x52, 0x4f, 0x10, 0x00, 0x12, 0x09, 0x0a, 0x05, 0x58, 0x41, 0x55, 0x54, 0x48,
	0x10, 0x01, 0x22, 0x39, 0x0a, 0x13, 0x52, 0x65, 0x66, 0x72, 0x65, 0x73, 0x68, 0x54, 0x6f, 0x6b,
	0x65, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x6b, 0x69, 0x6e,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6b, 0x69, 0x6e, 0x64, 0x12, 0x0e, 0x0a,
	0x02, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0x59, 0x0a,
	0x14, 0x52, 0x65, 0x66, 0x72, 0x65, 0x73, 0x68, 0x54, 0x6f, 0x6b, 0x65, 0x6e, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x41, 0x0a, 0x05, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x0b, 0x32, 0x2b, 0x2e, 0x66, 0x6f, 0x6f, 0x2e, 0x69, 0x6e, 0x74, 0x65, 0x72,
	0x6e, 0x61, 0x6c, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x70, 0x6c, 0x61,
	0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x49, 0x73, 0x73, 0x75, 0x65, 0x64, 0x54, 0x6f, 0x6b, 0x65,
	0x6e, 0x52, 0x05, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x22, 0x59, 0x0a, 0x0b, 0x49, 0x73, 0x73, 0x75,
	0x65, 0x64, 0x54, 0x6f, 0x6b, 0x65, 0x6e, 0x12, 0x14, 0x0a, 0x05, 0x74, 0x6f, 0x6b, 0x65, 0x6e,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x12, 0x34, 0x0a,
	0x07, 0x65, 0x78, 0x70, 0x69, 0x72, 0x65, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a,
	0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66,
	0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x52, 0x07, 0x65, 0x78, 0x70, 0x69,
	0x72, 0x65, 0x73, 0x32, 0x94, 0x02, 0x0a, 0x04, 0x5a, 0x65, 0x72, 0x6f, 0x12, 0x7e, 0x0a, 0x0d,
	0x42, 0x6f, 0x6f, 0x74, 0x73, 0x74, 0x72, 0x61, 0x70, 0x4e, 0x6f, 0x64, 0x65, 0x12, 0x34, 0x2e,
	0x66, 0x6f, 0x6f, 0x2e, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x2e, 0x73, 0x65, 0x72,
	0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x42,
	0x6f, 0x6f, 0x74, 0x73, 0x74, 0x72, 0x61, 0x70, 0x4e, 0x6f, 0x64, 0x65, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x35, 0x2e, 0x66, 0x6f, 0x6f, 0x2e, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x6e,
	0x61, 0x6c, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x70, 0x6c, 0x61, 0x74,
	0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x42, 0x6f, 0x6f, 0x74, 0x73, 0x74, 0x72, 0x61, 0x70, 0x4e, 0x6f,
	0x64, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x30, 0x01, 0x12, 0x8b, 0x01, 0x0a,
	0x12, 0x41, 0x64, 0x64, 0x45, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x41, 0x75, 0x74, 0x68,
	0x4b, 0x65, 0x79, 0x12, 0x39, 0x2e, 0x66, 0x6f, 0x6f, 0x2e, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x6e,
	0x61, 0x6c, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x70, 0x6c, 0x61, 0x74,
	0x66, 0x6f, 0x72, 0x6d, 0x2e, 0x41, 0x64, 0x64, 0x45, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c,
	0x41, 0x75, 0x74, 0x68, 0x4b, 0x65, 0x79, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x3a,
	0x2e, 0x66, 0x6f, 0x6f, 0x2e, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x2e, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2e, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2e,
	0x41, 0x64, 0x64, 0x45, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x41, 0x75, 0x74, 0x68, 0x4b,
	0x65, 0x79, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42, 0x34, 0x5a, 0x32, 0x62, 0x69,
	0x74, 0x62, 0x75, 0x63, 0x6b, 0x65, 0x74, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x66, 0x75, 0x66, 0x6f,
	0x6f, 0x2f, 0x66, 0x6f, 0x6f, 0x2f, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x2f, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x73, 0x2f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d,
	0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_zero_proto_rawDescOnce sync.Once
	file_zero_proto_rawDescData = file_zero_proto_rawDesc
)

func file_zero_proto_rawDescGZIP() []byte {
	file_zero_proto_rawDescOnce.Do(func() {
		file_zero_proto_rawDescData = protoimpl.X.CompressGZIP(file_zero_proto_rawDescData)
	})
	return file_zero_proto_rawDescData
}

var file_zero_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_zero_proto_msgTypes = make([]protoimpl.MessageInfo, 10)
var file_zero_proto_goTypes = []interface{}{
	(PublicKey_Authority)(0),           // 0: foo.internal.services.platform.PublicKey.Authority
	(*AddExternalAuthKeyRequest)(nil),  // 1: foo.internal.services.platform.AddExternalAuthKeyRequest
	(*AddExternalAuthKeyResponse)(nil), // 2: foo.internal.services.platform.AddExternalAuthKeyResponse
	(*BootstrapNodeRequest)(nil),       // 3: foo.internal.services.platform.BootstrapNodeRequest
	(*BootstrapNodeResponse)(nil),      // 4: foo.internal.services.platform.BootstrapNodeResponse
	(*KeySet)(nil),                     // 5: foo.internal.services.platform.KeySet
	(*PublicKey)(nil),                  // 6: foo.internal.services.platform.PublicKey
	(*RefreshTokenRequest)(nil),        // 7: foo.internal.services.platform.RefreshTokenRequest
	(*RefreshTokenResponse)(nil),       // 8: foo.internal.services.platform.RefreshTokenResponse
	(*IssuedToken)(nil),                // 9: foo.internal.services.platform.IssuedToken
	nil,                                // 10: foo.internal.services.platform.KeySet.IndexEntry
	(*Node)(nil),                       // 11: foo.internal.services.platform.Node
	(*Deployment)(nil),                 // 12: foo.internal.services.platform.Deployment
	(*timestamppb.Timestamp)(nil),      // 13: google.protobuf.Timestamp
}
var file_zero_proto_depIdxs = []int32{
	6,  // 0: foo.internal.services.platform.AddExternalAuthKeyRequest.key:type_name -> foo.internal.services.platform.PublicKey
	11, // 1: foo.internal.services.platform.BootstrapNodeRequest.node:type_name -> foo.internal.services.platform.Node
	9,  // 2: foo.internal.services.platform.BootstrapNodeResponse.token:type_name -> foo.internal.services.platform.IssuedToken
	12, // 3: foo.internal.services.platform.BootstrapNodeResponse.deployment:type_name -> foo.internal.services.platform.Deployment
	5,  // 4: foo.internal.services.platform.BootstrapNodeResponse.keys:type_name -> foo.internal.services.platform.KeySet
	10, // 5: foo.internal.services.platform.KeySet.index:type_name -> foo.internal.services.platform.KeySet.IndexEntry
	13, // 6: foo.internal.services.platform.PublicKey.expires:type_name -> google.protobuf.Timestamp
	0,  // 7: foo.internal.services.platform.PublicKey.authority:type_name -> foo.internal.services.platform.PublicKey.Authority
	9,  // 8: foo.internal.services.platform.RefreshTokenResponse.token:type_name -> foo.internal.services.platform.IssuedToken
	13, // 9: foo.internal.services.platform.IssuedToken.expires:type_name -> google.protobuf.Timestamp
	6,  // 10: foo.internal.services.platform.KeySet.IndexEntry.value:type_name -> foo.internal.services.platform.PublicKey
	3,  // 11: foo.internal.services.platform.Zero.BootstrapNode:input_type -> foo.internal.services.platform.BootstrapNodeRequest
	1,  // 12: foo.internal.services.platform.Zero.AddExternalAuthKey:input_type -> foo.internal.services.platform.AddExternalAuthKeyRequest
	4,  // 13: foo.internal.services.platform.Zero.BootstrapNode:output_type -> foo.internal.services.platform.BootstrapNodeResponse
	2,  // 14: foo.internal.services.platform.Zero.AddExternalAuthKey:output_type -> foo.internal.services.platform.AddExternalAuthKeyResponse
	13, // [13:15] is the sub-list for method output_type
	11, // [11:13] is the sub-list for method input_type
	11, // [11:11] is the sub-list for extension type_name
	11, // [11:11] is the sub-list for extension extendee
	0,  // [0:11] is the sub-list for field type_name
}

func init() { file_zero_proto_init() }
func file_zero_proto_init() {
	if File_zero_proto != nil {
		return
	}
	file_region_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_zero_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AddExternalAuthKeyRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_zero_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AddExternalAuthKeyResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_zero_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BootstrapNodeRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_zero_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BootstrapNodeResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_zero_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*KeySet); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_zero_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PublicKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_zero_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RefreshTokenRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_zero_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RefreshTokenResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_zero_proto_msgTypes[8].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*IssuedToken); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_zero_proto_rawDesc,
			NumEnums:      1,
			NumMessages:   10,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_zero_proto_goTypes,
		DependencyIndexes: file_zero_proto_depIdxs,
		EnumInfos:         file_zero_proto_enumTypes,
		MessageInfos:      file_zero_proto_msgTypes,
	}.Build()
	File_zero_proto = out.File
	file_zero_proto_rawDesc = nil
	file_zero_proto_goTypes = nil
	file_zero_proto_depIdxs = nil
}

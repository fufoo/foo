package scripting

import (
	"bytes"
	"context"
	"fmt"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/foo/internal/scripting/plugin"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/fulang/eval"
	"bitbucket.org/fufoo/fulang/icode"
	"bitbucket.org/fufoo/fulang/mod"
)

var log = logging.New("scripting")

type Script interface {
	Run(ctx context.Context, in *foo.Domain) error
}

type fooScript struct {
	pgm *eval.Program
}

func Compile(ctx context.Context, name string, text []byte) (Script, error) {
	plugin.Register()
	unary := mod.LiteralFileSet(ctx, name, ".fus", text)

	// initialize
	mod.Init(ctx)

	// never update, and we don't have a remote spec
	plan, err := makePlanFromFileSet(ctx, unary, nil, false)
	if err != nil {
		return nil, err
	}

	icp, err := plan.Compile(ctx)
	if err != nil {
		return nil, err
	}

	pgm, err := eval.Prepare(ctx, icp)
	if err != nil {
		return nil, err
	}

	return &fooScript{
		pgm,
	}, nil
}

func (fs *fooScript) Run(ctx context.Context, in *foo.Domain) error {
	return in.Update(ctx, func(ctx context.Context, tx foo.WriteTxn) error {
		return tryRun(plugin.WithTxnContext(ctx, tx), fs.pgm)
	})
}

func makePlanFromFileSet(ctx context.Context, files *mod.FileSet, rs *mod.RemoteSpec, update bool) (*mod.BuildPlan, error) {

	if rs != nil {
		log.Debugf(ctx, "building %s", rs)
	}

	var buf bytes.Buffer

	for _, f := range files.Members {
		buf.WriteByte('\n')
		fmt.Fprintf(&buf, "   %s %s (%s)", f.Hash, f.Name, f.Ext)
	}

	log.Debugf(ctx, "module root %s\n%d files in %s:%s",
		files.ModRoot,
		len(files.Members),
		files.Directory,
		buf.Bytes(),
	)

	main, err := mod.ParsePackagePreamble(ctx, files)
	if err != nil {
		return nil, errlib.Wrap("FCL-4401", err)
	}

	builtins := []*icode.Package{}

	plan, err := mod.MainPlan(ctx, rs, main, files, builtins, update)
	if err != nil {
		return nil, errlib.Wrap("FCL-4402", err) // could not develop a plan
	}
	log.Debugf(ctx, "here's the plan...")
	for i, unit := range plan.Units {
		if unit.Pre != nil {
			log.Debugf(ctx, " (step %d) build %d files in %s <%.9s>",
				i+1,
				len(unit.Pre.Files.Members),
				unit.Pre.Files.Directory,
				unit.Pre.Files.Hash)
		} else {
			log.Debugf(ctx, " (step %d) plugin",
				i+1,
			)
		}
	}
	return plan, nil
}

func tryRun(ctx context.Context, p *eval.Program) (err error) {

	// but for now, this is rather inconvenient to my development
	// style, which uses panics to define a wavefront of
	// functionality

	if false {
		defer func() {
			if oops := recover(); oops != nil {
				err = errlib.Newf("FUE-9000 program failed: %v", oops)
			}
		}()
	}

	log.Tracef(ctx, "-------------------------------------------------- RUNNING -----")
	p.Run(ctx, nil)
	fmt.Printf("\n")
	return
}

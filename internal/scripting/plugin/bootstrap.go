package plugin

import (
	"context"
	"fmt"

	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/bighash"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/fulang/icode"
	"bitbucket.org/fufoo/fulang/runtime"
	"google.golang.org/protobuf/proto"
)

type ctxkey int

const (
	txnkey = ctxkey(iota)
)

func WithTxnContext(ctx context.Context, tx foo.ReadTxn) context.Context {
	return context.WithValue(ctx, txnkey, tx)
}

func ContextWriteTxn(ctx context.Context) foo.WriteTxn {
	v := ctx.Value(txnkey)
	if v == nil {
		return nil
	}
	return v.(foo.WriteTxn)
}

func ContextTxn(ctx context.Context) foo.ReadTxn {
	v := ctx.Value(txnkey)
	if v == nil {
		return nil
	}
	return v.(foo.ReadTxn)
}

func rawGet(ctx context.Context, name string) string {
	tx := ContextTxn(ctx)
	p, err := tx.Get(ctx, name)
	if err != nil {
		panic(err)
	}
	return p.String()
}

func Register() {
	ts := runtime.NewTypes()

	bound := bindTypes(ts)

	pkg := &icode.Package{
		Name:    "foo",
		Path:    "foo/builtin",
		Version: "v0.0.1",
		TopFuncs: map[string]*icode.TopFunc{
			"Get": &icode.TopFunc{
				Fn: &icode.Func{
					Type: bound.getFuncType,
					Go:   &icode.GoBody{}, // needed for us to get included as a builtin
				},
			},
			"Set": &icode.TopFunc{
				Fn: &icode.Func{
					Type: bound.setFuncType,
					Go:   &icode.GoBody{}, // needed for us to get included as a builtin
				},
			},
			"Literal": &icode.TopFunc{
				Fn: &icode.Func{
					Type: bound.literalFuncType,
					Go:   &icode.GoBody{}, // needed for us to get included as a builtin
				},
			},
			"LoadMap": &icode.TopFunc{
				Fn: &icode.Func{
					Type: bound.loadMapFuncType,
					Go:   &icode.GoBody{}, // needed for us to get included as a builtin
				},
			},
			"StoreMap": &icode.TopFunc{
				Fn: &icode.Func{
					Type: bound.storeMapFuncType,
					Go:   &icode.GoBody{}, // needed for us to get included as a builtin
				},
			},
			"NewMap": &icode.TopFunc{
				Fn: &icode.Func{
					Type: bound.newMapFuncType,
					Go:   &icode.GoBody{}, // needed for us to get included as a builtin
				},
			},
			"MapSet": &icode.TopFunc{
				Fn: &icode.Func{
					Type: bound.mapSetFuncType,
					Go:   &icode.GoBody{}, // needed for us to get included as a builtin
				},
			},
			"MapGet": &icode.TopFunc{
				Fn: &icode.Func{
					Type: bound.mapGetFuncType,
					Go:   &icode.GoBody{}, // needed for us to get included as a builtin
				},
			},
		},
		Generics: map[string]*icode.Generic{
			"Load": &icode.Generic{
				Id: "foo/builtin.Load",
			},
		},
		// TODO how do you export types??
		Exports: []string{"Get", "Set", "Literal", "LoadMap", "StoreMap", "NewMap", "MapSet", "MapGet"},
	}

	myData, err := proto.Marshal(pkg)
	if err != nil {
		panic(err)
	}

	me := &runtime.Plugin{
		Name:        "foo",
		Path:        "foo/builtin",
		Version:     "v0.0.1",
		PackageData: myData,
		Initialize:  myInit,
	}
	runtime.RegisterPlugin(me)
}

type pluginTypes struct {
	trueValue        runtime.Value
	falseValue       runtime.Value
	genericPtrType   *runtime.Type
	literalFuncType  *runtime.Type
	getFuncType      *runtime.Type
	loadGeneric      *runtime.Type
	mapType          *runtime.Type
	loadMapFuncType  *runtime.Type
	storeMapFuncType *runtime.Type
	setFuncType      *runtime.Type
	newMapFuncType   *runtime.Type
	mapSetFuncType   *runtime.Type
	mapGetFuncType   *runtime.Type
}

func bindTypes(ts *runtime.TypeSystem) *pluginTypes {

	/*	genericPtrType := ts.Intern(&runtime.Type{
			K: runtime.Kind_Interface,
			F: &runtime.Type_Interface{
				Interface: &runtime.InterfaceType{
					Empty: true,
				},
			},
			P: "builtin/foo",
			N: "Ptr",
		})
	*/
	genericPtrType := ts.Intern(&runtime.Type{
		K: runtime.Kind_Struct,
		F: &runtime.Type_Record{
			Record: &runtime.StructType{},
		},
		P: "foo/builtin",
		N: "Ptr",
	})

	boolType := ts.ByName("builtin", "bool")

	getFuncType := ts.FuncOf(
		[]*runtime.Type{ts.ByName("builtin", "string")},
		[]*runtime.Type{genericPtrType},
		false)

	setFuncType := ts.FuncOf(
		[]*runtime.Type{ts.ByName("builtin", "string"), genericPtrType},
		[]*runtime.Type{},
		false)

	mapType := ts.Intern(&runtime.Type{
		K: runtime.Kind_Interface,
		F: &runtime.Type_Interface{
			Interface: &runtime.InterfaceType{
				Empty: true,
			},
		},
		P: "foo/builtin",
		N: "Map",
	})

	loadMapFuncType := ts.FuncOf([]*runtime.Type{genericPtrType}, []*runtime.Type{mapType}, false)
	storeMapFuncType := ts.FuncOf([]*runtime.Type{mapType}, []*runtime.Type{genericPtrType}, false)
	newMapFuncType := ts.FuncOf(nil, []*runtime.Type{mapType}, false)
	mapSetFuncType := ts.FuncOf([]*runtime.Type{mapType, genericPtrType, genericPtrType}, nil, false)
	mapGetFuncType := ts.FuncOf([]*runtime.Type{mapType, genericPtrType}, []*runtime.Type{genericPtrType, boolType}, false)

	// TODO this should be a generic
	literalFuncType := ts.FuncOf(
		[]*runtime.Type{ts.Any()},
		[]*runtime.Type{genericPtrType},
		false)

	return &pluginTypes{
		loadGeneric:      ts.Generic("foo/builtin.Load"),
		trueValue:        ts.MakeBool(true),
		falseValue:       ts.MakeBool(false),
		genericPtrType:   genericPtrType,
		getFuncType:      getFuncType,      // (string) -> Ptr
		setFuncType:      setFuncType,      // (string, Ptr) -> ()
		literalFuncType:  literalFuncType,  // (string) -> Ptr  (TODO, should be (α) -> Ptr[α]))
		mapType:          mapType,          //
		loadMapFuncType:  loadMapFuncType,  // (Ptr) -> Map
		storeMapFuncType: storeMapFuncType, // (Map) -> Ptr
		newMapFuncType:   newMapFuncType,   // () -> Map
		mapSetFuncType:   mapSetFuncType,   // (Map, Ptr, Ptr) -> ()
		mapGetFuncType:   mapGetFuncType,   // (Map, Ptr) -> (Ptr, bool)
	}
}

type literalFunc struct {
	bound *pluginTypes
}

func (fn literalFunc) Call(ctx context.Context, args []runtime.Value) []runtime.Value {
	var p ptr.Ptr

	switch arg := args[0].Native().(type) {
	case string:
		p = ptr.String(arg)
	case int:
		p = ptr.Int64(int64(arg))
	default:
		panic(fmt.Sprintf("not supported %T", arg))
	}

	// TODO, we could return Ptr[string] if given a string instead of just a Ptr...
	return []runtime.Value{runtime.MakeNative(fn.bound.genericPtrType, p)}
}

type getFunc struct {
	bound *pluginTypes
}

func (fn getFunc) Call(ctx context.Context, args []runtime.Value) []runtime.Value {
	name := args[0].Native().(string)

	tx := ContextTxn(ctx)
	value, err := tx.Get(ctx, name)
	if err != nil {
		panic(err)
	}
	return []runtime.Value{runtime.MakeNative(fn.bound.genericPtrType, value)}
}

type setFunc struct {
	bound *pluginTypes
}

func (fn setFunc) Call(ctx context.Context, args []runtime.Value) []runtime.Value {
	name := args[0].Native().(string)
	val := args[1].Native().(ptr.Ptr)

	tx := ContextWriteTxn(ctx)
	err := tx.Set(ctx, name, val)
	if err != nil {
		panic(err)
	}
	return []runtime.Value{}
}

type loadMapFunc struct {
	bound *pluginTypes
}

func (fn loadMapFunc) Call(ctx context.Context, args []runtime.Value) []runtime.Value {
	arg := args[0].Native().(ptr.Ptr)

	h := bighash.Load(ContextTxn(ctx), arg)
	return []runtime.Value{runtime.MakeNative(fn.bound.mapType, h)}
}

type newMapFunc struct {
	bound *pluginTypes
}

func (fn newMapFunc) Call(ctx context.Context, args []runtime.Value) []runtime.Value {
	h := bighash.New(ContextTxn(ctx))
	return []runtime.Value{runtime.MakeNative(fn.bound.mapType, h)}
}

type storeMapFunc struct {
	bound *pluginTypes
}

func (fn storeMapFunc) Call(ctx context.Context, args []runtime.Value) []runtime.Value {
	h := args[0].Native().(*bighash.Hash)

	p, err := h.Store(ctx, ContextWriteTxn(ctx))
	if err != nil {
		panic(err)
	}
	return []runtime.Value{runtime.MakeNative(fn.bound.genericPtrType, p)}
}

type mapGetFunc struct {
	bound *pluginTypes
}

func (fn mapGetFunc) Call(ctx context.Context, args []runtime.Value) []runtime.Value {
	h := args[0].Native().(*bighash.Hash)
	k := args[1].Native().(ptr.Ptr)

	v, err := h.Get(ctx, k)
	if err != nil {
		if err == bighash.ErrKeyNotFound {
			return []runtime.Value{
				runtime.MakeNative(fn.bound.genericPtrType, ptr.Null),
				fn.bound.falseValue,
			}
		}
		panic(err)
	}
	return []runtime.Value{
		runtime.MakeNative(fn.bound.genericPtrType, v),
		fn.bound.trueValue,
	}
}

type mapSetFunc struct {
	bound *pluginTypes
}

func (fn mapSetFunc) Call(ctx context.Context, args []runtime.Value) []runtime.Value {
	h := args[0].Native().(*bighash.Hash)
	k := args[1].Native().(ptr.Ptr)
	v := args[2].Native().(ptr.Ptr)

	err := h.Insert(ctx, k, v)
	if err != nil {
		panic(err)
	}
	return []runtime.Value{}
}

func myInit(ts *runtime.TypeSystem) map[string]runtime.Value {

	bound := bindTypes(ts)

	toc := make(map[string]runtime.Value)

	{
		toc["Get"] = runtime.MakeFunc(bound.getFuncType, getFunc{bound})
		toc["Set"] = runtime.MakeFunc(bound.setFuncType, setFunc{bound})
		toc["Literal"] = runtime.MakeFunc(bound.literalFuncType, literalFunc{bound})
		toc["LoadMap"] = runtime.MakeFunc(bound.loadMapFuncType, loadMapFunc{bound})
		toc["StoreMap"] = runtime.MakeFunc(bound.storeMapFuncType, storeMapFunc{bound})
		toc["MapGet"] = runtime.MakeFunc(bound.mapGetFuncType, mapGetFunc{bound})
		toc["MapSet"] = runtime.MakeFunc(bound.mapSetFuncType, mapSetFunc{bound})
		toc["NewMap"] = runtime.MakeFunc(bound.newMapFuncType, newMapFunc{bound})
	}
	/*
		{
			t := ts.Generic("fu://builtin.foo/Load")
			g := runtime.MakeGeneric(t)

			// TODO add methods

			beta := ts.TypeParameter("β")

			ins := []*runtime.Type{
				beta,
				ts.ByName("bitbucket.org/fufoo/foo-cam/foo2/lib/scriptx", "Ptr"),
			}
			outs := []*runtime.Type{}
			f := ts.FuncOf(ins, outs, false)

			sliceget := runtime.MakeFunc(f, nativeLoader{ts, f})

			g.AddFunc(sliceget)

			toc["Load"] = g
		}
	*/

	return toc
}

type nativeLoader struct {
	ts   *runtime.TypeSystem
	orig *runtime.Type
}

func (i nativeLoader) Call(ctx context.Context, value []runtime.Value) []runtime.Value {
	panic("interp")
}

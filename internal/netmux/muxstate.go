package netmux

import (
	"context"

	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/services/core"
	"bitbucket.org/fufoo/foo/services/state"
	//"bitbucket.org/fufoo/qrpc"
)

func (n *Node) getStateClient(ctx context.Context) (state.StateQClient, error) {
	x, err := n.acquire(ctx)
	if err != nil {
		return nil, err
	}
	return x.(state.StateQClient), nil
}

func (c *Cluster) getStateClientForDomain(ctx context.Context, domain uint64) (state.StateQClient, error) {
	p := ptr.Uint64(domain)
	nodes := c.NodesFor(core.From(p))

	// log.Debugf(ctx, "found %d nodes for D#%d using {%s}", len(nodes), domain, p)

	return nodes[0].getStateClient(ctx)
}

// Get implements state.StateClient
func (c *Cluster) Get(ctx context.Context, req *state.GetRequest) (*state.GetResponse, error) {

	if c == nil {
		panic("nil cluster")
	}
	sc, err := c.getStateClientForDomain(ctx, req.Domain)
	if err != nil {
		return nil, err
	}
	return sc.Get(ctx, req)
}

// Apply implements state.StateClient
func (c *Cluster) Apply(ctx context.Context, req *state.TxnRequest) (*state.TxnRecord, error) {

	sc, err := c.getStateClientForDomain(ctx, req.Domain)
	if err != nil {
		return nil, err
	}
	return sc.Apply(ctx, req)
}

func (c *Cluster) Scan(ctx context.Context, req *state.GetRequest) (state.State_ScanQClient, error) {
	if req.Domain == 0 {
		panic("multi-domain scan not supported")
	}
	sc, err := c.getStateClientForDomain(ctx, req.Domain)
	if err != nil {
		return nil, err
	}

	return sc.Scan(ctx, req)
}

func (c *Cluster) Follow(ctx context.Context, req *state.FollowRequest) (state.State_FollowQClient, error) {
	if req.Domain == 0 {
		// TODO we could support this by following *all* the
		// nodes but that doesn't really scale.  Need another
		// solution if that's the goal (e.g. how Kafka does
		// both client and server sharding, so e.g. we could
		// do a multi-follow by having each of node in a
		// cluster of clients connect to a subset of nodes in
		// the server)
		panic("domain=0, interpreting as multi-domain follow, which is not supported")
	}
	sc, err := c.getStateClientForDomain(ctx, req.Domain)
	if err != nil {
		return nil, err
	}

	go sc.(state.Keepaliver).Keepalive(ctx)
	return sc.Follow(ctx, req)
}

/*
	ch := make(chan *state.Obs, 10)

	go func() {
		defer close(ch)

		sc, err := c.getStateClientForDomain(ctx, domain)
		if err != nil {
			log.Warningf(ctx, "could not scan: %s", err)
			return
		}

		resp, err := sc.Scan(ctx, req)

		for {
			rec, err := resp.Recv()
			if err != nil {
				if !qrpc.IsEOF(err) {
					log.Warningf(ctx, "FCL-2703 scan ended: %s", err)
				}
				return
			}
			ch <- rec
		}
	}()

	return ch
}
*/

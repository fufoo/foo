package netmux

import (
	"context"

	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/foo/services/cam"
	"bitbucket.org/fufoo/foo/services/core"
)

type loadResult struct {
	datum *cam.Datum
	err   error
}

func (n *Node) getStorageClient(ctx context.Context) (cam.StorageQClient, error) {
	x, err := n.acquire(ctx)
	if err != nil {
		return nil, err
	}
	return x.(cam.StorageQClient), nil
}

// Load implements cam.StorageClient
func (c *Cluster) Load(ctx context.Context, req *cam.LoadRequest) (*cam.LoadResponse, error) {

	if len(req.Refs) != 1 {
		// we would want to partition the requests in an interesting way
		// (i.e., de-interleave them)
		panic("multiload not yet supported")
	}
	// TODO ideally we want to issue one request, wait a short while,
	// then issue two, etc., scaling up

	// first one wins
	ctx, cancel := context.WithCancel(ctx)

	p := req.Refs[0]

	nodes := c.NodesFor(p)
	result := make(chan loadResult, 1)

	trial := func(on *Node) {
		sc, err := on.getStorageClient(ctx)
		if err != nil {
			result <- loadResult{nil, err}
			return
		}
		datum, err := sc.Load(ctx, req)
		if err != nil {
			result <- loadResult{nil, err}
		} else {
			result <- loadResult{datum.Data[0], nil}
		}
	}

	for _, node := range nodes {
		go trial(node)
	}

	// collect results until we get a success
	var success *cam.Datum
	var errs []error

	i := 0
	for {
		r := <-result
		// log.Tracef(ctx, "%#v", r)
		i++
		if r.err != nil {
			errs = append(errs, r.err)
		} else {
			success = r.datum
			break
		}
		if i == len(nodes) {
			break
		}
	}
	cancel() // stop any in-flight requests to the best of our ability

	// consume any remaining requests async
	go func() {
		for ; i < len(nodes); i++ {
			<-result
		}
	}()
	if success != nil {
		// lose any errors
		return &cam.LoadResponse{
			Data: []*cam.Datum{success},
		}, nil
	} else {
		// return the first error
		return nil, errs[0]
	}
}

type storeResult struct {
	err error
}

/*func (n *Node) Next() api.CAM {
	return n.cam
}*/

func (c *Cluster) Store(ctx context.Context, req *cam.StoreRequest) (*cam.StoreResponse, error) {

	if len(req.Data) != 1 {
		// likewise, an interesting deinterleaving problem
		panic("multistore not yet supported")
	}

	// in this case, we send it to all the replicas, but only wait for
	// a quorum of acks (usually 2)

	p := req.Data[0].Ref // TODO handle the case where they don't supply the ref
	nodes := c.NodesFor(p)

	// make sure there's space for all the results, because
	// we are going to bail as soon as we get a quorum
	result := make(chan storeResult, len(nodes))
	// for the same reason, detach from the calling context
	bg := context.Background()
	submit := func(on *Node) {
		sc, err := on.getStorageClient(ctx)
		if err != nil {
			result <- storeResult{err}
			return
		}
		_, err = sc.Store(bg, req)
		result <- storeResult{err}
	}

	for _, n := range nodes {
		submit(n)
	}
	remain := c.writeQuorum
	ok := false
	var err0 error

	for {
		r := <-result
		if r.err != nil {
			log.Tracef(ctx, "failed: %s", r.err)
			err0 = r.err
		} else {
			ok = true
			remain--
			log.Tracef(ctx, "success, waiting for %d  more", remain)
			if remain == 0 {
				break
			}
		}
	}
	if ok {
		// how should we treat an insufficient quorum?
		// treat it as an error for now
		if remain > 0 {
			return nil, errlib.Newf("CAM-6442 %d writes remain to achieve quorum", remain)
		}
		return &cam.StoreResponse{
			Refs: []*core.Ptr{p},
		}, nil
	} else {
		return nil, err0
	}
}

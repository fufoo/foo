package netmux

import (
	"context"
	"encoding/binary"
	"fmt"
	"math"
	"math/bits"
	"sort"
	"sync"

	"bitbucket.org/dkolbly/logging"
	//"bitbucket.org/fufoo/core/errlib"
	//"bitbucket.org/fufoo/core/ptr"
	//"bitbucket.org/fufoo/foo/services/cam"
	"bitbucket.org/fufoo/foo/services/core"
	"github.com/spaolacci/murmur3"
)

var log = logging.New("netmux")

// We use rendezvous hashing also know as highest random weight (HRW)
// [https://en.wikipedia.org/wiki/Rendezvous_hashing]
// but over a large number of buckets (64k) instead of the entire
// namespace, principally so that we can precompute the mapping

//const NumBuckets = 1 << 16

type Cluster struct {
	buckets     [][]*Node
	writeQuorum int
}

type weighting struct {
	node  *Node
	score float64
}

type weightings []weighting

func (w weightings) Len() int           { return len(w) }
func (w weightings) Swap(i, j int)      { w[i], w[j] = w[j], w[i] }
func (w weightings) Less(i, j int) bool { return w[i].score > w[j].score }

func (n *Node) score(bucket int) float64 {

	buf := []byte{
		uint8(bucket),
		uint8(bucket >> 8),
		uint8(bucket >> 16),
		uint8(bucket >> 24),
	}

	h := murmur3.Sum64WithSeed(buf, n.seed)
	score := math.Ldexp(float64(h), -64)

	// the 1/10 scaling gets our weights for which nominal is 10000
	// into the O(1000) range
	return float64(n.weight) / 10 / -math.Log(score)
}

func (c *Cluster) Nodes() []*Node {
	toc := make(map[*Node]struct{})
	var lst []*Node

	for _, b := range c.buckets {
		for _, n := range b {
			if _, ok := toc[n]; !ok {
				toc[n] = struct{}{}
				lst = append(lst, n)
			}
		}
	}
	return lst
}

func New(numBuckets int, replicas int, nodes []*Node) *Cluster {
	b := make([][]*Node, numBuckets)

	if replicas > len(nodes) {
		replicas = len(nodes)
	}

	weights := weightings(make([]weighting, len(nodes)))

	for i := 0; i < numBuckets; i++ {
		for j, n := range nodes {
			weights[j] = weighting{
				node:  n,
				score: n.score(i),
			}
		}
		sort.Sort(weights)
		bucket := make([]*Node, replicas)
		if false {
			fmt.Printf("BUCKET [%d] ", i)
		}
		for j := range bucket {
			if false {
				fmt.Printf("   %.1f %d",
					weights[j].score,
					weights[j].node.seed,
				)
			}
			bucket[j] = weights[j].node
		}
		if false {
			fmt.Printf("\n")
		}
		b[i] = bucket
	}
	q := 2
	if q > replicas {
		q = replicas
	}

	clus := &Cluster{
		buckets:     b,
		writeQuorum: q,
	}
	return clus
}

type Node struct {
	seed    uint32
	weight  uint32
	lock    sync.Mutex
	factory func(context.Context) (interface{}, error)
	target  interface{}
}

func (n *Node) acquire(ctx context.Context) (interface{}, error) {
	n.lock.Lock()
	defer n.lock.Unlock()
	if n.target != nil {
		return n.target, nil
	}
	t, err := n.factory(ctx)
	if err != nil {
		return nil, err
	}
	n.target = t
	return t, nil
}

/*func (n *Node) Tell(fn func(api.CAM) error) error {
	return fn(n.cam)
}*/

type AddressableNode interface {
	InternalClusterAddr() string
}

func (n *Node) Target() interface{} {
	return n.target
}

func (n *Node) ID() uint32 {
	return n.seed
}

func (n *Node) Weight() uint32 {
	return n.weight
}

func (c *Cluster) NodesIn(b int) []*Node {
	return c.buckets[b]
}

func (c *Cluster) NodesFor(p *core.Ptr) []*Node {
	return c.NodesIn(BucketFor(p, len(c.buckets)))
}

// NewNode creates a new node that can be used in a Cluster.  The
// factory function is responsible for producing a qrpc Client of the
// appropriate type (e.g., a StateClient or a StorageClient
// respectively for state service or storage service)
func NewNode(id string, weight uint32, factory func(context.Context) (interface{}, error)) *Node {
	return &Node{
		seed:    murmur3.Sum32([]byte(id)),
		weight:  weight,
		factory: factory,
	}
}

func (c *Cluster) BucketBits() int {
	n := len(c.buckets)
	nbits := bits.TrailingZeros(uint(n))
	if 1<<nbits != n {
		panic(fmt.Sprintf("?? n of %d implies %d bits", n, nbits))
	}
	return nbits
}

func BucketFor(p *core.Ptr, n int) int {
	nbits := bits.TrailingZeros(uint(n))
	if 1<<nbits != n {
		panic("??")
	}
	k := binary.BigEndian.Uint64(p.Bits[:8])
	//return int(k % uint64(n))
	return int(k >> (64 - nbits))
}

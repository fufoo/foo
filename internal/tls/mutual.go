package tls

import (
	"crypto/tls"
	"crypto/x509"
)

var nextProtos = []string{"qrpc-foo"}

// PlatformClientMutual is used to produce a client TLS config set up
// for a platform service talking to a service that wants to do mutual
// auth (i.e., talking to the zero service)
func PlatformClientMutual(certFile, keyFile string, trust *x509.CertPool) (*tls.Config, error) {

	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		return nil, err
	}
	config := &tls.Config{
		Certificates: []tls.Certificate{cert},
		RootCAs:      trust,
		NextProtos:   nextProtos,
	}
	return config, nil
}

// PlatformServerMutual sets up TLS for a platform server that
// requires mutual auth (this is only used by the zero service)
func PlatformServerMutual(certFile, keyFile string, trust *x509.CertPool) (*tls.Config, error) {

	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		return nil, err
	}
	config := &tls.Config{
		Certificates: []tls.Certificate{cert},
		RootCAs:      trust,
		NextProtos:   nextProtos,
		// !TODO!  quic does not yet seem to fully support mutual authentication
		// ClientAuth:   tls.RequireAndVerifyClientCert,
	}
	return config, nil
}

package tls

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"math/big"
)

func PlatformServer(certFile, keyFile string, trust *x509.CertPool) (*tls.Config, error) {
	// this is identical to what we want:
	return PlatformClientMutual(certFile, keyFile, trust)
}

func PlatformClient(trust *x509.CertPool) (*tls.Config, error) {

	config := &tls.Config{
		RootCAs:    trust,
		NextProtos: nextProtos,
	}
	return config, nil
}

func Trust(certsFile string) *x509.CertPool {
	// don't use the system cert pool; we don't trust it for our purposes
	root := x509.NewCertPool()

	// Read in the cert file
	if certsFile == "" {
		panic("no certs file specified")
	}
	certs, err := ioutil.ReadFile(certsFile)
	if err != nil {
		panic(err)
	}

	// Append our cert to the system pool
	if ok := root.AppendCertsFromPEM(certs); !ok {
		panic("could not append trusted certs")
	}

	return root
}

func NewTLSConfig(server bool) *tls.Config {
	protos := []string{"qrpc-foo"}
	if !server {
		return &tls.Config{
			InsecureSkipVerify: true,
			NextProtos:         protos,
		}
	}

	key, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		panic(err)
	}
	template := x509.Certificate{SerialNumber: big.NewInt(1)}
	certDER, err := x509.CreateCertificate(rand.Reader, &template, &template, &key.PublicKey, key)
	if err != nil {
		panic(err)
	}
	keyPEM := pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(key)})
	certPEM := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: certDER})

	tlsCert, err := tls.X509KeyPair(certPEM, keyPEM)
	if err != nil {
		panic(err)
	}
	return &tls.Config{
		Certificates: []tls.Certificate{tlsCert},
		NextProtos:   protos,
	}
}

// Setup a bare-bones TLS config for the server
/*
func GenerateTLSConfig() *tls.Config {
	key, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		panic(err)
	}
	template := x509.Certificate{SerialNumber: big.NewInt(1)}
	certDER, err := x509.CreateCertificate(rand.Reader, &template, &template, &key.PublicKey, key)
	if err != nil {
		panic(err)
	}
	keyPEM := pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(key)})
	certPEM := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: certDER})

	tlsCert, err := tls.X509KeyPair(certPEM, keyPEM)
	if err != nil {
		panic(err)
	}
	return &tls.Config{
		Certificates: []tls.Certificate{tlsCert},
		NextProtos:   []string{"quic-echo-example"},
	}
}
*/

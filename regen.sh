#! /bin/bash

PATH=$HOME/go/bin:$HOME/lib/protoc-3.12.3/bin:$PATH

# protoc -I=../../.. \
#        -I=../../../github.com/gogo/protobuf/protobuf \
#        --gofaster_out=plugins=grpc,\
# Mgoogle/protobuf/any.proto=github.com/gogo/protobuf/types,\
# Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
# Mgoogle/protobuf/struct.proto=github.com/gogo/protobuf/types,\
# Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
# Mgoogle/protobuf/wrappers.proto=github.com/gogo/protobuf/types:../../.. \
#        bitbucket.org/fufoo/foo/lib/api/state.proto \
#        bitbucket.org/fufoo/foo/lib/api/cam.proto \
#        bitbucket.org/fufoo/foo/lib/api/cluster.proto

(cd ../qrpc/cmd/gen-qrpc ; go build -o /tmp/gen-qrpc)
#go build -o /tmp/gen-qrpc ../qrpc/cmd/gen-qrpc
#generate-qrpc-stubs.go


#x echo "================ lib/api ==============="
#x protoc \
#x     --go_out=lib/api \
#x     --go-grpc_out=lib/api \
#x     --proto_path=lib/api:. \
#x     --go-grpc_opt=paths=source_relative \
#x     --go_opt=paths=source_relative \
#x         lib/api/deprecateptr.proto \
#x         lib/api/state.proto \
#x         lib/api/cam.proto \
#x         lib/api/cluster.proto

echo "================ services/state ==============="

protoc \
    --descriptor_set_out=services/state/state.desc \
    --go_out=services/state \
    --go_opt=paths=source_relative \
    --go-grpc_out=services/state \
    --go-grpc_opt=paths=source_relative \
    --proto_path=services/state:. \
        services/state/state.proto

/tmp/gen-qrpc \
    -package=state \
    -domain=foo.services.state \
    -descriptor_set_in=services/state/state.desc \
    -go_out=services/state/state_qrpc.pb.go

echo "================ services/core ==============="
protoc \
    --go_out=services/core \
    --proto_path=services/core:. \
    --go_opt=paths=source_relative \
        services/core/ptr.proto

echo "================ services/cam ==============="
protoc \
    --descriptor_set_out=services/cam/cam.desc \
    --go_out=services/cam \
    --go_opt=paths=source_relative \
    --go-grpc_out=services/cam \
    --go-grpc_opt=paths=source_relative \
    --proto_path=services/cam:. \
        services/cam/cam.proto \
        services/cam/asset.proto

/tmp/gen-qrpc \
   -package cam \
   -domain foo.services.cam \
   -descriptor_set_in services/cam/cam.desc \
   -go_out=services/cam/cam_qrpc.pb.go

echo "================ services/platform ==============="

protoc \
    --descriptor_set_out=internal/services/platform/platform.desc \
    --go_out=internal/services/platform \
    --proto_path=internal/services/platform:. \
    --go_opt=paths=source_relative \
        internal/services/platform/region.proto \
        internal/services/platform/zero.proto \
        internal/services/platform/auth.proto

/tmp/gen-qrpc \
   -package platform \
   -domain foo.internal.services.platform \
   -descriptor_set_in internal/services/platform/platform.desc \
   -go_out=internal/services/platform/platform_qrpc.pb.go

echo "================ services/admin ==============="

protoc \
    --descriptor_set_out=services/admin/admin.desc \
    --go_out=services/admin \
    --proto_path=services/admin:. \
    --go_opt=paths=source_relative \
        services/admin/admin.proto

/tmp/gen-qrpc \
   -package admin \
   -domain foo.services.admin \
   -descriptor_set_in services/admin/admin.desc \
   -go_out=services/admin/admin_qrpc.pb.go

package main

import (
	"context"
	"os"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/fulang/eval"
	//"bitbucket.org/fufoo/fulang/runtime"
	_ "bitbucket.org/fufoo/foo/fooql"
	"bitbucket.org/fufoo/fulang/mod"
)

var log = logging.New("fooqld")

func main() {
	logging.Init(logging.LowestLevel(os.Getenv("FUDEBUG")))
	ctx := context.Background()
	a, err := MakeApp(ctx, "app1", app1)
	if err != nil {
		panic(err)
	}
	a.run(ctx)
}

const app1 = `
import "bitbucket.org/fufoo/foo/fooql"

print("start\n")
fooql.Open("hello")
print("end\n")
`

type App struct {
	program *eval.Program
}

func (app *App) run(ctx context.Context) {
	app.program.Run(ctx, nil)
}

func MakeApp(ctx context.Context, name string, text string) (*App, error) {

	// TODO intercept panics that represent compile-time errors

	unary := mod.LiteralFileSet(ctx, name, ".fus", []byte(text))

	// initialize
	mod.Init(ctx)

	// never update, and we don't have a remote spec
	plan, err := makePlanFromFileSet(ctx, unary, nil, false)
	if err != nil {
		log.Error(ctx, err)
		return nil, err
	}

	icp, err := plan.Compile(ctx)
	if err != nil {
		log.Error(ctx, err)
		return nil, err
	}

	pgm, err := eval.Prepare(ctx, icp)
	if err != nil {
		log.Error(ctx, err)
		return nil, err
	}

	app := &App{
		program: pgm,
	}
	return app, nil
}

func makePlanFromFileSet(ctx context.Context, files *mod.FileSet, rs *mod.RemoteSpec, update bool) (*mod.BuildPlan, error) {

	if rs != nil {
		log.Debugf(ctx, "building %s", rs)
	}

	main, err := mod.ParsePackagePreamble(ctx, files)
	if err != nil {
		// could not parse main package preamble
		log.Error(ctx, err)
		return nil, err
	}

	plan, err := mod.MainPlan(ctx, rs, main, files, update)
	if err != nil {
		// could not develop a plan
		log.Error(ctx, err)
		return nil, err
	}
	log.Debugf(ctx, "here's the plan...")
	for i, unit := range plan.Units {
		if unit.Pre != nil {
			log.Debugf(ctx, " (step %d) build %d files in %s <%.9s>",
				i+1,
				len(unit.Pre.Files.Members),
				unit.Pre.Files.Directory,
				unit.Pre.Files.Hash)
		} else {
			log.Debugf(ctx, " (step %d) plugin",
				i+1,
			)
		}
	}
	return plan, nil
}

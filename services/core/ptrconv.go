package core

import (
	"bitbucket.org/fufoo/core/ptr"
)

func From(p ptr.Ptr) *Ptr {
	if p.IsNull() {
		panic("not willing to convert ptr.Null")
	}
	return &Ptr{
		Bits: p.Bits[:],
	}
}

func (p *Ptr) Must() ptr.Ptr {
	return p.AsPtr()
}

// AsPtr produces the native Ptr object for the protobuf version of Ptr.
// (follows naming convention for timestamppb, a la, AsTime())
func (p *Ptr) AsPtr() ptr.Ptr {
	if p == nil {
		return ptr.Null
	}
	if len(p.Bits) != ptr.Size {
		panic("FLO-8230 incorrect length")
	}
	var x ptr.Ptr
	copy(x.Bits[:], p.Bits)
	if x.IsNull() {
		// a PRESENT value which is NULL is not allowed; it should
		// not be present at all (ie p==nil) in the protobuf
		panic("FLO-8232 ptr.Null value is invalid, should be nil instead")
	}
	if !x.IsValid() {
		panic("FLO-8231 invalid metatype")
	}
	return x
}

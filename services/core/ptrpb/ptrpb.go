// by analogy to timestamppb
package ptrpb

import (
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/services/core"
)

func New(p ptr.Ptr) *core.Ptr {
	return core.From(p)
}

// generated
package cam

import (
	"bufio"
	"context"

	"bitbucket.org/fufoo/qrpc"
	"google.golang.org/protobuf/proto"
)

type StorageQServer interface {
	Load(context.Context, *LoadRequest) (*LoadResponse, error)
	Store(context.Context, *StoreRequest) (*StoreResponse, error)
}

type StorageQClient interface {
	Load(context.Context, *LoadRequest) (*LoadResponse, error)
	Store(context.Context, *StoreRequest) (*StoreResponse, error)
}

type simpleStorageImpl struct {
	rpc *qrpc.Client
}

func NewStorageQClient(rpc *qrpc.Client) StorageQClient {
	return &simpleStorageImpl{
		rpc: rpc,
	}
}

func (c *simpleStorageImpl) Load(ctx context.Context, msg *LoadRequest) (*LoadResponse, error) {
	var ret LoadResponse
	err := c.rpc.RPC(ctx, "qrpc://foo.services.cam/Storage/Load", msg, &ret)
	if err != nil {
		return nil, err
	}
	return &ret, nil
}

type rxStorage_Load struct {
	next StorageQServer
}

func (b rxStorage_Load) Handle(ctx context.Context, _ qrpc.Stream, dst *bufio.Writer, recv []byte) error {
	var req LoadRequest

	err := proto.Unmarshal(recv, &req)
	if err != nil {
		return err
	}
	resp, err := b.next.Load(ctx, &req)

	return qrpc.WriteResponse(ctx, dst, resp, err)

}

func (c *simpleStorageImpl) Store(ctx context.Context, msg *StoreRequest) (*StoreResponse, error) {
	var ret StoreResponse
	err := c.rpc.RPC(ctx, "qrpc://foo.services.cam/Storage/Store", msg, &ret)
	if err != nil {
		return nil, err
	}
	return &ret, nil
}

type rxStorage_Store struct {
	next StorageQServer
}

func (b rxStorage_Store) Handle(ctx context.Context, _ qrpc.Stream, dst *bufio.Writer, recv []byte) error {
	var req StoreRequest

	err := proto.Unmarshal(recv, &req)
	if err != nil {
		return err
	}
	resp, err := b.next.Store(ctx, &req)

	return qrpc.WriteResponse(ctx, dst, resp, err)

}

func RegisterStorageQServer(s *qrpc.Server, srv StorageQServer) {
	s.RegisterServiceMethod(srv, "qrpc://foo.services.cam/Storage/Load", rxStorage_Load{srv})
	s.RegisterServiceMethod(srv, "qrpc://foo.services.cam/Storage/Store", rxStorage_Store{srv})
}

// generated
package state

import (
	"bufio"
	"context"

	"bitbucket.org/fufoo/qrpc"
	"google.golang.org/protobuf/proto"
)

type StateQServer interface {
	Get(context.Context, *GetRequest) (*GetResponse, error)
	Apply(context.Context, *TxnRequest) (*TxnRecord, error)
	Follow(context.Context, *FollowRequest) (State_FollowQClient, error)
	Scan(context.Context, *GetRequest) (State_ScanQClient, error)
}

type State_FollowQClient interface {
	Recv() (*TxnRecord, error)
}

type State_ScanQClient interface {
	Recv() (*Obs, error)
}

type StateQClient interface {
	Get(context.Context, *GetRequest) (*GetResponse, error)
	Apply(context.Context, *TxnRequest) (*TxnRecord, error)
	Follow(context.Context, *FollowRequest) (State_FollowQClient, error)
	Scan(context.Context, *GetRequest) (State_ScanQClient, error)
}

type simpleStateImpl struct {
	rpc *qrpc.Client
}

func NewStateQClient(rpc *qrpc.Client) StateQClient {
	return &simpleStateImpl{
		rpc: rpc,
	}
}

func (c *simpleStateImpl) Get(ctx context.Context, msg *GetRequest) (*GetResponse, error) {
	var ret GetResponse
	err := c.rpc.RPC(ctx, "qrpc://foo.services.state/State/Get", msg, &ret)
	if err != nil {
		return nil, err
	}
	return &ret, nil
}

type rxState_Get struct {
	next StateQServer
}

func (b rxState_Get) Handle(ctx context.Context, _ qrpc.Stream, dst *bufio.Writer, recv []byte) error {
	var req GetRequest

	err := proto.Unmarshal(recv, &req)
	if err != nil {
		return err
	}
	resp, err := b.next.Get(ctx, &req)

	return qrpc.WriteResponse(ctx, dst, resp, err)

}

func (c *simpleStateImpl) Apply(ctx context.Context, msg *TxnRequest) (*TxnRecord, error) {
	var ret TxnRecord
	err := c.rpc.RPC(ctx, "qrpc://foo.services.state/State/Apply", msg, &ret)
	if err != nil {
		return nil, err
	}
	return &ret, nil
}

type rxState_Apply struct {
	next StateQServer
}

func (b rxState_Apply) Handle(ctx context.Context, _ qrpc.Stream, dst *bufio.Writer, recv []byte) error {
	var req TxnRequest

	err := proto.Unmarshal(recv, &req)
	if err != nil {
		return err
	}
	resp, err := b.next.Apply(ctx, &req)

	return qrpc.WriteResponse(ctx, dst, resp, err)

}

func (c *simpleStateImpl) Follow(ctx context.Context, msg *FollowRequest) (State_FollowQClient, error) {
	var ret TxnRecord
	ch := make(chan interface{}, 10)
	err := c.rpc.RPCServerStream(ctx, "qrpc://foo.services.state/State/Follow", msg, &ret, ch)
	if err != nil {
		return nil, err
	}
	return simpleStateFollowStreamer{ch}, nil
}

type simpleStateFollowStreamer struct {
	src <-chan interface{}
}

func (s simpleStateFollowStreamer) Recv() (*TxnRecord, error) {
	item, ok := <-s.src
	if !ok {
		return nil, qrpc.EOF
	}
	if err, ok := item.(*qrpc.RPCError); ok {
		return nil, err
	}
	return item.(*TxnRecord), nil
}

type rxState_Follow struct {
	next StateQServer
}

func (b rxState_Follow) Handle(ctx context.Context, _ qrpc.Stream, dst *bufio.Writer, recv []byte) error {
	var req FollowRequest

	err := proto.Unmarshal(recv, &req)
	if err != nil {
		return err
	}
	resp, err := b.next.Follow(ctx, &req)

	if err != nil {
		return qrpc.WriteResponse(ctx, dst, nil, err)
	}
	for {
		item, err := resp.Recv()
		if err != nil {
			if !qrpc.IsEOF(err) {
				return qrpc.WriteResponse(ctx, dst, nil, err)
			}
			return nil
		}
		qrpc.WriteResponse(ctx, dst, item, nil)
	}

}

func (c *simpleStateImpl) Scan(ctx context.Context, msg *GetRequest) (State_ScanQClient, error) {
	var ret Obs
	ch := make(chan interface{}, 10)
	err := c.rpc.RPCServerStream(ctx, "qrpc://foo.services.state/State/Scan", msg, &ret, ch)
	if err != nil {
		return nil, err
	}
	return simpleStateScanStreamer{ch}, nil
}

type simpleStateScanStreamer struct {
	src <-chan interface{}
}

func (s simpleStateScanStreamer) Recv() (*Obs, error) {
	item, ok := <-s.src
	if !ok {
		return nil, qrpc.EOF
	}
	if err, ok := item.(*qrpc.RPCError); ok {
		return nil, err
	}
	return item.(*Obs), nil
}

type rxState_Scan struct {
	next StateQServer
}

func (b rxState_Scan) Handle(ctx context.Context, _ qrpc.Stream, dst *bufio.Writer, recv []byte) error {
	var req GetRequest

	err := proto.Unmarshal(recv, &req)
	if err != nil {
		return err
	}
	resp, err := b.next.Scan(ctx, &req)

	if err != nil {
		return qrpc.WriteResponse(ctx, dst, nil, err)
	}
	for {
		item, err := resp.Recv()
		if err != nil {
			if !qrpc.IsEOF(err) {
				return qrpc.WriteResponse(ctx, dst, nil, err)
			}
			return nil
		}
		qrpc.WriteResponse(ctx, dst, item, nil)
	}

}

func RegisterStateQServer(s *qrpc.Server, srv StateQServer) {
	s.RegisterServiceMethod(srv, "qrpc://foo.services.state/State/Get", rxState_Get{srv})
	s.RegisterServiceMethod(srv, "qrpc://foo.services.state/State/Apply", rxState_Apply{srv})
	s.RegisterServiceMethod(srv, "qrpc://foo.services.state/State/Follow", rxState_Follow{srv})
	s.RegisterServiceMethod(srv, "qrpc://foo.services.state/State/Scan", rxState_Scan{srv})
}

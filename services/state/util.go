package state

import (
	"bytes"
	"fmt"
	"time"

	"bitbucket.org/fufoo/foo/services/core"
)

// MaxNameLength is the longest length of an obs name we support.
// This is mostly a protection mechanism; things are tuned for this
// size, if you start trying to use 1MB obs names, things may get bad.
// We also use it to statically allocate buffers in some cases.
const MaxNameLength = 200

// MaxTransactionSize is the largest number of updates we support in a
// single transaction.  Some places use fixed-length bitmap encodings
// and so depend on this value.
const MaxTransactionSize = 64

// Short returns a short(er) string representation
func (o *Obs) Short() string {
	var val string

	if o.Mtime == nil {
		return fmt.Sprintf("D#%d [%s] NEW", o.Domain, o.Name)
	}

	if o.Value == nil {
		val = "MISSING"
	} else {
		pv := o.Value.Must()
		val = "{" + pv.String() + "}"
	}

	t := o.Mtime.AsTime()

	return fmt.Sprintf("D#%d [%s] %s #%d %s A#%d",
		o.Domain,
		o.Name,
		val,
		o.Seq,
		t.In(time.Local).Format(time.RFC3339Nano),
		o.Audit,
	)
}

func (t *TxnRecord) Debug() []byte {
	var buf bytes.Buffer
	fmt.Fprintf(&buf, "domain=%d", t.Request.Domain)

	// transactions with no effects don't need audit values,
	// and they're optional anyway since they're for the client
	// application's use
	if t.Request.Audit != 0 {
		fmt.Fprintf(&buf, " audit=%d", t.Request.Audit)
	}
	if t.Id != 0 {
		fmt.Fprintf(&buf, " id=%d", t.Id)
	}
	buf.WriteByte('\n')

	for i, s := range t.Request.Subjects {
		fmt.Fprintf(&buf, "%d: [%s]\n", i, s)
		for _, pre := range t.Request.Preconds {
			if int32(i) == pre.Slot {
				fmt.Fprintf(&buf, "    pre %s\n",
					ptrstr(pre.Value))
			}
		}
		for _, pre := range t.Request.Effects {
			if int32(i) == pre.Slot {
				fmt.Fprintf(&buf, "    set %s\n",
					ptrstr(pre.Value))
			}
		}
	}

	if t.Conflict {
		fmt.Fprintf(&buf, "CONFLICT!")
	}

	return buf.Bytes()
}

func ptrstr(p *core.Ptr) string {
	if p == nil {
		return "MISSING"
	}
	return "{" + p.Must().String() + "}"
}

/*
// Ptr computes the ptr for the datum.  panics if the datum is invalid
// (which happens if the payload is too big or if the type is a
// malformed pointer)
func (datum *cam.Datum) Ptr() ptr.Ptr {
	if len(datum.Payload) > MaxDatumSize {
		panic("payload is too big")
	}
	if datum.Type == nil {
		return ptr.UntypedContentAddress(datum.Payload)
	} else {
		return ptr.TypedContentAddress(datum.Type.MustPtr(), datum.Payload)
	}
}

// MaxDatumSize is the largest payload we support for a datum.  Note
// that the optional type pointer is not included in this size,
// because that's a lower-level encoding issue.
const MaxDatumSize = 1 << 20
*/

package state

import (
	"context"
	"time"
)

type Keepaliver interface {
	Keepalive(ctx context.Context)
}

func (c *simpleStateImpl) Keepalive(ctx context.Context) {
	c.rpc.Keepalive(ctx, 15*time.Second)
}

// generated
package admin

import (
	"bufio"
	"context"

	"bitbucket.org/fufoo/qrpc"
	"google.golang.org/protobuf/proto"
)

type AdminQServer interface {
	AcquireDomain(context.Context, *AcquireDomainRequest) (*AcquireDomainResponse, error)
	DescribeDomain(context.Context, *DescribeDomainRequest) (*DescribeDomainResponse, error)
}

type AdminQClient interface {
	AcquireDomain(context.Context, *AcquireDomainRequest) (*AcquireDomainResponse, error)
	DescribeDomain(context.Context, *DescribeDomainRequest) (*DescribeDomainResponse, error)
}

type simpleAdminImpl struct {
	rpc *qrpc.Client
}

func NewAdminQClient(rpc *qrpc.Client) AdminQClient {
	return &simpleAdminImpl{
		rpc: rpc,
	}
}

func (c *simpleAdminImpl) AcquireDomain(ctx context.Context, msg *AcquireDomainRequest) (*AcquireDomainResponse, error) {
	var ret AcquireDomainResponse
	err := c.rpc.RPC(ctx, "qrpc://foo.services.admin/Admin/AcquireDomain", msg, &ret)
	if err != nil {
		return nil, err
	}
	return &ret, nil
}

type rxAdmin_AcquireDomain struct {
	next AdminQServer
}

func (b rxAdmin_AcquireDomain) Handle(ctx context.Context, _ qrpc.Stream, dst *bufio.Writer, recv []byte) error {
	var req AcquireDomainRequest

	err := proto.Unmarshal(recv, &req)
	if err != nil {
		return err
	}
	resp, err := b.next.AcquireDomain(ctx, &req)

	return qrpc.WriteResponse(ctx, dst, resp, err)

}

func (c *simpleAdminImpl) DescribeDomain(ctx context.Context, msg *DescribeDomainRequest) (*DescribeDomainResponse, error) {
	var ret DescribeDomainResponse
	err := c.rpc.RPC(ctx, "qrpc://foo.services.admin/Admin/DescribeDomain", msg, &ret)
	if err != nil {
		return nil, err
	}
	return &ret, nil
}

type rxAdmin_DescribeDomain struct {
	next AdminQServer
}

func (b rxAdmin_DescribeDomain) Handle(ctx context.Context, _ qrpc.Stream, dst *bufio.Writer, recv []byte) error {
	var req DescribeDomainRequest

	err := proto.Unmarshal(recv, &req)
	if err != nil {
		return err
	}
	resp, err := b.next.DescribeDomain(ctx, &req)

	return qrpc.WriteResponse(ctx, dst, resp, err)

}

func RegisterAdminQServer(s *qrpc.Server, srv AdminQServer) {
	s.RegisterServiceMethod(srv, "qrpc://foo.services.admin/Admin/AcquireDomain", rxAdmin_AcquireDomain{srv})
	s.RegisterServiceMethod(srv, "qrpc://foo.services.admin/Admin/DescribeDomain", rxAdmin_DescribeDomain{srv})
}

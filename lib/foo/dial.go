package foo

import (
	"context"
	"io/ioutil"
	"strings"
	"sync"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/foo/internal/netmux"
	"bitbucket.org/fufoo/foo/internal/services/platform"
	"bitbucket.org/fufoo/foo/internal/tls"
	"bitbucket.org/fufoo/foo/lib/sequence"
	"bitbucket.org/fufoo/foo/services/cam"
	"bitbucket.org/fufoo/foo/services/state"
	"bitbucket.org/fufoo/qrpc"
	"google.golang.org/protobuf/encoding/protojson"
)

var log = logging.New("foo")

type seqKey struct {
	domain uint64
	name   string
}

type Conn struct {
	states *netmux.Cluster
	cams   *netmux.Cluster // cam.StorageClient
	lock   sync.Mutex
	dep    *platform.Deployment
	das    map[seqKey]*sequence.Issuer
	token  string
	auth   platform.AuthQClient
}

type RemoteConfig struct {
	TrustFile       string
	Server          string
	AuthMechanism   string
	AuthIdentity    string
	AuthCredentials string
}

func DialRemote(ctx context.Context, cfg *RemoteConfig) (*Conn, error) {
	t, err := tls.PlatformClient(tls.Trust(cfg.TrustFile))
	if err != nil {
		return nil, err
	}
	c, err := qrpc.Dial(ctx, cfg.Server, qrpc.DialWithTLS(t))
	if err != nil {
		return nil, err
	}
	auth := platform.NewAuthQClient(c)

	if cfg.AuthMechanism == "basic-salt" {
		panic("TODO..")
		_, err := auth.Salt(ctx, &platform.SaltRequest{
			Mechanism:  cfg.AuthMechanism,
			Identifier: cfg.AuthIdentity,
		})
		if err != nil {
			return nil, err
		}
	}
	authed, err := auth.Exchange(ctx, &platform.ExchangeRequest{
		Mechanism:   cfg.AuthMechanism,
		Identifier:  cfg.AuthIdentity,
		Credentials: cfg.AuthCredentials,
	})

	if err != nil {
		return nil, err
	}
	conn := dialDeployment(ctx, authed.Deployment)
	conn.auth = auth
	return conn, nil
}

func (c *Conn) EnrollExternalClient(ctx context.Context, req *platform.EnrollRequest) error {
	if c.auth == nil {
		return errlib.Newf("FOO-3091 connection does not have an auth endpoint")
	}
	_, err := c.auth.Enroll(ctx, req)
	return err
}

func DialDeployment(ctx context.Context, dep *platform.Deployment) *Conn {
	return dialDeployment(ctx, dep)
}

func dialDeployment(ctx context.Context, dep *platform.Deployment) *Conn {
	log.Debugf(ctx, "deployment/%d (%s)", dep.Id, dep.Name)

	services := make(map[string]*platform.ServiceDeployment)
	for _, s := range dep.Services {
		services[s.Kind] = s
	}

	conn := &Conn{
		dep: dep,
		das: make(map[seqKey]*sequence.Issuer),
	}

	if true {
		camsvc := services["cam"]
		if camsvc == nil {
			log.Warningf(ctx, "FOO-3011 cam service is not online")
		} else {
			var camnodes []*netmux.Node

			for _, node := range camsvc.Current.Nodes {
				camnodes = append(camnodes, conn.makeCamNode(node))
			}

			conn.cams = netmux.New(int(camsvc.Cam.Buckets), int(camsvc.Cam.Replicas), camnodes)
		}
	}

	if true {
		ss := services["state"]
		if ss == nil {
			log.Warningf(ctx, "FOO-3021 state service is not online")
		} else {
			var statenodes []*netmux.Node

			for _, node := range ss.Current.Nodes {
				statenodes = append(statenodes, conn.makeStateNode(node))
			}

			conn.states = netmux.New(int(ss.State.Buckets), 1, statenodes)
		}
	}
	return conn
}

func DialFile(ctx context.Context, zero string) (*Conn, error) {
	if !strings.HasPrefix(zero, "file://") {
		panic("missing file prefix")
	}

	buf, err := ioutil.ReadFile(strings.TrimPrefix(zero, "file://"))
	if err != nil {
		return nil, err
	}

	var dep platform.Deployment
	err = protojson.Unmarshal(buf, &dep)
	if err != nil {
		return nil, err
	}
	if dep.Id == 0 || dep.Name == "" {
		return nil, errlib.Newf("FOO-3090 invalid deployment info")
	}

	return dialDeployment(ctx, &dep), nil
}

func (c *Conn) makeStateNode(node *platform.Node) *netmux.Node {
	wt := 1000 + 10*int(node.Weight)
	if wt < 1 {
		wt = 0
	}
	secure := tls.NewTLSConfig(false)
	factory := func(ctx context.Context) (interface{}, error) {
		log.Debugf(ctx, "spinning up State node %q at %s", node.Name, node.Addr,
			logging.F("addr", node.Addr))

		c, err := qrpc.Dial(ctx, node.Addr, qrpc.DialWithTLS(secure))
		if err != nil {
			return nil, err
		}

		return state.NewStateQClient(c), nil
	}

	return netmux.NewNode(node.Seed, uint32(wt), factory)
}

func (c *Conn) makeCamNode(node *platform.Node) *netmux.Node {
	wt := 1000 + 10*int(node.Weight)
	if wt < 1 {
		wt = 0
	}
	secure := tls.NewTLSConfig(false)
	factory := func(ctx context.Context) (interface{}, error) {
		log.Debugf(ctx, "spinning up CAM node %q at %s", node.Name, node.Addr)

		c, err := qrpc.Dial(ctx, node.Addr, qrpc.DialWithTLS(secure))
		if err != nil {
			return nil, err
		}
		return cam.NewStorageQClient(c), nil
	}

	return netmux.NewNode(node.Seed, uint32(wt), factory)
}

// return the root domain
func (c *Conn) Root() uint64 {
	return 1 // TODO configure this with the deployment
}

// Access opens up access to a special domain.  Credentials may be
// obtained various ways, but an easy shared-key approach is using
// bitbucket.org/fufoo/foo/lib/keycreds.KeyFromString()
func (c *Conn) Access(domain uint64, creds Credentials) *Domain {
	return &Domain{
		conn:  c,
		id:    domain,
		creds: creds,
	}
}

/*
	var up sync.WaitGroup
	up.Add(len(cc.Nodes))

	bringup := func(k int, cnode *cammeta.Node) {
		defer up.Done()
		log.Debugf(ctx, "node %s", cnode.Addr)
		c, err := qrpc.Dial(cnode.Addr, qrpc.DialWithTLS(secure))
		if err != nil {
			panic(err)
		}

		stor := qapi.NewStorageClient(c)
		clus := qapi.NewClusterClient(c)

		ret, err := stor.Setup(ctx, &qapi.SessionSetup{Authorization: "HELLO"})
		log.Debugf(ctx, "node %s sid %q", cnode.Addr, ret.SessionId)
		nodes[k] = netmux.New(cnode.ID, cnode.Weight, stor, clus)
	}

}
*/

func (c *Conn) getSeq(ctx context.Context, sk seqKey) *sequence.Issuer {
	c.lock.Lock()
	defer c.lock.Unlock()

	seq := c.das[sk]
	if seq == nil {
		seq = sequence.New(ctx, c.states, sk.domain, "seq/"+sk.name).SetMin(1000).SetMix(1)
		c.das[sk] = seq
	}
	return seq
}

func (c *Conn) getAuditIssuer(ctx context.Context, domain uint64) *sequence.Issuer {
	return c.getSeq(ctx, seqKey{
		domain: domain,
		name:   "audit",
	})
}

func (c *Conn) allocAudit(ctx context.Context, domain uint64) uint64 {
	return c.getAuditIssuer(ctx, domain).Next(ctx)
}

// State returns a state client that knows how to route
// to the appropriate node in the cluster.
func (c *Conn) State() state.StateQClient {
	return c.states
}

// Storage returns a storage client that knows how to fan requests out
// across the cluster (first return across options for read operations
// and multiple writes)
func (c *Conn) Storage() cam.StorageQClient {
	return c.cams
}

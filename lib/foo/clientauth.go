package foo

import (
	"crypto/ed25519"
	"crypto/rand"
	"crypto/x509"
	"encoding/binary"
	"encoding/pem"
	"io"
	"io/ioutil"
	"math/big"
	"time"

	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"github.com/golang-jwt/jwt/v4"
)

type ClientAuth struct {
	Key ed25519.PrivateKey
	ID  ptr.Ptr
}

func (auth *ClientAuth) Enrollment() string {
	var tmp [500]byte

	i := 0
	i += binary.PutUvarint(tmp[i:], 0x4544)

	pub, err := x509.MarshalPKIXPublicKey(auth.Key.Public())
	if err != nil {
		panic(err)
	}
	i += binary.PutUvarint(tmp[i:], uint64(len(pub)))
	copy(tmp[i:], pub)
	i += len(pub)

	i += binary.PutUvarint(tmp[i:], ptr.Size)
	copy(tmp[i:], auth.ID.Bits[:])
	i += len(auth.ID.Bits)

	var z big.Int
	z.SetBytes(tmp[:i])
	return z.Text(62)
}

func ParseEnrollment(s string) (*ptr.Ptr, interface{}, error) {
	invalid := func() (*ptr.Ptr, interface{}, error) {
		return nil, nil, errlib.Newf("FOO-4105 invalid enrollment %q", s)
	}

	var z big.Int
	_, ok := z.SetString(s, 62)
	if !ok {
		return invalid()
	}
	tmp := z.Bytes()
	//
	tag, n := binary.Uvarint(tmp)
	if n <= 0 {
		return invalid()
	}
	if tag != 0x4544 {
		return invalid()
	}
	tmp = tmp[n:]
	//
	publen, n := binary.Uvarint(tmp)
	if n <= 0 {
		return invalid()
	}
	tmp = tmp[n:]
	//
	if publen > uint64(len(tmp)) {
		return invalid()
	}
	pubbuf := tmp[:publen]
	pub, err := x509.ParsePKIXPublicKey(pubbuf)
	if err != nil {
		return nil, nil, err
	}
	tmp = tmp[publen:]
	//
	idlen, n := binary.Uvarint(tmp)
	if n <= 0 {
		return invalid()
	}
	tmp = tmp[n:]
	//
	if idlen > uint64(len(tmp)) || idlen != ptr.Size {
		return invalid()
	}
	var id ptr.Ptr
	copy(id.Bits[:], tmp)
	tmp = tmp[idlen:]
	//
	if len(tmp) != 0 {
		return invalid()
	}
	// finally, success!
	return &id, pub, nil
}

func ReadClientAuth(src string) (*ClientAuth, error) {
	buf, err := ioutil.ReadFile(src)
	if err != nil {
		return nil, err
	}
	b, _ := pem.Decode(buf)
	if b == nil {
		panic("could not decode from: " + src)
	}
	if b.Type != "PRIVATE KEY" {
		panic("not a private key: " + b.Type)
	}

	id, ok := b.Headers["Foo-KeyId"]
	if !ok {
		panic("missing Foo-KeyId")
	}

	p, ok := ptr.DecodeStringPtr(id)
	if !ok {
		panic("Foo-KeyId is not valid")
	}

	priv, err := x509.ParsePKCS8PrivateKey(b.Bytes)
	if err != nil {
		return nil, err
	}

	return &ClientAuth{
		ID:  p,
		Key: priv.(ed25519.PrivateKey),
	}, nil
}

func (auth *ClientAuth) NewToken(audience string) (string, error) {

	var jwtid [8]byte

	_, err := io.ReadFull(rand.Reader, jwtid[:])
	if err != nil {
		return "", err
	}

	//

	now := time.Now().Truncate(time.Second)
	exp := now.Add(30 * time.Second)
	nbf := now.Add(-2 * time.Second)
	iat := now

	var z big.Int
	z.SetBytes(jwtid[:])

	claims := &jwt.RegisteredClaims{
		Audience:  []string{audience},
		ExpiresAt: &jwt.NumericDate{exp},
		IssuedAt:  &jwt.NumericDate{iat},
		NotBefore: &jwt.NumericDate{nbf},
		ID:        z.Text(62),
		Subject:   auth.ID.String(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodEdDSA, claims)
	token.Header["kid"] = auth.ID.String()
	return token.SignedString(auth.Key)
}

func (cfg *RemoteConfig) UseClientAuthFromFile(src string) error {
	auth, err := ReadClientAuth(src)
	if err != nil {
		return err
	}

	str, err := auth.NewToken("foo-xauth")
	if err != nil {
		return err
	}

	cfg.AuthMechanism = "client-key" // this is what we send to xauth
	cfg.AuthIdentity = auth.ID.String()
	cfg.AuthCredentials = str
	return nil
}

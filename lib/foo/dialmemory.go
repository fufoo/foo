package foo

import (
	"context"

	"bitbucket.org/fufoo/foo/internal/netmux"
	"bitbucket.org/fufoo/foo/internal/services/platform"
	"bitbucket.org/fufoo/foo/lib/inmem"
	"bitbucket.org/fufoo/foo/lib/sequence"
	"bitbucket.org/fufoo/foo/services/cam"
	"bitbucket.org/fufoo/foo/services/state"
)

// DialMemory builds a Conn to an in-memory foo store, useful for
// testing or transient databases
func DialMemory(ctx context.Context) (*Conn, error) {

	var camInmem cam.StorageQClient = inmem.NewStorage(nil).Client()
	var stateInmem state.StateQClient = inmem.NewState(nil).Client()

	cammer := func(ctx context.Context) (interface{}, error) {
		return camInmem, nil
	}
	cam := netmux.NewNode("cam1", 1, cammer)

	stater := func(ctx context.Context) (interface{}, error) {
		return stateInmem, nil
	}
	state := netmux.NewNode("state1", 1, stater)

	conn := &Conn{
		dep: &platform.Deployment{
			Id:   1,
			Name: "in-memory",
			Services: []*platform.ServiceDeployment{
				&platform.ServiceDeployment{},
			},
		},
		das:    make(map[seqKey]*sequence.Issuer),
		cams:   netmux.New(1, 1, []*netmux.Node{cam}),
		states: netmux.New(1, 1, []*netmux.Node{state}),
	}

	return conn, nil
}

type memoryCam struct {
}

type memoryState struct {
}

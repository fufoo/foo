package foo

import (
	"bytes"
	"context"
	"fmt"
	"sync"
	"time"

	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/sealed"
	"bitbucket.org/fufoo/foo/lib/sequence"
	"bitbucket.org/fufoo/foo/services/cam"
	"bitbucket.org/fufoo/foo/services/core"
	"bitbucket.org/fufoo/foo/services/state"
	"google.golang.org/protobuf/proto"
)

type Loader interface {
	Load(context.Context, ptr.Ptr, sealed.Loader) (interface{}, error)
}

type LoadStorer interface {
	Loader
	Store(context.Context, interface{}) (ptr.Ptr, error)
}

type ReadDriver interface {
	Get(ctx context.Context, domain uint64, name []string) ([]ptr.Ptr, []uint64, uint64, error)
	Read(ctx context.Context, p []ptr.Ptr) ([][]byte, error)
}

// these APIs only return errors when something is broken or the
// context is canceled.  (Broken can include permission errors
// attempting to access an object the user identified in the
// connection credentials is not allowed to access, so it doesn't
// necessarily mean something wrong with the platform.)
type ReadTxn interface {
	// Get returns the current state of the given observable.  It
	// is not an error if the observable does not exist; ptr.Null
	// is returned in that case
	Get(context.Context, string) (ptr.Ptr, error)

	// Load an object.  Loader is a function that can be obtained
	// as by sealed.ProtobufLoader, sealed.ExpectedTypeLoader, or
	// some other approach.
	Load(context.Context, ptr.Ptr, sealed.Loader) (interface{}, error)

	// LoadType just reads the type from given object; note that the type
	// is protected by encryption as well, so this still requires proper
	// credentials
	LoadType(context.Context, ptr.Ptr) (*ptr.Ptr, error)

	// Fill populates an object (protobuf) with the contents of
	// the object referred to by the pointer.
	Fill(context.Context, ptr.Ptr, proto.Message) error

	// Load an object.  Loader is a function that can be obtained
	// as by sealed.ProtobufLoader, sealed.ExpectedTypeLoader, or
	// some other approach.
	LoadUsingCreds(context.Context, ptr.Ptr, sealed.Loader, Credentials) (interface{}, error)

	// LoadType just reads the type from given object; note that the type
	// is protected by encryption as well, so this still requires proper
	// credentials
	LoadTypeUsingCreds(context.Context, ptr.Ptr, Credentials) (*ptr.Ptr, error)

	// Fill populates an object (protobuf) with the contents of
	// the object referred to by the pointer.
	FillUsingCreds(context.Context, ptr.Ptr, proto.Message, Credentials) error

	WaitForChanges(context.Context) ([]string, error)

	// Domain returns the domain that this transaction is operating on
	Domain() uint64
}

type VectorLoader interface {
	VectorLoad(ctx context.Context, addrs []ptr.Ptr) ([][]byte, error)
}

type WriteTxn interface {
	ReadTxn
	SetAudit(uint64)
	Alloc(context.Context, string) (string, error)
	Set(context.Context, string, ptr.Ptr) error
	Store(context.Context, interface{}) (ptr.Ptr, error)
	StoreUsingKey(context.Context, interface{}, *Key) (ptr.Ptr, error)
	Commit(context.Context) error
}

// ErrConflict is the error returned from Commit() when there is a MVCC conflict
type ErrConflict struct {
	foo []*state.Obs
}

func (err *ErrConflict) MessageID() string {
	return "FOO-2008"
}

func (err *ErrConflict) Error() string {
	return "MVCC confict"
}

type readtxn struct {
	domain *Domain
	lock   sync.Mutex
	mark   uint64
	deps   map[string]ptr.Ptr
	f      *follower
	driver ReadDriver
}

type AccessReporter interface {
	AccessReport() map[string]ptr.Ptr
}

func (tx *readtxn) AccessReport() map[string]ptr.Ptr {
	return tx.deps
}

/*func (tx *readtxn) Accessed() map[string]*state.Obs {
	return tx.deps
        }*/

func (tx *readtxn) Domain() uint64 {
	return tx.domain.id
}

type pendingWrite struct {
	start time.Time
	item  ptr.Ptr
	done  <-chan error
}

type writetxn struct {
	readtxn
	audit   uint64
	cancel  func()
	sets    map[string]ptr.Ptr
	pending []pendingWrite
}

func NewReadTxn(domain uint64, driver ReadDriver, creds Credentials) ReadTxn {
	return &readtxn{
		domain: &Domain{
			// NOTE there is no conn here.  this will prevent
			// upgrade to a writetxn
			id:    domain,
			creds: creds,
		},
		driver: driver,
	}
}

func (d *Domain) BeginRead() ReadTxn {
	return &readtxn{
		domain: d,
		driver: conndriver{d.conn},
	}
}

func (d *Domain) BeginWrite() WriteTxn {
	if d.id == 0 {
		panic("cannot BeginWrite on zero domain")
	}
	return &writetxn{
		readtxn: readtxn{
			domain: d,
			driver: conndriver{d.conn},
		},
	}
}

type Upgrader interface {
	Upgrade(domain uint64) WriteTxn
}

func Upgrade(txn ReadTxn, domain uint64) WriteTxn {
	if domain < 1 {
		panic(errlib.Newf("FOO-1640 must upgrade to a domain"))
	}

	if up, ok := txn.(Upgrader); ok {
		return up.Upgrade(domain)
	} else {
		panic(errlib.Newf("FOO-1644 don't know how to upgrade %T", txn))
	}
}

func (wr *writetxn) Upgrade(domain uint64) WriteTxn {
	if wr.domain.id != domain {
		panic(errlib.Newf("FOO-1641 cannot upgrade a different domain"))
	}
	return wr
}

func (rd *readtxn) Upgrade(domain uint64) WriteTxn {
	wr := &writetxn{
		readtxn: *rd,
	}
	if wr.readtxn.domain.id == 0 {
		wr.readtxn.domain.id = domain
	} else if wr.readtxn.domain.id != domain {
		panic(errlib.Newf("FOO-1642 cannot upgrade a different domain (was %d, asking for %d)",
			wr.readtxn.domain.id,
			domain))
	}
	return wr
}

func (d *Domain) View(ctx context.Context, fn func(context.Context, ReadTxn) error) error {
	t := &readtxn{
		domain: d,
		driver: conndriver{d.conn},
	}
	err := fn(ctx, t)
	if t.f != nil {
		t.f.cancel()
		t.f = nil // just in case??
	}
	return err
}

func (d *Domain) Update(ctx context.Context, fn func(context.Context, WriteTxn) error) error {
	if d.id == 0 {
		panic("cannot Update on zero domain")
	}
	t := &writetxn{
		readtxn: readtxn{
			domain: d,
			driver: conndriver{d.conn},
		},
	}
	ctx, t.cancel = context.WithCancel(ctx)
	defer t.cancel()

	err := fn(ctx, t)
	if err != nil {
		return err
	}

	if t.f != nil {
		t.f.cancel()
		t.f = nil
	}
	return t.Commit(ctx)
}

func (rd *readtxn) probe(ctx context.Context, name string) (ptr.Ptr, bool) {
	rd.lock.Lock()
	defer rd.lock.Unlock()

	if rd.deps == nil {
		rd.deps = make(map[string]ptr.Ptr)
	}
	p, ok := rd.deps[name]
	return p, ok
}

func (rd *readtxn) Get(ctx context.Context, name string) (ptr.Ptr, error) {
	if s, ok := rd.probe(ctx, name); ok {
		return s, nil
	}

	if rd.domain == nil {
		panic("no domain?")
	}
	if rd.driver == nil {
		panic("no driver!")
	}
	p, _, mark, err := rd.driver.Get(ctx, rd.domain.id, []string{name})
	if err != nil {
		return ptr.Null, err
	}

	rd.lock.Lock()
	defer rd.lock.Unlock()
	rd.deps[name] = p[0]

	if rd.mark == 0 {
		rd.mark = mark
	} else if mark < rd.mark {
		rd.mark = mark
	}

	if rd.f != nil {
		rd.f.including(name)
	}
	return p[0], nil
}

type conndriver struct {
	conn *Conn
}

func (cd conndriver) Read(ctx context.Context, addrs []ptr.Ptr) ([][]byte, error) {
	lr := &cam.LoadRequest{
		Refs: make([]*core.Ptr, len(addrs)),
	}
	for i, p := range addrs {
		lr.Refs[i] = core.From(p)
	}

	data, err := cd.conn.cams.Load(ctx, lr)
	if err != nil {
		return nil, err
	}

	result := make([][]byte, len(data.Data))

	for i, datum := range data.Data {
		result[i] = datum.Payload
	}
	return result, nil
}

func (cd conndriver) Get(ctx context.Context, domain uint64, names []string) ([]ptr.Ptr, []uint64, uint64, error) {
	result, err := cd.conn.states.Get(ctx, &state.GetRequest{
		Domain:   domain,
		Subjects: names,
	})
	if err != nil {
		return nil, nil, 0, err
	}

	n := len(names)
	ptrs := make([]ptr.Ptr, n)
	seqs := make([]uint64, n)
	for i, s := range result.States {
		if s.Name != names[i] {
			panic("misalignment")
		}
		if s.Value != nil {
			ptrs[i] = s.Value.Must()
		}
		seqs[i] = s.Seq
	}
	return ptrs, seqs, result.Mark, nil
}

func (wr *writetxn) Commit(ctx context.Context) error {
	t := wr
	if t.domain.id == 0 {
		panic("domain is not set??")
	}
	if len(t.pending) > 0 {
		log.Debugf(ctx, "waiting for %d writes to complete", len(t.pending))
		for _, pw := range t.pending {
			err := <-pw.done
			if err != nil {
				log.Debugf(ctx, "write of {%s} failed: %s", pw.item, err)
				return err
			}
			dt := time.Since(pw.start)
			log.Debugf(ctx, "write of {%s} done by %s", pw.item, dt.Round(time.Microsecond))
		}
	}

	if len(t.sets) > 0 {
		if t.audit == 0 {
			t.audit = t.domain.conn.allocAudit(ctx, t.domain.id)
		}
		txr := state.TxnRequest{
			Domain: t.domain.id,
			Audit:  t.audit,
		}

		index := make(map[string]int)

		for k, v := range t.deps {
			n := len(txr.Subjects)
			index[k] = n
			txr.Subjects = append(txr.Subjects, k)
			sv := &state.SlotValue{
				Slot: int32(n),
			}
			if !v.IsNull() {
				sv.Value = core.From(v)
			}
			txr.Preconds = append(txr.Preconds, sv)
		}
		for k, v := range t.sets {
			n, ok := index[k]
			if !ok {
				n = len(txr.Subjects)
				index[k] = n
				txr.Subjects = append(txr.Subjects, k)
			}
			sv := &state.SlotValue{
				Slot: int32(n),
			}
			if !v.IsNull() {
				sv.Value = core.From(v)
			}
			txr.Effects = append(txr.Effects, sv)
		}
		resp, err := wr.domain.conn.states.Apply(ctx, &txr)
		if err != nil {
			return err
		}

		if resp.Conflict {
			var buf bytes.Buffer
			for j, state := range resp.States {
				fmt.Fprintf(&buf, "\n[%d] %s %s", j, resp.Disposition[j], state.Short())
			}
			log.Warningf(ctx, "FOO-2007 conflicted:%s", buf.Bytes())
			return &ErrConflict{resp.States}
		}

		var buf bytes.Buffer
		for j, state := range resp.States {
			fmt.Fprintf(&buf, "\n[%d] %s %s", j, resp.Disposition[j], state.Short())
		}
		log.Debugf(ctx, "applied: D#%d T#%d (%s)%s",
			t.domain.id,
			resp.Id,
			resp.Time.AsTime().Format(time.RFC3339Nano),
			buf.Bytes())
		// push the changes into the deps index, so we will see
		// it as the new prereq if we commit again
		if t.deps == nil {
			t.deps = make(map[string]ptr.Ptr)
		}
		for k := range t.sets {
			delete(t.sets, k) // and clear the sets
		}
		for _, s := range resp.States {
			t.deps[s.Name] = s.Value.Must()
		}
		t.pending = t.pending[:0] // zero out the pending list too
	}
	return nil
}

func (wr *writetxn) Get(ctx context.Context, name string) (ptr.Ptr, error) {
	if v, ok := wr.sets[name]; ok {
		return v, nil
	}
	return wr.readtxn.Get(ctx, name)
}

func (wr *writetxn) SetAudit(a uint64) {
	wr.audit = a
}

func (wr *writetxn) Set(ctx context.Context, name string, value ptr.Ptr) error {
	if wr.sets == nil {
		wr.sets = make(map[string]ptr.Ptr)
	}
	wr.sets[name] = value
	return nil
}

type TypeIssuer interface {
	// resolves the appropriate type object; when storing, txn may
	// be a WriteTxn in which case can store data too
	IssueType(ctx context.Context, txn ReadTxn) (ptr.Ptr, error)
}

func TypeFor(ctx context.Context, txn ReadTxn, item interface{}) (*ptr.Ptr, error) {
	if ti, ok := item.(TypeIssuer); ok {
		it, err := ti.IssueType(ctx, txn)
		if err != nil {
			return nil, err
		}
		//log.Tracef(ctx, "type is {%s}", it)
		return &it, nil
	} else if pb, ok := item.(proto.Message); ok {
		n := string(proto.MessageName(pb))
		var it ptr.Ptr

		if len(n) < 30 {
			it = ptr.String(n)
		} else {
			it = ptr.HashAddress([]byte(n))
		}
		//log.Tracef(ctx, "type is %q -> {%s}", n, it)
		return &it, nil
	} else if _, ok := item.([]byte); ok {
		return nil, nil
	} else {
		panic(fmt.Sprintf("cannot discern a type for %T", item))
	}
}

func (wr *writetxn) StoreUsingKey(ctx context.Context, item interface{}, key *Key) (ptr.Ptr, error) {
	enc := sealed.NewSecureEncoding(key.Mode, key.ID, key.Cipher)

	t, err := TypeFor(ctx, wr, item)

	buf, err := enc.Encode(ctx, nil, t, item, nil)
	if err != nil {
		panic(err)
	}

	p := ptr.EncryptedContentAddress(buf)
	//log.Debugf(ctx, "--{%s}---\n%x", p, buf)

	pend := make(chan error, 1)

	go func() {
		defer close(pend)

		ret, err := wr.domain.conn.cams.Store(ctx, &cam.StoreRequest{
			Data: []*cam.Datum{
				&cam.Datum{
					Ref:     core.From(p),
					Payload: buf,
				},
			},
		})
		if err != nil {
			log.Debugf(ctx, "store failed: %s", err)
			pend <- err
			return
		}
		p2 := ret.Refs[0].Must()
		log.Debugf(ctx, "stored {%s} %d bytes", p2, len(buf))
		if p != p2 {
			log.Errorf(ctx, "FOO-4001 corrupted sent {%s} but got {%s}", p, p2)
			panic("corrupted")
		}
		pend <- nil
	}()

	wr.lock.Lock()
	defer wr.lock.Unlock()

	wr.pending = append(wr.pending, pendingWrite{
		start: time.Now(),
		item:  p,
		done:  pend,
	})
	return p, nil
}

func (rd *readtxn) FillUsingCreds(ctx context.Context, p ptr.Ptr, dst proto.Message, creds Credentials) error {
	t, err := TypeFor(ctx, rd, dst)
	if err != nil {
		return err
	}
	log.Debugf(ctx, "filling type %#v", t)

	ld := sealed.ExpectedTypeLoader(dst, *t)
	_, err = rd.LoadUsingCreds(ctx, p, ld, creds)
	return err
}

func (rd *readtxn) LoadTypeUsingCreds(ctx context.Context, p ptr.Ptr, creds Credentials) (*ptr.Ptr, error) {
	if !p.IsLoadable() {
		panic(fmt.Sprintf("not allowed to load {%s}", p))
	}
	log.Debugf(ctx, "loading {%s}", p)

	data, err := rd.driver.Read(ctx, []ptr.Ptr{p})
	if err != nil {
		return nil, err
	}
	payload := data[0]

	pre, err := sealed.ParsePreamble(ctx, payload)
	if err != nil {
		return nil, err
	}

	key, err := creds.GetKey(ctx, pre.KeyID)
	if err != nil {
		return nil, err
	}

	enc := sealed.NewSecureEncoding(key.Mode, key.ID, key.Cipher)

	return enc.DecodeType(ctx, payload)
}

func (rd *readtxn) VectorLoad(ctx context.Context, addrs []ptr.Ptr) ([][]byte, error) {
	return rd.driver.Read(ctx, addrs)
}

func (rd *readtxn) LoadUsingCreds(ctx context.Context, p ptr.Ptr, ld sealed.Loader, creds Credentials) (interface{}, error) {
	if !p.IsLoadable() {
		panic(fmt.Sprintf("not allowed to load {%s}", p))
	}
	log.Debugf(ctx, "loading {%s}", p)

	data, err := rd.driver.Read(ctx, []ptr.Ptr{p})
	if err != nil {
		log.Tracef(ctx, "load failed: %s", err)
		return nil, err
	}
	payload := data[0]
	return LoadFromBytes(ctx, creds, payload, ld)
}

func LoadFromBytes(ctx context.Context, creds Credentials, buf []byte, ld sealed.Loader) (interface{}, error) {
	pre, err := sealed.ParsePreamble(ctx, buf)
	if err != nil {
		log.Tracef(ctx, "could not parse preamble: %s\nfrom: %.12x..", err, buf)
		return nil, err
	}
	log.Tracef(ctx, "load preamble => key id %q", pre.KeyID)

	key, err := creds.GetKey(ctx, pre.KeyID)
	if err != nil {
		log.Tracef(ctx, "load failed on getkey: %s", err)
		return nil, err
	}

	enc := sealed.NewSecureEncoding(key.Mode, key.ID, key.Cipher)

	return enc.Decode(ctx, ld, buf)
}

func FillFromBytes(ctx context.Context, creds Credentials, src []byte, dst proto.Message) error {
	t, err := TypeFor(ctx, nil, dst) // should not need a txn for protobuf
	if err != nil {
		return err
	}

	ld := sealed.ExpectedTypeLoader(dst, *t)
	_, err = LoadFromBytes(ctx, creds, src, ld)
	return err
}

// these are convenience functions that use the *Domain's creds, which is the common
// case.  Sometimes a facility (like CDN) wants to use specialized credentials for accessing
// objects so it will use the XXXUsingCreds functions instead

func (rd *readtxn) Load(ctx context.Context, p ptr.Ptr, ld sealed.Loader) (interface{}, error) {
	return rd.LoadUsingCreds(ctx, p, ld, rd.domain.creds)
}

func (rd *readtxn) Fill(ctx context.Context, p ptr.Ptr, dst proto.Message) error {
	return rd.FillUsingCreds(ctx, p, dst, rd.domain.creds)
}

func (rd *readtxn) LoadType(ctx context.Context, p ptr.Ptr) (*ptr.Ptr, error) {
	return rd.LoadTypeUsingCreds(ctx, p, rd.domain.creds)
}

func (wr *writetxn) Store(ctx context.Context, item interface{}) (ptr.Ptr, error) {
	key, err := wr.domain.creds.UseKey(ctx)
	if err != nil {
		return ptr.Null, err
	}
	return wr.StoreUsingKey(ctx, item, key)
}

// Alloc finds a "/{id}" suffix for the given prefix that is not used;
// it works the same general way as domain-level Alloc() except that
// the allocation is part of the transaction.  That is, it uses a
// sequence observable.  The difference is that this allocation is
// transactional so that if the transaction does not commit, the
// sequence is not incremented. (Which means that concurrent
// allocations will conflict; for anything with moderate allocation
// rates, use domain-level allocation instead)
func (wr *writetxn) Alloc(ctx context.Context, prefix string) (string, error) {
	n, err := sequence.Next(ctx, wr, "seq/"+prefix, 100)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s/%d", prefix, n), nil
}

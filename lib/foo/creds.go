package foo

import (
	"context"
	"crypto/cipher"

	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/foo/lib/sealed"
)

type Key struct {
	ID     string
	Mode   sealed.CipherMode
	Cipher cipher.Block
}

// UseKey implements Credentials.  This way a single key
// can also serve as Credentials.
func (self *Key) UseKey(context.Context) (*Key, error) {
	return self, nil
}

// GetKey implements Credentials.  This way a single key
// can also serve as Credentials.
func (self *Key) GetKey(_ context.Context, kid string) (*Key, error) {
	if kid == self.ID {
		return self, nil
	}
	return nil, errlib.Newf("FOO-2110 key %q is not this one (this one is %s)", kid, self.ID)
}

type Credentials interface {
	UseKey(ctx context.Context) (*Key, error)
	GetKey(ctx context.Context, kid string) (*Key, error)
}

package foo

import (
	"context"

	"bitbucket.org/fufoo/foo/services/state"
	"bitbucket.org/fufoo/qrpc"
)

type Domain struct {
	conn  *Conn
	id    uint64
	creds Credentials
}

func (d *Domain) ID() uint64 {
	return d.id
}

func (d *Domain) Alloc(ctx context.Context, name string) (uint64, error) {
	sk := seqKey{
		domain: d.id,
		name:   name,
	}
	seq := d.conn.getSeq(ctx, sk)
	return seq.Next(ctx), nil
}

// TODO consider moving this onto ReadTxn
func (d *Domain) Scan(ctx context.Context, globs []string) <-chan *state.Obs {

	ch := make(chan *state.Obs, 10)

	resp, err := d.conn.states.Scan(ctx, &state.GetRequest{
		Domain:   d.id,
		Subjects: globs,
	})
	if err != nil {
		log.Warningf(ctx, "FOO-3017 could not scan: %s", err)
		close(ch)
	} else {
		go func() {
			defer close(ch)

			for {
				rec, err := resp.Recv()
				if err != nil {
					if !qrpc.IsEOF(err) {
						log.Warningf(ctx, "FOO-3018 scan ended: %s", err)
					}
					return
				}
				ch <- rec
			}
		}()
	}

	return ch
}

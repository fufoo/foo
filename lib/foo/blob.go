package foo

import (
	"bufio"
	"context"
	"io"

	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/sealed"
)

type TypedBlob struct {
	Type ptr.Ptr
	Data []byte
}

func (m *TypedBlob) MarshalCAM(typePtr *ptr.Ptr) (sealed.Marshaled, error) {
	return sealed.Marshaled{
		Payload: m.Data,
		Flags:   sealed.DeflateCompression,
	}, nil
}

func (m *TypedBlob) IssueType(context.Context, ReadTxn) (ptr.Ptr, error) {
	return m.Type, nil
}

func (m *TypedBlob) Loader(ctx context.Context, tp *ptr.Ptr, index []ptr.Ptr, src *bufio.Reader) (interface{}, error) {
	if tp == nil || *tp != m.Type {
		return nil, sealed.ErrUnexpectedMessage
	}
	buf, err := io.ReadAll(src)
	if err != nil {
		return nil, err
	}
	m.Data = buf
	return m.Data, nil
}

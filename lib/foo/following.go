package foo

import (
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/services/state"
	"context"
	"errors"
)

func (rd *readtxn) getfollower() *follower {
	rd.lock.Lock()
	defer rd.lock.Unlock()

	if rd.deps == nil {
		rd.deps = make(map[string]ptr.Ptr)
	}

	if rd.f != nil {
		return rd.f
	}

	ops := make(chan followerop, 10)

	rd.f = &follower{
		owner:    rd,
		ops:      ops,
		watching: keylist[string, ptr.Ptr](rd.deps), // by default we watch everything
		mark:     rd.mark,
	}
	log.Debugf(context.TODO(), "%p follower starting up", rd.f)
	go rd.f.run(ops)
	return rd.f
}

func (f *follower) cancel() {
	log.Debugf(context.TODO(), "%p follower is being canceled", f)
	f.ops <- exitFollower{}
	close(f.ops)
}

type follower struct {
	owner    *readtxn
	ops      chan<- followerop
	mon      chan monitorop
	pending  map[string]*state.Obs // staging area until caller syncs w/WaitForChanges
	watching []string
	waiters  []*waitForChangeOp
	canceler func()
	mark     uint64
}

type monitorop interface {
	frommonitor(*follower)
}

type followerop interface {
	apply(*follower) bool
}

func (f *follower) run(ops <-chan followerop) {
	defer func() {
		log.Debugf(context.TODO(), "%p follower is done", f)
		for x := range ops {
			log.Debugf(context.TODO(), "discard %T", x)
		}
		log.Debugf(context.TODO(), "follower ops channel drained")
	}()

	for {
		if f.mon == nil {
			// monitor is not running
			op := <-ops
			if !op.apply(f) {
				return
			}
		} else {
			// monitor is running

			select {
			case op := <-ops:
				if !op.apply(f) {
					return
				}
			case fm, ok := <-f.mon:
				if ok {
					fm.frommonitor(f)
				} else {
					log.Debugf(context.TODO(), "monitor has closed")
					f.mon = nil
				}
			}
		}
	}
}

type exitFollower struct {
}

func (exitFollower) apply(f *follower) bool {
	if f.canceler != nil {
		f.canceler()
		for x := range f.mon {
			log.Debugf(context.TODO(), "discard %T", x)
		}
		log.Debugf(context.TODO(), "monitor channel drained")
		f.mon = nil
	}
	// this is the only op that returns false
	return false
}

type waitForChangeOp struct {
	err    chan error
	result map[string]*state.Obs
}

func (rd *readtxn) WaitForChanges(ctx context.Context) ([]string, error) {
	f := rd.getfollower()
	op := &waitForChangeOp{
		err: make(chan error, 1),
	}

	f.ops <- op

	select {
	case <-ctx.Done():
		log.Debugf(ctx, "WaitForChanges context canceled!")
		f.ops <- cancelWait{op}
		return nil, context.Canceled
	case err := <-op.err:
		if err != nil {
			return nil, err
		}
		rd.lock.Lock()
		defer rd.lock.Unlock()

		var lst []string

		for k, v := range op.result {
			lst = append(lst, k)
			rd.deps[k] = v.Value.Must()
		}
		return lst, err
	}
}

type cancelWait struct {
	wfc *waitForChangeOp
}

func (cw cancelWait) apply(f *follower) bool {
	// if the monitor is running, cancel it
	if f.canceler != nil {
		f.canceler()
		f.canceler = nil
	}
	return true
}

func (op *waitForChangeOp) apply(f *follower) bool {
	if len(f.pending) == 0 {
		// nothing is pending, go ahead and just wait
		f.waiters = append(f.waiters, op)
		if f.canceler == nil {
			f.startmonitor()
		}
		return true
	}
	// transfer the map to the caller
	op.result = f.pending
	f.pending = nil
	op.err <- nil
	return true
}

func (ms monitorStopped) frommonitor(f *follower) {
	if ms.err != nil {
		log.Tracef(context.Background(), "monitor stopped: %s", ms.err)
	} else {
		log.Tracef(context.Background(), "monitor stopped normally")
	}
	f.canceler = nil

	if len(f.waiters) > 0 {
		// there are still waiters... need to restart the monitor, then
		f.startmonitor()
	}
}

func (f *follower) startmonitor() {
	ctx, cancel := context.WithCancel(context.Background())
	f.canceler = cancel
	f.mon = make(chan monitorop)

	go f.monitor(ctx, f.watching, f.mark, f.mon)
}

type monitorStopped struct {
	err error
}

type changebatch struct {
	rec *state.TxnRecord
}

func (c changebatch) frommonitor(f *follower) {

	if f.pending == nil {
		f.pending = make(map[string]*state.Obs)
	}
	rec := c.rec
	ctx := context.Background()

	f.mark = rec.NextMark
	log.Tracef(ctx, "-- mark [%d] --", rec.Mark)
	for i, s := range rec.States {
		log.Tracef(ctx, "[%d] %s %s", i, rec.Disposition[i], s.Short())
		if s.Value == nil {
			f.pending[s.Name] = nil
		} else {
			f.pending[s.Name] = s
		}
	}
	log.Tracef(ctx, "%d pending changes and %d waiters", len(f.pending), len(f.waiters))
	log.Tracef(ctx, "next mark %d", rec.NextMark)

	if len(f.waiters) > 0 {
		for _, w := range f.waiters {
			w.result = f.pending
			w.err <- nil
		}
		f.waiters = f.waiters[:0]
		f.pending = nil // we transferred this to them
	}
}

type addwatching struct {
	name string
}

var ErrChanged = errors.New("watch list changed")

func (a addwatching) apply(f *follower) bool {
	f.watching = append(f.watching, a.name)

	if len(f.waiters) > 0 {

		// this doesn't really count as a change (should it?)
		for _, w := range f.waiters {
			w.err <- ErrChanged
		}
		f.waiters = f.waiters[:0]
	}

	// stop the existing monitor for sure, because we need to
	// start a new one with the new obs
	if f.canceler != nil {
		f.canceler()
		f.canceler = nil
	}
	return true
}

func (f *follower) including(name string) {
	f.ops <- addwatching{name}
}

func (f *follower) monitor(ctx context.Context, args []string, mark uint64, mch chan<- monitorop) {
	defer close(mch)
	mch <- monitorStopped{f.runmonitor(ctx, args, mark, mch)}
}

func (f *follower) runmonitor(ctx context.Context, args []string, mark uint64, mch chan<- monitorop) error {
	log.Tracef(ctx, "monitor is running for %q after mark=%d", args, mark)
	req := &state.FollowRequest{
		Domain:    f.owner.domain.id,
		Subjects:  args,
		AfterMark: mark,
	}

	resp, err := f.owner.domain.conn.states.Follow(ctx, req)
	if err != nil {
		log.Debugf(ctx, "Follow failed: %s", err)
		return err
	}

	for {
		rec, err := resp.Recv()
		if err != nil {
			log.Debugf(ctx, "Recv failed: %s", err)
			return err
		}
		log.Debugf(ctx, "rx TX#%d", rec.Id)
		mch <- changebatch{rec}
	}

}

func keylist[K comparable, V any](m map[K]V) []K {
	lst := make([]K, len(m))
	i := 0
	for k := range m {
		lst[i] = k
		i++
	}
	return lst
}

/*
	cancelfollow func()
	nochanges    *sync.Cond
	followerror  error
*/

/*
func (rd *readtxn) WaitForChanges(ctx context.Context) ([]string, error) {

	if len(rd.deps) == 0 {
		return nil, io.EOF
	}

	var result []string

	rd.lock.Lock()
	defer rd.lock.Unlock()

	if rd.nochanges == nil {
		rd.nochanges = sync.NewCond(&rd.lock)
		rd.changes = make(map[string]ptr.Ptr)
	}

	if rd.cancelfollow == nil {
		// no follower started yet... spin one up
		ctx2, cancel := context.WithCancel(ctx)
		rd.cancelfollow = cancel
		rd.followerror = nil
		go rd.follow(ctx2, rd.mark, keylist(rd.deps))
	}

	for rd.followerror == nil && len(rd.changes) == 0 {
		log.Tracef(ctx, "no changes... waiting")
		rd.nochanges.Wait()
		log.Tracef(ctx, "checking now")
	}
	if rd.followerror != nil {
		return nil, rd.followerror
	}
	for k, v := range rd.changes {
		result = append(result, k)
		rd.deps[k] = v
		delete(rd.changes, k)
	}
	return result, nil
}

func (rd *readtxn) follow(ctx context.Context, mark uint64, args []string) {

	defer func() {
		rd.lock.Lock()
		rd.cancelfollow = nil
		rd.lock.Unlock()
	}()

reconnloop:
	for {
		req := &state.FollowRequest{
			Domain:    rd.domain.id,
			Subjects:  args,
			AfterMark: mark,
		}
		resp, err := rd.domain.conn.states.Follow(ctx, req)
		if err != nil {
			log.Warningf(ctx, "follow failed: %s", err)
			rd.lock.Lock()
			rd.followerror = err
			rd.nochanges.Broadcast()
			rd.lock.Unlock()
			return
		}

		for {
			rec, err := resp.Recv()
			if err != nil {
				if qrpc.IsCanceled(err) {
					log.Debugf(ctx, "follow canceled")
					rd.lock.Lock()
					rd.followerror = context.Canceled
					rd.nochanges.Broadcast()
					rd.lock.Unlock()
					return
				}
				log.Debugf(ctx, "follow failed, restarting: %s", err)
				continue reconnloop
			}
			rd.lock.Lock()
			mark = rec.NextMark
			rd.mark = rec.NextMark
			for i, s := range rec.States {
				log.Tracef(ctx, "[%d] %s", i, s.Short())
				if s.Value == nil {
					rd.changes[s.Name] = ptr.Null
				} else {
					rd.changes[s.Name] = s.Value.Must()
				}
			}
			log.Tracef(ctx, "%d pending changes", len(rd.changes))
			rd.nochanges.Signal()
			rd.lock.Unlock()
			log.Tracef(ctx, "next mark %d", rec.NextMark)
			mark = rec.NextMark
		}
	}
}
*/

package camasset

import (
	"bufio"
	"context"
	"io"
	"os"

	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/services/cam"
)

type assetFile struct {
	posn   int64
	fragix int
	infrag int
	asset  cam.Asset
	rd     foo.ReadTxn
	loaded []byte
}

func (f *assetFile) Seek(offset int64, whence int) (int64, error) {
	var at int64
	switch whence {
	case 0:
		at = offset
	case 1:
		at = f.posn + offset
	case 2:
		var total int64
		for _, frag := range f.asset.Frags {
			total += int64(frag.Size)
		}

		at = total + offset
	}
	if at < 0 {
		return 0, os.ErrInvalid
	}
	f.posn = at

	// find the indicated frag
	ctx := context.TODO()
	log.Tracef(ctx, "Seek to %d", at)

	i := 0
	for at > int64(f.asset.Frags[i].Size) {
		at -= int64(f.asset.Frags[i].Size)
		i++
	}
	if i != f.fragix {
		f.fragix = i
		f.loaded = nil // clear the loaded datax
	}

	f.infrag = int(at)
	log.Tracef(ctx, "  frag[%d] offset +%d", f.fragix, f.infrag)

	return f.posn, nil
}

func (f *assetFile) Read(dst []byte) (n int, err error) {
	ctx := context.TODO()
	log.Tracef(ctx, "Read offset=%d - want %d bytes", f.posn, len(dst))

	total := 0
	remain := len(dst)
	for remain > 0 {
		log.Tracef(ctx, "  read offset=%d in frag[%d] - %d bytes remain",
			f.posn,
			f.fragix,
			remain,
		)

		// which frag are we on?
		frag := f.asset.Frags[f.fragix]

		// read what we want out of it

		amount := remain
		thisSize := int(frag.Size)
		if f.infrag+amount > thisSize {
			amount = thisSize - f.infrag
		}
		if amount == 0 {
			log.Tracef(ctx, "  nothing left, looping")
			f.fragix++ // advance to the next frag
			f.infrag = 0
			f.loaded = nil // drop the buffered data
			continue
		}

		// load the data if needed
		if f.loaded == nil {
			f.loaded = frag.Inline

			/*
				if addr := frag.Addr; addr != nil {
					buf, err := f.rd.Load(ctx, addr.Must(), bytesLoader)
					if err != nil {
						panic(err)
						return 0, err
					}
					f.loaded = buf.([]byte)
				}
			*/
			if f.loaded == nil {
				panic("neither inline nor addr set")
			}
			log.Tracef(ctx, "  loaded %d bytes", len(f.loaded))
			if len(f.loaded) == 0 {
				panic("nothing loaded")
			}
		}

		n := copy(dst, f.loaded[f.infrag:])
		log.Tracef(ctx, "  copied %d bytes from frag[%d][%d:]",
			n,
			f.fragix,
			f.infrag,
		)
		if n == 0 {
			panic("copied nothing")
		}

		f.infrag += n
		f.posn += int64(n)
		remain -= n
		total += n
		dst = dst[n:]

	}
	return total, nil
}

func bytesLoader(ctx context.Context, tp *ptr.Ptr, ix []ptr.Ptr, src *bufio.Reader) (interface{}, error) {
	buf, err := io.ReadAll(src)
	if err != nil {
		panic(err)
		return nil, err
	}
	log.Debugf(ctx, "loaded %d bytes", len(buf))
	return buf, nil
}

package camasset

import (
	"context"
	"net/http"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/errors"
	"bitbucket.org/fufoo/foo/lib/foo"
)

var log = logging.New("asset")

func Serve(w http.ResponseWriter, r *http.Request, conn *foo.Conn, creds foo.Credentials, p ptr.Ptr) {
	ctx := r.Context()

	d := conn.Access(1, creds)

	//found := false
	file := &assetFile{}

	err := d.View(ctx, func(ctx context.Context, rd foo.ReadTxn) error {
		file.rd = rd
		return rd.Fill(ctx, p, &file.asset)
		/*		err :=
				if err != nil {
					if errors.IsNoSuchObject(err) {
						// no error, just don't set found to true
						return nil
					}
					log.Warningf(ctx, "rats %T: %s", err, err)
					return err
				}
				found = true
				return nil*/
	})

	if err != nil {
		if errors.IsNoSuchObject(err) {
			w.WriteHeader(http.StatusNotFound)
		} else {
			log.Warningf(ctx, "internal error: %s", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	h := w.Header()

	h.Set("Cache-Control", "public,max-age=31536000,immutable")
	h.Set("Content-Type", file.asset.Mime)
	/*
		if r.Header.Get("Range") == "" {
			var total uint64
			for _, frag := range file.asset.Frags {
				total += uint64(frag.Size)
			}
			h.Set("Content-Length", strconv.FormatUint(total, 10))
		}
	*/
	http.ServeContent(w, r, "", file.asset.ModTime.AsTime(), file)
}

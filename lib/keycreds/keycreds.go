// package keycreds provides a simple method to produce keys based
// on a secret value
package keycreds

import (
	"crypto/aes"
	"crypto/sha256"

	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/lib/sealed"
	"math/big"
)

func KeyFromString(key string) *foo.Key {
	if key == "" {
		panic("encoding key is not set!")
	}
	var kidhash = sha256.Sum224([]byte("KEYID=" + key))

	var kid big.Int
	kid.SetBytes(kidhash[:14])

	var secretKey = sha256.Sum256([]byte(key))

	crypt, err := aes.NewCipher(secretKey[:])
	if err != nil {
		panic(err)
	}

	return &foo.Key{
		ID:     "foo:key:" + kid.Text(62),
		Mode:   sealed.CipherAES256CFB,
		Cipher: crypt,
	}
}

func EncodingFromKey(key string) sealed.Encoding {
	if key == "" {
		panic("encoding key is not set!")
	}
	var kidhash = sha256.Sum224([]byte("KEYID=" + key))

	var kid big.Int
	kid.SetBytes(kidhash[:14])

	var secretKey = sha256.Sum256([]byte(key))

	crypt, err := aes.NewCipher(secretKey[:])
	if err != nil {
		panic(err)
	}

	return sealed.NewSecureEncoding(
		sealed.CipherAES256CFB,
		"play-storage/"+kid.Text(62),
		crypt)
}

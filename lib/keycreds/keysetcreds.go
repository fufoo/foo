package keycreds

import (
	"context"
	"crypto/aes"

	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/lib/sealed"
	"bitbucket.org/fufoo/foo/services/cam"
)

type keysetCreds struct {
	origin ptr.Ptr
	index  map[string]*foo.Key
	use    *foo.Key
}

func KeySetCreds(from ptr.Ptr, src *cam.KeySet) foo.Credentials {
	// eagerly unpack them

	index := make(map[string]*foo.Key)

	var first *foo.Key

	for _, k := range src.Keys {
		if _, ok := index[k.Id]; ok {
			panic("duplicate key id")
		}

		var mode sealed.CipherMode
		switch k.Mode {
		case cam.CipherMode_CipherAES128CBC:
			mode = sealed.CipherAES128CBC
			if len(k.Secret) != 16 {
				panic("wrong secret length")
			}
		case cam.CipherMode_CipherAES192CBC:
			mode = sealed.CipherAES192CBC
			if len(k.Secret) != 24 {
				panic("wrong secret length")
			}
		case cam.CipherMode_CipherAES256CBC:
			mode = sealed.CipherAES256CBC
			if len(k.Secret) != 32 {
				panic("wrong secret length")
			}
		default:
			panic("unsupported cipher mode")
		}

		crypt, err := aes.NewCipher(k.Secret)
		if err != nil {
			panic(err)
		}

		key := &foo.Key{
			ID:     k.Id,
			Mode:   mode,
			Cipher: crypt,
		}
		index[k.Id] = key
		if first == nil {
			first = key
		}
	}
	return &keysetCreds{
		origin: from,
		index:  index,
		use:    first,
	}
}

func (ks *keysetCreds) GetKey(ctx context.Context, kid string) (*foo.Key, error) {
	if key, ok := ks.index[kid]; ok {
		return key, nil
	}
	return nil, errlib.Newf("FOO-1650 key %q not found in {%s}", kid, ks.origin)
}

func (ks *keysetCreds) UseKey(ctx context.Context) (*foo.Key, error) {
	if ks.use == nil {
		return nil, errlib.Newf("FOO-1651 no keys to use in {%s}", ks.origin)
	}
	return ks.use, nil
}

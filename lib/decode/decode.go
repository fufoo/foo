package decode

import (
	"context"

	//"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/services/cam"
	//"bitbucket.org/fufoo/foo/lib/api"
	//"bitbucket.org/fufoo/foo/lib/datatype"
)

func DumpObject(ctx context.Context, cam cam.StorageClient, src []byte, t ptr.Ptr) ([]byte, error) {
	panic("TODO")
	/*
		dt, err := datatype.TypeFor(ctx, cam, t)
		if err != nil {
			return nil, errlib.Wrap("FOL-4431", err)
		}

		if dumper, ok := dt.(ObjectDumper); ok {
			return dumper.DumpObject(src)
		}

		if t.IsLiteral() {
			return nil, errlib.Newf("FOL-4432 don't know how to dump object with type %s ; %s",
				t, t.LiteralString())
		} else {
			return nil, errlib.Newf("FOL-4433 don't know how to dump object with type %s", t)
		}*/
}

type ObjectDumper interface {
	DumpObject(src []byte) ([]byte, error)
}

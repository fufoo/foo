// The obscache package provides a facility for loading a fairly
// homogeneous set of observable objects, keeping them in cache, and
// watching for changes.
package obscache

import (
	"container/list"
	"context"
	"errors"
	"io"
	"regexp"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/followfilter"
	"bitbucket.org/fufoo/foo/services/cam"
	"bitbucket.org/fufoo/foo/services/core"
	"bitbucket.org/fufoo/foo/services/state"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

var log = logging.New("obscache")

type key struct {
	domain  uint64
	subject string
}

type Loader interface {
	Load(context.Context, *cam.Datum) (interface{}, error)
}

// TODO if we store the currently loaded-ptr and latest-ptr ptr.Ptr's
// in here, we can solve two problems:
//  1. when we get an update, we don't have to throw away the loadedItem
//  2. if an update arrives while a load is in flight (i.e., if
//     there are waiters), then it'll work correctly *including*
//     handling the race condition (i.e., regardless of whether the
//     in-flight load picks up the new value or the old value).
//     When retrieving an item, we simply compare the loaded-ptr
//     with the latest-ptr and if they differ, initiate the
//     second-half of the load (i.e., we don't need the Get, only
//     the Load+Unmarshal)
type loadedItem struct {
	first   time.Time
	update  time.Time
	item    interface{}
	posn    *list.Element
	waiters []*loadOp
}

// loadError is a special type of returnable result; it allows us
// to return a single interface instead of a (datum, error) tuple

type loadError struct {
	err error
}

type cancelOp struct {
	*loadOp
}

type updateOp struct {
	ctx    context.Context
	domain uint64
	sub    string
	val    ptr.Ptr
}

type loadOp struct {
	ctx  context.Context
	what key
	ret  chan<- interface{}
}

type finishedOp struct {
	ctx    context.Context
	what   key
	entry  *loadedItem
	result interface{}
	mark   uint64
}

type op interface {
	perform(*Cache)
}

type watcher struct {
	domain uint64
}

// TODO we need to limit the cache size by throwing away old entries
type Cache struct {
	states   state.StateClient
	cam      cam.StorageClient
	cache    map[key]*loadedItem
	loader   Loader
	lru      *list.List
	cmds     chan<- op
	watchers map[uint64]*watcher
	glob     string
	globre   *regexp.Regexp
}

func New(states state.StateClient, cam cam.StorageClient, pattern string, ld Loader) *Cache {
	cmds := make(chan op, 100)

	ctx := context.TODO()
	re, err := followfilter.CompileGlobs(ctx, []string{pattern})
	if err != nil {
		panic(err)
	}

	c := &Cache{
		states:   states,
		cam:      cam,
		cache:    make(map[key]*loadedItem),
		lru:      list.New(),
		cmds:     cmds,
		loader:   ld,
		watchers: make(map[uint64]*watcher),
		glob:     pattern,
		globre:   re,
	}
	go c.run(cmds)
	return c
}

func (w *watcher) watch(c *Cache, after uint64) {
	retries := 0

	for {
		var n int
		n, after = w.watch1(c, after)
		if n == 0 {
			// nothing was accomplished; this is a retry
			retries++
			if retries > 10 {
				retries = 10
			}
			time.Sleep((10 << retries) * time.Millisecond)
		} else {
			retries = 0
		}
	}
}

func (w *watcher) watch1(c *Cache, after uint64) (int, uint64) {

	ctx := context.Background()
	fr := &state.FollowRequest{
		Domain:    w.domain,
		Subjects:  []string{c.glob},
		AllowGlob: true,
		AfterMark: after,
	}
	txrs, err := c.states.Follow(ctx, fr)
	if err != nil {
		log.Warningf(ctx, "FOA-8801 follow failed: %s", err)
		return 0, after
	}

	n := 0
	for {
		txr, err := txrs.Recv()
		if err != nil {
			if errors.Is(err, io.EOF) {
				log.Tracef(ctx, "end of follow stream")
				return n, after
			} else if grpc.Code(err) == codes.DeadlineExceeded {
				log.Tracef(ctx, "follow stream timed out")
				return n, after
			} else {
				log.Warningf(ctx, "FOA-8806 follow problem: %s", err)
				return n, after
			}
		}

		n++
		log.Tracef(ctx, "follow audit=%d mark=%d",
			txr.Request.Audit,
			txr.NextMark)
		if txr.NextMark != 0 {
			after = txr.NextMark
		}

		// process the contents
		for _, state := range txr.States {
			if c.globre.MatchString(state.Name) {
				log.Tracef(ctx, "FOUND %q", state.Name)
				c.cmds <- &updateOp{
					ctx:    ctx,
					domain: w.domain,
					sub:    state.Name,
					val:    state.Value.Must(),
				}
			}
		}
	}
}

func (c *Cache) run(cmds <-chan op) {
	for cmd := range cmds {
		cmd.perform(c)
	}
}

func (c *Cache) Get(ctx context.Context, domain uint64, subject string) (interface{}, error) {
	ret := make(chan interface{})

	ldop := &loadOp{
		ctx: ctx,
		what: key{
			domain:  domain,
			subject: subject,
		},
		ret: ret,
	}
	c.cmds <- ldop

	select {
	case r := <-ret:
		if lderr, ok := r.(loadError); ok {
			return nil, lderr.err
		} else {
			return r, nil
		}
	case <-ctx.Done():
		c.cmds <- cancelOp{ldop}
		return nil, context.Canceled
	}
}

func (up *updateOp) perform(c *Cache) {
	ctx := up.ctx
	log.Tracef(ctx, "updating %d<%s> to %s", up.domain, up.sub, up.val)

	// actually, just eject it from the cache, at the cost of some
	// latency next time someone asks for it.  But we don't know a
	// priori that we need to load the current value
	k := key{
		domain:  up.domain,
		subject: up.sub,
	}
	if entry, ok := c.cache[k]; ok {
		if entry.waiters != nil {
			// unless there are waiters...  choices here:
			// 1. cancel the outstanding load
			//    and start a new one (tricky!)
			// 2. go ahead and delete the entry; the existing
			//    load should finish and might not crash
			// 3. ... something else?
			//
			// we don't want to just ignore it, otherwise
			// not only will the current pending load return
			// stale data, but FUTURE ones will as well because
			// we'll have effectively lost this update
			log.Warningf(ctx, "FOA-8813 existing waiters")
		}
		delete(c.cache, k)
		c.lru.Remove(entry.posn)
		entry.posn = nil
	}
}

func (fin *finishedOp) perform(c *Cache) {
	ctx := fin.ctx
	entry := fin.entry
	log.Tracef(ctx, "finished; %d waiters", len(entry.waiters))

	for _, w := range entry.waiters {
		w.ret <- fin.result
	}

	if entry.posn == nil {
		// this happens when the entry gets retired
		// (currently, only by updateOp) while there are
		// waiters (i.e., we are expecting a finishedOp).
		// Here, the finishedOp is executing after the
		// updateOp
		log.Tracef(ctx, "cache entry retired (updated) while in flight")
		return
	}

	// in case of error, don't leave it in the cache
	if _, ok := fin.result.(loadError); ok {
		delete(c.cache, fin.what)
		c.lru.Remove(entry.posn)
	} else {
		entry.item = fin.result
		entry.waiters = nil
		entry.first = time.Now()
		entry.update = entry.first

		// see if this is the first one in the domain, in which
		// case we need to start watching
		_, ok := c.watchers[fin.what.domain]
		if !ok {
			log.Tracef(ctx, "starting to watch domain %d after mark %d",
				fin.what.domain, fin.mark)
			w := &watcher{
				domain: fin.what.domain,
			}
			c.watchers[fin.what.domain] = w
			go w.watch(c, fin.mark)
		}
	}
}

func (ld *loadOp) perform(c *Cache) {
	ctx := ld.ctx

	if entry, ok := c.cache[ld.what]; ok {
		if entry.waiters != nil {
			log.Tracef(ctx, "request is in flight")
			entry.waiters = append(entry.waiters, ld)
		} else {
			log.Tracef(ctx, "hit in cache, loaded %s ago updated %s ago",
				time.Since(entry.first).Round(time.Millisecond),
				time.Since(entry.update).Round(time.Millisecond))
			ld.ret <- entry.item
			c.lru.MoveToFront(entry.posn)
		}
		return
	}
	entry := &loadedItem{
		waiters: []*loadOp{ld},
	}
	entry.posn = c.lru.PushFront(entry)
	c.cache[ld.what] = entry

	go entry.process(ld.what, c)
}

func (entry *loadedItem) process(what key, c *Cache) {

	// note we don't want to borrow the context from the loadOp
	// that first initiates the process load.  That's because if
	// multiple requests get stacked up and the first request gets
	// canceled, but a second request is not canceled, we will
	// wind up canceling the whole operation.  Instead, we'll just
	// run the request to completion even if all requests are
	// canceled.  In the future, we can look at canceling this
	// process if ALL requests get canceled.
	ctx := context.Background()

	failure := func(err error) {
		c.cmds <- &finishedOp{
			ctx:    ctx,
			what:   what,
			entry:  entry,
			result: loadError{err},
		}
	}

	success := func(item interface{}, mark uint64) {
		c.cmds <- &finishedOp{
			ctx:    ctx,
			what:   what,
			entry:  entry,
			result: item,
			mark:   mark,
		}
	}

	// TODO we can consider a GraphQL-style dataloader pattern
	// here... if we wait a few milliseconds and see if other Get
	// requests arrive, we could use the (already) vectorized Get
	// api and save some overhead

	req := &state.GetRequest{
		Domain:   what.domain,
		Subjects: []string{what.subject},
	}
	resp, err := c.states.Get(ctx, req)
	if err != nil {
		failure(errlib.Wrap("FOA-8833", err))
		return
	}
	if resp.States[0].Value == nil {
		// TODO is this OK?  As long as the cache gets updated
		// if a first value comes in, this should be good.  Since we're
		// watching at the glob level, we are fine.
		success(nil, resp.Mark)
		return
	}

	log.Tracef(ctx, "READ: %s", resp.States[0].Short())

	ld := &cam.LoadRequest{
		Refs: []*core.Ptr{resp.States[0].Value},
	}
	datum, err := c.cam.Load(ctx, ld)
	if err != nil {
		failure(errlib.Wrap("FOA-8834", err))
		return
	}
	item, err := c.loader.Load(ctx, datum.Data[0])
	if err != nil {
		failure(err)
	} else {
		success(item, resp.Mark)
	}
}

/*
	tc.sublock.Lock()
	defer tc.sublock.Unlock()

	ls, ok := tc.subcache[iid]
	if ok {
		if time.Until(ls.expires) > 0 {
			return ls.subject, nil
		}
		delete(tc.subcache, iid)
	}

	// TODO, if we were running these operations concurrently,
	// this would be a good place for the data loader pattern
	// since GetRequest is vectorizable
	req := &api.GetRequest{
		Domain: 1,
		Subjects: []string{
			fmt.Sprintf("subject/%d", iid),
		},
	}
	resp, err := tc.states.Get(ctx, req)
	if err != nil {
		return nil, errlib.Wrap("FOA-8833", err)
	}
	log.Tracef(ctx, "READ: %s", resp.States[0].Short())

	datum, err := tc.cam.Load(ctx, resp.States[0].Value)
	if err != nil {
		return nil, errlib.Wrap("FOA-8834", err)
	}
	pt, err := api.PtrFromProto(datum.Type)
	if err != nil {
		return nil, errlib.Wrap("FOA-8835", err)
	}
	if pt != ptr.String("foo:Subject/1") {
		return nil, errlib.Newf("FOA-8836 subject object has invalid type %s", pt)
	}

	ls = loadedSubject{
		subject: new(auth.Subject),
		expires: time.Now().Add(time.Minute),
	}

	err = proto.Unmarshal(datum.Payload, ls.subject)
	if err != nil {
		return nil, errlib.Wrap("FOA-8837", err)
	}

	tc.subcache[iid] = ls
	return ls.subject, nil
*/

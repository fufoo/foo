package inmem

import (
	"context"
	"crypto/sha256"
	"sync"
	"time"

	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/errors"
	"bitbucket.org/fufoo/foo/services/cam"
	"bitbucket.org/fufoo/foo/services/core"
)

type datum struct {
	domain uint64
	stored time.Time
	data   []byte
}

type Storage struct {
	lock  sync.Mutex
	index map[ptr.Ptr]datum
}

func NewStorage(backing Backing) *Storage {
	return &Storage{
		index: make(map[ptr.Ptr]datum),
	}
}

func (s *Storage) Client() cam.StorageQClient {
	return storageClient{s}
}

func (s *Storage) Server() cam.StorageQServer {
	panic("TODO")
	//return storageServer{s}
}

type storageClient struct {
	base *Storage
}

// Load implements cam.StorageClient
func (c storageClient) Load(ctx context.Context, in *cam.LoadRequest) (*cam.LoadResponse, error) {
	c.base.lock.Lock()
	defer c.base.lock.Unlock()

	out := &cam.LoadResponse{}
	for _, req := range in.Refs {
		p := req.Must()

		d, ok := c.base.index[p]
		if !ok {
			return nil, errors.ErrNoSuchObject{&p}
		}
		cp := make([]byte, len(d.data))
		copy(cp, d.data)
		log.Debugf(ctx, "load {%s} => %d bytes", p, len(cp))
		item := &cam.Datum{
			Ref:     core.From(p),
			Payload: cp,
		}
		out.Data = append(out.Data, item)
	}
	return out, nil
}

func RecomputePtr(k ptr.Kind, data []byte) (ptr.Ptr, error) {

	if !k.IsLoadable() {
		// this should have been caught earlier
		return ptr.Null, errlib.Newf("FLO-1313 {%s} is not loadable")
	}

	h := sha256.Sum256(data)
	p := ptr.FromHash(k, h[:])
	return p, nil
}

// Store implements cam.StorageClient
func (c storageClient) Store(ctx context.Context, in *cam.StoreRequest) (*cam.StoreResponse, error) {
	c.base.lock.Lock()
	defer c.base.lock.Unlock()

	out := &cam.StoreResponse{}
	for _, req := range in.Data {
		var p ptr.Ptr
		var k ptr.Kind

		if req.Ref == nil {
			k = ptr.Untyped
			p, _ = RecomputePtr(ptr.Untyped, req.Payload)
		} else {
			expected := req.Ref.Must()
			k = expected.Kind()
			var err error
			p, err = RecomputePtr(k, req.Payload)
			if err != nil {
				return nil, err
			}
			if p != expected {
				return nil, errlib.Newf("FLO-1319 ptr mismatch given {%s} != computed {%s}",
					expected, p)
			}
		}

		if _, ok := c.base.index[p]; !ok {
			cp := make([]byte, len(req.Payload))
			copy(cp, req.Payload)
			c.base.index[p] = datum{
				stored: time.Now(),
				domain: in.Domain,
				data:   cp,
			}
		}
		out.Refs = append(out.Refs, core.From(p))
	}
	return out, nil
}

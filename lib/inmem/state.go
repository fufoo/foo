package inmem

import (
	"bytes"
	"context"
	"sync"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/special"
	"bitbucket.org/fufoo/foo/services/core"
	"bitbucket.org/fufoo/foo/services/state"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var log = logging.New("inmem")

const zeroLogIndex = 700

type key struct {
	domain uint64
	obs    string
}

type entry struct {
	state ptr.Ptr
	mtime time.Time
	seq   uint64
	audit uint64
}

func (e *entry) fill(dst *state.Obs) {
	dst.Value = core.From(e.state)
	dst.Mtime = timestamppb.New(e.mtime)
	dst.Seq = e.seq
	dst.Audit = e.audit
}

type State struct {
	lock      sync.Mutex
	index     map[key]*entry
	seq       uint64
	log       []*state.TxnRecord
	followers []*following
}

type following struct {
	spec *state.FollowRequest
	q    chan *state.TxnRecord
	all  bool
}

func (f *following) matches(change map[key]*state.Obs) bool {
	if f.all {
		return true
	}

	if f.spec.AllowGlob {
		panic("partial glob not implemented")
	}
	for _, subject := range f.spec.Subjects {
		k := key{
			domain: f.spec.Domain,
			obs:    subject,
		}
		if _, ok := change[k]; ok {
			return true
		}
	}
	return false
}

func (f *following) Recv() (*state.TxnRecord, error) {
	return <-f.q, nil
}

func NewState(backing Backing) *State {
	return &State{
		index: make(map[key]*entry),
	}
}

func (s *State) Client() state.StateQClient {
	return stateClient{s}
}

type stateClient struct {
	base *State
}

// Keepalive implements state.Keepaliver
func (s stateClient) Keepalive(ctx context.Context) {
}

func (c stateClient) Follow(ctx context.Context, in *state.FollowRequest) (state.State_FollowQClient, error) {
	c.base.lock.Lock()
	defer c.base.lock.Unlock()

	f := &following{
		q:    make(chan *state.TxnRecord, 20),
		spec: in,
	}

	if in.AllowGlob && len(in.Subjects) == 1 && in.Subjects[0] == "*" {
		f.all = true
	}

	c.base.followers = append(c.base.followers, f)
	return f, nil
}

func (c stateClient) Scan(ctx context.Context, in *state.GetRequest) (state.State_ScanQClient, error) {
	panic("TODO")
}

func (c stateClient) Get(ctx context.Context, in *state.GetRequest) (*state.GetResponse, error) {
	c.base.lock.Lock()
	defer c.base.lock.Unlock()

	out := &state.GetResponse{
		Mark: c.base.seq + 1,
	}

	for _, subject := range in.Subjects {
		k := key{
			domain: in.Domain,
			obs:    subject,
		}
		entry, ok := c.base.index[k]
		obs := &state.Obs{
			Domain: in.Domain,
			Name:   subject,
		}

		if ok {
			entry.fill(obs)
		}
		out.States = append(out.States, obs)
	}
	return out, nil
}

func (c stateClient) Apply(ctx context.Context, in *state.TxnRequest) (*state.TxnRecord, error) {

	// compile the specials

	var specials []special.Handler

	req := in // renaming

	for _, s := range req.Specials {
		h, err := special.Compile(ctx, s, req)
		if err != nil {
			// could not compile a special
			return nil, errlib.Wrap("FOA-7503", err)
		}
		specials = append(specials, h)
	}
	c.base.lock.Lock()
	defer c.base.lock.Unlock()

	result := &state.TxnRecord{
		Request: req,
	}

	var states []*state.Obs
	for _, s := range req.Subjects {
		dn := key{
			domain: req.Domain,
			obs:    s,
		}
		entry, ok := c.base.index[dn]
		obs := &state.Obs{
			Domain: req.Domain,
			Name:   s,
		}
		if ok {
			entry.fill(obs)
		}
		states = append(states, obs)
	}
	result.States = states
	result.Disposition = make([]state.Disposition, len(req.Subjects))

	// check preconds
	success := true
	for _, pre := range req.Preconds {
		if !equal(states[pre.Slot], pre.Value) {
			log.Debugf(ctx, "precondition slot[%d] %q...", pre.Slot, req.Subjects[pre.Slot])
			if states[pre.Slot] == nil {
				log.Debugf(ctx, "no state")
			} else {
				log.Debugf(ctx, "state = %s", states[pre.Slot].Short())
			}
			if pre.Value == nil {
				log.Debugf(ctx, "no value")
			} else {
				log.Debugf(ctx, "value = {%s}", pre.Value.Must())
			}
			result.Disposition[pre.Slot] = state.Disposition_CONFLICT
			success = false
		} else {
			// being read... if this subject appears in the Effects, will
			// be updated to either CHANGED or TOUCHED
			result.Disposition[pre.Slot] = state.Disposition_SILENT
		}
	}
	if !success {
		result.Conflict = true
		return result, nil
	}

	// preconditions passed, we are going to try to commit
	result.Time = timestamppb.Now()

	edits := false
	change := make(map[key]*state.Obs)

	postchange := func(s *state.Obs) {
		dn := key{
			domain: s.Domain,
			obs:    s.Name,
		}

		if _, ok := change[dn]; ok {
			// already in the change set
		}

		s.Mtime = result.Time
		if s.Seq == 0 {
			s.Seq = 100
		} else {
			s.Seq = s.Seq + 1
		}
		s.Audit = req.Audit
		change[dn] = s
	}

	for _, eff := range req.Effects {
		//log.Tracef(ctx, "applying effect %s", eff)
		differ := !equal(states[eff.Slot], eff.Value)
		if eff.Touch || differ {
			s := states[eff.Slot]

			if s.Value == nil && eff.Value != nil {
				log.Tracef(ctx, "setting new value for %s", s.Name)
			} else if s.Value != nil && eff.Value == nil {
				log.Tracef(ctx, "deleting value for %s", s.Name)
			}

			s.Value = eff.Value
			if differ {
				result.Disposition[eff.Slot] = state.Disposition_CHANGED
			} else {
				result.Disposition[eff.Slot] = state.Disposition_TOUCHED
			}
			postchange(s)
		}
	}

	// apply the specials
	for _, spec := range specials {
		changed, err := spec.Apply(states)
		if err != nil {
			// rather than reporting this as a
			// conflict, we report an error.
			// Otherwise, we'd want to roll back
			// the states so the client can see
			// the state of things before the txn
			// started to apply.  If we find that
			// it's important to give the client
			// information, other than the error
			// code, about why the special failed
			// (i.e., the state of the database)
			// then we might change this protocol
			// later.
			return nil, err
		}
		if len(changed) > 0 {
			log.Tracef(ctx, "applied special effect to: %v", changed)
			for _, k := range changed {
				postchange(states[k])
			}
			edits = true
		} else {
			log.Tracef(ctx, "special effect did nothing")
		}
	}

	for dn, s := range change {
		e, ok := c.base.index[dn]
		if !ok {
			e = &entry{seq: 999}
			c.base.index[dn] = e
		}

		e.state = s.Value.Must()
		e.mtime = result.Time.AsTime()
		e.seq++
		e.audit = req.Audit
		edits = true
	}

	if !edits {
		log.Tracef(ctx, "there were no edits, actually")
		return result, nil
	} else if result.Conflict {
		// there was a conflict... don't assign a log seq
		log.Tracef(ctx, "FOA-7518 conflict")
	} else {
		result.Id = uint64(len(c.base.log)) + zeroLogIndex
		c.base.log = append(c.base.log, result)
		// TODO notify followers
	}

	for _, f := range c.base.followers {
		if f.matches(change) {
			f.q <- result
		}
	}

	return result, nil
}

func equal(current *state.Obs, precond *core.Ptr) bool {
	if current.Value == nil {
		return precond == nil
	}
	if precond == nil {
		return false
	}
	// no need to convert to a ptr.Ptr; just compare the bytes
	return bytes.Equal(current.Value.Bits, precond.Bits)
}

package inmem

import (
	"context"
)

// A Backing implementation can be provided so that data is persisted,
// but this package still provides only a local implementation (since
// part of the whole point of foo is inter-process communication, that
// feature does not exist even with a persisted in-memory foo)
type Backing interface {
	Set(ctx context.Context, key string, value []byte) error
	Get(ctx context.Context, key string) ([]byte, error)
}

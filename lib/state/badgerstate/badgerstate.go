package badgerstate

import (
	"bytes"
	"context"
	"encoding/binary"
	"fmt"
	"io"
	"sync"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/list"
	"bitbucket.org/fufoo/foo/lib/followfilter"
	"bitbucket.org/fufoo/foo/lib/special"
	"bitbucket.org/fufoo/foo/services/core"
	"bitbucket.org/fufoo/foo/services/state"
	"github.com/dgraph-io/badger/v3"
	badgerpb "github.com/dgraph-io/badger/v3/pb"
	"github.com/kelindar/bitmap"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var log = logging.New("badgerstate")

// a simple badger-based implementation, where everything is stored
// in a single badger database
//
//    two kinds of keys:
//
//          TXN transaction log entry
//          OBS observable state
//
//    'M' <domain>         => <mark>
//    'T' <domain> <mark>  => TxnRecord
//    'S' <domain> <name>  => Obs

const logCacheSize = 10

type Foo struct {
	db       *badger.DB
	log      list.List[txnKey]
	logLock  sync.RWMutex
	logIndex map[txnKey]*state.TxnRecord
	maxMark  map[uint64]uint64
	logSize  int
}

type txnKey struct {
	domain uint64
	mark   uint64
}

func (lk txnKey) String() string {
	return fmt.Sprintf("D#%d L[%d]", lk.domain, lk.mark)
}

type cacheKey struct {
	domain uint64
	name   string
}

func (k cacheKey) String() string {
	return fmt.Sprintf("D#%d [%s]", k.domain, k.name)
}

// See FOO-2 for notes about this encoding

func decodeKey(k []byte) (cacheKey, bool) {
	if len(k) == 0 || k[0] != 'S' {
		return cacheKey{}, false
	}

	domain, n := binary.Uvarint(k[1:])
	if n < 1 {
		return cacheKey{}, false
	}
	// note that we regard domain==0 and len(name)==0 as valid to
	// be encoded, which is what {0} decodes to.  HOWEVER, there
	// are special keys that start with 0 which are used by this
	// layer (e.g., see MinLogPosn)
	return cacheKey{
		domain: domain,
		name:   string(k[n+1:]),
	}, true
}

func txnRecordKey(domain, mark uint64) []byte {
	var tmp [11 + 8]byte
	tmp[0] = 'T'
	i := 1
	i += binary.PutUvarint(tmp[i:], domain)
	binary.BigEndian.PutUint64(tmp[i:], mark)
	i += 8
	return tmp[:i]
}

func domainMarkKey(domain uint64) []byte {
	var tmp [11]byte
	tmp[0] = 'M'
	i := 1
	i += binary.PutUvarint(tmp[i:], domain)
	return tmp[:i]
}

func (k cacheKey) encode() []byte {
	var tmp [11 + state.MaxNameLength]byte
	tmp[0] = 'S'
	i := 1
	i += binary.PutUvarint(tmp[i:], k.domain)
	i += copy(tmp[i:], k.name)
	return tmp[:i]
}

func Open(file string) (*Foo, error) {
	opts := badger.DefaultOptions(file)
	db, err := badger.Open(opts)
	if err != nil {
		return nil, err
	}
	f := &Foo{
		db:       db,
		logIndex: make(map[txnKey]*state.TxnRecord, logCacheSize),
		maxMark:  make(map[uint64]uint64),
	}
	go f.cleaner()
	return f, nil
}

func (f *Foo) cleaner() {
	t := time.NewTicker(10 * time.Second)
	for range t.C {
		f.cleanup()
	}
}

func (f *Foo) cleanup() {
	if f.logSize <= logCacheSize {
		return
	}

	ctx := context.Background()

	f.logLock.Lock()
	defer f.logLock.Unlock()
	n := f.logSize - logCacheSize

	log.Tracef(ctx, "discarding %d entries from cache", n)
	for i := 0; i < n; i++ {
		key := f.log.PopFront()
		delete(f.logIndex, key)
	}
	f.logSize -= n
}

var _ state.StateQClient = &Foo{}

func (f *Foo) Get(ctx context.Context, req *state.GetRequest) (*state.GetResponse, error) {
	log.Infof(ctx, "FOS-2000 Get {%s}", req)

	txn := f.db.NewTransaction(false)
	defer txn.Discard()

	response := &state.GetResponse{}

	var err error
	response.Mark, err = readMark(ctx, txn, req.Domain)
	if err != nil {
		return nil, err
	}

	for _, s := range req.Subjects {
		key := cacheKey{
			domain: req.Domain,
			name:   s,
		}
		obs, err := f.getobs(ctx, txn, key)
		if err != nil {
			return nil, err
		}
		response.States = append(response.States, obs)
	}
	return response, nil
}

func (f *Foo) getobs(ctx context.Context, txn *badger.Txn, key cacheKey) (*state.Obs, error) {

	var obs *state.Obs

	item, err := txn.Get(key.encode())
	if err != nil {
		if err == badger.ErrKeyNotFound {
			log.Debugf(ctx, "FOO-2002 brand new obs %s", key)
			obs = &state.Obs{
				Domain: key.domain,
				Name:   key.name,
			}
			err = nil
		}
	} else {
		err = item.Value(func(value []byte) error {
			tmp := new(state.Obs)
			err := proto.Unmarshal(value, tmp)
			if err != nil {
				return err
			}
			obs = tmp
			return nil
		})
	}
	return obs, err
}

const markVersionTag = 0x41

func writeMark(ctx context.Context, txn *badger.Txn, domain, mark uint64) error {
	var buf [11]byte
	buf[0] = markVersionTag // version
	i := 1
	i += binary.PutUvarint(buf[i:], mark)
	log.Tracef(ctx, "writeMark key=|%x| value=|%x|",
		domainMarkKey(domain),
		buf[:i])
	return txn.Set(domainMarkKey(domain), buf[:i])
}

func decodeMark(value []byte) (uint64, error) {

	i := 0
	if i >= len(value) || value[i] != markVersionTag {
		return 0, errlib.Newf("FOS-2004 incompatible mark |%x|", value)
	}
	i++

	mark, n := binary.Uvarint(value[i:])
	if n < 1 {
		return 0, errlib.Newf("FOS-2003 corrupt mark |%x|", value)
	}
	return mark, nil
}

func readMark(ctx context.Context, txn *badger.Txn, domain uint64) (uint64, error) {

	item, err := txn.Get(domainMarkKey(domain))

	if err != nil {
		if err == badger.ErrKeyNotFound {
			log.Debugf(ctx, "FOS-2001 brand new domain D#%d", domain)
			return 0, nil
		}
		return 0, err
	}
	var mark uint64

	err = item.Value(func(value []byte) error {
		m, err := decodeMark(value)
		if err != nil {
			return err
		}
		mark = m
		return nil
	})
	return mark, err
}

func (f *Foo) Apply(ctx context.Context, req *state.TxnRequest) (*state.TxnRecord, error) {
	log.Infof(ctx, "FOS-2100 Apply {%s}", req)

	txn := f.db.NewTransaction(true)
	defer txn.Discard()

	response := &state.TxnRecord{
		Request: req,
	}

	for _, s := range req.Subjects {
		key := cacheKey{
			domain: req.Domain,
			name:   s,
		}
		obs, err := f.getobs(ctx, txn, key)
		if err != nil {
			return nil, err
		}
		response.States = append(response.States, obs)
	}
	n := len(req.Subjects)
	response.Disposition = make([]state.Disposition, n)

	// check preconds
	success := true
	states := response.States
	for _, pre := range req.Preconds {
		if !equal(states[pre.Slot], pre.Value) {
			log.Debugf(ctx, "precondition slot[%d] %q...", pre.Slot, req.Subjects[pre.Slot])
			if states[pre.Slot] == nil {
				log.Debugf(ctx, "no state")
			} else {
				log.Debugf(ctx, "state = %s", states[pre.Slot].Short())
			}
			if pre.Value == nil {
				log.Debugf(ctx, "no value")
			} else {
				log.Debugf(ctx, "value = {%s}", pre.Value.Must())
			}
			response.Disposition[pre.Slot] = state.Disposition_CONFLICT
			success = false
		} else {
			// being read... if this subject appears in the Effects, will
			// be updated to either CHANGED or TOUCHED
			response.Disposition[pre.Slot] = state.Disposition_SILENT
		}
	}
	if !success {
		response.Conflict = true
		return response, nil
	}

	// preconditions passed, we are going to try to commit
	response.Time = timestamppb.Now()

	var changes bitmap.Bitmap

	// apply the changes
	for _, eff := range req.Effects {
		log.Tracef(ctx, "applying effect {%s}", eff)
		diff := !equal(states[eff.Slot], eff.Value)
		if eff.Touch || diff {
			s := response.States[eff.Slot]
			if s.Value == nil && eff.Value != nil {
				log.Tracef(ctx, "setting new value for %s", s.Name)
			} else if s.Value != nil && eff.Value == nil {
				log.Tracef(ctx, "deleting value for %s", s.Name)
			}

			s.Value = eff.Value
			d := state.Disposition_TOUCHED
			if diff {
				d = state.Disposition_CHANGED
			}

			response.Disposition[eff.Slot] = d
			changes.Set(uint32(eff.Slot))
		}
	}
	// TODO apply the specials
	for _, spec := range req.Specials {
		log.Tracef(ctx, "applying special {%s}", spec)
		h, err := special.Compile(ctx, spec, req)
		if err != nil {
			return nil, errlib.Wrap("FOA-7503", err)
		}
		changed, err := h.Apply(response.States)
		if err != nil {
			// rather than reporting this as a
			// conflict, we report an error.
			// Otherwise, we'd want to roll back
			// the states so the client can see
			// the state of things before the txn
			// started to apply.  If we find that
			// it's important to give the client
			// information, other than the error
			// code, about why the special failed
			// (i.e., the state of the database)
			// then we might change this protocol
			// later.
			return nil, err
		}
		if len(changed) > 0 {
			// we assume here that the special will only mark
			// things as changed when appropriate
			log.Tracef(ctx, "applied special effect to: %v", changed)
			for _, k := range changed {
				changes.Set(uint32(k))
				response.Disposition[k] = state.Disposition_CHANGED
			}
		}
	}

	// write the changes if any
	if count := changes.Count(); count > 0 {

		mark, err := readMark(ctx, txn, req.Domain)
		if err != nil {
			return nil, err
		}

		mark++
		log.Tracef(ctx, "there were %d changes, assigning mark [%d]",
			count,
			mark)

		response.Id = mark
		response.Mark = mark
		response.NextMark = mark + 1 // we do consecutive marks here

		writeMark(ctx, txn, req.Domain, mark)

		changes.Range(func(k uint32) {
			s := response.States[k]

			s.Mtime = response.Time
			if s.Seq == 0 {
				s.Seq = 100
			} else {
				s.Seq = s.Seq + 1
			}
			s.Audit = req.Audit

			writeback, err := proto.Marshal(s)
			if err != nil {
				panic(err)
			}
			key := cacheKey{
				domain: s.Domain,
				name:   s.Name,
			}
			log.Tracef(ctx, "set |%x| to %d bytes for [%s]",
				key.encode(),
				len(writeback),
				s.Name)
			e := txn.Set(key.encode(), writeback)
			if e != nil {
				err = e
			}
			log.Tracef(ctx, "set {%s}", s)
		})
		if err != nil {
			return nil, err
		}

		// write the log record

		txbuf, err := proto.Marshal(response)
		if err != nil {
			return nil, err
		}
		err = txn.Set(txnRecordKey(req.Domain, mark), txbuf)
		if err != nil {
			return nil, err
		}
	}

	// cache the txn record, but grab the lock *before*
	// committing because otherwise the follower might
	// get called before we have a chance to do and and
	// it'll have to page it in anyway
	if response.Mark != 0 {
		f.logLock.Lock()
		defer f.logLock.Unlock()
	}

	err := txn.Commit()
	if err != nil {
		log.Warningf(ctx, "did not commit: %s", err)
		// TODO rewrite "conflict" error
		return nil, err
	}

	// cache the txn record
	lk := txnKey{
		domain: req.Domain,
		mark:   response.Mark,
	}
	log.Tracef(ctx, "stored %s", lk)
	f.logIndex[lk] = response
	f.maxMark[req.Domain] = response.Mark
	f.log.PushBack(lk)
	f.logSize++

	return response, nil
}

func equal(current *state.Obs, precond *core.Ptr) bool {
	if current.Value == nil {
		return precond == nil
	}
	if precond == nil {
		return false
	}
	// no need to convert to a ptr.Ptr; just compare the bytes
	return bytes.Equal(current.Value.Bits, precond.Bits)
}

type scanclient struct {
	ch <-chan *state.Obs
}

func (sc *scanclient) Recv() (*state.Obs, error) {
	item, ok := <-sc.ch
	if ok {
		return item, nil
	}
	return nil, io.EOF
}

func (f *Foo) Scan(ctx context.Context, req *state.GetRequest) (state.State_ScanQClient, error) {

	var prefix []byte
	var all bool
	var ff followfilter.Subscription
	key := cacheKey{
		domain: req.Domain,
	}
	prefix = key.encode()

	if len(req.Subjects) == 0 {
		// here we take advantage of the structure of the
		// encoding for state keys; an empty name will be
		// just the prefix we want
		all = true
	} else {
		// this is tricky because we also want to optimize
		// for the case of trailing globs, because that
		// lets us set a prefix.  e.g., if there is only
		// one subject, "user/*", then we want to use a prefix
		// scan to find it
		var err error
		ff, err = followfilter.New(ctx, req.Subjects, true)
		if err != nil {
			return nil, err
		}
		all = false
	}

	ch := make(chan *state.Obs, 10)
	sc := &scanclient{ch}

	go func() {
		defer close(ch)

		txn := f.db.NewTransaction(false)
		defer txn.Discard()

		// a couple of special cases

		opt := badger.DefaultIteratorOptions
		opt.Prefix = prefix
		opt.PrefetchValues = true
		opt.PrefetchSize = 10

		iter := txn.NewIterator(opt)
		defer iter.Close()
		log.Tracef(ctx, "opt {%#v} prefix |%x|", opt, prefix)

		numDiscard := 0
		numReturn := 0

		for iter.Rewind(); iter.Valid(); iter.Next() {
			if key, ok := decodeKey(iter.Item().Key()); ok {
				if key.domain != req.Domain {
					//log.Errorf(ctx, "came across domain %d while scanning %d",
					panic("did not expect that domain")
				}
				if !all && !ff.QualifyOne(key.name) {
					numDiscard++
					continue
				}

				iter.Item().Value(func(value []byte) error {
					log.Tracef(ctx, "  %s => |%x|", key, value)
					s := new(state.Obs)
					proto.Unmarshal(value, s)
					ch <- s
					numReturn++
					return nil
				})
			} else {
				log.Tracef(ctx, "bad key |%x|", iter.Item().Key())
			}
		}
		log.Tracef(ctx, "scan returned %d and discarded %d",
			numReturn, numDiscard)

	}()
	return sc, nil
}

type follower struct {
	ch <-chan *state.TxnRecord
}

func (f follower) Recv() (*state.TxnRecord, error) {
	item, ok := <-f.ch
	if ok {
		return item, nil
	}
	return nil, io.EOF
}

func (f *Foo) startFollow(ctx context.Context, req *state.FollowRequest) (uint64, error) {
	mark := req.AfterMark
	if req.AtEnd {
		txn := f.db.NewTransaction(false)
		defer txn.Discard()

		if req.AfterMark != 0 {
			return 0, errlib.Newf("FOS-2401 invalid")
		}
		return readMark(ctx, txn, req.Domain)
	}
	if mark == 0 {
		return 0, nil
	}
	return mark, nil
}

func (f *Foo) Follow(ctx context.Context, req *state.FollowRequest) (state.State_FollowQClient, error) {
	log.Infof(ctx, "FOS-2400 Follow {%s}", req)

	domain := req.Domain
	ch := make(chan *state.TxnRecord, 1)

	pump := func(mark uint64) bool {
		log.Tracef(ctx, "pump(%d)", mark)

		txn := f.db.NewTransaction(false)
		defer txn.Discard()

		lk := txnKey{
			domain: domain,
			mark:   mark,
		}
		f.logLock.RLock()
		tx, ok := f.logIndex[lk]
		max, havemax := f.maxMark[domain]
		f.logLock.RUnlock()

		if ok {
			log.Tracef(ctx, "cache hit for %s", lk)
			// found it in cache; return it right away
			ch <- tx
			return true
		}

		if havemax && mark > max {
			log.Tracef(ctx, "cache miss for %s, but it's beyond max", lk)
			return false
		}

		log.Tracef(ctx, "cache miss for %s", lk)
		item, err := txn.Get(txnRecordKey(domain, mark))
		if err != nil {
			if err == badger.ErrKeyNotFound {
				log.Tracef(ctx, "stall")
				return false
			}
			log.Warningf(ctx, "bad read, discarding: %s", err)
			return true
		}

		item.Value(func(value []byte) error {
			tx := new(state.TxnRecord)
			err := proto.Unmarshal(value, tx)
			if err != nil {
				log.Warningf(ctx, "bad D#%d TxnRecord[%d], discarding: %s",
					domain,
					err)
				return nil
			}
			// TODO should we cache this log record??
			ch <- tx
			return nil
		})
		return true
	}

	bwatch := make(chan uint64, 1) // updates from badger

	// note that we are following the DOMAIN MARK, which
	// corresponds to the transaction log as a whole, and not
	// specific observables.  TODO consider whether we can just
	// follow observables [in some cases]
	match := []badgerpb.Match{
		{
			Prefix: domainMarkKey(req.Domain),
		},
	}

	go func() {
		defer close(bwatch)

		fn := func(kv *badger.KVList) error {
			log.Tracef(ctx, "popped with %d kvs", len(kv.Kv))
			var max uint64

			// in case we get multiples, just report the biggest
			// one (not sure about the semantics here, so be defensive)
			for i, kv := range kv.Kv {
				log.Tracef(ctx, "   [%d] version %d value |%x|",
					i,
					kv.Version,
					kv.Value)
				m, err := decodeMark(kv.Value)
				if err != nil {
					panic(err)
				}
				log.Tracef(ctx, "D#%d mark is now %d", domain, m)
				if m > max {
					max = m
				}
			}
			bwatch <- max
			return nil
		}

		err := f.db.Subscribe(ctx, fn, match)
		if err != nil {
			log.Warningf(ctx, "subscribe failed: %s", err)
		}
	}()

	mark, err := f.startFollow(ctx, req)
	if err != nil {
		return nil, err
	}

	go func() {
		defer close(ch)

		// run the prefix first, as far as we can
		for pump(mark + 1) {
			mark++
		}
		log.Tracef(ctx, "stalling; waiting for more")
		for adv := range bwatch {
			log.Tracef(ctx, "advanced to %d (currently at %d)",
				adv, mark)
			for pump(mark + 1) {
				mark++
			}
			log.Tracef(ctx, "stalling")
		}
	}()

	log.Tracef(ctx, "off to races")
	return follower{ch}, nil
}

#! /bin/bash

export FOO_CONFIG=config.json

export FOO_NODE_CERT=/tmp/foo-cluster.d/ca/4xbQSvtfn3EFoLxOYq8WvxOrjrjn5zYFJ8gAwH.cert 
export FOO_NODE_KEY=/tmp/foo-cluster.d/ca/4xbQSvtfn3EFoLxOYq8WvxOrjrjn5zYFJ8gAwH.key 

state() {
    FOO_CA_TRUST=/tmp/foo-cluster.d/ca/root.cert \
        FOO_DEBUG=trace  \
        FOO_STATE_PORT=2321  \
        FOO_NODE_ADVERTISE=localhost:2321  \
        FOO_NODE_NAME=state-0  \
        FOO_SERVER=localhost:2300  \
        FOO_NODE_ID=dev.state.0  \
        FOO_STATE_DATAFILE=/test/donovan/dev-state0.badger \
        /tmp/foo-stated
}


zero() {
    FOO_CA_TRUST=/tmp/foo-cluster.d/ca/root.cert \
        FOO_DEBUG=trace \
        FOO_ZERO_PORT=2300 \
        /tmp/foo-zero
}

cam() {
    FOO_CA_TRUST=/tmp/foo-cluster.d/ca/root.cert \
        FOO_DEBUG=trace \
        FOO_CAM_PORT=2310 \
        FOO_CAM_DATAFILE=/tmp/dev-cam0.badger \
        FOO_NODE_ADVERTISE=localhost:2310 \
        FOO_NODE_NAME=cam-0 \
        FOO_SERVER=localhost:2300 \
        FOO_NODE_ID=dev.cam.0 \
        /tmp/foo-camd
}

xauth() {
    FOO_CA_TRUST=/tmp/foo-cluster.d/ca/root.cert \
        FOO_DEBUG=trace \
        FOO_XAUTH_PORT=2330 \
        FOO_NODE_ADVERTISE=localhost:2330 \
        FOO_NODE_NAME=auth \
        FOO_NODE_ID=dev.auth.0 \
        FOO_SERVER=localhost:2300 \
        FOO_XAUTH_BOOTSTRAP=nG1X0lUrEp6vUY8c9aIrzIpMzCm3Vs0WXlHQDwzGFZPfAOOYyLymUaozK5PJ6qn0HdrRWgfOEG65jSpqfy5UoKrecBq4cOi3zGGpD6uy26cjl \
        /tmp/foo-authd
}

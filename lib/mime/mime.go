package mime

import (
	"bitbucket.org/fufoo/core/ptr"
)

// TypeMap maps from MIME types to the corresponding omen3 type Ptr,
// which are (in most cases, but try not to rely on that) represented
// as string literals
var PtrForType = map[string]ptr.Ptr{
	"application/binary":           ptr.String("mime:a/binary"),
	"application/json":             ptr.String("mime:a/json"),
	"application/javascript":       ptr.String("mime:a/javascript"),
	"application/gzip":             ptr.String("mime:a/gzip"),
	"application/pdf":              ptr.String("mime:a/pdf"),
	"application/zip":              ptr.String("mime:a/zip"),
	"application/xml":              ptr.String("mime:a/xml"),
	"application/x-graphql-schema": ptr.String("mime:a/gqls"),
	"application/x-executable":     ptr.String("mime:a/x-executable"),
	"image/png":                    ptr.String("mime:i/png"),
	"image/jpeg":                   ptr.String("mime:i/jpeg"),
	"image/svg+xml":                ptr.String("mime:i/svg+xml"),
	"text/html":                    ptr.String("mime:t/html"),
	"text/css":                     ptr.String("mime:t/css"),
	"text/csv":                     ptr.String("mime:t/csv"),
	"text/plain":                   ptr.String("mime:t/plain"),
	"application/font-sfnt":        ptr.String("mime:a/font-sfnt"), // ttf + otf
	"application/font-woff":        ptr.String("mime:a/font-woff"),
	"video/mpeg":                   ptr.String("mime:v/mpeg"),
	"video/mp4":                    ptr.String("mime:v/mp4"),
	"video/x-matroska":             ptr.String("mime:v/x-matroska"),
}

// TypeUnmap maps from omen3 type Ptr to the corresponding MIME type
var TypeForPtr = make(map[ptr.Ptr]string, 10)

func init() {
	for mime, ptr := range PtrForType {
		TypeForPtr[ptr] = mime
	}
}

/*
type Object struct {
	Type string
	Data []byte
}

func (o *Object) Extension() string {
	lst, err := stdmime.ExtensionsByType(o.Type)
	if err != nil || len(lst) == 0 {
		return ""
	}
	return lst[0]
}
*/

func Must(mime string) ptr.Ptr {
	p, ok := PtrForType[mime]
	if !ok {
		panic("missing mime: " + mime)
	}
	return p
}

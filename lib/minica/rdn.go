package minica

import (
	"crypto/x509/pkix"
	"regexp"
	"strings"

	"bitbucket.org/fufoo/core/errlib"
)

var rdn = regexp.MustCompile(`\s*([a-zA-Z]+)\s*=(.*)$`)

// ParseDN parses simple distinguished names, suitable for our purpose
// but not as sophisticated as needed for, say, LDAP support.
//
// example DN:
//
//	c=US, st=Texas, l=Austin, o=Fufoo Net, ou=Test server 1
func ParseDN(name string) (*pkix.Name, error) {
	var n pkix.Name

	for _, part := range strings.Split(name, ",") {
		if part == "" {
			return nil, errlib.Newf("FOO-5801 invalid blank RDN")
		}
		m := rdn.FindStringSubmatch(part)
		if m == nil {
			return nil, errlib.Newf("FOO-5802 invalid RDN %q", part)
		}

		value := strings.TrimSpace(m[2])
		switch strings.ToUpper(m[1]) {
		case "C":
			n.Country = []string{value}
		case "ST":
			n.Province = []string{value}
		case "L":
			n.Locality = []string{value}
		case "O":
			n.Organization = append(n.Organization, value)
		case "OU":
			n.OrganizationalUnit = append(n.OrganizationalUnit, value)
		case "CN":
			n.CommonName = value

		case "EMAILADDRESS":
			n.ExtraNames = append(n.ExtraNames,
				pkix.AttributeTypeAndValue{
					Type:  emailAddressOid,
					Value: value,
				})
		}
	}

	return &n, nil
}

var commonNameOid = []int{2, 5, 4, 3}
var emailAddressOid = []int{1, 2, 840, 113549, 1, 9, 1}

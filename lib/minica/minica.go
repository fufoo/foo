package minica

import (
	"context"
	"crypto/ed25519"
	"crypto/rand"
	"crypto/sha256"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"math/big"
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
)

var log = logging.New("minica")

type Registry struct {
	dir  string
	root *x509.Certificate
}

func New(ctx context.Context, dir string, rootdn, altname string) (*Registry, error) {
	r := &Registry{
		dir: dir,
	}
	err := r.initRoot(ctx, rootdn, altname)
	if err != nil {
		return nil, err
	}

	return r, nil
}

func (r *Registry) initRoot(ctx context.Context, rootdn, altname string) error {
	file := filepath.Join(r.dir, "root.cert")
	buf, err := os.ReadFile(file)
	if err != nil {
		if rootdn == "" {
			return errlib.Newf("FOO-5818 no root cert and root DN not configured")
		}
		err = r.createRoot(ctx, rootdn, altname)
		if err != nil {
			return err
		}
		buf, err = os.ReadFile(file)
		if err != nil {
			return err
		}
	}

	p, _ := pem.Decode(buf)
	if p == nil {
		return errlib.Newf("FOO-5816 invalid cert in %s", file)
	}

	if p.Type != "CERTIFICATE" {
		return errlib.Newf("FOO-5817 PEM file is %s, not a CERTIFICATE",
			p.Type)
	}

	cert, err := x509.ParseCertificate(p.Bytes)
	if err != nil {
		return err
	}
	r.root = cert
	return nil
}

func (r *Registry) CAFile() string {
	return filepath.Join(r.dir, "root.cert")
}

func (r *Registry) GetCertAndKeyForHost(ctx context.Context, host string) (string, string, error) {
	h := sha256.Sum224([]byte(host))
	var z big.Int

	z.SetBytes(h[:])
	base := fmt.Sprintf("%s-%s",
		z.Text(62),
		time.Now().Format("2006-Jan"),
	)

	certFile := filepath.Join(r.dir, base+".cert")
	keyFile := filepath.Join(r.dir, base+".key")

	if _, err := os.Stat(certFile); err != nil {
		err := r.issueCert(ctx, host, certFile, keyFile)
		if err != nil {
			return "", "", err
		}
	}
	return certFile, keyFile, nil
}

func writePEM(dst string, b *pem.Block) error {
	out, err := os.Create(dst + "~")
	if err != nil {
		return err
	}
	defer out.Close()

	err = pem.Encode(out, b)
	if err != nil {
		return err
	}
	return os.Rename(dst+"~", dst)
}

func (r *Registry) createRoot(ctx context.Context, rootdn, altname string) error {

	if altname == "" {
		return errlib.Newf("FOO-5814 missing subject alternate name for DNS")
	}

	if _, err := os.Stat(r.dir); err != nil {
		os.MkdirAll(r.dir, 0700)
	}

	var kid [16]byte
	n, err := rand.Reader.Read(kid[:])
	if n != len(kid) {
		return errlib.Newf("FOO-5813 short read %d", n)
	}

	pub, priv, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		return err
	}

	if rootdn == "" {
		return errlib.Newf("FOO-5819 cluster name is required if initializing CA")
	}

	subject, err := ParseDN(rootdn)
	if err != nil {
		return err
	}

	t := time.Now().Add(-time.Second).Truncate(time.Minute)
	template := &x509.Certificate{
		AuthorityKeyId:        kid[:],
		SubjectKeyId:          kid[:],
		SerialNumber:          big.NewInt(1),
		BasicConstraintsValid: true,
		IsCA:                  true,
		MaxPathLen:            1,
		NotBefore:             t,
		NotAfter:              t.AddDate(1, 0, 0), // valid for 1 year
		Subject:               *subject,
		DNSNames:              []string{altname},
	}

	buf, err := x509.CreateCertificate(
		rand.Reader,
		template,
		template,
		pub,
		priv)
	if err != nil {
		return err
	}

	privbuf, err := x509.MarshalPKCS8PrivateKey(priv)
	if err != nil {
		return err
	}
	err = writePEM(
		filepath.Join(r.dir, "root.key"),
		&pem.Block{
			Type:  "PRIVATE KEY",
			Bytes: privbuf,
		})

	return writePEM(
		filepath.Join(r.dir, "root.cert"),
		&pem.Block{
			Type:  "CERTIFICATE",
			Bytes: buf,
		})
}

func (r *Registry) readRootPriv() (interface{}, error) {
	file := filepath.Join(r.dir, "root.key")
	buf, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	p, _ := pem.Decode(buf)
	if p == nil {
		return nil, errlib.Newf("FOO-5816 invalid key in %s", file)
	}

	if p.Type != "PRIVATE KEY" {
		return nil, errlib.Newf("FOO-5817 file is %s, not a PRIVATE KEY",
			p.Type)
	}
	return x509.ParsePKCS8PrivateKey(p.Bytes)
}

func (r *Registry) next() *big.Int {
	serno := filepath.Join(r.dir, "serno")
	buf, _ := ioutil.ReadFile(serno)
	var z big.Int
	if len(buf) == 0 {
		z.SetInt64(1000)
	} else {
		z.SetBytes(buf)
	}
	z.Add(&z, big.NewInt(1))
	return &z
}

func (r *Registry) issueCert(ctx context.Context, host string, certFile, keyFile string) error {

	var kid [16]byte
	n, err := rand.Reader.Read(kid[:])
	if n != len(kid) {
		return errlib.Newf("FOO-5813 short read %d", n)
	}

	pub, priv, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		return err
	}

	subject := &pkix.Name{
		CommonName: host,
	}

	t := time.Now().Add(-time.Second).Truncate(time.Minute)
	template := &x509.Certificate{
		AuthorityKeyId:        kid[:],
		SubjectKeyId:          kid[:],
		SerialNumber:          r.next(),
		BasicConstraintsValid: true,
		IsCA:                  true,
		MaxPathLen:            1,
		NotBefore:             t,
		NotAfter:              t.AddDate(0, 3, 0), // valid for 3 months
		Subject:               *subject,
		DNSNames:              []string{host},
	}

	rootpriv, err := r.readRootPriv()
	if err != nil {
		return err
	}

	buf, err := x509.CreateCertificate(
		rand.Reader,
		template,
		r.root,
		pub,
		rootpriv)
	if err != nil {
		return err
	}

	privbuf, err := x509.MarshalPKCS8PrivateKey(priv)
	if err != nil {
		return err
	}
	err = writePEM(
		keyFile,
		&pem.Block{
			Type:  "PRIVATE KEY",
			Bytes: privbuf,
		})

	return writePEM(
		certFile,
		&pem.Block{
			Type:  "CERTIFICATE",
			Bytes: buf,
		})
}

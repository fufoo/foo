package admin

import (
	"context"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/foo/internal/services/platform"
	"bitbucket.org/fufoo/foo/internal/tls"
	"bitbucket.org/fufoo/foo/services/admin"
	"bitbucket.org/fufoo/qrpc"
)

var log = logging.New("admin")

type Conn struct {
	xr     *platform.ExchangeRequest
	client admin.AdminQClient
	auth   platform.AuthQClient
}

type Config struct {
	TrustFile       string
	Server          string
	AuthServer      string
	AuthMechanism   string
	AuthIdentity    string
	AuthCredentials string
}

func Dial(ctx context.Context, cfg *Config) (*Conn, error) {
	t, err := tls.PlatformClient(tls.Trust(cfg.TrustFile))
	if err != nil {
		panic(err)
		return nil, err
	}
	ac, err := qrpc.Dial(ctx, cfg.AuthServer, qrpc.DialWithTLS(t))
	if err != nil {
		panic(err)
		return nil, err
	}

	c, err := qrpc.Dial(ctx, cfg.Server, qrpc.DialWithTLS(t))
	if err != nil {
		panic(err)
		return nil, err
	}

	auth := platform.NewAuthQClient(ac)

	xr := &platform.ExchangeRequest{
		Mechanism:   cfg.AuthMechanism,
		Identifier:  cfg.AuthIdentity,
		Credentials: cfg.AuthCredentials,
	}

	if err != nil {
		return nil, err
	}

	return &Conn{
		xr:     xr,
		auth:   auth,
		client: admin.NewAdminQClient(c),
	}, nil
}

func (c *Conn) AcquireDomain(ctx context.Context, req *admin.AcquireDomainRequest) (*admin.AcquireDomainResponse, error) {

	resp, err := c.auth.Exchange(ctx, c.xr)
	if err != nil {
		panic(err)
	}
	log.Debugf(ctx, "exchanged to: %s", resp.Token)
	r := *req
	r.Authz = &admin.Authz{
		Token: resp.Token.Token,
	}

	return c.client.AcquireDomain(ctx, &r)
}

func (c *Conn) DescribeDomain(ctx context.Context, req *admin.DescribeDomainRequest) (*admin.DescribeDomainResponse, error) {

	resp, err := c.auth.Exchange(ctx, c.xr)
	if err != nil {
		panic(err)
	}
	r := *req
	r.Authz = &admin.Authz{
		Token: resp.Token.Token,
	}

	return c.client.DescribeDomain(ctx, &r)
}

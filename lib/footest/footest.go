package footest

import (
	"context"
	"fmt"

	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/lib/sealed"
	"google.golang.org/protobuf/proto"
)

type entry struct {
	data []byte
}

type InMemory struct {
	cam map[ptr.Ptr]entry
	enc *sealed.SecureEncoding
}

func New() *InMemory {
	return &InMemory{
		cam: make(map[ptr.Ptr]entry),
		enc: sealed.NewInsecure(),
	}
}

var _ foo.WriteTxn = &InMemory{}

func (m *InMemory) Get(ctx context.Context, name string) (ptr.Ptr, error) {
	panic("not implemented")
}

func (m *InMemory) Load(ctx context.Context, src ptr.Ptr, dst sealed.Loader) (interface{}, error) {
	e, ok := m.cam[src]
	if !ok {
		return nil, fmt.Errorf("ptr {%s} not found in cam", src)
	}
	return m.enc.Decode(ctx, dst, e.data)
}

func (m *InMemory) Fill(ctx context.Context, src ptr.Ptr, dst proto.Message) error {
	panic("not implemented")
}

func (m *InMemory) WaitForChanges(ctx context.Context) ([]string, error) {
	panic("not implemented")
}

func (m *InMemory) SetAudit(audit uint64) {
	panic("not implemented")
}

func (m *InMemory) Set(ctx context.Context, name string, value ptr.Ptr) error {
	panic("not implemented")
}

func (m *InMemory) Store(ctx context.Context, item interface{}) (ptr.Ptr, error) {

	e := entry{}

	tp, err := foo.TypeFor(ctx, m, item)
	if err != nil {
		return ptr.Null, err
	}
	buf, err := m.enc.Encode(ctx, nil, tp, item, nil)
	if err != nil {
		return ptr.Null, err
	}
	e.data = buf
	p := ptr.EncryptedContentAddress(e.data)
	m.cam[p] = e
	return p, nil
}

func (m *InMemory) Commit(ctx context.Context) error {
	return nil
}

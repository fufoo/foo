package pki

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	//"crypto/x509/pkix"
	//"encoding/asn1"
	"encoding/pem"

	"bitbucket.org/dkolbly/logging"
	//"time"
)

var log = logging.New("pki")

// c.f. omen3 lib/pki

/*
func (e *Entity) TLSClientConfig(ctx context.Contex) *tls.Config {
	certPool := x509.NewCertPool()
	ca := e.certs[len(e.certs)-1]
	certPool.AddCert(ca)
	log.Debugf(ctx, "cert pool: %s", ca.Subject)
	client := tls.Certificate{
		PrivateKey: e.priv,
	}
	for _, c := range e.certs[:len(e.certs)-1] {
		client.Certificate = append(client.Certificate, c.Raw)
		log.Debugf(ctx, "supply chain: %s", c.Subject)
		if c.NotAfter.Before(time.Now()) {
			log.Errorf(ctx, "expires %s",
				c.NotAfter.Format("2006-01-02 15:04:05"))
		} else {
			log.Debugf(ctx, "expires %s",
				c.NotAfter.Format("2006-01-02 15:04:05"))
		}
	}

	return &tls.Config{
		Certificates: []tls.Certificate{client},
		RootCAs:      certPool,
	}
}
*/

/*func ClientConfig(ctx context.Context, present []byte, key []byte) *tls.Config {
	t := ServerConfig(ctx, present, key)

	// flip the polarity
	return &tls.Config{
		Certificates: t.Certificates,
		//PrivateKey:  t.PrivateKey,
		RootCAs: t.ClientCAs,
	}
}*/

func ServerConfig(ctx context.Context, present []byte, key []byte) *tls.Config {

	self := tls.Certificate{}

	depth := 0
	for len(present) > 0 {
		block, rest := pem.Decode(present)
		switch block.Type {
		case "CERTIFICATE":
			// parsing it just for debug (and early error
			// detection); the tls.Certificate contains
			// raw bytes
			if true {
				cert, err := x509.ParseCertificate(block.Bytes)
				if err != nil {
					panic(err)
				}
				if depth == 0 {
					log.Tracef(ctx, "presenting as: %s\n    issued by: %s",
						cert.Subject,
						cert.Issuer)
				} else {
					log.Tracef(ctx, "as assured by: %s\n    issued by: %s",
						cert.Subject,
						cert.Issuer)
				}
				depth++
			}
			self.Certificate = append(self.Certificate, block.Bytes)

		case "PRIVATE KEY":
			k, err := x509.ParsePKCS8PrivateKey(block.Bytes)
			if err != nil {
				panic(err)
			}
			self.PrivateKey = k

		case "EC PRIVATE KEY":
			k, err := x509.ParseECPrivateKey(block.Bytes)
			if err != nil {
				panic(err)
			}
			self.PrivateKey = k
		}

		present = rest
	}
	if self.PrivateKey == nil {
		b, rest := pem.Decode(key)
		if b == nil {
			if len(rest) == 0 {
				panic("no private key specified, either explicitly or in cert file")
			} else {
				panic(fmt.Sprintf("missing block (%d bytes remain)", len(rest)))
			}
		}

		if b.Type == "EC PRIVATE KEY" {
			private, err := x509.ParseECPrivateKey(b.Bytes)
			if err != nil {
				panic(err)
			}
			self.PrivateKey = private
		} else if b.Type == "PRIVATE KEY" {
			private, err := x509.ParsePKCS8PrivateKey(b.Bytes)
			if err != nil {
				panic(err)
			}
			self.PrivateKey = private
		} else {
			panic("unknown private key type: " + b.Type)
		}
	}

	return &tls.Config{
		//ClientAuth:   tls.VerifyClientCertIfGiven, //tls.RequireAndVerifyClientCert,
		Certificates: []tls.Certificate{self},
	}
}

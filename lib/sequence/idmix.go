package sequence

import (
	"math/bits"
	"math/rand"
)

type Key struct {
	schedule [16][16]uint8
}

func NewMix(seed uint64) *Key {
	return newmix(seed)
}

func newmix(seed uint64) *Key {
	k := &Key{}
	r := rand.New(rand.NewSource(int64(seed)))

	for i := 0; i < 16; i++ {
		for j := 0; j < 16; j++ {
			k.schedule[i][j] = uint8(j)
		}
		r.Shuffle(16, func(a, b int) {
			k.schedule[i][a], k.schedule[i][b] = k.schedule[i][b], k.schedule[i][a]
		})
		//fmt.Printf("    (%d) %x\n", i, k.schedule[i][:])
	}
	return k
}

func (k *Key) Obscure(a uint64) uint64 {
	if a == 0 {
		return 0
	}

	//a0 := a
	//fmt.Printf(" original: %064b\n", a)

	n := bits.LeadingZeros64(a) / 4
	//hibit := uint64(1) << (64 - n*4)
	//fmt.Printf("    hibit: %064b\n", hibit)

	var carry uint8

	for nib := 0; nib < (64/4 - n); nib++ {
		rplc := k.schedule[nib][(carry+uint8((a>>(nib*4))))&0xF]
		carry = rplc
		mask := uint64(0xF) << (nib * 4)
		a = (a &^ mask) + (uint64(rplc) << (nib * 4))
		//fmt.Printf("   nib[%2d]: %064b\n", nib, a)
	}
	//fmt.Printf("from -> %016x  %d\n", a0, a0)
	//fmt.Printf("  to -> %016x  %d\n", a|hibit, a)
	//fmt.Printf("\n")
	//fmt.Printf("\n")
	return a | (1 << (4 * (64/4 - n)))
}

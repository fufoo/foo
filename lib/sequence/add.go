package sequence

import (
	"context"

	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
)

type GetSetter interface {
	Get(context.Context, string) (ptr.Ptr, error)
	Set(context.Context, string, ptr.Ptr) error
}

// Add performs a read-modify-write within the transaction against an
// int-valued observable with the given (seq) name.  This is the
// simplest, uncache-aware (but transaction-aware) method to perform
// an increment.
func Next(ctx context.Context, wr GetSetter, seq string, initial uint64) (uint64, error) {

	state, err := wr.Get(ctx, seq)
	if err != nil {
		return 0, err
	}
	var next uint64

	if state.IsNull() {
		next = initial
	} else if !state.IsUint64() {
		return 0, errlib.Newf("FOO-1710 sequence %s has value {%s}, not a uint64",
			seq,
			state)
	} else {
		n := state.Uint64Value()
		if n >= (1<<64)-1 {
			return 0, errlib.Newf("FOO-1711 sequence %s is maxed out",
				seq)
		}
		next = n + 1
	}

	err = wr.Set(ctx, seq, ptr.Uint64(next))
	if err != nil {
		return 0, err
	}
	return next, nil
}

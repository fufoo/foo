package sequence

import (
	"context"
	"sync"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/foo/lib/special"
	"bitbucket.org/fufoo/foo/services/state"
)

var log = logging.New("seq")

type Issuer struct {
	states    state.StateQClient
	domain    uint64
	min       uint64
	name      string
	mix       *Key
	lock      sync.Mutex
	lastgrab  time.Time
	chunksize int
	remain    int
	nextid    uint64
	chunks    int
}

func New(ctx context.Context, states state.StateQClient, domain uint64, name string) *Issuer {
	return &Issuer{
		states:    states,
		domain:    domain,
		name:      name,
		chunksize: 1,
		min:       1,
	}
}

func (iss *Issuer) SetMin(min uint64) *Issuer {
	iss.min = min
	return iss
}

func (iss *Issuer) SetMix(seed uint64) *Issuer {
	iss.mix = newmix(seed)
	return iss
}

func (iss *Issuer) Next(ctx context.Context) uint64 {
	id := iss.nextUnmixed(ctx)
	if id != 0 && iss.mix != nil {
		id = iss.mix.Obscure(id)
	}
	return id
}

func (iss *Issuer) nextUnmixed(ctx context.Context) uint64 {
	iss.lock.Lock()
	defer iss.lock.Unlock()

	if iss.remain > 0 {
		iss.remain--
		id := iss.nextid
		log.Tracef(ctx, "issued[%s] %d ; %d remain", iss.name, id, iss.remain)
		return id
	}

	now := time.Now()
	if iss.chunks == 0 {
		log.Tracef(ctx, "first chunk")
	} else {
		if now.Sub(iss.lastgrab) < time.Second {
			log.Tracef(ctx, "%d chunks previously; grabbing another fast", iss.chunks)
			iss.chunksize *= 2
			if iss.chunksize > 1000 {
				iss.chunksize = 1000
			}
		} else {
			log.Tracef(ctx, "%d chunks previously; grabbing another slow", iss.chunks)
			iss.chunksize /= 2
			if iss.chunksize < 1 {
				iss.chunksize = 1
			}
		}
		log.Tracef(ctx, "chunk size now %d", iss.chunksize)
	}
	id, err := special.Add(ctx, iss.states, uint64(now.UnixNano()), iss.domain, iss.name, iss.min, int64(iss.chunksize))
	if err != nil {
		log.Warningf(ctx, "FLO-6201 could not effectuate special: %s", err)
		return 0
	}
	iss.nextid = id + 1
	iss.remain = iss.chunksize - 1

	log.Tracef(ctx, "issuing %d with %d remain", id, iss.remain)

	iss.lastgrab = now

	return id
}

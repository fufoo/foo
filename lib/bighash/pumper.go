package bighash

import (
	"context"

	"bitbucket.org/fufoo/core/ptr"
)

type Tuple struct {
	Key   ptr.Ptr
	Value ptr.Ptr
}

func (h *Hash) Pump(ctx context.Context) <-chan Tuple {
	ch := make(chan Tuple, 50)
	h.ensureloaded(ctx)
	go h.runpump(ctx, ch)
	return ch
}

func (h *Hash) runpump(ctx context.Context, dst chan<- Tuple) {
	defer close(dst)
	h.root.pump(ctx, h.in, dst)
}

func (b *leaf) pump(ctx context.Context, tx loader, dst chan<- Tuple) {
	err := b.ensureloaded(ctx, tx)
	if err != nil {
		log.Warningf(ctx, "BIG-3724 could not load: %s", err)
		return
	}

	for _, e := range b.entries {
		dst <- Tuple{
			Key:   e.key,
			Value: e.value,
		}
	}
}

func (f *fanout) pump(ctx context.Context, tx loader, dst chan<- Tuple) {
	err := f.ensureloaded(ctx, tx)
	if err != nil {
		log.Warningf(ctx, "BIG-3725 could not load: %s", err)
		return
	}
	for _, ch := range f.children {
		if ch != nil {
			ch.pump(ctx, tx, dst)
		}
	}
}

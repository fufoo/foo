package bighash

import (
	"context"
	"errors"
	"sort"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/foo"
)

var ErrKeyNotFound = errors.New("key not found")

var log = logging.New("bighash")

const (
	fanoutBits = 4
	bucketSize = 32
)

type Hash struct {
	in    loader
	rootp ptr.Ptr
	root  node
}

type opDisposition uint8

const (
	noChange opDisposition = iota
	insertAddedKey
	insertChangedValue
	deleteRemovedKey
)

type loader = foo.Loader

type storer = foo.LoadStorer

type node interface {
	flush(context.Context, storer) (ptr.Ptr, error)
	insert(context.Context, loader, *entry, uint) (node, opDisposition, error)
	get(context.Context, loader, *ptr.Ptr, uint) (*ptr.Ptr, error)
	remove(context.Context, loader, *ptr.Ptr, uint) (*ptr.Ptr, opDisposition, error)
	pump(context.Context, loader, chan<- Tuple)
	length(context.Context, loader) (uint64, error)
	cam() ptr.Ptr
}

type entry struct {
	key   ptr.Ptr
	value ptr.Ptr
}

type entrySlice []entry

func (l entrySlice) Len() int           { return len(l) }
func (l entrySlice) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
func (l entrySlice) Less(i, j int) bool { return l[i].key.Less(l[j].key) }

type leaf struct {
	p       ptr.Ptr
	loaded  bool
	entries []entry
}

func (l *leaf) markdirty() {
	// conditional is under assumption that zero'ing out 32 bytes is expensive;
	// TODO test that assumption
	if !l.p.IsNull() {
		l.p = ptr.Null
	}
}

func (f *fanout) markdirty() {
	// conditional is under assumption that zero'ing out 32 bytes is expensive;
	// TODO test that assumption
	if !f.p.IsNull() {
		f.p = ptr.Null
	}
}

type fanout struct {
	p      ptr.Ptr
	loaded bool
	count  uint64
	//bitOffset uint8
	bitCount uint8
	children []node
}

var _ node = &fanout{}

func (f *fanout) cam() ptr.Ptr {
	if f.p.IsNull() {
		panic("fanout was not flushed")
	}
	return f.p
}

func (l *leaf) cam() ptr.Ptr {
	if l.p.IsNull() {
		panic("fanout was not flushed")
	}
	return l.p
}

func (f *fanout) flush(ctx context.Context, wr storer) (ptr.Ptr, error) {
	if !f.p.IsNull() {
		// already flushed (or never modified)
		return f.p, nil
	}

	// flush all the children
	for _, ch := range f.children {
		if ch != nil {
			_, err := ch.flush(ctx, wr)
			if err != nil {
				return ptr.Null, err
			}
		}
	}

	p, err := wr.Store(ctx, f)
	if err != nil {
		return ptr.Null, err
	}
	f.p = p
	return p, nil
}

func (l *leaf) flush(ctx context.Context, wr storer) (ptr.Ptr, error) {
	if !l.p.IsNull() {
		// already flushed (or never modified)
		return l.p, nil
	}
	p, err := wr.Store(ctx, l)
	if err != nil {
		return ptr.Null, err
	}

	l.p = p
	return p, nil
}

func (l *leaf) get(ctx context.Context, rd loader, key *ptr.Ptr, _ uint) (*ptr.Ptr, error) {
	err := l.ensureloaded(ctx, rd)
	if err != nil {
		return nil, err
	}

	// TODO binary search
	for _, entry := range l.entries {
		if entry.key == *key {
			return &entry.value, nil
		}
	}
	return nil, nil
}

func New(txn loader) *Hash {
	return &Hash{
		in: txn,
		root: &leaf{
			loaded: true,
		},
	}
}

func Load(txn loader, p ptr.Ptr) *Hash {
	if p.IsNull() {
		return New(txn)
	}

	return &Hash{
		in:    txn,
		rootp: p,
	}
}

func (h *Hash) ensureloaded(ctx context.Context) error {
	if h.root != nil {
		return nil
	}
	root, err := h.in.Load(ctx, h.rootp, polymorphicLoader)
	if err != nil {
		return err
	}
	// stick the original root pointer back in there so we don't
	// have to re-serialize it if we don't need to
	if leaf, ok := root.(*leaf); ok {
		leaf.p = h.rootp
	} else if fanout, ok := root.(*fanout); ok {
		fanout.p = h.rootp
	}
	h.root = root.(node)
	return nil
}

func (h *Hash) Store(ctx context.Context, wr foo.LoadStorer) (ptr.Ptr, error) {
	h.ensureloaded(ctx)
	return h.root.flush(ctx, wr)
}

func (h *Hash) Has(ctx context.Context, key ptr.Ptr) (bool, error) {
	h.ensureloaded(ctx)
	p, err := h.root.get(ctx, h.in, &key, 0)
	if err != nil {
		return false, err
	}
	if p == nil {
		return false, nil
	} else {
		return true, nil
	}
}

func (h *Hash) Get(ctx context.Context, key ptr.Ptr) (ptr.Ptr, error) {
	h.ensureloaded(ctx)
	p, err := h.root.get(ctx, h.in, &key, 0)

	if err != nil {
		return ptr.Null, err
	}
	if p == nil {
		return ptr.Null, ErrKeyNotFound
	}
	return *p, nil
}

func (h *Hash) Remove(ctx context.Context, key ptr.Ptr) (*ptr.Ptr, error) {
	h.ensureloaded(ctx)
	p, _, err := h.root.remove(ctx, h.in, &key, 0)
	if err != nil {
		return nil, err
	}
	if p == nil {
		return nil, ErrKeyNotFound
	}
	return p, nil
}

func (h *Hash) Insert(ctx context.Context, key, value ptr.Ptr) error {
	h.ensureloaded(ctx)
	r, _, err := h.root.insert(ctx, h.in, &entry{key: key, value: value}, 0)
	if err != nil {
		return err
	}
	h.root = r
	return nil
}

func (l *leaf) remove(ctx context.Context, rd loader, key *ptr.Ptr, pre uint) (*ptr.Ptr, opDisposition, error) {
	err := l.ensureloaded(ctx, rd)
	if err != nil {
		return nil, noChange, err
	}

	// TODO binary search
	for i, e := range l.entries {
		if e.key == *key {
			/*log.Debugf(ctx, "found at slot [%d]", i)

			for k, e := range l.entries {
				log.Debugf(ctx, "  [%d] {%s} {%s}", k, e.key, e.value)
			}*/

			copy(l.entries[i:], l.entries[i+1:])
			l.entries = l.entries[:len(l.entries)-1]

			/*log.Debugf(ctx, "AFTER:")
			for k, e := range l.entries {
				log.Debugf(ctx, "  [%d] {%s} {%s}", k, e.key, e.value)
			}*/
			l.markdirty()
			return &e.value, deleteRemovedKey, nil
		}
	}
	return nil, noChange, ErrKeyNotFound
}

func (l *leaf) insert(ctx context.Context, rd loader, ins *entry, pre uint) (node, opDisposition, error) {

	err := l.ensureloaded(ctx, rd)
	if err != nil {
		return l, noChange, err
	}

	// if it's already here, just change the value
	// TODO binary search
	for i, e := range l.entries {
		if e.key == ins.key {
			if e.value == ins.value {
				// no change at all
				return l, noChange, nil
			}
			l.markdirty()
			l.entries[i].value = ins.value
			return l, insertAddedKey, nil
		}
	}

	if len(l.entries) >= bucketSize {

		var fan node = l.split(ctx, pre)
		fan, _, err = fan.insert(ctx, rd, ins, pre)
		if err != nil {
			return l, noChange, err
		}
		return fan, insertAddedKey, nil
	}

	l.markdirty()
	l.entries = append(l.entries, *ins)
	// TODO insertion sort (since we did binary search earlier :lol:)
	sort.Sort(entrySlice(l.entries))
	return l, insertAddedKey, nil
}

func (h *Hash) Len(ctx context.Context) (uint64, error) {
	h.ensureloaded(ctx)
	return h.root.length(ctx, h.in)
}

func (l *leaf) length(ctx context.Context, rd loader) (uint64, error) {
	err := l.ensureloaded(ctx, rd)
	if err != nil {
		return 0, err
	}
	return uint64(len(l.entries)), nil
}

func (l *leaf) split(ctx context.Context, prefix uint) *fanout {
	// slice another fanoutBits off the prefix
	var sub [1 << fanoutBits]*leaf
	children := make([]node, 1<<fanoutBits)

	for _, entry := range l.entries {
		portion := bitSlice(entry.key.Bits[:], prefix, prefix+fanoutBits)
		l := sub[portion]
		if l == nil {
			l = &leaf{
				loaded: true,
			}
			sub[portion] = l
			children[portion] = l
		}
		l.entries = append(l.entries, entry)
	}
	return &fanout{
		loaded:   true,
		children: children,
		//bitOffset: uint8(prefix),
		bitCount: fanoutBits,
		count:    uint64(len(l.entries)),
	}
}

func (f *fanout) get(ctx context.Context, rd loader, k *ptr.Ptr, pre uint) (*ptr.Ptr, error) {
	err := f.ensureloaded(ctx, rd)
	if err != nil {
		return nil, err
	}

	branch := bitSlice(k.Bits[:], pre, pre+uint(f.bitCount))
	ch := f.children[branch]
	if ch == nil {
		return nil, nil
	}
	return ch.get(ctx, rd, k, pre+uint(f.bitCount))
}

func (f *fanout) length(ctx context.Context, rd loader) (uint64, error) {
	err := f.ensureloaded(ctx, rd)
	if err != nil {
		return 0, err
	}
	return f.count, nil
}

func (f *fanout) insert(ctx context.Context, rd loader, ins *entry, pre uint) (node, opDisposition, error) {
	err := f.ensureloaded(ctx, rd)
	if err != nil {
		return f, noChange, err
	}

	branch := bitSlice(ins.key.Bits[:], pre, pre+uint(f.bitCount))

	next := f.children[branch]
	if next == nil {
		next = &leaf{
			loaded: true,
		}
	}
	n, disposition, err := next.insert(ctx, rd, ins, pre+uint(f.bitCount))
	if err != nil {
		return f, noChange, err
	}
	if disposition == noChange {
		return f, noChange, nil
	}
	f.children[branch] = n
	f.count++
	f.markdirty()
	return f, insertAddedKey, nil
}

func (f *fanout) remove(ctx context.Context, rd loader, key *ptr.Ptr, pre uint) (*ptr.Ptr, opDisposition, error) {
	err := f.ensureloaded(ctx, rd)
	if err != nil {
		return nil, noChange, err
	}

	branch := bitSlice(key.Bits[:], pre, pre+uint(f.bitCount))

	next := f.children[branch]
	if next == nil {
		return nil, noChange, nil
	}
	v, disposition, err := next.remove(ctx, rd, key, pre+uint(f.bitCount))
	if err != nil {
		return nil, noChange, err
	}
	if disposition == noChange {
		return nil, noChange, nil
	}

	f.count--
	// TODO handle compaction? (i.e., if f.count <= fanout size)

	f.markdirty()
	return v, deleteRemovedKey, nil
}

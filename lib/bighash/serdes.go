package bighash

import (
	"bufio"
	"bytes"
	"context"
	"encoding/binary"
	"io"

	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/lib/sealed"
)

var leafType = ptr.String("foo:bighash.leaf/1")
var fanoutType = ptr.String("foo:bighash.fanout/1")

// note that these have to be the exact type, because we need to implement foo.TypeIssuer
// verify that at compile time:
var _ foo.TypeIssuer = (*leaf)(nil)
var _ foo.TypeIssuer = (*fanout)(nil)

func (l *leaf) IssueType(context.Context, foo.ReadTxn) (ptr.Ptr, error) {
	return leafType, nil
}

func (f *fanout) IssueType(context.Context, foo.ReadTxn) (ptr.Ptr, error) {
	return fanoutType, nil
}

func (l *leaf) MarshalCAM(tp *ptr.Ptr) (sealed.Marshaled, error) {
	// pass 1, find all the pointers
	index := make(map[ptr.Ptr]uint16)

	for _, entry := range l.entries {
		if entry.key.IsLoadable() {
			index[entry.key] = 0
		}
		if entry.value.IsLoadable() {
			index[entry.value] = 0
		}
	}
	// assign them indices
	ptrs := sealed.AssignPointerIndices(index)

	// output the data
	var buf bytes.Buffer

	var tmp [10]byte

	n := binary.PutUvarint(tmp[:], uint64(len(l.entries)))
	buf.Write(tmp[:n])

	for _, entry := range l.entries {
		kl := entry.key.IsLoadable()
		vl := entry.value.IsLoadable()
		var preamble uint8
		if kl {
			preamble |= 1
		}
		if vl {
			preamble |= 2
		}
		buf.WriteByte(preamble)
		if kl {
			n := binary.PutUvarint(tmp[:], uint64(index[entry.key]))
			buf.Write(tmp[:n])
		} else {
			buf.Write(entry.key.Bits[:])
		}
		if vl {
			n := binary.PutUvarint(tmp[:], uint64(index[entry.value]))
			buf.Write(tmp[:n])
		} else {
			buf.Write(entry.value.Bits[:])
		}
	}
	return sealed.Marshaled{
		Payload: buf.Bytes(),
		Index:   ptrs,
		Flags:   sealed.DeflateCompression,
	}, nil
}

func (l *leaf) loader(ctx context.Context, tp *ptr.Ptr, index []ptr.Ptr, src *bufio.Reader) (interface{}, error) {
	if *tp != leafType {
		panic("rats")
		return nil, errlib.Newf("BIG-3703 unexpected type {%s} for leaf", *tp)
	}

	n, err := binary.ReadUvarint(src)
	if err != nil {
		panic(err)
		return nil, errlib.Wrap("BIG-3704", err)
	}

	l.loaded = true
	l.entries = make([]entry, n)
	for i := range l.entries {
		pre, err := src.ReadByte()
		if err != nil {
			return nil, errlib.Wrap("BIG-3708", err)
		}
		if pre&1 != 0 { // key is loadable
			j, err := binary.ReadUvarint(src)
			if err != nil {
				return nil, errlib.Wrap("BIG-3705", err)
			}
			l.entries[i].key = index[j]
		} else {
			n, err := src.Read(l.entries[i].key.Bits[:])
			if err != nil {
				return nil, errlib.Wrap("BIG-3706", err)
			}
			if n != ptr.Size {
				return nil, errlib.Newf("BIG-3707 only read %d for ptr", n)
			}
		}
		if pre&2 != 0 { // value is loadable
			j, err := binary.ReadUvarint(src)
			if err != nil {
				return nil, errlib.Wrap("BIG-3715", err)
			}
			l.entries[i].value = index[j]
		} else {
			n, err := src.Read(l.entries[i].value.Bits[:])
			if err != nil {
				return nil, errlib.Wrap("BIG-3716", err)
			}
			if n != ptr.Size {
				return nil, errlib.Newf("BIG-3717 only read %d for ptr", n)
			}
		}
	}
	return l, nil
}

func (l *leaf) ensureloaded(ctx context.Context, rd loader) error {
	if l.loaded {
		return nil
	}
	_, err := rd.Load(ctx, l.p, l.loader)
	if err != nil {
		return err
	}
	return nil
}

func (f *fanout) MarshalCAM(tp *ptr.Ptr) (sealed.Marshaled, error) {
	// pass 1, find all the pointers
	index := make(map[ptr.Ptr]uint16)

	for _, ch := range f.children {
		if ch != nil {
			index[ch.cam()] = 0
		}
	}
	// assign them indices
	ptrs := sealed.AssignPointerIndices(index)

	var tmp [20 + 10*(1<<fanoutBits)]byte

	k := 0
	k += binary.PutUvarint(tmp[k:], uint64(f.bitCount))
	k += binary.PutUvarint(tmp[k:], f.count)

	for i, ch := range f.children {
		if ch == nil {
			continue
		}

		var x uint64

		// in the lowest bit, encode whether it is a leaf or a fanout
		_, isleaf := ch.(*leaf)
		if isleaf {
			x += 1
		}
		// in the next f.bitCount bits, encode the index of the child
		x += uint64(i) << 1

		// then, encode the index of the ptr
		x += uint64(index[ch.cam()]) << (1 + f.bitCount)

		k += binary.PutUvarint(tmp[k:], x)
	}
	return sealed.Marshaled{
		Payload: tmp[:k],
		Index:   ptrs,
		Flags:   sealed.DeflateCompression,
	}, nil
}

func (f *fanout) loader(ctx context.Context, tp *ptr.Ptr, index []ptr.Ptr, src *bufio.Reader) (interface{}, error) {
	if *tp != fanoutType {
		return nil, errlib.Newf("BIG-3713 unexpected type {%s} for fanout", *tp)
	}

	fanoutBits, err := binary.ReadUvarint(src)
	if err != nil {
		panic(err)
		return nil, errlib.Wrap("BIG-3714", err)
	}

	count, err := binary.ReadUvarint(src)
	if err != nil {
		panic(err)
		return nil, errlib.Wrap("BIG-3716", err)
	}

	f.count = count
	f.loaded = true
	f.bitCount = uint8(fanoutBits)
	f.children = make([]node, 1<<f.bitCount)

	indexMask := ((uint64(1) << f.bitCount) - 1) << 1

	for {
		item, err := binary.ReadUvarint(src)
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, errlib.Wrap("BIG-3705", err)
		}
		isleaf := (item & 1) != 0
		branch := (item & indexMask) >> 1
		k := item >> (1 + f.bitCount)

		var child node

		if isleaf {
			child = &leaf{
				p: index[k],
			}
		} else {
			child = &fanout{
				p: index[k],
			}
		}
		f.children[branch] = child
	}
	return f, nil
}

func (f *fanout) ensureloaded(ctx context.Context, rd loader) error {
	if f.loaded {
		return nil
	}
	_, err := rd.Load(ctx, f.p, f.loader)
	if err != nil {
		return err
	}
	return nil
}

func polymorphicLoader(ctx context.Context, tp *ptr.Ptr, index []ptr.Ptr, src *bufio.Reader) (interface{}, error) {
	if tp == nil {
		return nil, errlib.Newf("BIG-3781 missing type")
	}
	if *tp == leafType {
		l := &leaf{}
		return l.loader(ctx, tp, index, src)
	} else if *tp == fanoutType {
		f := &fanout{}
		return f.loader(ctx, tp, index, src)
	} else {
		return nil, errlib.Newf("BIG-3782 unexpected type {%s}", *tp)
	}
}

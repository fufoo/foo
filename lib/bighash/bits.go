package bighash

/*import (
	"fmt"
)
*/
/*
func bit(buf []byte, i uint) int {
	if (buf[i/8]<<(i%8))&0x80 == 0 {
		return 0
	} else {
		return 1
	}
}
*/

// bitSlice() is only responsible for bit widths that we would
// expect in a fanout, which is at most 16 bits.  Hence, it need
// access at most 3 bytes of the buffer
func bitSlice(buf []byte, i, j uint) (ret uint64) {
	ibyte := i / 8
	jbyte := (j - 1) / 8

	var accum uint32

	switch jbyte - ibyte {
	case 0:
		accum = uint32(buf[ibyte])
	case 1:
		accum = (uint32(buf[ibyte]) << 8) + uint32(buf[ibyte+1])
	case 2:
		accum = (uint32(buf[ibyte]) << 16) +
			(uint32(buf[ibyte+1]) << 8) +
			uint32(buf[ibyte+2])
	default:
		panic("unexpected bit width")
	}

	shift := ((8 - j) & 7)
	mask := (uint32(1) << (j - i)) - 1

	/*fmt.Printf(" bitSlice( %2d , %2d ) : accum %06x shift %d mask %#x\n",
	i,
	j,
	accum,
	shift,
	mask)*/
	return uint64(mask & (accum >> shift))
	/*
		for k := i; k < j; k++ {
			ret = (ret << 1) + uint64(bit(buf, k))
		}
		return
	*/
}

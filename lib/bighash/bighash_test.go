package bighash

import (
	"context"
	"encoding/hex"
	"fmt"
	//"math/rand"
	"testing"

	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/footest"
)

func hexPtr(s string) (p ptr.Ptr) {
	data, err := hex.DecodeString(s)
	if err != nil {
		panic(err)
	}
	if len(data) != ptr.Size {
		panic("bad length")
	}
	copy(p.Bits[:], data)
	if !p.IsValid() {
		panic("not really valid")
	}
	return
}

var x = hexPtr("888888888888888888888888888888888888888888888888888888888888881c")
var y = hexPtr("000000000000000000000000000000000000000000000000000000000000001c")
var z = hexPtr("ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff1c")

func (h *Hash) MustLen(ctx context.Context) uint64 {
	n, err := h.Len(ctx)
	if err != nil {
		panic(err)
	}
	return n
}

func (h *Hash) MustInsert(ctx context.Context, k, v ptr.Ptr) {
	err := h.Insert(ctx, k, v)
	if err != nil {
		panic(err)
	}
}

func TestHashInsert(t *testing.T) {
	c := footest.New()
	ctx := context.Background()

	h := New(c)

	h.MustInsert(ctx, x, z)
	if h.MustLen(ctx) != 1 {
		t.Fatalf("Expected len=1, got %d", h.MustLen(ctx))
	}

	h.MustInsert(ctx, y, z)
	if h.MustLen(ctx) != 2 {
		t.Fatalf("Expected len=2, got %d", h.MustLen(ctx))
	}
}

func TestHashInsertLots(t *testing.T) {
	c := footest.New()
	ctx := context.Background()

	h := New(c)

	for i := 0; i < 10000; i++ {
		k := synth("key:%d", i)
		v := synth("value:%d", i)

		h.Insert(ctx, k, v)
		if h.MustLen(ctx) != uint64(i)+1 {
			//h.printout(ctx)
			t.Fatalf("Expected len=%d, got %d", i+1, h.MustLen(ctx))
		}
	}
	//h.printout(ctx)
}

func testSerdes(t *testing.T, fillup func(context.Context, *Hash, map[ptr.Ptr]ptr.Ptr)) {

	ctx := context.Background()
	txn := footest.New()

	stored := make(map[ptr.Ptr]ptr.Ptr)

	// build the store
	h := New(txn)
	fillup(ctx, h, stored)

	// flush it to CAM
	p, err := h.Store(ctx, txn)
	if err != nil {
		t.Fatal(err) //
	}
	if h.MustLen(ctx) != uint64(len(stored)) {
		t.Errorf("Expected len=%d, got %d", len(stored), h.MustLen(ctx))
	}

	// load it back up
	h2 := Load(txn, p)

	// verify its contents
	if h.MustLen(ctx) != h2.MustLen(ctx) {
		t.Fatalf("Expected len=%d, got %d", h.MustLen(ctx), h2.MustLen(ctx))
	}

	count := 0
	for tuple := range h2.Pump(ctx) {
		count++
		k := tuple.Key
		v := tuple.Value
		if stored[k] != v {
			t.Errorf("Expected key {%s} to have {%s} but has {%s}", k, stored[k], v)
		}
	}
	if count != len(stored) {
		t.Errorf("Expected to retrieve %d items, got %d", len(stored), count)
	}

	// probe explicitly this time
	for k, v := range stored {
		v2, err := h.Get(ctx, k)
		if err != nil {
			t.Error(err)
		} else if v2 != v {
			t.Errorf("Expected key {%s} to have {%s} but has {%s}", k, v, v2)
		}
	}
}

func TestSerdesEmpty(t *testing.T) {
	testSerdes(t, func(context.Context, *Hash, map[ptr.Ptr]ptr.Ptr) {})
}

func TestSerdesLeaf(t *testing.T) {
	testSerdes(t, func(ctx context.Context, h *Hash, ix map[ptr.Ptr]ptr.Ptr) {
		for i := 0; i < 10; i++ {
			k := fmt.Sprintf("K %d", i)
			v := fmt.Sprintf("V %d", i)
			ix[ptr.String(k)] = ptr.String(v)
			h.MustInsert(ctx, ptr.String(k), ptr.String(v))
		}
	})
}

func TestSerdesBig(t *testing.T) {
	testSerdes(t, func(ctx context.Context, h *Hash, ix map[ptr.Ptr]ptr.Ptr) {
		for i := 0; i < 1000; i++ {
			k := fmt.Sprintf("K %d", i)
			v := fmt.Sprintf("V %d", i)
			ix[ptr.String(k)] = ptr.String(v)
			h.MustInsert(ctx, ptr.String(k), ptr.String(v))
		}
	})
}

func TestHashInsertGet(t *testing.T) {
	c := footest.New()
	ctx := context.Background()

	h := New(c)

	h.MustInsert(ctx, x, z)
	if h.MustLen(ctx) != 1 {
		t.Fatalf("Expected len=1, got %d", h.MustLen(ctx))
	}

	h.MustInsert(ctx, y, z)
	if h.MustLen(ctx) != 2 {
		t.Fatalf("Expected len=2, got %d", h.MustLen(ctx))
	}

	ok, err := h.Has(ctx, x)
	if err != nil {
		t.Fatal(err)
	}
	if !ok {
		t.Fatalf("Expected to find value for key {%s}, but didn't", x)
	}

	v, err := h.Get(ctx, x)
	if err != nil {
		t.Fatal(err)
	}
	if v != z {
		t.Fatalf("Expected to find value {%s}, found {%s}", z, v)
	}
}

func TestHashInsertRemove(t *testing.T) {
	c := footest.New()
	ctx := context.Background()

	h := New(c)

	h.MustInsert(ctx, x, z)
	if h.MustLen(ctx) != 1 {
		t.Fatalf("Expected len=1, got %d", h.MustLen(ctx))
	}

	h.MustInsert(ctx, y, z)
	if h.MustLen(ctx) != 2 {
		t.Fatalf("Expected len=2, got %d", h.MustLen(ctx))
	}

	p, err := h.Remove(ctx, x)
	if err != nil {
		t.Fatal(err)
	}
	if p == nil {
		t.Fatalf("Expected to remove item, didn't")
	}
	if *p != z {
		t.Fatalf("Expected to remove correct item {%s}, removed {%s}", z, *p)
	}
	if h.MustLen(ctx) != 1 {
		t.Fatalf("Expected len=1, got %d", h.MustLen(ctx))
	}

	// try to remove the same key again; should fail
	p, err = h.Remove(ctx, x)
	if err != nil {
		if err != ErrKeyNotFound {
			t.Fatal(err)
		}
	} else {
		t.Fatalf("Expected remove to fail, but didn't")
	}

	if h.MustLen(ctx) != 1 {
		t.Fatalf("Expected len=1, got %d", h.MustLen(ctx))
	}

	// now remove y
	p, err = h.Remove(ctx, y)
	if p == nil {
		t.Fatalf("Expected to remove item, didn't")
	}
	if *p != z {
		t.Fatalf("Expected to remove correct item, removed %s", *p)
	}
	if h.MustLen(ctx) != 0 {
		t.Fatalf("Expected len=0, got %d", h.MustLen(ctx))
	}
}

func TestHashDontInsertTwice(t *testing.T) {
	c := footest.New()
	ctx := context.Background()

	h := New(c)

	h.MustInsert(ctx, x, z)
	if h.MustLen(ctx) != 1 {
		t.Fatalf("Expected len=1, got %d", h.MustLen(ctx))
	}

	h.MustInsert(ctx, x, y)
	if h.MustLen(ctx) != 1 {
		t.Fatalf("Expected len=1, got %d", h.MustLen(ctx))
	}
	v, err := h.Get(ctx, x)
	if err != nil {
		t.Fatal(err)
	}
	if v != y {
		t.Fatalf("Expected to retrieve latest value, got %s", v)
	}
}

/*
func TestHashInsertLotsMore(t *testing.T) {
	c := footest.New()
	ctx := context.Background()

	h := New(c)

	var index []int

	// 1.84 seconds for 100K inserts (insert rate (per sec): 5400)
	for i := 0; i < 100000; i++ {
		k := synth("key:%d", i)
		v := synth("value:%d", i)

		h = h.Insert(ctx, k, v)
		if h.MustLen(ctx) != uint64(i)+1 {
			t.Fatalf("Expected len=%d, got %d", i+1, h.MustLen(ctx))
		}
		index = append(index, i)
	}

	// look them up
	r := rand.New(rand.NewSource(123))
	r.Shuffle(len(index), func(i, j int) {
		index[i], index[j] = index[j], index[i]
	})

	for _, i := range index {
		k := synth("key:%d", i)
		v := synth("value:%d", i)
		if v2, ok := h.Get(ctx, k); ok {
			if v2 != v {
				t.Fatalf("Expected to get %s but got %s", v, v2)
			}
		} else {
			t.Fatalf("Expected to get %s but got nothing", v)
		}
	}

	//h.printout(ctx)
}
*/

// test synthetic worst-case behavior by not actually hashing
func TestHashInsertDeep(t *testing.T) {
	c := footest.New()
	ctx := context.Background()

	h := New(c)
	buildDeep(ctx, t, h, make(map[ptr.Ptr]ptr.Ptr))
	//h.printout(ctx)
}

func buildDeep(ctx context.Context, t *testing.T, h *Hash, toc map[ptr.Ptr]ptr.Ptr) {
	k0 := ptr.Int64(0)
	k1 := ptr.Int64(1)

	// but make them the same in the first 16 bits
	k1.Bits[0] = k0.Bits[0]
	k1.Bits[1] = k0.Bits[1]

	for i := 0; i < 256; i++ {
		// note that we are formulating specifically bad keys here
		k := k0
		k.Bits[29] = 0x01
		k.Bits[30] = uint8(i)
		v := synth("value:%d", i)

		h.MustInsert(ctx, k, v)
		if h.MustLen(ctx) != uint64(2*i)+1 {
			t.Fatalf("Expected len=%d, got %d", 2*i+1, h.MustLen(ctx))
		}
		toc[k] = v

		// drill down a second deep path
		k = k1
		k.Bits[29] = 0x01
		k.Bits[30] = uint8(i)

		h.MustInsert(ctx, k, v)
		if h.MustLen(ctx) != uint64(2*i)+2 {
			t.Fatalf("Expected len=%d, got %d", 2*i+2, h.MustLen(ctx))
		}
		toc[k] = v
	}
}

func TestSerdesDeep(t *testing.T) {
	testSerdes(t, func(ctx context.Context, h *Hash, toc map[ptr.Ptr]ptr.Ptr) {
		buildDeep(ctx, t, h, toc)
	})
}

func synth(form string, args ...interface{}) ptr.Ptr {
	return ptr.String(fmt.Sprintf(form, args...))
}

type printouter interface {
	printout(ctx context.Context, depth int, index int, prefix int)
}

func (h Hash) printout(ctx context.Context) {
	h.root.(printouter).printout(ctx, 0, 0, 0)
}

func (b *leaf) printout(ctx context.Context, d, i, pre int) {
	in := indent(d)
	fmt.Printf("%s[%d] Leaf at [%d:] with %d entries:\n", in, i, pre, len(b.entries))

	for j, e := range b.entries {
		fmt.Printf("%s  [%d] %x\n", in, j, e.key.Bits[:])
	}
}

func (f *fanout) printout(ctx context.Context, d, i, pre int) {
	fmt.Printf("%s[%d] Fanout [%d:%d] containing %d\n",
		indent(d), i, pre, pre+int(f.bitCount),
		f.count)
	for i, entry := range f.children {
		if entry != nil {
			p := entry.(printouter)
			p.printout(ctx, d+1, i, pre+int(f.bitCount))
		}
	}
}

func indent(k int) string {
	return "                                                                                                                                                                                                                                                                "[:k]
}

package special

import (
	"context"

	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/foo/services/state"
)

func Add(ctx context.Context, states state.StateQClient, audit uint64, domain uint64, seq string, first uint64, delta int64) (uint64, error) {
	incr := &state.Increment{
		Slot:  0,
		Delta: delta,
		First: int64(first),
	}
	spec := &state.Special{
		Op: "incr",
		Info: &state.Special_Incr{
			Incr: incr,
		},
	}
	txn := &state.TxnRequest{
		Domain:   domain,
		Audit:    audit,
		Subjects: []string{seq},
		Specials: []*state.Special{
			spec,
		},
	}
	resp, err := states.Apply(ctx, txn)
	if err != nil {
		return 0, errlib.Wrap("FLO-6151", err)
	}
	p := resp.States[0].Value.Must()
	if err != nil {
		return 0, errlib.Wrap("FLO-6152", err)
	}
	return p.IntValue().Uint64(), nil
}

func Incr(ctx context.Context, states state.StateQClient, audit uint64, domain uint64, seq string, first uint64) (uint64, error) {
	return Add(ctx, states, audit, domain, seq, first, 1)
}

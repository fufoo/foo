package special

import (
	"context"
	"math/big"

	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/services/core"
	"bitbucket.org/fufoo/foo/services/state"
)

type Handler interface {
	Apply(states []*state.Obs) ([]int, error)
}

func Compile(ctx context.Context, eff *state.Special, in *state.TxnRequest) (Handler, error) {
	switch eff.Op {
	case "incr":
		return incrHandler{eff.GetIncr()}, nil
	default:
		return nil, errlib.Newf("FLO-6299 unknown special op %q", eff.Op)
	}
}

type incrHandler struct {
	incr *state.Increment
}

func (h incrHandler) Apply(state []*state.Obs) ([]int, error) {
	var x *big.Rat
	k := int(h.incr.Slot)

	old := state[k].Value
	if old == nil {
		x = big.NewRat(h.incr.First, 1)
	} else {
		p := old.Must()
		if !p.IsRat() {
			return nil, errlib.Newf("FLO-6321 existing value {%s} is not a rat", p)
		}
		x = p.RatValue()
		x.Add(x, big.NewRat(h.incr.Delta, 1))
	}
	state[k].Value = core.From(ptr.Rat(x))
	return []int{k}, nil
}

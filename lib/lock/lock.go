package lock

import (
	"context"
	"encoding/binary"
	"errors"
	"math/big"
	"math/rand"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/foo"
)

var log = logging.New("lock")

type Ticket struct {
	value    ptr.Ptr
	domain   *foo.Domain
	name     string
	held     bool
	acquired time.Time
}

func (t *Ticket) IsValid() bool {
	if !t.value.IsBytes() {
		return false
	}
	b := t.value.BytesValue()
	return b[ptr.MaxStringLength-1] == 'L'
}

func suffix(b []byte) string {
	var z big.Int
	z.SetBytes(b[8 : ptr.MaxStringLength-1])
	return z.Text(62)
}

func expires(b []byte) time.Time {
	ns := binary.BigEndian.Uint64(b[0:8])
	return time.Unix(0, int64(ns))
}

func (t *Ticket) Expires() time.Time {
	return expires(t.value.BytesValue())
}

func (t *Ticket) String() string {
	b := t.value.BytesValue()
	return expires(b).Format(time.RFC3339Nano) + "/" + suffix(b)
}

func New(lease time.Duration) *Ticket {
	ns := time.Now().Add(lease).UnixNano()
	var buf [ptr.MaxStringLength]byte

	binary.BigEndian.PutUint32(buf[8:12], rand.Uint32())
	binary.BigEndian.PutUint32(buf[ptr.MaxStringLength-8:], rand.Uint32())
	buf[ptr.MaxStringLength-1] = 'L'
	binary.BigEndian.PutUint64(buf[0:8], uint64(ns))

	return &Ticket{
		value: ptr.Bytes(buf[:]),
	}
}

// Release as part of a different transaction
func (t *Ticket) ReleaseIn(ctx context.Context, wr foo.WriteTxn) error {
	t.held = false // TODO how to deal with the txn we're in rolling back?
	return wr.Set(ctx, t.name, ptr.Null)
}

func (t *Ticket) Release(ctx context.Context) error {
	if !t.held {
		// duplicate release
		return nil
	}

	err := t.domain.Update(ctx, func(ctx context.Context, wr foo.WriteTxn) (err error) {
		return wr.Set(ctx, t.name, ptr.Null)
	})
	if err == nil {
		t.held = false
	}
	return err
}

func Acquire(ctx context.Context, d *foo.Domain, name string, lease time.Duration) (*Ticket, error) {

	var t *Ticket

	for {
		alreadyHeld := false
		var wait time.Duration
		var now time.Time

		err := d.Update(ctx, func(ctx context.Context, wr foo.WriteTxn) (err error) {
			t = New(lease)
			log.Debugf(ctx, "trying with {%s}=[%s]", t.value, t)

			var ex Ticket

			ex.value, err = wr.Get(ctx, name)
			if err != nil {
				return err
			}
			attempt := false

			if ex.value.IsNull() {
				log.Debugf(ctx, "trying to acquire lock: [%s]", t)
				attempt = true

			}
			if !ex.IsValid() {
				log.Debugf(ctx, "existing state is not a valid ticket {%s}", ex.value)
				// try to take it over
				attempt = true
			} else {
				// how long until the lease expires on the existing lock?
				lx := time.Until(ex.Expires())

				if lx > 0 {
					log.Debugf(ctx, "existing lock {%s} still held: [%s] but will expire in %s",
						ex.value,
						&ex,
						lx)
					wait = lx
				} else {
					log.Debugf(ctx, "existing lock {%s} held: [%s] but expired %s ago",
						ex.value,
						&ex,
						-lx)
					attempt = true
				}
				/*
					ctx2 = context.WithTimeout(ctx, dt)
					_, err = wr.WaitForChanges(ctx)
					if err != nil {
						return err
					}
					log.Debugf(ctx, "existing lock still held: [%s]", t)
					stillHeld = true
				*/
			}
			if attempt {
				now = time.Now()
				return wr.Set(ctx, name, t.value)
			} else {
				alreadyHeld = true
				return nil
			}
		})
		if err != nil {
			var x *foo.ErrConflict
			if errors.As(err, &x) {
				log.Debugf(ctx, "acquisition conflict... trying again")
				time.Sleep(30 * time.Millisecond)
			} else {
				log.Warningf(ctx, "acquisition error %T : %s", err, err)
				return nil, err
			}
		} else if alreadyHeld {
			if wait > 100*time.Millisecond {
				wait = 100 * time.Millisecond
			} else {
				wait = wait.Round(time.Millisecond) + 10*time.Millisecond
			}
			log.Debugf(ctx, "already held; waiting and trying again in %s", wait)
			time.Sleep(wait)
		} else {
			log.Debugf(ctx, "acquired successfully")

			t.domain = d
			t.name = name
			t.held = true
			t.acquired = now
			return t, nil
		}
	}
}

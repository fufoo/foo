package bigtree

import (
	"fmt"
	"io"
)

func (t *Tree) Debug(dst io.Writer) error {
	fmt.Fprintf(dst, "minLoad=%d maxLoad=%d\n", t.minLoad, t.maxLoad)

	var recur func(n *node, prefix string)

	recur = func(n *node, prefix string) {

		fmt.Fprintf(dst, "%sstatus=%b", prefix, n.status)
		if !t.root.p.IsNull() {
			fmt.Fprintf(dst, " ptr={%s}", n.p)
		}
		fmt.Fprintf(dst, "\n")

		if n.status&isInterior == 0 {
			for i, k := range n.keys {
				fmt.Fprintf(dst, "%s [%d]  key={%s} value={%s}\n", prefix, i, k, n.values[i])
			}
		} else {
			for i, k := range n.keys {
				fmt.Fprintf(dst, "%s (count=%d:)\n", prefix, n.children[i].count)
				recur(n.children[i].sub, prefix+"  ")
				fmt.Fprintf(dst, "%s [%d]  -- key={%s} --\n", prefix, i, k)
			}
			fmt.Fprintf(dst, "%s (count=%d:)\n", prefix, n.children[len(n.keys)].count)
			recur(n.children[len(n.keys)].sub, prefix+"  ")
		}
	}
	recur(t.root, "")

	return nil
}

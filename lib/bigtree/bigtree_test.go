package bigtree

import (
	"context"
	"encoding/binary"
	"fmt"
	"math/rand"
	//"os"
	"testing"

	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/footest"
)

type simple struct {
	value int
}

func (s simple) String() string {
	return fmt.Sprintf("%d", s.value)
}

func (s simple) Less(b Key) bool {
	return s.value < b.(simple).value
}

func (s simple) Scan() ([]ptr.Ptr, error) {
	return nil, nil
}

func (s simple) Encode(_ map[ptr.Ptr]uint16) ([]byte, error) {
	var tmp [10]byte
	i := binary.PutVarint(tmp[:], int64(s.value))
	return tmp[:i], nil
}

func simpleLoader(buf []byte, _ []ptr.Ptr) (simple, error) {
	value, n := binary.Varint(buf)
	if n <= 0 {
		return simple{}, fmt.Errorf("invalid simple encoding")
	}
	return simple{int(value)}, nil
}

func simpleKeyLoader(buf []byte, toc []ptr.Ptr) (Key, error) {
	return simpleLoader(buf, toc)
}

func simpleValueLoader(buf []byte, toc []ptr.Ptr) (Value, error) {
	return simpleLoader(buf, toc)
}

func TestTreeInsert(t *testing.T) {
	c := footest.New()
	ctx := context.Background()

	tree := New(c, simpleKeyLoader, simpleValueLoader)

	tree.Insert(ctx, simple{27}, simple{100})
	tree.Insert(ctx, simple{50}, simple{90})
	tree.Insert(ctx, simple{13}, simple{80})

	//tree.Debug(os.Stdout)

	k, v, err := tree.Next(ctx, simple{26})
	if err != nil {
		t.Fatal(err)
	}
	if k != (simple{27}) {
		t.Errorf("expected to find next key is {27}, found {%s}", k)
	} else if v != (simple{100}) {
		t.Errorf("expected to find next value {100}, found {%s}", v)
	}

	v, err = tree.Get(ctx, simple{27})

	if err != nil {
		t.Fatal(err)
	}
	if v != (simple{100}) {
		t.Errorf("expected to find value {100}, found {%s}", v)
	}

	k, v, err = tree.Next(ctx, simple{45})
	if err != nil {
		t.Fatal(err)
	}
	if k != (simple{50}) {
		t.Errorf("expected to find next key is {50}, found {%s}", k)
	} else if v != (simple{90}) {
		t.Errorf("expected to find next value {90}, found {%s}", v)
	}

	v, err = tree.Get(ctx, simple{50})

	if err != nil {
		t.Fatal(err)
	}
	if v != (simple{90}) {
		t.Errorf("expected to find value {90}, found {%s}", v)
	}
}

func TestDuplicateKeys(t *testing.T) {
	c := footest.New()
	ctx := context.Background()

	tree := New(c, simpleKeyLoader, simpleValueLoader)

	tree.Insert(ctx, simple{3}, simple{30})
	tree.Insert(ctx, simple{2}, simple{20})
	tree.Insert(ctx, simple{2}, simple{10})

	expect := []Tuple{
		{
			Key:   simple{2},
			Value: simple{20},
		},
		{
			Key:   simple{2},
			Value: simple{10},
		},
		{
			Key:   simple{3},
			Value: simple{30},
		},
	}

	for tuple := range tree.Pump(ctx, nil, nil) {
		if tuple != expect[0] {
			t.Fatalf("expected %#v but got %#v", expect[0], tuple)
		}
		expect = expect[1:]
	}
	if len(expect) > 0 {
		t.Fatalf("expected %d more tuples", len(expect))
	}
}

func TestPersist(t *testing.T) {

	c := footest.New()
	ctx := context.Background()

	tree := New(c, simpleKeyLoader, simpleValueLoader)

	expect := []Tuple{
		{
			Key:   simple{2},
			Value: simple{20},
		},
		{
			Key:   simple{3},
			Value: simple{30},
		},
		{
			Key:   simple{4},
			Value: simple{44},
		},
	}

	reorder := make([]int, len(expect))
	for i := range expect {
		reorder[i] = i
	}

	r := rand.New(rand.NewSource(123))

	r.Shuffle(len(reorder), func(i, j int) {
		reorder[i], reorder[j] = reorder[j], reorder[i]
	})

	for _, k := range reorder {
		tree.Insert(ctx, expect[k].Key, expect[k].Value)
	}

	p0, err := tree.Store(ctx, c)
	if err != nil {
		panic(err)
	}

	t2 := Load(c, p0, simpleKeyLoader, simpleValueLoader)

	//tree.Debug(os.Stdout)
	//t2.Debug(os.Stdout)

	for tuple := range t2.Pump(ctx, nil, nil) {
		if tuple != expect[0] {
			t.Fatalf("expected %#v but got %#v", expect[0], tuple)
		}
		expect = expect[1:]
	}
	if len(expect) > 0 {
		t.Fatalf("expected %d more tuples", len(expect))
	}
}

func TestPumper(t *testing.T) {

	c := footest.New()
	ctx := context.Background()

	tree := New(c, simpleKeyLoader, simpleValueLoader)

	for i := 0; i < 7; i++ {
		tree.Insert(ctx, simple{i}, simple{i * 101})
	}
	tree.Insert(ctx, simple{3}, simple{333})

	checkpump := func(vals []int, from, to Key) func(*testing.T) {
		return func(t *testing.T) {
			for tuple := range tree.Pump(ctx, from, to) {
				x := simple{vals[0]}
				if tuple.Value != x {
					t.Fatalf("expected value %d but got %#v", vals[0], tuple)
				}
				vals = vals[1:]
			}
			if len(vals) > 0 {
				t.Fatalf("expected %d more keys", len(vals))
			}
		}
	}

	t.Run("(-,-)", checkpump([]int{0, 101, 202, 303, 333, 404, 505, 606}, nil, nil))
	t.Run("(-,4)", checkpump([]int{0, 101, 202, 303, 333}, nil, simple{4}))
	t.Run("(2,-)", checkpump([]int{202, 303, 333, 404, 505, 606}, simple{2}, nil))
	t.Run("(3,5)", checkpump([]int{303, 333, 404}, simple{3}, simple{5}))
	t.Run("(-1,5)", checkpump([]int{0, 101, 202, 303, 333, 404}, simple{-1}, simple{5}))
	t.Run("(3,10)", checkpump([]int{303, 333, 404, 505, 606}, simple{3}, simple{10}))
}

func TestInsertMoreThanAFew(t *testing.T) {
	c := footest.New()
	ctx := context.Background()

	tree := New(c, simpleKeyLoader, simpleValueLoader)

	for i := 0; i < 12; i++ {
		tree.Insert(ctx, simple{i}, simple{i * 10})
	}
	//tree.Debug(os.Stdout)
}

func TestInsertLots(t *testing.T) {
	c := footest.New()
	ctx := context.Background()

	checkmany := func(n int) func(*testing.T) {
		return func(t *testing.T) {
			tree := New(c, simpleKeyLoader, simpleValueLoader)

			for i := 0; i < n; i++ {
				tree.Insert(ctx, simple{i}, simple{i * 10})
			}

			// tree.Debug(os.Stdout)

			i := 0
			for tuple := range tree.Pump(ctx, nil, nil) {
				expect := simple{i}

				if tuple.Key != expect {
					t.Fatalf("expected %#v but got %#v (value is %#v)",
						expect, tuple.Key, tuple.Value)
				}
				i++
			}
			if i != n {
				t.Fatalf("expected to find %d, but found %d", n, i)
			}
		}
	}

	t.Run("50", checkmany(50))
	t.Run("100", checkmany(100))
	t.Run("5000", checkmany(5000))
}

func BenchmarkInsertSequential(b *testing.B) {
	c := footest.New()
	ctx := context.Background()
	tree := New(c, simpleKeyLoader, simpleValueLoader)

	for i := 0; i < b.N; i++ {
		tree.Insert(ctx, simple{i}, simple{i})
	}
}

func BenchmarkInsertRandom(b *testing.B) {
	c := footest.New()
	ctx := context.Background()
	tree := New(c, simpleKeyLoader, simpleValueLoader)

	r := rand.New(rand.NewSource(123))

	for i := 0; i < b.N; i++ {
		tree.Insert(ctx, simple{r.Int()}, simple{i})
	}
}

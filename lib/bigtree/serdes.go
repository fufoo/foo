package bigtree

import (
	"bufio"
	"bytes"
	"context"
	"encoding/binary"

	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/lib/sealed"
)

const maxFragSize = 256

var treeType = ptr.String("foo:bigtree.node/1")

func (n *node) IssueType(context.Context, foo.ReadTxn) (ptr.Ptr, error) {
	return treeType, nil
}

func (n *node) flush(ctx context.Context, txn foo.WriteTxn) error {
	if n.status&isClean != 0 {
		// already flushed or never modified
		return nil
	}

	if n.status&isInterior != 0 {
		panic("TODO")
	} else {
		p, err := txn.Store(ctx, n)
		if err != nil {
			return err
		}
		n.p = p
		n.status &^= isClean
		return nil
	}
}

func (n *node) MarshalCAM(tp *ptr.Ptr) (m sealed.Marshaled, ret error) {
	index := make(map[ptr.Ptr]uint16)

	// pass 1, roll up all the pointers
	for _, k := range n.keys {
		lst, err := k.Scan()
		if err != nil {
			ret = err
			return
		}
		for _, p := range lst {
			index[p] = 0
		}
	}

	if n.status&isInterior != 0 {
		if len(n.children) != len(n.keys)+1 {
			panic("expected len(children)=len(keys)+1")
		}
		for _, ch := range n.children {
			if ch.sub.status&isClean == 0 {
				panic("child node should be clean at this point")
			}
			index[ch.sub.p] = 0
		}
	} else {
		if len(n.values) != len(n.keys) {
			panic("expected len(values)=len(keys)")
		}
		for _, v := range n.values {
			lst, err := v.Scan()
			if err != nil {
				ret = err
				return
			}
			for _, p := range lst {
				index[p] = 0
			}
		}
	}

	ptrs := sealed.AssignPointerIndices(index)

	// pass 2, serialize the actual data

	var buf bytes.Buffer

	var tmp [20]byte

	i := 0
	if n.status&isInterior != 0 {
		tmp[i] = 'I'
	} else {
		tmp[i] = 'L'
	}
	i++

	i += binary.PutUvarint(tmp[i:], uint64(len(n.keys)))

	buf.Write(tmp[:i])

	// write the keys
	for _, k := range n.keys {
		chunk, err := k.Encode(index)
		if err != nil {
			ret = err
			return
		}
		i := binary.PutUvarint(tmp[:], uint64(len(chunk)))
		buf.Write(tmp[:i])
		buf.Write(chunk)
	}

	// write the values or children
	if n.status&isInterior != 0 {
		for _, ch := range n.children {
			i := binary.PutUvarint(tmp[:], uint64(index[ch.sub.p]))
			i += binary.PutUvarint(tmp[:], uint64(ch.count))
			buf.Write(tmp[i:])
		}
	} else {
		for _, v := range n.values {
			chunk, err := v.Encode(index)
			if err != nil {
				ret = err
				return
			}
			i := binary.PutUvarint(tmp[:], uint64(len(chunk)))
			buf.Write(tmp[:i])
			buf.Write(chunk)
		}
	}

	m = sealed.Marshaled{
		Payload: buf.Bytes(),
		Index:   ptrs,
		Flags:   sealed.DeflateCompression,
	}
	return
}

func (n *node) loader(ctx context.Context, tp *ptr.Ptr, index []ptr.Ptr, src *bufio.Reader) (interface{}, error) {
	if *tp != treeType {
		return nil, errlib.Newf("BIG-4701 unexpected type {%s} for tree node", *tp)
	}

	kind, err := src.ReadByte()
	if err != nil {
		return nil, err
	}

	if kind == 'L' {
	} else if kind == 'I' {
		n.status |= isInterior
	} else {
		return nil, errlib.Newf("BIG-4702 bad tree node serz")
	}

	num, err := binary.ReadUvarint(src)
	if err != nil {
		return nil, err
	}
	n.keys = make([]Key, num)

	// read the keys

	var buf []byte

	for i := range n.keys {
		value, err := binary.ReadUvarint(src)
		if err != nil {
			return nil, err
		}
		if value > maxFragSize {
			return nil, errlib.Newf("BIG-4704 key size %d too big", value)
		}
		chunksize := int(value)

		if chunksize > len(buf) {
			buf = make([]byte, chunksize)
		}

		read, err := src.Read(buf[:chunksize])
		if err != nil {
			return nil, err
		}
		if read != chunksize {
			return nil, errlib.Newf("BIG-4703 short read")
		}
		key, err := n.owner.kloader(buf[:chunksize], index)
		if err != nil {
			return nil, err
		}
		n.keys[i] = key
	}

	if n.status&isInterior != 0 {
		panic("TODO")
	} else {
		n.values = make([]Value, num)

		for i := range n.keys {
			value, err := binary.ReadUvarint(src)
			if err != nil {
				return nil, err
			}
			if value > maxFragSize {
				return nil, errlib.Newf("BIG-4705 value size %d too big", value)
			}
			chunksize := int(value)

			if chunksize > len(buf) {
				buf = make([]byte, chunksize)
			}

			read, err := src.Read(buf[:chunksize])
			if err != nil {
				return nil, err
			}
			if read != chunksize {
				return nil, errlib.Newf("BIG-4706 short read")
			}
			v, err := n.owner.vloader(buf[:chunksize], index)
			if err != nil {
				return nil, err
			}
			n.values[i] = v
		}
	}

	n.status |= isLoaded
	return n, nil
}

func (n *node) ensureloaded(ctx context.Context) error {
	if n.status&isLoaded != 0 {
		// already loaded
		return nil
	}

	n.owner.in.Load(ctx, n.p, n.loader)
	n.status |= isLoaded
	return nil
}

func (t *Tree) Store(ctx context.Context, txn foo.WriteTxn) (ptr.Ptr, error) {
	err := t.root.flush(ctx, txn)
	if err != nil {
		return ptr.Null, err
	}
	return t.root.p, nil
}

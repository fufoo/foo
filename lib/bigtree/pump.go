package bigtree

import (
	"context"
	"sort"
)

type Tuple struct {
	Key   Key
	Value Value
}

func (t *Tree) pumpAndMaybeDelete(ctx context.Context, first, limit Key, del bool) <-chan Tuple {
	ch := make(chan Tuple, 10)

	go func() {
		var recur func(*node) error
		defer close(ch)

		recur = func(n *node) error {
			err := n.ensureloaded(ctx)
			if err != nil {
				return err
			}

			num := len(n.keys)
			i0 := 0
			i1 := num

			if first != nil {
				i0 = sort.Search(num, func(i int) bool {
					return !n.keys[i].Less(first)
				})
			}
			if limit != nil {
				i1 = sort.Search(num, func(i int) bool {
					return !n.keys[i].Less(limit)
				})
			}

			if n.status&isInterior != 0 {
				for i := i0; i <= i1; i++ {
					err := recur(n.children[i].sub)
					if err != nil {
						return err
					}
				}
			} else {

				for i := i0; i < i1; i++ {
					next := Tuple{
						Key:   n.keys[i],
						Value: n.values[i],
					}
					select {
					case ch <- next:
						continue
					case <-ctx.Done():
						return context.Canceled
					}
				}
			}
			return nil
		}
		recur(t.root)
	}()

	return ch
}

func (t *Tree) Delete(ctx context.Context, first, limit Key) <-chan Tuple {
	return t.pumpAndMaybeDelete(ctx, first, limit, true)
}

func (t *Tree) Pump(ctx context.Context, first, limit Key) <-chan Tuple {
	return t.pumpAndMaybeDelete(ctx, first, limit, false)
}

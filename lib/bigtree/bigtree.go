package bigtree

import (
	"context"
	"errors"
	//"fmt"
	"sort"

	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/foo"
)

var ErrKeyNotFound = errors.New("key not found")

type Fragment interface {
	Scan() ([]ptr.Ptr, error)
	Encode(xref map[ptr.Ptr]uint16) ([]byte, error)
}

type Key interface {
	Fragment
	Less(Key) bool
}

type Value interface {
	Fragment
}

type KeyLoader func([]byte, []ptr.Ptr) (Key, error)
type ValueLoader func([]byte, []ptr.Ptr) (Value, error)

type Tree struct {
	in      foo.ReadTxn
	minLoad uint8 // minimum number of keys we'd like to keep
	maxLoad uint8 // max number of keys before we split
	root    *node
	kloader KeyLoader
	vloader ValueLoader
}

type status uint

const (
	isLoaded   status = 1 << iota // is keys valid?
	isClean                       // is pointer valid?
	isInterior                    // do we use values or children?
)

type node struct {
	owner    *Tree
	p        ptr.Ptr
	status   status
	keys     []Key
	values   []Value // leaf only
	children []child // interior only
}

type child struct {
	count uint64
	sub   *node
}

func New(txn foo.ReadTxn, kl KeyLoader, vl ValueLoader) *Tree {
	return Load(txn, ptr.Null, kl, vl)
}

func Load(txn foo.ReadTxn, p ptr.Ptr, kl KeyLoader, vl ValueLoader) *Tree {
	t := &Tree{
		in:      txn,
		minLoad: 3,
		maxLoad: 10,
		kloader: kl,
		vloader: vl,
	}
	if p.IsNull() {
		t.root = &node{
			owner:  t,
			status: isLoaded,
		}
	} else {
		t.root = &node{
			owner:  t,
			status: isClean,
			p:      p,
		}
	}
	return t
}

type splitting struct {
	split       Key
	left, right child
}

func (t *Tree) splitLeaf(n *node) *splitting {
	nleft := len(n.keys) / 2
	nright := len(n.keys) - nleft

	sp := &splitting{
		left: child{
			count: uint64(nleft),
			sub: &node{
				owner:  t,
				status: isLoaded,
				keys:   make([]Key, nleft),
				values: make([]Value, nleft),
			},
		},
		right: child{
			count: uint64(nright),
			sub: &node{
				owner:  t,
				status: isLoaded,
				keys:   make([]Key, nright),
				values: make([]Value, nright),
			},
		},
		split: n.keys[nleft],
	}
	copy(sp.left.sub.keys, n.keys)
	copy(sp.left.sub.values, n.values)

	copy(sp.right.sub.keys, n.keys[nleft:])
	copy(sp.right.sub.values, n.values[nleft:])

	return sp
}

func (t *Tree) interiorSplit(n *node, i int, sub *splitting) *splitting {

	// temporarily make a bigger node, then split it in the middle

	// fortunately, interiorInsert doesn't care, so just use it
	interiorInsert(n, i, sub)

	// the -1 is because we are actually *removing* a key from this interior
	// node; it is being lifted up as part of the split (splitting.split)

	nleft := (len(n.keys) - 1) / 2
	nright := (len(n.keys) - 1) - nleft

	sp := &splitting{
		left: child{
			sub: &node{
				owner:    t,
				status:   isLoaded | isInterior,
				keys:     make([]Key, nleft),
				children: make([]child, nleft+1),
			},
		},
		right: child{
			sub: &node{
				owner:    t,
				status:   isLoaded | isInterior,
				keys:     make([]Key, nright),
				children: make([]child, nright+1),
			},
		},
		split: n.keys[nleft],
	}

	copy(sp.left.sub.keys, n.keys)
	copy(sp.left.sub.children, n.children)

	copy(sp.right.sub.keys, n.keys[nleft+1:])
	copy(sp.right.sub.children, n.children[nleft+1:])

	// fix up the counts
	sp.left.count = sumcount(sp.left.sub.children)
	sp.right.count = sumcount(sp.right.sub.children)

	return sp
}

func sumcount(lst []child) (total uint64) {
	for _, ch := range lst {
		total += ch.count
	}
	return
}

func leafInsert(n *node, i int, key Key, value Value) {
	num := len(n.keys)
	k := make([]Key, num+1)
	v := make([]Value, num+1)
	copy(k[:i], n.keys[:i])
	copy(k[i+1:], n.keys[i:])
	copy(v[:i], n.values[:i])
	copy(v[i+1:], n.values[i:])
	k[i], v[i] = key, value
	n.keys, n.values = k, v
}

func interiorInsert(n *node, i int, sp *splitting) {
	//fmt.Printf("interior insert: left count=%d right count=%d\n", sp.left.count, sp.right.count)
	k := make([]Key, len(n.keys)+1)
	c := make([]child, len(n.children)+1)

	copy(k[:i], n.keys[:i])
	copy(k[i+1:], n.keys[i:])
	k[i] = sp.split

	copy(c[:i], n.children[:i])
	copy(c[i+2:], n.children[i+1:])
	c[i], c[i+1] = sp.left, sp.right
	n.keys, n.children = k, c
}

func (t *Tree) Insert(ctx context.Context, key Key, value Value) error {

	var recur func(n *node) (*splitting, error)

	recur = func(n *node) (*splitting, error) {
		err := n.ensureloaded(ctx)
		if err != nil {
			return nil, err
		}

		n.status &^= isClean

		num := len(n.keys)
		i := sort.Search(num, func(i int) bool {
			return key.Less(n.keys[i])
		})

		if n.status&isInterior != 0 {
			// we are on an interior node; recurse
			sp, err := recur(n.children[i].sub)
			if err != nil {
				return nil, err
			}
			if sp == nil {
				n.children[i].count++
			} else {
				// the child split... see if we can accomodate that directly
				if len(n.keys) < int(t.maxLoad) {
					// it can fit in here
					interiorInsert(n, i, sp)
				} else {
					// it can't fit, we need to split ourselves
					re := t.interiorSplit(n, i, sp)
					return re, nil
				}
			}
			return nil, nil
		}

		var sp *splitting

		if len(n.keys) >= int(t.maxLoad) {
			sp = t.splitLeaf(n)

			nleft := len(sp.left.sub.keys)
			nright := len(sp.right.sub.keys)
			if i < nleft {
				n = sp.left.sub
				sp.left.count++
				num = nleft
				//fmt.Printf("split left @%d %#v\n", i, n)
			} else {
				n = sp.right.sub
				sp.right.count++
				i -= nleft
				num = nright
				//fmt.Printf("split right @%d %#v\n", i, n)
			}
		}

		// we are on a leaf, but there is room to insert

		//fmt.Printf("i=%d of %d\n", i, num)
		leafInsert(n, i, key, value)
		return sp, nil
	}

	sp, err := recur(t.root)
	if err != nil {
		return err
	}
	if sp == nil {
		return nil
	}
	// the root needs to split
	t.root = &node{
		owner:    t,
		status:   isLoaded | isInterior,
		keys:     []Key{sp.split},
		children: []child{sp.left, sp.right},
	}
	return nil
}

func (t *Tree) Get(ctx context.Context, key Key) (Value, error) {
	n := t.root
	for {
		err := n.ensureloaded(ctx)
		if err != nil {
			return nil, err
		}

		num := len(n.keys)
		i := sort.Search(num, func(i int) bool {
			return key.Less(n.keys[i])
		})
		// if !(key < entry) and !(entry < key)
		// then key == entry
		if i > 0 && !n.keys[i-1].Less(key) {
			return n.values[i-1], nil
		}
		return nil, ErrKeyNotFound
	}
}

func (t *Tree) Next(ctx context.Context, key Key) (Key, Value, error) {
	n := t.root
	for {
		err := n.ensureloaded(ctx)
		if err != nil {
			return nil, nil, err
		}

		num := len(n.keys)
		i := sort.Search(num, func(i int) bool {
			return key.Less(n.keys[i])
		})
		if i < num {
			return n.keys[i], n.values[i], nil
		}
		return nil, nil, ErrKeyNotFound
	}
}

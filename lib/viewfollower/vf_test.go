package viewfollower

import (
	"context"
	"fmt"
	"strings"
	"testing"
	"time"

	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/lib/keycreds"
)

func TestFollower(t *testing.T) {
	ctx := context.Background()
	mem, _ := foo.DialMemory(ctx)

	f := New(mem.State(), mem.Storage())

	f.Follow(Key{1, "alice"})
	f.Follow(Key{2, "alice"})

	creds := keycreds.KeyFromString("test")
	d1 := mem.Access(1, creds)
	//d2 := mem.Access(2, creds)

	bg, cancel := context.WithCancel(ctx)
	defer cancel()

	reads := make(chan map[Key]Value, 1)
	go func() {
		for {
			m, err := f.Read(bg)
			if err != nil {
				return
			}
			reads <- m
		}
	}()

	setbob := func(str string) {
		d1.Update(ctx, func(ctx context.Context, wr foo.WriteTxn) error {
			wr.Set(ctx, "bob", ptr.String(str))
			return nil
		})
	}

	// make a change
	setbob("Hello, world")

	// make sure we _don't_ read that change
	time.Sleep(20 * time.Millisecond)
	select {
	case m := <-reads:
		t.Fatalf("did not expect to read change to bob; read %s", mapstr(m))
	default:
	}

	// start following bob
	f.Follow(Key{1, "bob"})
	// make another change
	setbob("Announcement")

	// make sure we _do_ read that change
	select {
	case m := <-reads:
		fmt.Printf("did read: %s\n", mapstr(m))
	case <-time.After(time.Second):
		t.Fatalf("did not read the expected change")
	}

	v, err := f.Get(ctx, Key{1, "bob"})
	if err != nil {
		t.Fatal(err)
	}

	if v.Seq != 1001 {
		t.Fatalf("expected #%d but got #%d", 1001, v.Seq)
	}
}

func mapstr(m map[Key]Value) string {
	var s strings.Builder

	first := true
	for k, v := range m {
		if !first {
			s.WriteString(", ")
		}
		fmt.Fprintf(&s, "%d:%s=#%d", k.Domain, k.Name, v.Seq)
	}
	return s.String()
}

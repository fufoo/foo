// this package provides helpers to build a viewmaster followers
// on top of the foo API.  Works out-of-the-box with a the Key and
// Value from this package

package viewfollower

import (
	"context"
	"time"

	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/services/cam"
	"bitbucket.org/fufoo/foo/services/core"
	"bitbucket.org/fufoo/foo/services/state"
)

type Key struct {
	Domain uint64
	Name   string
}

type Value struct {
	Addr ptr.Ptr
	Seq  uint64
}

func (v Value) Sequence() uint64 {
	return v.Seq
}

type Follower struct {
	state   state.StateQClient
	store   cam.StorageQClient
	ops     chan<- op
	pending []*reading
	cache   map[Key]Value
	dirty   map[Key]Value
	domains map[uint64]*domain
	dcache  map[ptr.Ptr]*dentry
}

type dentry struct {
	key   ptr.Ptr
	datum []byte
	atime time.Time
	ltime time.Time
}

type domain struct {
	id     uint64
	count  int
	cancel func()
}

type op interface {
	apply(*Follower)
}

func New(state state.StateQClient, store cam.StorageQClient) *Follower {
	ch := make(chan op, 10)

	f := &Follower{
		state:   state,
		store:   store,
		ops:     ch,
		cache:   make(map[Key]Value),
		domains: make(map[uint64]*domain),
		dcache:  make(map[ptr.Ptr]*dentry),
	}
	go f.run(ch)
	return f
}

func (f *Follower) run(ch <-chan op) {
	for x := range ch {
		x.apply(f)
	}
	panic("TODO shutdown")
}

type start struct {
	what Key
}

type end struct {
	what Key
}

func (s start) apply(f *Follower) {

	_, ok := f.cache[s.what]
	if ok {
		// already following this one
		return
	}

	cur, err := f.state.Get(
		context.Background(),
		&state.GetRequest{
			Domain:   s.what.Domain,
			Subjects: []string{s.what.Name},
		})
	if err != nil {
		panic(err)
	}

	x := cur.States[0]
	f.cache[s.what] = Value{
		Addr: x.Value.AsPtr(),
		Seq:  x.Seq,
	}

	d, ok := f.domains[s.what.Domain]
	if ok {
		d.count++
	} else {
		ctx, cancel := context.WithCancel(context.Background())
		resp, err := f.state.Follow(
			ctx,
			&state.FollowRequest{
				Domain:    s.what.Domain,
				Subjects:  []string{"*"},
				AllowGlob: true,
				AfterMark: cur.Mark,
			})
		if err != nil {
			panic(err) // TODO
		}

		d = &domain{
			id: s.what.Domain,
			//stream: resp,
			count:  1,
			cancel: cancel,
		}
		f.domains[s.what.Domain] = d
		go f.listenon(ctx, d, resp)
	}
}

type updates struct {
	edits map[Key]Value
}

func (up updates) apply(f *Follower) {
	if f.dirty == nil {
		// the only reason we're rebuilding the map here is
		// that listenon() is basically listening on
		// EVERYTHING (glob *) and we only want to return to
		// Read() the things that have been Follow()'d.
		// Otherwise, we could totally return the entire
		// up.edits now (also note that for racy reasons, it
		// isn't convenient to do that filtering in listenon()
		// itself; we have kernel exclusion here)
		f.dirty = make(map[Key]Value)
	}

	for k, v := range up.edits {
		if _, ok := f.cache[k]; ok {
			f.dirty[k] = v
			f.cache[k] = v
		}
	}

	if len(f.dirty) > 0 && len(f.pending) > 0 {
		// resolve a pending read right now
		next := f.pending[0]
		f.pending = drop(0, f.pending)
		next.data = f.dirty
		f.dirty = nil
		next.done <- nil
	}
}

func (f *Follower) listenon(ctx context.Context, d *domain, resp state.State_FollowQClient) {
	for {
		item, err := resp.Recv()
		if err != nil {
			panic(err)
		}
		batch := make(map[Key]Value)
		for _, s := range item.States {
			if s.Domain == d.id {
				key := Key{
					Domain: s.Domain,
					Name:   s.Name,
				}
				value := Value{
					Addr: s.Value.AsPtr(),
					Seq:  s.Seq,
				}
				batch[key] = value
			}
		}
		f.ops <- updates{batch}
	}
}

func (e end) apply(f *Follower) {
	_, ok := f.cache[e.what]
	if !ok {
		// already not following this one
		return
	}
	delete(f.cache, e.what)
	delete(f.dirty, e.what)

	d := f.domains[e.what.Domain]
	d.count--

	if d.count == 0 {
		// not following anything in this domain anymore, drop
		// the stream
		d.cancel()
		delete(f.domains, e.what.Domain)
	}
}

func (f *Follower) Follow(k Key) {
	f.ops <- start{
		what: k,
	}
}

func (f *Follower) Unfollow(k Key) {
	f.ops <- end{
		what: k,
	}
}

type reading struct {
	done chan<- error
	data map[Key]Value
}

type readcancel struct {
	rd *reading
}

func (r *reading) apply(f *Follower) {
	if f.dirty == nil {
		f.pending = append(f.pending, r)
	} else {
		r.data = f.dirty
		f.dirty = nil
		r.done <- nil
	}
}

func drop(i int, lst []*reading) []*reading {
	copy(lst[i:], lst[i+1:])
	return lst[:len(lst)-1]
}

func (c readcancel) apply(f *Follower) {
	for i, r := range f.pending {
		if r == c.rd {
			close(r.done)
			f.pending = drop(i, f.pending)
			return
		}
	}
	// note that we might not find the entry pending, if there was
	// a race between resolving the pending read and canceling it.
	// In that case, since the canceler is waiting for our result,
	// they should wind up returning the value even though they
	// initiated a cancel
}

func (f *Follower) Read(ctx context.Context) (map[Key]Value, error) {
	done := make(chan error, 1)

	r := &reading{
		done: done,
	}

	f.ops <- r

	select {
	case err := <-done:
		return r.data, err
	case <-ctx.Done():
		f.ops <- readcancel{r}
	}

	err, ok := <-done
	if !ok {
		// the channel was closed, which means the kernel
		// received the cancelation
		return nil, context.Canceled
	}
	// otherwise, there was a race between our cancelation and the
	// kernel returning data; most likely, we have valid data to
	// return and no error
	return r.data, err
}

type getting struct {
	subjects []Key
	done     chan<- error
	data     []Value
	mark     uint64
}

func (g *getting) apply(f *Follower) {

	n := len(g.subjects)
	result := make([]Value, n)

	for i, subject := range g.subjects {

		// TODO make this more efficient
		v, ok := f.cache[subject]
		if ok {
			result[i] = v
			continue
		}

		// don't cache it... that will happen if the item is Follow()'d
		cur, err := f.state.Get(
			context.Background(),
			&state.GetRequest{
				Domain:   subject.Domain,
				Subjects: []string{subject.Name},
			})
		if err != nil {
			g.done <- err
			return
		}

		x := cur.States[0]
		result[i] = Value{
			Addr: x.Value.AsPtr(),
			Seq:  x.Seq,
		}
		g.mark = cur.Mark // TODO make this correct; with multiple
		// domains there can be different marks, and worse even with
		// a single domain we're getting the mark multiple times
	}
	g.data = result
	g.done <- nil
}

func (c getcancel) apply(f *Follower) {
	// there is no getting queue, so nothing to pop out
	close(c.g.done)
}

type getcancel struct {
	g *getting
}

func (f *Follower) Get(ctx context.Context, k Key) (Value, error) {
	v, _, err := f.Getv(ctx, []Key{k})
	if err != nil {
		return Value{}, err
	}
	return v[0], nil
}

func (f *Follower) Getv(ctx context.Context, k []Key) ([]Value, uint64, error) {
	done := make(chan error, 1)

	g := &getting{
		subjects: k,
		done:     done,
	}

	f.ops <- g

	select {
	case err := <-done:
		return g.data, g.mark, err
	case <-ctx.Done():
		f.ops <- getcancel{g}
	}

	err, ok := <-done
	if !ok {
		// the channel was closed, which means the kernel
		// received the cancelation
		return nil, 0, context.Canceled
	}
	// otherwise, there was a race between our cancelation and the
	// kernel returning data; most likely, we have valid data to
	// return and no error
	return g.data, g.mark, err
}

type loading struct {
	subject []ptr.Ptr
	data    [][]byte
	done    chan<- error
}

func (l *loading) apply(f *Follower) {

	n := len(l.subject)
	result := make([][]byte, n)

	var index []int
	var req cam.LoadRequest

	t0 := time.Now()

	// populate from the cache first
	for i, p := range l.subject {
		if e, ok := f.dcache[p]; ok {
			result[i] = e.datum
			e.atime = t0
		} else {
			req.Refs = append(req.Refs, core.From(p))
			index = append(index, i)
		}
	}
	if len(req.Refs) > 0 {
		// there are still some we need... make the call
		ctx := context.Background()
		resp, err := f.store.Load(ctx, &req)
		if err != nil {
			l.done <- err
			return
		}

		for j, datum := range resp.Data {
			i := index[j]
			p := l.subject[i]
			result[i] = datum.Payload
			f.dcache[p] = &dentry{
				key:   p,
				datum: datum.Payload,
				atime: t0,
				ltime: t0,
			}
		}
	}
	l.data = result
	l.done <- nil
}

type loadcancel struct {
	l *loading
}

func (can loadcancel) apply(f *Follower) {
	close(can.l.done)
}

func (f *Follower) Load(ctx context.Context, p []ptr.Ptr) ([][]byte, error) {
	done := make(chan error, 1)
	l := &loading{
		subject: p,
		done:    done,
	}

	f.ops <- l

	select {
	case err := <-done:
		return l.data, err
	case <-ctx.Done():
		f.ops <- loadcancel{l}
	}

	err, ok := <-done
	if !ok {
		// the channel was closed, which means the kernel
		// processed the cancelation
		return nil, context.Canceled
	}
	// otherwise, there was a race between our cancelation and the
	// kernel returning data; most likely, we have valid data to
	// return and no error
	return l.data, err
}

package viewfollower

import (
	"context"

	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/foo"
	//"bitbucket.org/fufoo/foo/lib/sealed"
	//"bitbucket.org/fufoo/foo/services/cam"
	//"bitbucket.org/fufoo/foo/services/state"
	//"google.golang.org/protobuf/proto"
)

type fdriver struct {
	f      *Follower
	domain uint64
}

func (f *Follower) ReadDriver(domain uint64) foo.ReadDriver {
	return fdriver{
		f:      f,
		domain: domain,
	}
}

type FollowerReader struct {
	foo.ReadTxn
	Access map[Key]Value
}

func (f *Follower) BeginRead(domain uint64, creds foo.Credentials) *FollowerReader {
	return &FollowerReader{
		ReadTxn: foo.NewReadTxn(domain, f.ReadDriver(domain), creds),
		Access:  make(map[Key]Value),
	}
}

func (fd fdriver) Get(ctx context.Context, domain uint64, names []string) ([]ptr.Ptr, []uint64, uint64, error) {
	n := len(names)
	keys := make([]Key, n)
	for i, name := range names {
		keys[i] = Key{
			Domain: domain,
			Name:   name,
		}
	}
	vals, mark, err := fd.f.Getv(ctx, keys)
	if err != nil {
		return nil, nil, 0, err
	}

	addrs := make([]ptr.Ptr, n)
	seqs := make([]uint64, n)
	for i, val := range vals {
		addrs[i] = val.Addr
		seqs[i] = val.Seq
	}
	return addrs, seqs, mark, nil
}

func (fd fdriver) Read(ctx context.Context, p []ptr.Ptr) ([][]byte, error) {
	return fd.f.Load(ctx, p)
}

/*
// Get returns the current state of the given observable.  It
// is not an error if the observable does not exist; ptr.Null
// is returned in that case
func (rd *readtxn) Get(context.Context, string) (ptr.Ptr, error) {
	panic("TODO")
}

// Load an object.  Loader is a function that can be obtained
// as by sealed.ProtobufLoader, sealed.ExpectedTypeLoader, or
// some other approach.
func (rd *readtxn) Load(ctx context.Context, p ptr.Ptr, ld sealed.Loader) (interface{}, error) {
	return rd.LoadUsingCreds(ctx, p, ld, rd.creds)
}

// LoadType just reads the type from given object; note that the type
// is protected by encryption as well, so this still requires proper
// credentials
func (rd *readtxn) LoadType(ctx context.Context, p ptr.Ptr) (*ptr.Ptr, error) {
	return rd.LoadTypeUsingCreds(ctx, p, rd.creds)
}

// Fill populates an object (protobuf) with the contents of
// the object referred to by the pointer.
func (rd *readtxn) Fill(ctx context.Context, p ptr.Ptr, dst proto.Message) error {
	return rd.FillUsingCreds(ctx, p, dst, rd.creds)
}

// Load an object.  Loader is a function that can be obtained
// as by sealed.ProtobufLoader, sealed.ExpectedTypeLoader, or
// some other approach.
func (rd *readtxn) LoadUsingCreds(ctx context.Context, p ptr.Ptr, ld sealed.Loader, creds foo.Credentials) (interface{}, error) {
}

// LoadType just reads the type from given object; note that the type
// is protected by encryption as well, so this still requires proper
// credentials
func (rd *readtxn) LoadTypeUsingCreds(ctx context.Context, p ptr.Ptr, creds foo.Credentials) (*ptr.Ptr, error) {
}

// Fill populates an object (protobuf) with the contents of
// the object referred to by the pointer.
func (rd *readtxn) FillUsingCreds(ctx context.Context, p ptr.Ptr, dst proto.Message, creds foo.Credentials) error {
}

func (rd *readtxn) WaitForChanges(ctx context.Context) ([]string, error) {
	panic("TODO")
}

// Domain returns the domain that this transaction is operating on
func (rd *readtxn) Domain() uint64 {
	return rd.domain
}
*/

package followfilter

import (
	"bytes"
	"context"
	"regexp"
	"strings"
)

// special case for matching evertything

type matchAll struct {
}

func (matchAll) Prefilter(context.Context) MicroBloom {
	return MicroBloom{^uint64(0)}
}

func (matchAll) Qualify(map[string]int) bool {
	return true
}

func (matchAll) QualifyOne(string) bool {
	return true
}

type globbed struct {
	re *regexp.Regexp
}

func (g globbed) Prefilter(ctx context.Context) MicroBloom {
	return MicroBloom{^uint64(0)}
}

func (g globbed) QualifyOne(subject string) bool {
	return g.re.MatchString(subject)
}

func (g globbed) Qualify(subjects map[string]int) bool {
	for s := range subjects {
		if g.re.MatchString(s) {
			return true
		}
	}
	return false
}

func makeGlobSubjects(ctx context.Context, lst []string) (SubjectFilter, error) {
	for _, pattern := range lst {
		if pattern == "*" {
			log.Debugf(ctx, "matching everything")
			return matchAll{}, nil
		}
	}

	re, err := CompileGlobs(ctx, lst)
	if err != nil {
		return nil, err
	}
	return globbed{re}, nil
}

func CompileGlobs(ctx context.Context, globs []string) (*regexp.Regexp, error) {
	var buf bytes.Buffer
	buf.WriteByte('^')

	for i, glob := range globs {
		if i > 0 {
			buf.WriteByte('|')
		}
		buf.WriteByte('(')
		parts := strings.Split(glob, "*")
		log.Tracef(ctx, ">> %q", parts)

		if len(parts) == 1 {
			buf.WriteString(regexp.QuoteMeta(parts[0]))
		} else {
			for i := 0; i < len(parts); i++ {
				if (i+1) < len(parts) && parts[i] == "" {
					i++
					buf.WriteString(`.*`)
				} else {
					if i > 0 {
						buf.WriteString(`[^/]+`)
					}
					buf.WriteString(regexp.QuoteMeta(parts[i]))
				}
			}
		}
		buf.WriteByte(')')
	}
	buf.WriteByte('$')
	log.Debugf(ctx, "glob regex is: %s", buf.String())
	return regexp.Compile(buf.String())
}

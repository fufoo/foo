package followfilter

import (
	"context"
	"fmt"
	"strings"
	"sync/atomic"
	"time"

	"bitbucket.org/dkolbly/logging"
	"github.com/spaolacci/murmur3"
)

var log = logging.New("ff")

type MicroBloom struct {
	bits uint64
}

// returns true if haystack might contain needle, which is
// true iff all the bits in needle are present in haystack
func (haystack MicroBloom) Contains(needle MicroBloom) bool {
	return needle.bits&haystack.bits == needle.bits
}

func NewMicroBloom(entry string) (m MicroBloom) {
	m.Add(context.TODO(), entry)
	return
}

//var bloomHashTable = crc64.MakeTable(crc64.ECMA)

var seed = uint32(time.Now().UnixNano())

func (m *MicroBloom) Add(ctx context.Context, entry string) {
	h := murmur3.Sum64WithSeed([]byte(entry), seed)
	//h := crc64.Update(0, bloomHashTable, []byte(entry))
	log.Tracef(ctx, "%016x is murmur3_64(%q)", h, entry)

	m.bits |= 1 << (h & 0x3f)
	m.bits |= 1 << ((h >> 6) & 0x3f)
	m.bits |= 1 << ((h >> 12) & 0x3f)
	m.bits |= 1 << ((h >> 18) & 0x3f)
	log.Tracef(ctx, "BLOOM is %016x", m.bits)
}

var subids uint64 = 1

type Subscription struct {
	id        uint64
	prefilter MicroBloom
	filter    SubjectFilter
}

type SubjectFilter interface {
	Qualify(map[string]int) bool
	QualifyOne(string) bool
	Prefilter(context.Context) MicroBloom
}

func anyGlob(subjects []string) bool {
	for _, s := range subjects {
		if strings.IndexByte(s, '*') >= 0 {
			return true
		}
	}
	return false
}

func (s Subscription) String() string {
	return fmt.Sprintf("{(%d) %016x}", s.id, s.prefilter.bits)

}

func New(ctx context.Context, subjects []string, glob bool) (Subscription, error) {
	var f SubjectFilter

	if glob && anyGlob(subjects) {
		var err error
		f, err = makeGlobSubjects(ctx, subjects)
		if err != nil {
			return Subscription{}, err
		}
	} else if len(subjects) == 1 {
		f = singleSubject(subjects[0])
	} else if len(subjects) == 2 {
		f = twoSubjects{subjects[0], subjects[1]}
	} else {
		tbl := make(map[string]struct{})
		for _, s := range subjects {
			tbl[s] = struct{}{}
		}
		f = multiSubject{tbl}
	}

	sub := Subscription{
		id:     atomic.AddUint64(&subids, 1),
		filter: f,
	}
	ctx = logging.Set(ctx, "sub", sub.id)
	sub.prefilter = f.Prefilter(ctx)
	return sub, nil
}

type singleSubject string

func (ss singleSubject) Prefilter(ctx context.Context) MicroBloom {
	m := MicroBloom{}
	m.Add(ctx, string(ss))
	return m
}

func (ss singleSubject) Qualify(subjects map[string]int) bool {
	_, ok := subjects[string(ss)]
	return ok
}

func (ss singleSubject) QualifyOne(subject string) bool {
	return subject == string(ss)
}

type twoSubjects struct {
	a, b string
}

func (me twoSubjects) Prefilter(ctx context.Context) MicroBloom {
	m := MicroBloom{}
	m.Add(ctx, me.a)
	m.Add(ctx, me.b)
	return m
}

func (me twoSubjects) QualifyOne(subject string) bool {
	return me.a == subject || me.b == subject
}

func (me twoSubjects) Qualify(subjects map[string]int) bool {
	_, ok := subjects[me.a]
	if ok {
		return true
	}
	_, ok = subjects[me.b]
	return ok
}

type multiSubject struct {
	tbl map[string]struct{}
}

func (me multiSubject) Prefilter(ctx context.Context) MicroBloom {
	m := MicroBloom{}
	for subject := range me.tbl {
		m.Add(ctx, subject)
	}
	return m
}

func (me multiSubject) QualifyOne(subject string) bool {
	_, ok := me.tbl[subject]
	return ok
}

func (me multiSubject) Qualify(subjects map[string]int) bool {
	for n := range subjects {
		if _, ok := me.tbl[n]; ok {
			return true
		}
	}
	return false
}

type Pattern struct {
	disjunct MicroBloom
	conjunct MicroBloom
	lst      []MicroBloom
	exact    map[string]int
}

func Compile(ctx context.Context, subjects []string) (pat Pattern) {
	pat.exact = make(map[string]int, len(subjects))
	pat.lst = make([]MicroBloom, len(subjects))
	for i, s := range subjects {
		var b MicroBloom
		pat.exact[s] = i
		b.Add(ctx, s)
		pat.lst[i] = b
		if i == 0 {
			pat.conjunct = b
		} else {
			pat.conjunct.bits &= b.bits
		}
		pat.disjunct.bits |= b.bits
	}
	return pat
}

func (sub *Subscription) QualifyOne(subject string) bool {
	// don't bother with bloom filtering, just get straight to it;
	// this is better in most cases where the filter does not have
	// high cardinality
	return sub.filter.QualifyOne(subject)
}

func (p Pattern) Match(ctx context.Context, sub *Subscription) bool {
	ctx = logging.Set(ctx, "sub", sub.id)

	if !sub.prefilter.Contains(p.conjunct) {
		// in order for the subscription to match the event,
		// it has to contain at least the intersection of the
		// event bits the intersection of events bits are bits
		// that *every* member of the event has
		log.Tracef(ctx, "short circuit: does not contain intersection %016x",
			p.conjunct.bits)
		return false
	}
	if sub.prefilter.bits&p.disjunct.bits == 0 {
		// no overlap between this subscription
		// and the current event
		log.Tracef(ctx, "short circuit: no overlap at all with %016x", p.conjunct.bits)
		return false
	}

	for _, b := range p.lst {
		if sub.prefilter.Contains(b) {
			q := sub.filter.Qualify(p.exact)
			if q {
				log.Tracef(ctx, "prefilter pass; match (prefilter true positive)")
			} else {
				log.Tracef(ctx, "prefilter pass; no match (prefilter false positive)")
			}
			return q
		}
	}
	log.Tracef(ctx, "match: failed prefilter check over %d", len(p.lst))
	return false
}

package errors

import (
	"errors"

	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/qrpc"
	"google.golang.org/grpc/codes"
)

type ErrNoSuchObject struct {
	P *ptr.Ptr
}

func (err ErrNoSuchObject) MessageID() string {
	return "CAM-1404"
}

func (err ErrNoSuchObject) Code() codes.Code {
	return codes.NotFound
}

func (err ErrNoSuchObject) Error() string {
	return "No such object " + err.P.String()
}

func IsNoSuchObject(err error) bool {
	if err == nil {
		return false
	}
	var nosuch ErrNoSuchObject
	if errors.As(err, &nosuch) {
		return true
	}
	var remote *qrpc.RPCError
	if errors.As(err, &remote) {
		return remote.Code == int(codes.NotFound)
	}
	return false
}

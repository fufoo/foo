// implements Highest Random Weight
package rendezvous

import (
	"sort"

	"bitbucket.org/fufoo/core/ptr"
	"github.com/spaolacci/murmur3"
)

type Space struct {
	Nodes []uint32
}

func nodehash(addr *ptr.Ptr, nodeid uint32) uint32 {
	return murmur3.Sum32WithSeed(addr.Bits[:8], nodeid)
}

type weighting struct {
	nodeid uint32
	weight uint32
}

type weightings []weighting

func (w weightings) Len() int           { return len(w) }
func (w weightings) Swap(i, j int)      { w[i], w[j] = w[j], w[i] }
func (w weightings) Less(i, j int) bool { return w[i].weight < w[j].weight }

func (s *Space) Replicas(addr *ptr.Ptr, dst []uint32, n int) []uint32 {
	if len(s.Nodes) < 2 {
		return s.Nodes
	}

	var backing [20]weighting
	lst := weightings(backing[:0])

	for _, id := range s.Nodes {
		lst = append(lst, weighting{
			nodeid: id,
			weight: nodehash(addr, id),
		})
	}
	sort.Sort(lst)

	for i := 0; i < n; i++ {
		dst = append(dst, lst[i].nodeid)
	}
	return dst
}

package sealed

import (
	"bufio"
	"context"
	"io"

	"bitbucket.org/fufoo/core/ptr"
)

// BlobLoader is a loader for blobs; if supplied, it also checks
// the type of the object.  The returned object is a []byte
func BlobLoader(t *ptr.Ptr) Loader {
	return func(ctx context.Context, tp *ptr.Ptr, index []ptr.Ptr, src *bufio.Reader) (interface{}, error) {
		if tp != nil && t != nil && *tp != *t {
			return nil, ErrUnexpectedMessage
		}

		return io.ReadAll(src)
	}
}

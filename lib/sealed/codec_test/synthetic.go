package codec_test

import (
	"fmt"
)

func gen_int32(k int) int32 {
	if k%3 == 0 {
		return 3 - int32(k)*171
	} else {
		return 3 + int32(k)*171
	}
}

func gen_int64(k int) int64 {
	if k%3 == 0 {
		return 3 - int64(k)*172
	} else {
		return 3 + int64(k)*172
	}
}

func gen_uint32(k int) uint32 {
	return 3 * uint32(k) * 173
}

func gen_uint64(k int) uint64 {
	return 3 * uint64(k) * 174
}

func gen_float64(k int) float64 {
	return 3 * float64(k) / 4
}

func gen_float32(k int) float32 {
	return 3 * float32(k) / 4
}

func gen_bool(k int) bool {
	return k%2 == 0
}

func gen_string(k int) string {
	return fmt.Sprintf("k=%d", k)
}

func gen_bytes(k int) []byte {
	return []byte(fmt.Sprintf("bytes(k=%d)", k))
}

func gen_Item(k int) *Item {
	return &Item{
		Name: fmt.Sprintf("item %d", k),
	}
}

func itemEqual(a, b *Item) bool {
	return a.Name == b.Name
}

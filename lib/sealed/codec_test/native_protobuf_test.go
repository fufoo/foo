package codec_test

import (
	"testing"

	"google.golang.org/protobuf/proto"
)

func TestNativeRoundTrip(t *testing.T) {
	var out, in Maps

	fillMaps(&out, allhot())
	buf, err := proto.Marshal(&out)
	if err != nil {
		t.Fatal(err)
	}

	err = proto.Unmarshal(buf, &in)
	if err != nil {
		t.Fatal(err)
	}

	checkMaps(t, &in, allhot())
}

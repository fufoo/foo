#! /usr/bin/env python

keys = ['int32',
        'int64',
        'uint32',
        'uint64',
        'sint32',
        'sint64',
        'fixed32',
        'fixed64',
        'sfixed32',
        'sfixed64',
        'bool',
        'string']

vals = ['double',
        'float',
        'int32',
        'int64',
        'uint32',
        'uint64',
        'sint32',
        'sint64',
        'fixed32',
        'fixed64',
        'sfixed32',
        'sfixed64',
        'bool',
        'string',
        'bytes',
        'Item']

gen = {
    'double': 'float64',
    'float': 'float32',
    'int32': 'int32',
    'int64': 'int64',
    'uint32': 'uint32',
    'uint64': 'uint64',
    'sint32': 'int32',
    'sint64': 'int64',
    'fixed32': 'uint32',
    'fixed64': 'uint64',
    'sfixed32': 'int32',
    'sfixed64': 'int64',
    'bool': 'bool',
    'string': 'string',
    'bytes': 'bytes',
    'Item': 'Item',
}

go = {
    'double': 'float64',
    'float': 'float32',
    'int32': 'int32',
    'int64': 'int64',
    'uint32': 'uint32',
    'uint64': 'uint64',
    'sint32': 'int32',
    'sint64': 'int64',
    'fixed32': 'uint32',
    'fixed64': 'uint64',
    'sfixed32': 'int32',
    'sfixed64': 'int64',
    'bool': 'bool',
    'string': 'string',
    'bytes': '[]byte',
    'Item': '*Item',
}

protoFile = open('generated.proto', 'w')
testFile = open('driver.go', 'w')

print("package codec_test", file=testFile)
print(file=testFile)
print('import (', file=testFile)
print('\t"bytes"', file=testFile)
print('\t"testing"', file=testFile)
print(')', file=testFile)

print("message Maps {", file=protoFile)
j = 0
for k in keys:
    for v in vals:
        j += 1
        print("  map<{0},{1}> map_{0}_{1} = {2};".format(k, v, j), file=protoFile)
print("}", file=protoFile)

print(file=protoFile)

print("message Lists {", file=protoFile)
j = 0
for v in vals:
    j += 1
    print("  repeated {0} list_{0} = {1};".format(v, j), file=protoFile)
print("}", file=protoFile)


print("func fillMaps(dst *Maps, include []bool) {", file=testFile)

def protoname(s):
    if s == "Item":
        return "_Item"
    return s.title()

i = 100
j = 0
inc = 0
for k in keys:
    for v in vals:
        fname = "dst.Map{0}{1}".format(protoname(k), protoname(v))
        print("\t{0} = make(map[{1}]{2})".format(fname, go[k], go[v]), file=testFile)
        alt = [0, 1, 2, 3, 4, 5]
        if k == "bool":
            # only room in the map for two alternatives
            alt = [0, 1]
            pass
        for z in alt:
            j += 1
            print("\tif include[{0}] {{".format(inc), file=testFile)
            print("\t\tk := gen_{0}({1})".format(gen[k], j), file=testFile)
            i += 1
            print("\t\tv := gen_{0}({1})".format(gen[v], i), file=testFile)
            print("\t\t{0}[k] = v".format(fname), file=testFile)
            print("\t}", file=testFile)
            pass
        inc += 1
        pass
    pass

print("}", file=testFile)

print("var hotnames = []string{", file=testFile)

inc = 0
for k in keys:
    for v in vals:
        print("\t\"Map_{0}_{1}\", // hot_{2}".format(protoname(k), protoname(v), inc), file=testFile)
        inc += 1
        pass
    pass

print("}", file=testFile)

print("func checkMaps(t *testing.T, src *Maps, include []bool) {", file=testFile)

i = 100
j = 0
inc = 0
for k in keys:
    for v in vals:
        fname = "src.Map{0}{1}".format(protoname(k), protoname(v))
        alt = [0, 1, 2, 3, 4, 5]
        if k == "bool":
            # only room for two alternatives
            alt = [0, 1]
            pass
        for z in alt:
            j += 1
            print("\tif include[{0}] {{".format(inc), file=testFile)
            print("\t\tk := gen_{0}({1})".format(gen[k], j), file=testFile)
            i += 1
            print("\t\tvx := gen_{0}({1})".format(gen[v], i), file=testFile)
            print("\t\tv, ok := {0}[k]".format(fname), file=testFile)
            print("\t\tif ok {", file=testFile)
            if v == "bytes":
                print("\t\t\teq := bytes.Equal(v, vx)", file=testFile)
            elif v == "Item":
                print("\t\t\teq := itemEqual(v, vx)", file=testFile)
            else:
                print("\t\t\teq := v == vx", file=testFile)
            print("\t\t\tif !eq {", file=testFile)
            print('\t\t\t\tt.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)', file=testFile)
            print("\t\t\t}", file=testFile)
            print("\t\t} else {", file=testFile)
            print('\t\t\tt.Errorf("value %#v is missing from map", k)', file=testFile)
            print("\t\t}", file=testFile)
            print("\t}", file=testFile)
            pass
        inc += 1
        pass
    pass

print("}", file=testFile)

print(file=testFile)

print("message Lists {", file=protoFile)
j = 0
for v in vals:
    j += 1
    print("  repeated {0} list_{0} = {1};".format(v, j), file=protoFile)
print("}", file=protoFile)


testFile.close()
protoFile.close()


package codec_test

import (
	"bytes"
	"testing"
)

func fillMaps(dst *Maps, include []bool) {
	dst.MapInt32Double = make(map[int32]float64)
	if include[0] {
		k := gen_int32(1)
		v := gen_float64(101)
		dst.MapInt32Double[k] = v
	}
	if include[0] {
		k := gen_int32(2)
		v := gen_float64(102)
		dst.MapInt32Double[k] = v
	}
	if include[0] {
		k := gen_int32(3)
		v := gen_float64(103)
		dst.MapInt32Double[k] = v
	}
	if include[0] {
		k := gen_int32(4)
		v := gen_float64(104)
		dst.MapInt32Double[k] = v
	}
	if include[0] {
		k := gen_int32(5)
		v := gen_float64(105)
		dst.MapInt32Double[k] = v
	}
	if include[0] {
		k := gen_int32(6)
		v := gen_float64(106)
		dst.MapInt32Double[k] = v
	}
	dst.MapInt32Float = make(map[int32]float32)
	if include[1] {
		k := gen_int32(7)
		v := gen_float32(107)
		dst.MapInt32Float[k] = v
	}
	if include[1] {
		k := gen_int32(8)
		v := gen_float32(108)
		dst.MapInt32Float[k] = v
	}
	if include[1] {
		k := gen_int32(9)
		v := gen_float32(109)
		dst.MapInt32Float[k] = v
	}
	if include[1] {
		k := gen_int32(10)
		v := gen_float32(110)
		dst.MapInt32Float[k] = v
	}
	if include[1] {
		k := gen_int32(11)
		v := gen_float32(111)
		dst.MapInt32Float[k] = v
	}
	if include[1] {
		k := gen_int32(12)
		v := gen_float32(112)
		dst.MapInt32Float[k] = v
	}
	dst.MapInt32Int32 = make(map[int32]int32)
	if include[2] {
		k := gen_int32(13)
		v := gen_int32(113)
		dst.MapInt32Int32[k] = v
	}
	if include[2] {
		k := gen_int32(14)
		v := gen_int32(114)
		dst.MapInt32Int32[k] = v
	}
	if include[2] {
		k := gen_int32(15)
		v := gen_int32(115)
		dst.MapInt32Int32[k] = v
	}
	if include[2] {
		k := gen_int32(16)
		v := gen_int32(116)
		dst.MapInt32Int32[k] = v
	}
	if include[2] {
		k := gen_int32(17)
		v := gen_int32(117)
		dst.MapInt32Int32[k] = v
	}
	if include[2] {
		k := gen_int32(18)
		v := gen_int32(118)
		dst.MapInt32Int32[k] = v
	}
	dst.MapInt32Int64 = make(map[int32]int64)
	if include[3] {
		k := gen_int32(19)
		v := gen_int64(119)
		dst.MapInt32Int64[k] = v
	}
	if include[3] {
		k := gen_int32(20)
		v := gen_int64(120)
		dst.MapInt32Int64[k] = v
	}
	if include[3] {
		k := gen_int32(21)
		v := gen_int64(121)
		dst.MapInt32Int64[k] = v
	}
	if include[3] {
		k := gen_int32(22)
		v := gen_int64(122)
		dst.MapInt32Int64[k] = v
	}
	if include[3] {
		k := gen_int32(23)
		v := gen_int64(123)
		dst.MapInt32Int64[k] = v
	}
	if include[3] {
		k := gen_int32(24)
		v := gen_int64(124)
		dst.MapInt32Int64[k] = v
	}
	dst.MapInt32Uint32 = make(map[int32]uint32)
	if include[4] {
		k := gen_int32(25)
		v := gen_uint32(125)
		dst.MapInt32Uint32[k] = v
	}
	if include[4] {
		k := gen_int32(26)
		v := gen_uint32(126)
		dst.MapInt32Uint32[k] = v
	}
	if include[4] {
		k := gen_int32(27)
		v := gen_uint32(127)
		dst.MapInt32Uint32[k] = v
	}
	if include[4] {
		k := gen_int32(28)
		v := gen_uint32(128)
		dst.MapInt32Uint32[k] = v
	}
	if include[4] {
		k := gen_int32(29)
		v := gen_uint32(129)
		dst.MapInt32Uint32[k] = v
	}
	if include[4] {
		k := gen_int32(30)
		v := gen_uint32(130)
		dst.MapInt32Uint32[k] = v
	}
	dst.MapInt32Uint64 = make(map[int32]uint64)
	if include[5] {
		k := gen_int32(31)
		v := gen_uint64(131)
		dst.MapInt32Uint64[k] = v
	}
	if include[5] {
		k := gen_int32(32)
		v := gen_uint64(132)
		dst.MapInt32Uint64[k] = v
	}
	if include[5] {
		k := gen_int32(33)
		v := gen_uint64(133)
		dst.MapInt32Uint64[k] = v
	}
	if include[5] {
		k := gen_int32(34)
		v := gen_uint64(134)
		dst.MapInt32Uint64[k] = v
	}
	if include[5] {
		k := gen_int32(35)
		v := gen_uint64(135)
		dst.MapInt32Uint64[k] = v
	}
	if include[5] {
		k := gen_int32(36)
		v := gen_uint64(136)
		dst.MapInt32Uint64[k] = v
	}
	dst.MapInt32Sint32 = make(map[int32]int32)
	if include[6] {
		k := gen_int32(37)
		v := gen_int32(137)
		dst.MapInt32Sint32[k] = v
	}
	if include[6] {
		k := gen_int32(38)
		v := gen_int32(138)
		dst.MapInt32Sint32[k] = v
	}
	if include[6] {
		k := gen_int32(39)
		v := gen_int32(139)
		dst.MapInt32Sint32[k] = v
	}
	if include[6] {
		k := gen_int32(40)
		v := gen_int32(140)
		dst.MapInt32Sint32[k] = v
	}
	if include[6] {
		k := gen_int32(41)
		v := gen_int32(141)
		dst.MapInt32Sint32[k] = v
	}
	if include[6] {
		k := gen_int32(42)
		v := gen_int32(142)
		dst.MapInt32Sint32[k] = v
	}
	dst.MapInt32Sint64 = make(map[int32]int64)
	if include[7] {
		k := gen_int32(43)
		v := gen_int64(143)
		dst.MapInt32Sint64[k] = v
	}
	if include[7] {
		k := gen_int32(44)
		v := gen_int64(144)
		dst.MapInt32Sint64[k] = v
	}
	if include[7] {
		k := gen_int32(45)
		v := gen_int64(145)
		dst.MapInt32Sint64[k] = v
	}
	if include[7] {
		k := gen_int32(46)
		v := gen_int64(146)
		dst.MapInt32Sint64[k] = v
	}
	if include[7] {
		k := gen_int32(47)
		v := gen_int64(147)
		dst.MapInt32Sint64[k] = v
	}
	if include[7] {
		k := gen_int32(48)
		v := gen_int64(148)
		dst.MapInt32Sint64[k] = v
	}
	dst.MapInt32Fixed32 = make(map[int32]uint32)
	if include[8] {
		k := gen_int32(49)
		v := gen_uint32(149)
		dst.MapInt32Fixed32[k] = v
	}
	if include[8] {
		k := gen_int32(50)
		v := gen_uint32(150)
		dst.MapInt32Fixed32[k] = v
	}
	if include[8] {
		k := gen_int32(51)
		v := gen_uint32(151)
		dst.MapInt32Fixed32[k] = v
	}
	if include[8] {
		k := gen_int32(52)
		v := gen_uint32(152)
		dst.MapInt32Fixed32[k] = v
	}
	if include[8] {
		k := gen_int32(53)
		v := gen_uint32(153)
		dst.MapInt32Fixed32[k] = v
	}
	if include[8] {
		k := gen_int32(54)
		v := gen_uint32(154)
		dst.MapInt32Fixed32[k] = v
	}
	dst.MapInt32Fixed64 = make(map[int32]uint64)
	if include[9] {
		k := gen_int32(55)
		v := gen_uint64(155)
		dst.MapInt32Fixed64[k] = v
	}
	if include[9] {
		k := gen_int32(56)
		v := gen_uint64(156)
		dst.MapInt32Fixed64[k] = v
	}
	if include[9] {
		k := gen_int32(57)
		v := gen_uint64(157)
		dst.MapInt32Fixed64[k] = v
	}
	if include[9] {
		k := gen_int32(58)
		v := gen_uint64(158)
		dst.MapInt32Fixed64[k] = v
	}
	if include[9] {
		k := gen_int32(59)
		v := gen_uint64(159)
		dst.MapInt32Fixed64[k] = v
	}
	if include[9] {
		k := gen_int32(60)
		v := gen_uint64(160)
		dst.MapInt32Fixed64[k] = v
	}
	dst.MapInt32Sfixed32 = make(map[int32]int32)
	if include[10] {
		k := gen_int32(61)
		v := gen_int32(161)
		dst.MapInt32Sfixed32[k] = v
	}
	if include[10] {
		k := gen_int32(62)
		v := gen_int32(162)
		dst.MapInt32Sfixed32[k] = v
	}
	if include[10] {
		k := gen_int32(63)
		v := gen_int32(163)
		dst.MapInt32Sfixed32[k] = v
	}
	if include[10] {
		k := gen_int32(64)
		v := gen_int32(164)
		dst.MapInt32Sfixed32[k] = v
	}
	if include[10] {
		k := gen_int32(65)
		v := gen_int32(165)
		dst.MapInt32Sfixed32[k] = v
	}
	if include[10] {
		k := gen_int32(66)
		v := gen_int32(166)
		dst.MapInt32Sfixed32[k] = v
	}
	dst.MapInt32Sfixed64 = make(map[int32]int64)
	if include[11] {
		k := gen_int32(67)
		v := gen_int64(167)
		dst.MapInt32Sfixed64[k] = v
	}
	if include[11] {
		k := gen_int32(68)
		v := gen_int64(168)
		dst.MapInt32Sfixed64[k] = v
	}
	if include[11] {
		k := gen_int32(69)
		v := gen_int64(169)
		dst.MapInt32Sfixed64[k] = v
	}
	if include[11] {
		k := gen_int32(70)
		v := gen_int64(170)
		dst.MapInt32Sfixed64[k] = v
	}
	if include[11] {
		k := gen_int32(71)
		v := gen_int64(171)
		dst.MapInt32Sfixed64[k] = v
	}
	if include[11] {
		k := gen_int32(72)
		v := gen_int64(172)
		dst.MapInt32Sfixed64[k] = v
	}
	dst.MapInt32Bool = make(map[int32]bool)
	if include[12] {
		k := gen_int32(73)
		v := gen_bool(173)
		dst.MapInt32Bool[k] = v
	}
	if include[12] {
		k := gen_int32(74)
		v := gen_bool(174)
		dst.MapInt32Bool[k] = v
	}
	if include[12] {
		k := gen_int32(75)
		v := gen_bool(175)
		dst.MapInt32Bool[k] = v
	}
	if include[12] {
		k := gen_int32(76)
		v := gen_bool(176)
		dst.MapInt32Bool[k] = v
	}
	if include[12] {
		k := gen_int32(77)
		v := gen_bool(177)
		dst.MapInt32Bool[k] = v
	}
	if include[12] {
		k := gen_int32(78)
		v := gen_bool(178)
		dst.MapInt32Bool[k] = v
	}
	dst.MapInt32String = make(map[int32]string)
	if include[13] {
		k := gen_int32(79)
		v := gen_string(179)
		dst.MapInt32String[k] = v
	}
	if include[13] {
		k := gen_int32(80)
		v := gen_string(180)
		dst.MapInt32String[k] = v
	}
	if include[13] {
		k := gen_int32(81)
		v := gen_string(181)
		dst.MapInt32String[k] = v
	}
	if include[13] {
		k := gen_int32(82)
		v := gen_string(182)
		dst.MapInt32String[k] = v
	}
	if include[13] {
		k := gen_int32(83)
		v := gen_string(183)
		dst.MapInt32String[k] = v
	}
	if include[13] {
		k := gen_int32(84)
		v := gen_string(184)
		dst.MapInt32String[k] = v
	}
	dst.MapInt32Bytes = make(map[int32][]byte)
	if include[14] {
		k := gen_int32(85)
		v := gen_bytes(185)
		dst.MapInt32Bytes[k] = v
	}
	if include[14] {
		k := gen_int32(86)
		v := gen_bytes(186)
		dst.MapInt32Bytes[k] = v
	}
	if include[14] {
		k := gen_int32(87)
		v := gen_bytes(187)
		dst.MapInt32Bytes[k] = v
	}
	if include[14] {
		k := gen_int32(88)
		v := gen_bytes(188)
		dst.MapInt32Bytes[k] = v
	}
	if include[14] {
		k := gen_int32(89)
		v := gen_bytes(189)
		dst.MapInt32Bytes[k] = v
	}
	if include[14] {
		k := gen_int32(90)
		v := gen_bytes(190)
		dst.MapInt32Bytes[k] = v
	}
	dst.MapInt32_Item = make(map[int32]*Item)
	if include[15] {
		k := gen_int32(91)
		v := gen_Item(191)
		dst.MapInt32_Item[k] = v
	}
	if include[15] {
		k := gen_int32(92)
		v := gen_Item(192)
		dst.MapInt32_Item[k] = v
	}
	if include[15] {
		k := gen_int32(93)
		v := gen_Item(193)
		dst.MapInt32_Item[k] = v
	}
	if include[15] {
		k := gen_int32(94)
		v := gen_Item(194)
		dst.MapInt32_Item[k] = v
	}
	if include[15] {
		k := gen_int32(95)
		v := gen_Item(195)
		dst.MapInt32_Item[k] = v
	}
	if include[15] {
		k := gen_int32(96)
		v := gen_Item(196)
		dst.MapInt32_Item[k] = v
	}
	dst.MapInt64Double = make(map[int64]float64)
	if include[16] {
		k := gen_int64(97)
		v := gen_float64(197)
		dst.MapInt64Double[k] = v
	}
	if include[16] {
		k := gen_int64(98)
		v := gen_float64(198)
		dst.MapInt64Double[k] = v
	}
	if include[16] {
		k := gen_int64(99)
		v := gen_float64(199)
		dst.MapInt64Double[k] = v
	}
	if include[16] {
		k := gen_int64(100)
		v := gen_float64(200)
		dst.MapInt64Double[k] = v
	}
	if include[16] {
		k := gen_int64(101)
		v := gen_float64(201)
		dst.MapInt64Double[k] = v
	}
	if include[16] {
		k := gen_int64(102)
		v := gen_float64(202)
		dst.MapInt64Double[k] = v
	}
	dst.MapInt64Float = make(map[int64]float32)
	if include[17] {
		k := gen_int64(103)
		v := gen_float32(203)
		dst.MapInt64Float[k] = v
	}
	if include[17] {
		k := gen_int64(104)
		v := gen_float32(204)
		dst.MapInt64Float[k] = v
	}
	if include[17] {
		k := gen_int64(105)
		v := gen_float32(205)
		dst.MapInt64Float[k] = v
	}
	if include[17] {
		k := gen_int64(106)
		v := gen_float32(206)
		dst.MapInt64Float[k] = v
	}
	if include[17] {
		k := gen_int64(107)
		v := gen_float32(207)
		dst.MapInt64Float[k] = v
	}
	if include[17] {
		k := gen_int64(108)
		v := gen_float32(208)
		dst.MapInt64Float[k] = v
	}
	dst.MapInt64Int32 = make(map[int64]int32)
	if include[18] {
		k := gen_int64(109)
		v := gen_int32(209)
		dst.MapInt64Int32[k] = v
	}
	if include[18] {
		k := gen_int64(110)
		v := gen_int32(210)
		dst.MapInt64Int32[k] = v
	}
	if include[18] {
		k := gen_int64(111)
		v := gen_int32(211)
		dst.MapInt64Int32[k] = v
	}
	if include[18] {
		k := gen_int64(112)
		v := gen_int32(212)
		dst.MapInt64Int32[k] = v
	}
	if include[18] {
		k := gen_int64(113)
		v := gen_int32(213)
		dst.MapInt64Int32[k] = v
	}
	if include[18] {
		k := gen_int64(114)
		v := gen_int32(214)
		dst.MapInt64Int32[k] = v
	}
	dst.MapInt64Int64 = make(map[int64]int64)
	if include[19] {
		k := gen_int64(115)
		v := gen_int64(215)
		dst.MapInt64Int64[k] = v
	}
	if include[19] {
		k := gen_int64(116)
		v := gen_int64(216)
		dst.MapInt64Int64[k] = v
	}
	if include[19] {
		k := gen_int64(117)
		v := gen_int64(217)
		dst.MapInt64Int64[k] = v
	}
	if include[19] {
		k := gen_int64(118)
		v := gen_int64(218)
		dst.MapInt64Int64[k] = v
	}
	if include[19] {
		k := gen_int64(119)
		v := gen_int64(219)
		dst.MapInt64Int64[k] = v
	}
	if include[19] {
		k := gen_int64(120)
		v := gen_int64(220)
		dst.MapInt64Int64[k] = v
	}
	dst.MapInt64Uint32 = make(map[int64]uint32)
	if include[20] {
		k := gen_int64(121)
		v := gen_uint32(221)
		dst.MapInt64Uint32[k] = v
	}
	if include[20] {
		k := gen_int64(122)
		v := gen_uint32(222)
		dst.MapInt64Uint32[k] = v
	}
	if include[20] {
		k := gen_int64(123)
		v := gen_uint32(223)
		dst.MapInt64Uint32[k] = v
	}
	if include[20] {
		k := gen_int64(124)
		v := gen_uint32(224)
		dst.MapInt64Uint32[k] = v
	}
	if include[20] {
		k := gen_int64(125)
		v := gen_uint32(225)
		dst.MapInt64Uint32[k] = v
	}
	if include[20] {
		k := gen_int64(126)
		v := gen_uint32(226)
		dst.MapInt64Uint32[k] = v
	}
	dst.MapInt64Uint64 = make(map[int64]uint64)
	if include[21] {
		k := gen_int64(127)
		v := gen_uint64(227)
		dst.MapInt64Uint64[k] = v
	}
	if include[21] {
		k := gen_int64(128)
		v := gen_uint64(228)
		dst.MapInt64Uint64[k] = v
	}
	if include[21] {
		k := gen_int64(129)
		v := gen_uint64(229)
		dst.MapInt64Uint64[k] = v
	}
	if include[21] {
		k := gen_int64(130)
		v := gen_uint64(230)
		dst.MapInt64Uint64[k] = v
	}
	if include[21] {
		k := gen_int64(131)
		v := gen_uint64(231)
		dst.MapInt64Uint64[k] = v
	}
	if include[21] {
		k := gen_int64(132)
		v := gen_uint64(232)
		dst.MapInt64Uint64[k] = v
	}
	dst.MapInt64Sint32 = make(map[int64]int32)
	if include[22] {
		k := gen_int64(133)
		v := gen_int32(233)
		dst.MapInt64Sint32[k] = v
	}
	if include[22] {
		k := gen_int64(134)
		v := gen_int32(234)
		dst.MapInt64Sint32[k] = v
	}
	if include[22] {
		k := gen_int64(135)
		v := gen_int32(235)
		dst.MapInt64Sint32[k] = v
	}
	if include[22] {
		k := gen_int64(136)
		v := gen_int32(236)
		dst.MapInt64Sint32[k] = v
	}
	if include[22] {
		k := gen_int64(137)
		v := gen_int32(237)
		dst.MapInt64Sint32[k] = v
	}
	if include[22] {
		k := gen_int64(138)
		v := gen_int32(238)
		dst.MapInt64Sint32[k] = v
	}
	dst.MapInt64Sint64 = make(map[int64]int64)
	if include[23] {
		k := gen_int64(139)
		v := gen_int64(239)
		dst.MapInt64Sint64[k] = v
	}
	if include[23] {
		k := gen_int64(140)
		v := gen_int64(240)
		dst.MapInt64Sint64[k] = v
	}
	if include[23] {
		k := gen_int64(141)
		v := gen_int64(241)
		dst.MapInt64Sint64[k] = v
	}
	if include[23] {
		k := gen_int64(142)
		v := gen_int64(242)
		dst.MapInt64Sint64[k] = v
	}
	if include[23] {
		k := gen_int64(143)
		v := gen_int64(243)
		dst.MapInt64Sint64[k] = v
	}
	if include[23] {
		k := gen_int64(144)
		v := gen_int64(244)
		dst.MapInt64Sint64[k] = v
	}
	dst.MapInt64Fixed32 = make(map[int64]uint32)
	if include[24] {
		k := gen_int64(145)
		v := gen_uint32(245)
		dst.MapInt64Fixed32[k] = v
	}
	if include[24] {
		k := gen_int64(146)
		v := gen_uint32(246)
		dst.MapInt64Fixed32[k] = v
	}
	if include[24] {
		k := gen_int64(147)
		v := gen_uint32(247)
		dst.MapInt64Fixed32[k] = v
	}
	if include[24] {
		k := gen_int64(148)
		v := gen_uint32(248)
		dst.MapInt64Fixed32[k] = v
	}
	if include[24] {
		k := gen_int64(149)
		v := gen_uint32(249)
		dst.MapInt64Fixed32[k] = v
	}
	if include[24] {
		k := gen_int64(150)
		v := gen_uint32(250)
		dst.MapInt64Fixed32[k] = v
	}
	dst.MapInt64Fixed64 = make(map[int64]uint64)
	if include[25] {
		k := gen_int64(151)
		v := gen_uint64(251)
		dst.MapInt64Fixed64[k] = v
	}
	if include[25] {
		k := gen_int64(152)
		v := gen_uint64(252)
		dst.MapInt64Fixed64[k] = v
	}
	if include[25] {
		k := gen_int64(153)
		v := gen_uint64(253)
		dst.MapInt64Fixed64[k] = v
	}
	if include[25] {
		k := gen_int64(154)
		v := gen_uint64(254)
		dst.MapInt64Fixed64[k] = v
	}
	if include[25] {
		k := gen_int64(155)
		v := gen_uint64(255)
		dst.MapInt64Fixed64[k] = v
	}
	if include[25] {
		k := gen_int64(156)
		v := gen_uint64(256)
		dst.MapInt64Fixed64[k] = v
	}
	dst.MapInt64Sfixed32 = make(map[int64]int32)
	if include[26] {
		k := gen_int64(157)
		v := gen_int32(257)
		dst.MapInt64Sfixed32[k] = v
	}
	if include[26] {
		k := gen_int64(158)
		v := gen_int32(258)
		dst.MapInt64Sfixed32[k] = v
	}
	if include[26] {
		k := gen_int64(159)
		v := gen_int32(259)
		dst.MapInt64Sfixed32[k] = v
	}
	if include[26] {
		k := gen_int64(160)
		v := gen_int32(260)
		dst.MapInt64Sfixed32[k] = v
	}
	if include[26] {
		k := gen_int64(161)
		v := gen_int32(261)
		dst.MapInt64Sfixed32[k] = v
	}
	if include[26] {
		k := gen_int64(162)
		v := gen_int32(262)
		dst.MapInt64Sfixed32[k] = v
	}
	dst.MapInt64Sfixed64 = make(map[int64]int64)
	if include[27] {
		k := gen_int64(163)
		v := gen_int64(263)
		dst.MapInt64Sfixed64[k] = v
	}
	if include[27] {
		k := gen_int64(164)
		v := gen_int64(264)
		dst.MapInt64Sfixed64[k] = v
	}
	if include[27] {
		k := gen_int64(165)
		v := gen_int64(265)
		dst.MapInt64Sfixed64[k] = v
	}
	if include[27] {
		k := gen_int64(166)
		v := gen_int64(266)
		dst.MapInt64Sfixed64[k] = v
	}
	if include[27] {
		k := gen_int64(167)
		v := gen_int64(267)
		dst.MapInt64Sfixed64[k] = v
	}
	if include[27] {
		k := gen_int64(168)
		v := gen_int64(268)
		dst.MapInt64Sfixed64[k] = v
	}
	dst.MapInt64Bool = make(map[int64]bool)
	if include[28] {
		k := gen_int64(169)
		v := gen_bool(269)
		dst.MapInt64Bool[k] = v
	}
	if include[28] {
		k := gen_int64(170)
		v := gen_bool(270)
		dst.MapInt64Bool[k] = v
	}
	if include[28] {
		k := gen_int64(171)
		v := gen_bool(271)
		dst.MapInt64Bool[k] = v
	}
	if include[28] {
		k := gen_int64(172)
		v := gen_bool(272)
		dst.MapInt64Bool[k] = v
	}
	if include[28] {
		k := gen_int64(173)
		v := gen_bool(273)
		dst.MapInt64Bool[k] = v
	}
	if include[28] {
		k := gen_int64(174)
		v := gen_bool(274)
		dst.MapInt64Bool[k] = v
	}
	dst.MapInt64String = make(map[int64]string)
	if include[29] {
		k := gen_int64(175)
		v := gen_string(275)
		dst.MapInt64String[k] = v
	}
	if include[29] {
		k := gen_int64(176)
		v := gen_string(276)
		dst.MapInt64String[k] = v
	}
	if include[29] {
		k := gen_int64(177)
		v := gen_string(277)
		dst.MapInt64String[k] = v
	}
	if include[29] {
		k := gen_int64(178)
		v := gen_string(278)
		dst.MapInt64String[k] = v
	}
	if include[29] {
		k := gen_int64(179)
		v := gen_string(279)
		dst.MapInt64String[k] = v
	}
	if include[29] {
		k := gen_int64(180)
		v := gen_string(280)
		dst.MapInt64String[k] = v
	}
	dst.MapInt64Bytes = make(map[int64][]byte)
	if include[30] {
		k := gen_int64(181)
		v := gen_bytes(281)
		dst.MapInt64Bytes[k] = v
	}
	if include[30] {
		k := gen_int64(182)
		v := gen_bytes(282)
		dst.MapInt64Bytes[k] = v
	}
	if include[30] {
		k := gen_int64(183)
		v := gen_bytes(283)
		dst.MapInt64Bytes[k] = v
	}
	if include[30] {
		k := gen_int64(184)
		v := gen_bytes(284)
		dst.MapInt64Bytes[k] = v
	}
	if include[30] {
		k := gen_int64(185)
		v := gen_bytes(285)
		dst.MapInt64Bytes[k] = v
	}
	if include[30] {
		k := gen_int64(186)
		v := gen_bytes(286)
		dst.MapInt64Bytes[k] = v
	}
	dst.MapInt64_Item = make(map[int64]*Item)
	if include[31] {
		k := gen_int64(187)
		v := gen_Item(287)
		dst.MapInt64_Item[k] = v
	}
	if include[31] {
		k := gen_int64(188)
		v := gen_Item(288)
		dst.MapInt64_Item[k] = v
	}
	if include[31] {
		k := gen_int64(189)
		v := gen_Item(289)
		dst.MapInt64_Item[k] = v
	}
	if include[31] {
		k := gen_int64(190)
		v := gen_Item(290)
		dst.MapInt64_Item[k] = v
	}
	if include[31] {
		k := gen_int64(191)
		v := gen_Item(291)
		dst.MapInt64_Item[k] = v
	}
	if include[31] {
		k := gen_int64(192)
		v := gen_Item(292)
		dst.MapInt64_Item[k] = v
	}
	dst.MapUint32Double = make(map[uint32]float64)
	if include[32] {
		k := gen_uint32(193)
		v := gen_float64(293)
		dst.MapUint32Double[k] = v
	}
	if include[32] {
		k := gen_uint32(194)
		v := gen_float64(294)
		dst.MapUint32Double[k] = v
	}
	if include[32] {
		k := gen_uint32(195)
		v := gen_float64(295)
		dst.MapUint32Double[k] = v
	}
	if include[32] {
		k := gen_uint32(196)
		v := gen_float64(296)
		dst.MapUint32Double[k] = v
	}
	if include[32] {
		k := gen_uint32(197)
		v := gen_float64(297)
		dst.MapUint32Double[k] = v
	}
	if include[32] {
		k := gen_uint32(198)
		v := gen_float64(298)
		dst.MapUint32Double[k] = v
	}
	dst.MapUint32Float = make(map[uint32]float32)
	if include[33] {
		k := gen_uint32(199)
		v := gen_float32(299)
		dst.MapUint32Float[k] = v
	}
	if include[33] {
		k := gen_uint32(200)
		v := gen_float32(300)
		dst.MapUint32Float[k] = v
	}
	if include[33] {
		k := gen_uint32(201)
		v := gen_float32(301)
		dst.MapUint32Float[k] = v
	}
	if include[33] {
		k := gen_uint32(202)
		v := gen_float32(302)
		dst.MapUint32Float[k] = v
	}
	if include[33] {
		k := gen_uint32(203)
		v := gen_float32(303)
		dst.MapUint32Float[k] = v
	}
	if include[33] {
		k := gen_uint32(204)
		v := gen_float32(304)
		dst.MapUint32Float[k] = v
	}
	dst.MapUint32Int32 = make(map[uint32]int32)
	if include[34] {
		k := gen_uint32(205)
		v := gen_int32(305)
		dst.MapUint32Int32[k] = v
	}
	if include[34] {
		k := gen_uint32(206)
		v := gen_int32(306)
		dst.MapUint32Int32[k] = v
	}
	if include[34] {
		k := gen_uint32(207)
		v := gen_int32(307)
		dst.MapUint32Int32[k] = v
	}
	if include[34] {
		k := gen_uint32(208)
		v := gen_int32(308)
		dst.MapUint32Int32[k] = v
	}
	if include[34] {
		k := gen_uint32(209)
		v := gen_int32(309)
		dst.MapUint32Int32[k] = v
	}
	if include[34] {
		k := gen_uint32(210)
		v := gen_int32(310)
		dst.MapUint32Int32[k] = v
	}
	dst.MapUint32Int64 = make(map[uint32]int64)
	if include[35] {
		k := gen_uint32(211)
		v := gen_int64(311)
		dst.MapUint32Int64[k] = v
	}
	if include[35] {
		k := gen_uint32(212)
		v := gen_int64(312)
		dst.MapUint32Int64[k] = v
	}
	if include[35] {
		k := gen_uint32(213)
		v := gen_int64(313)
		dst.MapUint32Int64[k] = v
	}
	if include[35] {
		k := gen_uint32(214)
		v := gen_int64(314)
		dst.MapUint32Int64[k] = v
	}
	if include[35] {
		k := gen_uint32(215)
		v := gen_int64(315)
		dst.MapUint32Int64[k] = v
	}
	if include[35] {
		k := gen_uint32(216)
		v := gen_int64(316)
		dst.MapUint32Int64[k] = v
	}
	dst.MapUint32Uint32 = make(map[uint32]uint32)
	if include[36] {
		k := gen_uint32(217)
		v := gen_uint32(317)
		dst.MapUint32Uint32[k] = v
	}
	if include[36] {
		k := gen_uint32(218)
		v := gen_uint32(318)
		dst.MapUint32Uint32[k] = v
	}
	if include[36] {
		k := gen_uint32(219)
		v := gen_uint32(319)
		dst.MapUint32Uint32[k] = v
	}
	if include[36] {
		k := gen_uint32(220)
		v := gen_uint32(320)
		dst.MapUint32Uint32[k] = v
	}
	if include[36] {
		k := gen_uint32(221)
		v := gen_uint32(321)
		dst.MapUint32Uint32[k] = v
	}
	if include[36] {
		k := gen_uint32(222)
		v := gen_uint32(322)
		dst.MapUint32Uint32[k] = v
	}
	dst.MapUint32Uint64 = make(map[uint32]uint64)
	if include[37] {
		k := gen_uint32(223)
		v := gen_uint64(323)
		dst.MapUint32Uint64[k] = v
	}
	if include[37] {
		k := gen_uint32(224)
		v := gen_uint64(324)
		dst.MapUint32Uint64[k] = v
	}
	if include[37] {
		k := gen_uint32(225)
		v := gen_uint64(325)
		dst.MapUint32Uint64[k] = v
	}
	if include[37] {
		k := gen_uint32(226)
		v := gen_uint64(326)
		dst.MapUint32Uint64[k] = v
	}
	if include[37] {
		k := gen_uint32(227)
		v := gen_uint64(327)
		dst.MapUint32Uint64[k] = v
	}
	if include[37] {
		k := gen_uint32(228)
		v := gen_uint64(328)
		dst.MapUint32Uint64[k] = v
	}
	dst.MapUint32Sint32 = make(map[uint32]int32)
	if include[38] {
		k := gen_uint32(229)
		v := gen_int32(329)
		dst.MapUint32Sint32[k] = v
	}
	if include[38] {
		k := gen_uint32(230)
		v := gen_int32(330)
		dst.MapUint32Sint32[k] = v
	}
	if include[38] {
		k := gen_uint32(231)
		v := gen_int32(331)
		dst.MapUint32Sint32[k] = v
	}
	if include[38] {
		k := gen_uint32(232)
		v := gen_int32(332)
		dst.MapUint32Sint32[k] = v
	}
	if include[38] {
		k := gen_uint32(233)
		v := gen_int32(333)
		dst.MapUint32Sint32[k] = v
	}
	if include[38] {
		k := gen_uint32(234)
		v := gen_int32(334)
		dst.MapUint32Sint32[k] = v
	}
	dst.MapUint32Sint64 = make(map[uint32]int64)
	if include[39] {
		k := gen_uint32(235)
		v := gen_int64(335)
		dst.MapUint32Sint64[k] = v
	}
	if include[39] {
		k := gen_uint32(236)
		v := gen_int64(336)
		dst.MapUint32Sint64[k] = v
	}
	if include[39] {
		k := gen_uint32(237)
		v := gen_int64(337)
		dst.MapUint32Sint64[k] = v
	}
	if include[39] {
		k := gen_uint32(238)
		v := gen_int64(338)
		dst.MapUint32Sint64[k] = v
	}
	if include[39] {
		k := gen_uint32(239)
		v := gen_int64(339)
		dst.MapUint32Sint64[k] = v
	}
	if include[39] {
		k := gen_uint32(240)
		v := gen_int64(340)
		dst.MapUint32Sint64[k] = v
	}
	dst.MapUint32Fixed32 = make(map[uint32]uint32)
	if include[40] {
		k := gen_uint32(241)
		v := gen_uint32(341)
		dst.MapUint32Fixed32[k] = v
	}
	if include[40] {
		k := gen_uint32(242)
		v := gen_uint32(342)
		dst.MapUint32Fixed32[k] = v
	}
	if include[40] {
		k := gen_uint32(243)
		v := gen_uint32(343)
		dst.MapUint32Fixed32[k] = v
	}
	if include[40] {
		k := gen_uint32(244)
		v := gen_uint32(344)
		dst.MapUint32Fixed32[k] = v
	}
	if include[40] {
		k := gen_uint32(245)
		v := gen_uint32(345)
		dst.MapUint32Fixed32[k] = v
	}
	if include[40] {
		k := gen_uint32(246)
		v := gen_uint32(346)
		dst.MapUint32Fixed32[k] = v
	}
	dst.MapUint32Fixed64 = make(map[uint32]uint64)
	if include[41] {
		k := gen_uint32(247)
		v := gen_uint64(347)
		dst.MapUint32Fixed64[k] = v
	}
	if include[41] {
		k := gen_uint32(248)
		v := gen_uint64(348)
		dst.MapUint32Fixed64[k] = v
	}
	if include[41] {
		k := gen_uint32(249)
		v := gen_uint64(349)
		dst.MapUint32Fixed64[k] = v
	}
	if include[41] {
		k := gen_uint32(250)
		v := gen_uint64(350)
		dst.MapUint32Fixed64[k] = v
	}
	if include[41] {
		k := gen_uint32(251)
		v := gen_uint64(351)
		dst.MapUint32Fixed64[k] = v
	}
	if include[41] {
		k := gen_uint32(252)
		v := gen_uint64(352)
		dst.MapUint32Fixed64[k] = v
	}
	dst.MapUint32Sfixed32 = make(map[uint32]int32)
	if include[42] {
		k := gen_uint32(253)
		v := gen_int32(353)
		dst.MapUint32Sfixed32[k] = v
	}
	if include[42] {
		k := gen_uint32(254)
		v := gen_int32(354)
		dst.MapUint32Sfixed32[k] = v
	}
	if include[42] {
		k := gen_uint32(255)
		v := gen_int32(355)
		dst.MapUint32Sfixed32[k] = v
	}
	if include[42] {
		k := gen_uint32(256)
		v := gen_int32(356)
		dst.MapUint32Sfixed32[k] = v
	}
	if include[42] {
		k := gen_uint32(257)
		v := gen_int32(357)
		dst.MapUint32Sfixed32[k] = v
	}
	if include[42] {
		k := gen_uint32(258)
		v := gen_int32(358)
		dst.MapUint32Sfixed32[k] = v
	}
	dst.MapUint32Sfixed64 = make(map[uint32]int64)
	if include[43] {
		k := gen_uint32(259)
		v := gen_int64(359)
		dst.MapUint32Sfixed64[k] = v
	}
	if include[43] {
		k := gen_uint32(260)
		v := gen_int64(360)
		dst.MapUint32Sfixed64[k] = v
	}
	if include[43] {
		k := gen_uint32(261)
		v := gen_int64(361)
		dst.MapUint32Sfixed64[k] = v
	}
	if include[43] {
		k := gen_uint32(262)
		v := gen_int64(362)
		dst.MapUint32Sfixed64[k] = v
	}
	if include[43] {
		k := gen_uint32(263)
		v := gen_int64(363)
		dst.MapUint32Sfixed64[k] = v
	}
	if include[43] {
		k := gen_uint32(264)
		v := gen_int64(364)
		dst.MapUint32Sfixed64[k] = v
	}
	dst.MapUint32Bool = make(map[uint32]bool)
	if include[44] {
		k := gen_uint32(265)
		v := gen_bool(365)
		dst.MapUint32Bool[k] = v
	}
	if include[44] {
		k := gen_uint32(266)
		v := gen_bool(366)
		dst.MapUint32Bool[k] = v
	}
	if include[44] {
		k := gen_uint32(267)
		v := gen_bool(367)
		dst.MapUint32Bool[k] = v
	}
	if include[44] {
		k := gen_uint32(268)
		v := gen_bool(368)
		dst.MapUint32Bool[k] = v
	}
	if include[44] {
		k := gen_uint32(269)
		v := gen_bool(369)
		dst.MapUint32Bool[k] = v
	}
	if include[44] {
		k := gen_uint32(270)
		v := gen_bool(370)
		dst.MapUint32Bool[k] = v
	}
	dst.MapUint32String = make(map[uint32]string)
	if include[45] {
		k := gen_uint32(271)
		v := gen_string(371)
		dst.MapUint32String[k] = v
	}
	if include[45] {
		k := gen_uint32(272)
		v := gen_string(372)
		dst.MapUint32String[k] = v
	}
	if include[45] {
		k := gen_uint32(273)
		v := gen_string(373)
		dst.MapUint32String[k] = v
	}
	if include[45] {
		k := gen_uint32(274)
		v := gen_string(374)
		dst.MapUint32String[k] = v
	}
	if include[45] {
		k := gen_uint32(275)
		v := gen_string(375)
		dst.MapUint32String[k] = v
	}
	if include[45] {
		k := gen_uint32(276)
		v := gen_string(376)
		dst.MapUint32String[k] = v
	}
	dst.MapUint32Bytes = make(map[uint32][]byte)
	if include[46] {
		k := gen_uint32(277)
		v := gen_bytes(377)
		dst.MapUint32Bytes[k] = v
	}
	if include[46] {
		k := gen_uint32(278)
		v := gen_bytes(378)
		dst.MapUint32Bytes[k] = v
	}
	if include[46] {
		k := gen_uint32(279)
		v := gen_bytes(379)
		dst.MapUint32Bytes[k] = v
	}
	if include[46] {
		k := gen_uint32(280)
		v := gen_bytes(380)
		dst.MapUint32Bytes[k] = v
	}
	if include[46] {
		k := gen_uint32(281)
		v := gen_bytes(381)
		dst.MapUint32Bytes[k] = v
	}
	if include[46] {
		k := gen_uint32(282)
		v := gen_bytes(382)
		dst.MapUint32Bytes[k] = v
	}
	dst.MapUint32_Item = make(map[uint32]*Item)
	if include[47] {
		k := gen_uint32(283)
		v := gen_Item(383)
		dst.MapUint32_Item[k] = v
	}
	if include[47] {
		k := gen_uint32(284)
		v := gen_Item(384)
		dst.MapUint32_Item[k] = v
	}
	if include[47] {
		k := gen_uint32(285)
		v := gen_Item(385)
		dst.MapUint32_Item[k] = v
	}
	if include[47] {
		k := gen_uint32(286)
		v := gen_Item(386)
		dst.MapUint32_Item[k] = v
	}
	if include[47] {
		k := gen_uint32(287)
		v := gen_Item(387)
		dst.MapUint32_Item[k] = v
	}
	if include[47] {
		k := gen_uint32(288)
		v := gen_Item(388)
		dst.MapUint32_Item[k] = v
	}
	dst.MapUint64Double = make(map[uint64]float64)
	if include[48] {
		k := gen_uint64(289)
		v := gen_float64(389)
		dst.MapUint64Double[k] = v
	}
	if include[48] {
		k := gen_uint64(290)
		v := gen_float64(390)
		dst.MapUint64Double[k] = v
	}
	if include[48] {
		k := gen_uint64(291)
		v := gen_float64(391)
		dst.MapUint64Double[k] = v
	}
	if include[48] {
		k := gen_uint64(292)
		v := gen_float64(392)
		dst.MapUint64Double[k] = v
	}
	if include[48] {
		k := gen_uint64(293)
		v := gen_float64(393)
		dst.MapUint64Double[k] = v
	}
	if include[48] {
		k := gen_uint64(294)
		v := gen_float64(394)
		dst.MapUint64Double[k] = v
	}
	dst.MapUint64Float = make(map[uint64]float32)
	if include[49] {
		k := gen_uint64(295)
		v := gen_float32(395)
		dst.MapUint64Float[k] = v
	}
	if include[49] {
		k := gen_uint64(296)
		v := gen_float32(396)
		dst.MapUint64Float[k] = v
	}
	if include[49] {
		k := gen_uint64(297)
		v := gen_float32(397)
		dst.MapUint64Float[k] = v
	}
	if include[49] {
		k := gen_uint64(298)
		v := gen_float32(398)
		dst.MapUint64Float[k] = v
	}
	if include[49] {
		k := gen_uint64(299)
		v := gen_float32(399)
		dst.MapUint64Float[k] = v
	}
	if include[49] {
		k := gen_uint64(300)
		v := gen_float32(400)
		dst.MapUint64Float[k] = v
	}
	dst.MapUint64Int32 = make(map[uint64]int32)
	if include[50] {
		k := gen_uint64(301)
		v := gen_int32(401)
		dst.MapUint64Int32[k] = v
	}
	if include[50] {
		k := gen_uint64(302)
		v := gen_int32(402)
		dst.MapUint64Int32[k] = v
	}
	if include[50] {
		k := gen_uint64(303)
		v := gen_int32(403)
		dst.MapUint64Int32[k] = v
	}
	if include[50] {
		k := gen_uint64(304)
		v := gen_int32(404)
		dst.MapUint64Int32[k] = v
	}
	if include[50] {
		k := gen_uint64(305)
		v := gen_int32(405)
		dst.MapUint64Int32[k] = v
	}
	if include[50] {
		k := gen_uint64(306)
		v := gen_int32(406)
		dst.MapUint64Int32[k] = v
	}
	dst.MapUint64Int64 = make(map[uint64]int64)
	if include[51] {
		k := gen_uint64(307)
		v := gen_int64(407)
		dst.MapUint64Int64[k] = v
	}
	if include[51] {
		k := gen_uint64(308)
		v := gen_int64(408)
		dst.MapUint64Int64[k] = v
	}
	if include[51] {
		k := gen_uint64(309)
		v := gen_int64(409)
		dst.MapUint64Int64[k] = v
	}
	if include[51] {
		k := gen_uint64(310)
		v := gen_int64(410)
		dst.MapUint64Int64[k] = v
	}
	if include[51] {
		k := gen_uint64(311)
		v := gen_int64(411)
		dst.MapUint64Int64[k] = v
	}
	if include[51] {
		k := gen_uint64(312)
		v := gen_int64(412)
		dst.MapUint64Int64[k] = v
	}
	dst.MapUint64Uint32 = make(map[uint64]uint32)
	if include[52] {
		k := gen_uint64(313)
		v := gen_uint32(413)
		dst.MapUint64Uint32[k] = v
	}
	if include[52] {
		k := gen_uint64(314)
		v := gen_uint32(414)
		dst.MapUint64Uint32[k] = v
	}
	if include[52] {
		k := gen_uint64(315)
		v := gen_uint32(415)
		dst.MapUint64Uint32[k] = v
	}
	if include[52] {
		k := gen_uint64(316)
		v := gen_uint32(416)
		dst.MapUint64Uint32[k] = v
	}
	if include[52] {
		k := gen_uint64(317)
		v := gen_uint32(417)
		dst.MapUint64Uint32[k] = v
	}
	if include[52] {
		k := gen_uint64(318)
		v := gen_uint32(418)
		dst.MapUint64Uint32[k] = v
	}
	dst.MapUint64Uint64 = make(map[uint64]uint64)
	if include[53] {
		k := gen_uint64(319)
		v := gen_uint64(419)
		dst.MapUint64Uint64[k] = v
	}
	if include[53] {
		k := gen_uint64(320)
		v := gen_uint64(420)
		dst.MapUint64Uint64[k] = v
	}
	if include[53] {
		k := gen_uint64(321)
		v := gen_uint64(421)
		dst.MapUint64Uint64[k] = v
	}
	if include[53] {
		k := gen_uint64(322)
		v := gen_uint64(422)
		dst.MapUint64Uint64[k] = v
	}
	if include[53] {
		k := gen_uint64(323)
		v := gen_uint64(423)
		dst.MapUint64Uint64[k] = v
	}
	if include[53] {
		k := gen_uint64(324)
		v := gen_uint64(424)
		dst.MapUint64Uint64[k] = v
	}
	dst.MapUint64Sint32 = make(map[uint64]int32)
	if include[54] {
		k := gen_uint64(325)
		v := gen_int32(425)
		dst.MapUint64Sint32[k] = v
	}
	if include[54] {
		k := gen_uint64(326)
		v := gen_int32(426)
		dst.MapUint64Sint32[k] = v
	}
	if include[54] {
		k := gen_uint64(327)
		v := gen_int32(427)
		dst.MapUint64Sint32[k] = v
	}
	if include[54] {
		k := gen_uint64(328)
		v := gen_int32(428)
		dst.MapUint64Sint32[k] = v
	}
	if include[54] {
		k := gen_uint64(329)
		v := gen_int32(429)
		dst.MapUint64Sint32[k] = v
	}
	if include[54] {
		k := gen_uint64(330)
		v := gen_int32(430)
		dst.MapUint64Sint32[k] = v
	}
	dst.MapUint64Sint64 = make(map[uint64]int64)
	if include[55] {
		k := gen_uint64(331)
		v := gen_int64(431)
		dst.MapUint64Sint64[k] = v
	}
	if include[55] {
		k := gen_uint64(332)
		v := gen_int64(432)
		dst.MapUint64Sint64[k] = v
	}
	if include[55] {
		k := gen_uint64(333)
		v := gen_int64(433)
		dst.MapUint64Sint64[k] = v
	}
	if include[55] {
		k := gen_uint64(334)
		v := gen_int64(434)
		dst.MapUint64Sint64[k] = v
	}
	if include[55] {
		k := gen_uint64(335)
		v := gen_int64(435)
		dst.MapUint64Sint64[k] = v
	}
	if include[55] {
		k := gen_uint64(336)
		v := gen_int64(436)
		dst.MapUint64Sint64[k] = v
	}
	dst.MapUint64Fixed32 = make(map[uint64]uint32)
	if include[56] {
		k := gen_uint64(337)
		v := gen_uint32(437)
		dst.MapUint64Fixed32[k] = v
	}
	if include[56] {
		k := gen_uint64(338)
		v := gen_uint32(438)
		dst.MapUint64Fixed32[k] = v
	}
	if include[56] {
		k := gen_uint64(339)
		v := gen_uint32(439)
		dst.MapUint64Fixed32[k] = v
	}
	if include[56] {
		k := gen_uint64(340)
		v := gen_uint32(440)
		dst.MapUint64Fixed32[k] = v
	}
	if include[56] {
		k := gen_uint64(341)
		v := gen_uint32(441)
		dst.MapUint64Fixed32[k] = v
	}
	if include[56] {
		k := gen_uint64(342)
		v := gen_uint32(442)
		dst.MapUint64Fixed32[k] = v
	}
	dst.MapUint64Fixed64 = make(map[uint64]uint64)
	if include[57] {
		k := gen_uint64(343)
		v := gen_uint64(443)
		dst.MapUint64Fixed64[k] = v
	}
	if include[57] {
		k := gen_uint64(344)
		v := gen_uint64(444)
		dst.MapUint64Fixed64[k] = v
	}
	if include[57] {
		k := gen_uint64(345)
		v := gen_uint64(445)
		dst.MapUint64Fixed64[k] = v
	}
	if include[57] {
		k := gen_uint64(346)
		v := gen_uint64(446)
		dst.MapUint64Fixed64[k] = v
	}
	if include[57] {
		k := gen_uint64(347)
		v := gen_uint64(447)
		dst.MapUint64Fixed64[k] = v
	}
	if include[57] {
		k := gen_uint64(348)
		v := gen_uint64(448)
		dst.MapUint64Fixed64[k] = v
	}
	dst.MapUint64Sfixed32 = make(map[uint64]int32)
	if include[58] {
		k := gen_uint64(349)
		v := gen_int32(449)
		dst.MapUint64Sfixed32[k] = v
	}
	if include[58] {
		k := gen_uint64(350)
		v := gen_int32(450)
		dst.MapUint64Sfixed32[k] = v
	}
	if include[58] {
		k := gen_uint64(351)
		v := gen_int32(451)
		dst.MapUint64Sfixed32[k] = v
	}
	if include[58] {
		k := gen_uint64(352)
		v := gen_int32(452)
		dst.MapUint64Sfixed32[k] = v
	}
	if include[58] {
		k := gen_uint64(353)
		v := gen_int32(453)
		dst.MapUint64Sfixed32[k] = v
	}
	if include[58] {
		k := gen_uint64(354)
		v := gen_int32(454)
		dst.MapUint64Sfixed32[k] = v
	}
	dst.MapUint64Sfixed64 = make(map[uint64]int64)
	if include[59] {
		k := gen_uint64(355)
		v := gen_int64(455)
		dst.MapUint64Sfixed64[k] = v
	}
	if include[59] {
		k := gen_uint64(356)
		v := gen_int64(456)
		dst.MapUint64Sfixed64[k] = v
	}
	if include[59] {
		k := gen_uint64(357)
		v := gen_int64(457)
		dst.MapUint64Sfixed64[k] = v
	}
	if include[59] {
		k := gen_uint64(358)
		v := gen_int64(458)
		dst.MapUint64Sfixed64[k] = v
	}
	if include[59] {
		k := gen_uint64(359)
		v := gen_int64(459)
		dst.MapUint64Sfixed64[k] = v
	}
	if include[59] {
		k := gen_uint64(360)
		v := gen_int64(460)
		dst.MapUint64Sfixed64[k] = v
	}
	dst.MapUint64Bool = make(map[uint64]bool)
	if include[60] {
		k := gen_uint64(361)
		v := gen_bool(461)
		dst.MapUint64Bool[k] = v
	}
	if include[60] {
		k := gen_uint64(362)
		v := gen_bool(462)
		dst.MapUint64Bool[k] = v
	}
	if include[60] {
		k := gen_uint64(363)
		v := gen_bool(463)
		dst.MapUint64Bool[k] = v
	}
	if include[60] {
		k := gen_uint64(364)
		v := gen_bool(464)
		dst.MapUint64Bool[k] = v
	}
	if include[60] {
		k := gen_uint64(365)
		v := gen_bool(465)
		dst.MapUint64Bool[k] = v
	}
	if include[60] {
		k := gen_uint64(366)
		v := gen_bool(466)
		dst.MapUint64Bool[k] = v
	}
	dst.MapUint64String = make(map[uint64]string)
	if include[61] {
		k := gen_uint64(367)
		v := gen_string(467)
		dst.MapUint64String[k] = v
	}
	if include[61] {
		k := gen_uint64(368)
		v := gen_string(468)
		dst.MapUint64String[k] = v
	}
	if include[61] {
		k := gen_uint64(369)
		v := gen_string(469)
		dst.MapUint64String[k] = v
	}
	if include[61] {
		k := gen_uint64(370)
		v := gen_string(470)
		dst.MapUint64String[k] = v
	}
	if include[61] {
		k := gen_uint64(371)
		v := gen_string(471)
		dst.MapUint64String[k] = v
	}
	if include[61] {
		k := gen_uint64(372)
		v := gen_string(472)
		dst.MapUint64String[k] = v
	}
	dst.MapUint64Bytes = make(map[uint64][]byte)
	if include[62] {
		k := gen_uint64(373)
		v := gen_bytes(473)
		dst.MapUint64Bytes[k] = v
	}
	if include[62] {
		k := gen_uint64(374)
		v := gen_bytes(474)
		dst.MapUint64Bytes[k] = v
	}
	if include[62] {
		k := gen_uint64(375)
		v := gen_bytes(475)
		dst.MapUint64Bytes[k] = v
	}
	if include[62] {
		k := gen_uint64(376)
		v := gen_bytes(476)
		dst.MapUint64Bytes[k] = v
	}
	if include[62] {
		k := gen_uint64(377)
		v := gen_bytes(477)
		dst.MapUint64Bytes[k] = v
	}
	if include[62] {
		k := gen_uint64(378)
		v := gen_bytes(478)
		dst.MapUint64Bytes[k] = v
	}
	dst.MapUint64_Item = make(map[uint64]*Item)
	if include[63] {
		k := gen_uint64(379)
		v := gen_Item(479)
		dst.MapUint64_Item[k] = v
	}
	if include[63] {
		k := gen_uint64(380)
		v := gen_Item(480)
		dst.MapUint64_Item[k] = v
	}
	if include[63] {
		k := gen_uint64(381)
		v := gen_Item(481)
		dst.MapUint64_Item[k] = v
	}
	if include[63] {
		k := gen_uint64(382)
		v := gen_Item(482)
		dst.MapUint64_Item[k] = v
	}
	if include[63] {
		k := gen_uint64(383)
		v := gen_Item(483)
		dst.MapUint64_Item[k] = v
	}
	if include[63] {
		k := gen_uint64(384)
		v := gen_Item(484)
		dst.MapUint64_Item[k] = v
	}
	dst.MapSint32Double = make(map[int32]float64)
	if include[64] {
		k := gen_int32(385)
		v := gen_float64(485)
		dst.MapSint32Double[k] = v
	}
	if include[64] {
		k := gen_int32(386)
		v := gen_float64(486)
		dst.MapSint32Double[k] = v
	}
	if include[64] {
		k := gen_int32(387)
		v := gen_float64(487)
		dst.MapSint32Double[k] = v
	}
	if include[64] {
		k := gen_int32(388)
		v := gen_float64(488)
		dst.MapSint32Double[k] = v
	}
	if include[64] {
		k := gen_int32(389)
		v := gen_float64(489)
		dst.MapSint32Double[k] = v
	}
	if include[64] {
		k := gen_int32(390)
		v := gen_float64(490)
		dst.MapSint32Double[k] = v
	}
	dst.MapSint32Float = make(map[int32]float32)
	if include[65] {
		k := gen_int32(391)
		v := gen_float32(491)
		dst.MapSint32Float[k] = v
	}
	if include[65] {
		k := gen_int32(392)
		v := gen_float32(492)
		dst.MapSint32Float[k] = v
	}
	if include[65] {
		k := gen_int32(393)
		v := gen_float32(493)
		dst.MapSint32Float[k] = v
	}
	if include[65] {
		k := gen_int32(394)
		v := gen_float32(494)
		dst.MapSint32Float[k] = v
	}
	if include[65] {
		k := gen_int32(395)
		v := gen_float32(495)
		dst.MapSint32Float[k] = v
	}
	if include[65] {
		k := gen_int32(396)
		v := gen_float32(496)
		dst.MapSint32Float[k] = v
	}
	dst.MapSint32Int32 = make(map[int32]int32)
	if include[66] {
		k := gen_int32(397)
		v := gen_int32(497)
		dst.MapSint32Int32[k] = v
	}
	if include[66] {
		k := gen_int32(398)
		v := gen_int32(498)
		dst.MapSint32Int32[k] = v
	}
	if include[66] {
		k := gen_int32(399)
		v := gen_int32(499)
		dst.MapSint32Int32[k] = v
	}
	if include[66] {
		k := gen_int32(400)
		v := gen_int32(500)
		dst.MapSint32Int32[k] = v
	}
	if include[66] {
		k := gen_int32(401)
		v := gen_int32(501)
		dst.MapSint32Int32[k] = v
	}
	if include[66] {
		k := gen_int32(402)
		v := gen_int32(502)
		dst.MapSint32Int32[k] = v
	}
	dst.MapSint32Int64 = make(map[int32]int64)
	if include[67] {
		k := gen_int32(403)
		v := gen_int64(503)
		dst.MapSint32Int64[k] = v
	}
	if include[67] {
		k := gen_int32(404)
		v := gen_int64(504)
		dst.MapSint32Int64[k] = v
	}
	if include[67] {
		k := gen_int32(405)
		v := gen_int64(505)
		dst.MapSint32Int64[k] = v
	}
	if include[67] {
		k := gen_int32(406)
		v := gen_int64(506)
		dst.MapSint32Int64[k] = v
	}
	if include[67] {
		k := gen_int32(407)
		v := gen_int64(507)
		dst.MapSint32Int64[k] = v
	}
	if include[67] {
		k := gen_int32(408)
		v := gen_int64(508)
		dst.MapSint32Int64[k] = v
	}
	dst.MapSint32Uint32 = make(map[int32]uint32)
	if include[68] {
		k := gen_int32(409)
		v := gen_uint32(509)
		dst.MapSint32Uint32[k] = v
	}
	if include[68] {
		k := gen_int32(410)
		v := gen_uint32(510)
		dst.MapSint32Uint32[k] = v
	}
	if include[68] {
		k := gen_int32(411)
		v := gen_uint32(511)
		dst.MapSint32Uint32[k] = v
	}
	if include[68] {
		k := gen_int32(412)
		v := gen_uint32(512)
		dst.MapSint32Uint32[k] = v
	}
	if include[68] {
		k := gen_int32(413)
		v := gen_uint32(513)
		dst.MapSint32Uint32[k] = v
	}
	if include[68] {
		k := gen_int32(414)
		v := gen_uint32(514)
		dst.MapSint32Uint32[k] = v
	}
	dst.MapSint32Uint64 = make(map[int32]uint64)
	if include[69] {
		k := gen_int32(415)
		v := gen_uint64(515)
		dst.MapSint32Uint64[k] = v
	}
	if include[69] {
		k := gen_int32(416)
		v := gen_uint64(516)
		dst.MapSint32Uint64[k] = v
	}
	if include[69] {
		k := gen_int32(417)
		v := gen_uint64(517)
		dst.MapSint32Uint64[k] = v
	}
	if include[69] {
		k := gen_int32(418)
		v := gen_uint64(518)
		dst.MapSint32Uint64[k] = v
	}
	if include[69] {
		k := gen_int32(419)
		v := gen_uint64(519)
		dst.MapSint32Uint64[k] = v
	}
	if include[69] {
		k := gen_int32(420)
		v := gen_uint64(520)
		dst.MapSint32Uint64[k] = v
	}
	dst.MapSint32Sint32 = make(map[int32]int32)
	if include[70] {
		k := gen_int32(421)
		v := gen_int32(521)
		dst.MapSint32Sint32[k] = v
	}
	if include[70] {
		k := gen_int32(422)
		v := gen_int32(522)
		dst.MapSint32Sint32[k] = v
	}
	if include[70] {
		k := gen_int32(423)
		v := gen_int32(523)
		dst.MapSint32Sint32[k] = v
	}
	if include[70] {
		k := gen_int32(424)
		v := gen_int32(524)
		dst.MapSint32Sint32[k] = v
	}
	if include[70] {
		k := gen_int32(425)
		v := gen_int32(525)
		dst.MapSint32Sint32[k] = v
	}
	if include[70] {
		k := gen_int32(426)
		v := gen_int32(526)
		dst.MapSint32Sint32[k] = v
	}
	dst.MapSint32Sint64 = make(map[int32]int64)
	if include[71] {
		k := gen_int32(427)
		v := gen_int64(527)
		dst.MapSint32Sint64[k] = v
	}
	if include[71] {
		k := gen_int32(428)
		v := gen_int64(528)
		dst.MapSint32Sint64[k] = v
	}
	if include[71] {
		k := gen_int32(429)
		v := gen_int64(529)
		dst.MapSint32Sint64[k] = v
	}
	if include[71] {
		k := gen_int32(430)
		v := gen_int64(530)
		dst.MapSint32Sint64[k] = v
	}
	if include[71] {
		k := gen_int32(431)
		v := gen_int64(531)
		dst.MapSint32Sint64[k] = v
	}
	if include[71] {
		k := gen_int32(432)
		v := gen_int64(532)
		dst.MapSint32Sint64[k] = v
	}
	dst.MapSint32Fixed32 = make(map[int32]uint32)
	if include[72] {
		k := gen_int32(433)
		v := gen_uint32(533)
		dst.MapSint32Fixed32[k] = v
	}
	if include[72] {
		k := gen_int32(434)
		v := gen_uint32(534)
		dst.MapSint32Fixed32[k] = v
	}
	if include[72] {
		k := gen_int32(435)
		v := gen_uint32(535)
		dst.MapSint32Fixed32[k] = v
	}
	if include[72] {
		k := gen_int32(436)
		v := gen_uint32(536)
		dst.MapSint32Fixed32[k] = v
	}
	if include[72] {
		k := gen_int32(437)
		v := gen_uint32(537)
		dst.MapSint32Fixed32[k] = v
	}
	if include[72] {
		k := gen_int32(438)
		v := gen_uint32(538)
		dst.MapSint32Fixed32[k] = v
	}
	dst.MapSint32Fixed64 = make(map[int32]uint64)
	if include[73] {
		k := gen_int32(439)
		v := gen_uint64(539)
		dst.MapSint32Fixed64[k] = v
	}
	if include[73] {
		k := gen_int32(440)
		v := gen_uint64(540)
		dst.MapSint32Fixed64[k] = v
	}
	if include[73] {
		k := gen_int32(441)
		v := gen_uint64(541)
		dst.MapSint32Fixed64[k] = v
	}
	if include[73] {
		k := gen_int32(442)
		v := gen_uint64(542)
		dst.MapSint32Fixed64[k] = v
	}
	if include[73] {
		k := gen_int32(443)
		v := gen_uint64(543)
		dst.MapSint32Fixed64[k] = v
	}
	if include[73] {
		k := gen_int32(444)
		v := gen_uint64(544)
		dst.MapSint32Fixed64[k] = v
	}
	dst.MapSint32Sfixed32 = make(map[int32]int32)
	if include[74] {
		k := gen_int32(445)
		v := gen_int32(545)
		dst.MapSint32Sfixed32[k] = v
	}
	if include[74] {
		k := gen_int32(446)
		v := gen_int32(546)
		dst.MapSint32Sfixed32[k] = v
	}
	if include[74] {
		k := gen_int32(447)
		v := gen_int32(547)
		dst.MapSint32Sfixed32[k] = v
	}
	if include[74] {
		k := gen_int32(448)
		v := gen_int32(548)
		dst.MapSint32Sfixed32[k] = v
	}
	if include[74] {
		k := gen_int32(449)
		v := gen_int32(549)
		dst.MapSint32Sfixed32[k] = v
	}
	if include[74] {
		k := gen_int32(450)
		v := gen_int32(550)
		dst.MapSint32Sfixed32[k] = v
	}
	dst.MapSint32Sfixed64 = make(map[int32]int64)
	if include[75] {
		k := gen_int32(451)
		v := gen_int64(551)
		dst.MapSint32Sfixed64[k] = v
	}
	if include[75] {
		k := gen_int32(452)
		v := gen_int64(552)
		dst.MapSint32Sfixed64[k] = v
	}
	if include[75] {
		k := gen_int32(453)
		v := gen_int64(553)
		dst.MapSint32Sfixed64[k] = v
	}
	if include[75] {
		k := gen_int32(454)
		v := gen_int64(554)
		dst.MapSint32Sfixed64[k] = v
	}
	if include[75] {
		k := gen_int32(455)
		v := gen_int64(555)
		dst.MapSint32Sfixed64[k] = v
	}
	if include[75] {
		k := gen_int32(456)
		v := gen_int64(556)
		dst.MapSint32Sfixed64[k] = v
	}
	dst.MapSint32Bool = make(map[int32]bool)
	if include[76] {
		k := gen_int32(457)
		v := gen_bool(557)
		dst.MapSint32Bool[k] = v
	}
	if include[76] {
		k := gen_int32(458)
		v := gen_bool(558)
		dst.MapSint32Bool[k] = v
	}
	if include[76] {
		k := gen_int32(459)
		v := gen_bool(559)
		dst.MapSint32Bool[k] = v
	}
	if include[76] {
		k := gen_int32(460)
		v := gen_bool(560)
		dst.MapSint32Bool[k] = v
	}
	if include[76] {
		k := gen_int32(461)
		v := gen_bool(561)
		dst.MapSint32Bool[k] = v
	}
	if include[76] {
		k := gen_int32(462)
		v := gen_bool(562)
		dst.MapSint32Bool[k] = v
	}
	dst.MapSint32String = make(map[int32]string)
	if include[77] {
		k := gen_int32(463)
		v := gen_string(563)
		dst.MapSint32String[k] = v
	}
	if include[77] {
		k := gen_int32(464)
		v := gen_string(564)
		dst.MapSint32String[k] = v
	}
	if include[77] {
		k := gen_int32(465)
		v := gen_string(565)
		dst.MapSint32String[k] = v
	}
	if include[77] {
		k := gen_int32(466)
		v := gen_string(566)
		dst.MapSint32String[k] = v
	}
	if include[77] {
		k := gen_int32(467)
		v := gen_string(567)
		dst.MapSint32String[k] = v
	}
	if include[77] {
		k := gen_int32(468)
		v := gen_string(568)
		dst.MapSint32String[k] = v
	}
	dst.MapSint32Bytes = make(map[int32][]byte)
	if include[78] {
		k := gen_int32(469)
		v := gen_bytes(569)
		dst.MapSint32Bytes[k] = v
	}
	if include[78] {
		k := gen_int32(470)
		v := gen_bytes(570)
		dst.MapSint32Bytes[k] = v
	}
	if include[78] {
		k := gen_int32(471)
		v := gen_bytes(571)
		dst.MapSint32Bytes[k] = v
	}
	if include[78] {
		k := gen_int32(472)
		v := gen_bytes(572)
		dst.MapSint32Bytes[k] = v
	}
	if include[78] {
		k := gen_int32(473)
		v := gen_bytes(573)
		dst.MapSint32Bytes[k] = v
	}
	if include[78] {
		k := gen_int32(474)
		v := gen_bytes(574)
		dst.MapSint32Bytes[k] = v
	}
	dst.MapSint32_Item = make(map[int32]*Item)
	if include[79] {
		k := gen_int32(475)
		v := gen_Item(575)
		dst.MapSint32_Item[k] = v
	}
	if include[79] {
		k := gen_int32(476)
		v := gen_Item(576)
		dst.MapSint32_Item[k] = v
	}
	if include[79] {
		k := gen_int32(477)
		v := gen_Item(577)
		dst.MapSint32_Item[k] = v
	}
	if include[79] {
		k := gen_int32(478)
		v := gen_Item(578)
		dst.MapSint32_Item[k] = v
	}
	if include[79] {
		k := gen_int32(479)
		v := gen_Item(579)
		dst.MapSint32_Item[k] = v
	}
	if include[79] {
		k := gen_int32(480)
		v := gen_Item(580)
		dst.MapSint32_Item[k] = v
	}
	dst.MapSint64Double = make(map[int64]float64)
	if include[80] {
		k := gen_int64(481)
		v := gen_float64(581)
		dst.MapSint64Double[k] = v
	}
	if include[80] {
		k := gen_int64(482)
		v := gen_float64(582)
		dst.MapSint64Double[k] = v
	}
	if include[80] {
		k := gen_int64(483)
		v := gen_float64(583)
		dst.MapSint64Double[k] = v
	}
	if include[80] {
		k := gen_int64(484)
		v := gen_float64(584)
		dst.MapSint64Double[k] = v
	}
	if include[80] {
		k := gen_int64(485)
		v := gen_float64(585)
		dst.MapSint64Double[k] = v
	}
	if include[80] {
		k := gen_int64(486)
		v := gen_float64(586)
		dst.MapSint64Double[k] = v
	}
	dst.MapSint64Float = make(map[int64]float32)
	if include[81] {
		k := gen_int64(487)
		v := gen_float32(587)
		dst.MapSint64Float[k] = v
	}
	if include[81] {
		k := gen_int64(488)
		v := gen_float32(588)
		dst.MapSint64Float[k] = v
	}
	if include[81] {
		k := gen_int64(489)
		v := gen_float32(589)
		dst.MapSint64Float[k] = v
	}
	if include[81] {
		k := gen_int64(490)
		v := gen_float32(590)
		dst.MapSint64Float[k] = v
	}
	if include[81] {
		k := gen_int64(491)
		v := gen_float32(591)
		dst.MapSint64Float[k] = v
	}
	if include[81] {
		k := gen_int64(492)
		v := gen_float32(592)
		dst.MapSint64Float[k] = v
	}
	dst.MapSint64Int32 = make(map[int64]int32)
	if include[82] {
		k := gen_int64(493)
		v := gen_int32(593)
		dst.MapSint64Int32[k] = v
	}
	if include[82] {
		k := gen_int64(494)
		v := gen_int32(594)
		dst.MapSint64Int32[k] = v
	}
	if include[82] {
		k := gen_int64(495)
		v := gen_int32(595)
		dst.MapSint64Int32[k] = v
	}
	if include[82] {
		k := gen_int64(496)
		v := gen_int32(596)
		dst.MapSint64Int32[k] = v
	}
	if include[82] {
		k := gen_int64(497)
		v := gen_int32(597)
		dst.MapSint64Int32[k] = v
	}
	if include[82] {
		k := gen_int64(498)
		v := gen_int32(598)
		dst.MapSint64Int32[k] = v
	}
	dst.MapSint64Int64 = make(map[int64]int64)
	if include[83] {
		k := gen_int64(499)
		v := gen_int64(599)
		dst.MapSint64Int64[k] = v
	}
	if include[83] {
		k := gen_int64(500)
		v := gen_int64(600)
		dst.MapSint64Int64[k] = v
	}
	if include[83] {
		k := gen_int64(501)
		v := gen_int64(601)
		dst.MapSint64Int64[k] = v
	}
	if include[83] {
		k := gen_int64(502)
		v := gen_int64(602)
		dst.MapSint64Int64[k] = v
	}
	if include[83] {
		k := gen_int64(503)
		v := gen_int64(603)
		dst.MapSint64Int64[k] = v
	}
	if include[83] {
		k := gen_int64(504)
		v := gen_int64(604)
		dst.MapSint64Int64[k] = v
	}
	dst.MapSint64Uint32 = make(map[int64]uint32)
	if include[84] {
		k := gen_int64(505)
		v := gen_uint32(605)
		dst.MapSint64Uint32[k] = v
	}
	if include[84] {
		k := gen_int64(506)
		v := gen_uint32(606)
		dst.MapSint64Uint32[k] = v
	}
	if include[84] {
		k := gen_int64(507)
		v := gen_uint32(607)
		dst.MapSint64Uint32[k] = v
	}
	if include[84] {
		k := gen_int64(508)
		v := gen_uint32(608)
		dst.MapSint64Uint32[k] = v
	}
	if include[84] {
		k := gen_int64(509)
		v := gen_uint32(609)
		dst.MapSint64Uint32[k] = v
	}
	if include[84] {
		k := gen_int64(510)
		v := gen_uint32(610)
		dst.MapSint64Uint32[k] = v
	}
	dst.MapSint64Uint64 = make(map[int64]uint64)
	if include[85] {
		k := gen_int64(511)
		v := gen_uint64(611)
		dst.MapSint64Uint64[k] = v
	}
	if include[85] {
		k := gen_int64(512)
		v := gen_uint64(612)
		dst.MapSint64Uint64[k] = v
	}
	if include[85] {
		k := gen_int64(513)
		v := gen_uint64(613)
		dst.MapSint64Uint64[k] = v
	}
	if include[85] {
		k := gen_int64(514)
		v := gen_uint64(614)
		dst.MapSint64Uint64[k] = v
	}
	if include[85] {
		k := gen_int64(515)
		v := gen_uint64(615)
		dst.MapSint64Uint64[k] = v
	}
	if include[85] {
		k := gen_int64(516)
		v := gen_uint64(616)
		dst.MapSint64Uint64[k] = v
	}
	dst.MapSint64Sint32 = make(map[int64]int32)
	if include[86] {
		k := gen_int64(517)
		v := gen_int32(617)
		dst.MapSint64Sint32[k] = v
	}
	if include[86] {
		k := gen_int64(518)
		v := gen_int32(618)
		dst.MapSint64Sint32[k] = v
	}
	if include[86] {
		k := gen_int64(519)
		v := gen_int32(619)
		dst.MapSint64Sint32[k] = v
	}
	if include[86] {
		k := gen_int64(520)
		v := gen_int32(620)
		dst.MapSint64Sint32[k] = v
	}
	if include[86] {
		k := gen_int64(521)
		v := gen_int32(621)
		dst.MapSint64Sint32[k] = v
	}
	if include[86] {
		k := gen_int64(522)
		v := gen_int32(622)
		dst.MapSint64Sint32[k] = v
	}
	dst.MapSint64Sint64 = make(map[int64]int64)
	if include[87] {
		k := gen_int64(523)
		v := gen_int64(623)
		dst.MapSint64Sint64[k] = v
	}
	if include[87] {
		k := gen_int64(524)
		v := gen_int64(624)
		dst.MapSint64Sint64[k] = v
	}
	if include[87] {
		k := gen_int64(525)
		v := gen_int64(625)
		dst.MapSint64Sint64[k] = v
	}
	if include[87] {
		k := gen_int64(526)
		v := gen_int64(626)
		dst.MapSint64Sint64[k] = v
	}
	if include[87] {
		k := gen_int64(527)
		v := gen_int64(627)
		dst.MapSint64Sint64[k] = v
	}
	if include[87] {
		k := gen_int64(528)
		v := gen_int64(628)
		dst.MapSint64Sint64[k] = v
	}
	dst.MapSint64Fixed32 = make(map[int64]uint32)
	if include[88] {
		k := gen_int64(529)
		v := gen_uint32(629)
		dst.MapSint64Fixed32[k] = v
	}
	if include[88] {
		k := gen_int64(530)
		v := gen_uint32(630)
		dst.MapSint64Fixed32[k] = v
	}
	if include[88] {
		k := gen_int64(531)
		v := gen_uint32(631)
		dst.MapSint64Fixed32[k] = v
	}
	if include[88] {
		k := gen_int64(532)
		v := gen_uint32(632)
		dst.MapSint64Fixed32[k] = v
	}
	if include[88] {
		k := gen_int64(533)
		v := gen_uint32(633)
		dst.MapSint64Fixed32[k] = v
	}
	if include[88] {
		k := gen_int64(534)
		v := gen_uint32(634)
		dst.MapSint64Fixed32[k] = v
	}
	dst.MapSint64Fixed64 = make(map[int64]uint64)
	if include[89] {
		k := gen_int64(535)
		v := gen_uint64(635)
		dst.MapSint64Fixed64[k] = v
	}
	if include[89] {
		k := gen_int64(536)
		v := gen_uint64(636)
		dst.MapSint64Fixed64[k] = v
	}
	if include[89] {
		k := gen_int64(537)
		v := gen_uint64(637)
		dst.MapSint64Fixed64[k] = v
	}
	if include[89] {
		k := gen_int64(538)
		v := gen_uint64(638)
		dst.MapSint64Fixed64[k] = v
	}
	if include[89] {
		k := gen_int64(539)
		v := gen_uint64(639)
		dst.MapSint64Fixed64[k] = v
	}
	if include[89] {
		k := gen_int64(540)
		v := gen_uint64(640)
		dst.MapSint64Fixed64[k] = v
	}
	dst.MapSint64Sfixed32 = make(map[int64]int32)
	if include[90] {
		k := gen_int64(541)
		v := gen_int32(641)
		dst.MapSint64Sfixed32[k] = v
	}
	if include[90] {
		k := gen_int64(542)
		v := gen_int32(642)
		dst.MapSint64Sfixed32[k] = v
	}
	if include[90] {
		k := gen_int64(543)
		v := gen_int32(643)
		dst.MapSint64Sfixed32[k] = v
	}
	if include[90] {
		k := gen_int64(544)
		v := gen_int32(644)
		dst.MapSint64Sfixed32[k] = v
	}
	if include[90] {
		k := gen_int64(545)
		v := gen_int32(645)
		dst.MapSint64Sfixed32[k] = v
	}
	if include[90] {
		k := gen_int64(546)
		v := gen_int32(646)
		dst.MapSint64Sfixed32[k] = v
	}
	dst.MapSint64Sfixed64 = make(map[int64]int64)
	if include[91] {
		k := gen_int64(547)
		v := gen_int64(647)
		dst.MapSint64Sfixed64[k] = v
	}
	if include[91] {
		k := gen_int64(548)
		v := gen_int64(648)
		dst.MapSint64Sfixed64[k] = v
	}
	if include[91] {
		k := gen_int64(549)
		v := gen_int64(649)
		dst.MapSint64Sfixed64[k] = v
	}
	if include[91] {
		k := gen_int64(550)
		v := gen_int64(650)
		dst.MapSint64Sfixed64[k] = v
	}
	if include[91] {
		k := gen_int64(551)
		v := gen_int64(651)
		dst.MapSint64Sfixed64[k] = v
	}
	if include[91] {
		k := gen_int64(552)
		v := gen_int64(652)
		dst.MapSint64Sfixed64[k] = v
	}
	dst.MapSint64Bool = make(map[int64]bool)
	if include[92] {
		k := gen_int64(553)
		v := gen_bool(653)
		dst.MapSint64Bool[k] = v
	}
	if include[92] {
		k := gen_int64(554)
		v := gen_bool(654)
		dst.MapSint64Bool[k] = v
	}
	if include[92] {
		k := gen_int64(555)
		v := gen_bool(655)
		dst.MapSint64Bool[k] = v
	}
	if include[92] {
		k := gen_int64(556)
		v := gen_bool(656)
		dst.MapSint64Bool[k] = v
	}
	if include[92] {
		k := gen_int64(557)
		v := gen_bool(657)
		dst.MapSint64Bool[k] = v
	}
	if include[92] {
		k := gen_int64(558)
		v := gen_bool(658)
		dst.MapSint64Bool[k] = v
	}
	dst.MapSint64String = make(map[int64]string)
	if include[93] {
		k := gen_int64(559)
		v := gen_string(659)
		dst.MapSint64String[k] = v
	}
	if include[93] {
		k := gen_int64(560)
		v := gen_string(660)
		dst.MapSint64String[k] = v
	}
	if include[93] {
		k := gen_int64(561)
		v := gen_string(661)
		dst.MapSint64String[k] = v
	}
	if include[93] {
		k := gen_int64(562)
		v := gen_string(662)
		dst.MapSint64String[k] = v
	}
	if include[93] {
		k := gen_int64(563)
		v := gen_string(663)
		dst.MapSint64String[k] = v
	}
	if include[93] {
		k := gen_int64(564)
		v := gen_string(664)
		dst.MapSint64String[k] = v
	}
	dst.MapSint64Bytes = make(map[int64][]byte)
	if include[94] {
		k := gen_int64(565)
		v := gen_bytes(665)
		dst.MapSint64Bytes[k] = v
	}
	if include[94] {
		k := gen_int64(566)
		v := gen_bytes(666)
		dst.MapSint64Bytes[k] = v
	}
	if include[94] {
		k := gen_int64(567)
		v := gen_bytes(667)
		dst.MapSint64Bytes[k] = v
	}
	if include[94] {
		k := gen_int64(568)
		v := gen_bytes(668)
		dst.MapSint64Bytes[k] = v
	}
	if include[94] {
		k := gen_int64(569)
		v := gen_bytes(669)
		dst.MapSint64Bytes[k] = v
	}
	if include[94] {
		k := gen_int64(570)
		v := gen_bytes(670)
		dst.MapSint64Bytes[k] = v
	}
	dst.MapSint64_Item = make(map[int64]*Item)
	if include[95] {
		k := gen_int64(571)
		v := gen_Item(671)
		dst.MapSint64_Item[k] = v
	}
	if include[95] {
		k := gen_int64(572)
		v := gen_Item(672)
		dst.MapSint64_Item[k] = v
	}
	if include[95] {
		k := gen_int64(573)
		v := gen_Item(673)
		dst.MapSint64_Item[k] = v
	}
	if include[95] {
		k := gen_int64(574)
		v := gen_Item(674)
		dst.MapSint64_Item[k] = v
	}
	if include[95] {
		k := gen_int64(575)
		v := gen_Item(675)
		dst.MapSint64_Item[k] = v
	}
	if include[95] {
		k := gen_int64(576)
		v := gen_Item(676)
		dst.MapSint64_Item[k] = v
	}
	dst.MapFixed32Double = make(map[uint32]float64)
	if include[96] {
		k := gen_uint32(577)
		v := gen_float64(677)
		dst.MapFixed32Double[k] = v
	}
	if include[96] {
		k := gen_uint32(578)
		v := gen_float64(678)
		dst.MapFixed32Double[k] = v
	}
	if include[96] {
		k := gen_uint32(579)
		v := gen_float64(679)
		dst.MapFixed32Double[k] = v
	}
	if include[96] {
		k := gen_uint32(580)
		v := gen_float64(680)
		dst.MapFixed32Double[k] = v
	}
	if include[96] {
		k := gen_uint32(581)
		v := gen_float64(681)
		dst.MapFixed32Double[k] = v
	}
	if include[96] {
		k := gen_uint32(582)
		v := gen_float64(682)
		dst.MapFixed32Double[k] = v
	}
	dst.MapFixed32Float = make(map[uint32]float32)
	if include[97] {
		k := gen_uint32(583)
		v := gen_float32(683)
		dst.MapFixed32Float[k] = v
	}
	if include[97] {
		k := gen_uint32(584)
		v := gen_float32(684)
		dst.MapFixed32Float[k] = v
	}
	if include[97] {
		k := gen_uint32(585)
		v := gen_float32(685)
		dst.MapFixed32Float[k] = v
	}
	if include[97] {
		k := gen_uint32(586)
		v := gen_float32(686)
		dst.MapFixed32Float[k] = v
	}
	if include[97] {
		k := gen_uint32(587)
		v := gen_float32(687)
		dst.MapFixed32Float[k] = v
	}
	if include[97] {
		k := gen_uint32(588)
		v := gen_float32(688)
		dst.MapFixed32Float[k] = v
	}
	dst.MapFixed32Int32 = make(map[uint32]int32)
	if include[98] {
		k := gen_uint32(589)
		v := gen_int32(689)
		dst.MapFixed32Int32[k] = v
	}
	if include[98] {
		k := gen_uint32(590)
		v := gen_int32(690)
		dst.MapFixed32Int32[k] = v
	}
	if include[98] {
		k := gen_uint32(591)
		v := gen_int32(691)
		dst.MapFixed32Int32[k] = v
	}
	if include[98] {
		k := gen_uint32(592)
		v := gen_int32(692)
		dst.MapFixed32Int32[k] = v
	}
	if include[98] {
		k := gen_uint32(593)
		v := gen_int32(693)
		dst.MapFixed32Int32[k] = v
	}
	if include[98] {
		k := gen_uint32(594)
		v := gen_int32(694)
		dst.MapFixed32Int32[k] = v
	}
	dst.MapFixed32Int64 = make(map[uint32]int64)
	if include[99] {
		k := gen_uint32(595)
		v := gen_int64(695)
		dst.MapFixed32Int64[k] = v
	}
	if include[99] {
		k := gen_uint32(596)
		v := gen_int64(696)
		dst.MapFixed32Int64[k] = v
	}
	if include[99] {
		k := gen_uint32(597)
		v := gen_int64(697)
		dst.MapFixed32Int64[k] = v
	}
	if include[99] {
		k := gen_uint32(598)
		v := gen_int64(698)
		dst.MapFixed32Int64[k] = v
	}
	if include[99] {
		k := gen_uint32(599)
		v := gen_int64(699)
		dst.MapFixed32Int64[k] = v
	}
	if include[99] {
		k := gen_uint32(600)
		v := gen_int64(700)
		dst.MapFixed32Int64[k] = v
	}
	dst.MapFixed32Uint32 = make(map[uint32]uint32)
	if include[100] {
		k := gen_uint32(601)
		v := gen_uint32(701)
		dst.MapFixed32Uint32[k] = v
	}
	if include[100] {
		k := gen_uint32(602)
		v := gen_uint32(702)
		dst.MapFixed32Uint32[k] = v
	}
	if include[100] {
		k := gen_uint32(603)
		v := gen_uint32(703)
		dst.MapFixed32Uint32[k] = v
	}
	if include[100] {
		k := gen_uint32(604)
		v := gen_uint32(704)
		dst.MapFixed32Uint32[k] = v
	}
	if include[100] {
		k := gen_uint32(605)
		v := gen_uint32(705)
		dst.MapFixed32Uint32[k] = v
	}
	if include[100] {
		k := gen_uint32(606)
		v := gen_uint32(706)
		dst.MapFixed32Uint32[k] = v
	}
	dst.MapFixed32Uint64 = make(map[uint32]uint64)
	if include[101] {
		k := gen_uint32(607)
		v := gen_uint64(707)
		dst.MapFixed32Uint64[k] = v
	}
	if include[101] {
		k := gen_uint32(608)
		v := gen_uint64(708)
		dst.MapFixed32Uint64[k] = v
	}
	if include[101] {
		k := gen_uint32(609)
		v := gen_uint64(709)
		dst.MapFixed32Uint64[k] = v
	}
	if include[101] {
		k := gen_uint32(610)
		v := gen_uint64(710)
		dst.MapFixed32Uint64[k] = v
	}
	if include[101] {
		k := gen_uint32(611)
		v := gen_uint64(711)
		dst.MapFixed32Uint64[k] = v
	}
	if include[101] {
		k := gen_uint32(612)
		v := gen_uint64(712)
		dst.MapFixed32Uint64[k] = v
	}
	dst.MapFixed32Sint32 = make(map[uint32]int32)
	if include[102] {
		k := gen_uint32(613)
		v := gen_int32(713)
		dst.MapFixed32Sint32[k] = v
	}
	if include[102] {
		k := gen_uint32(614)
		v := gen_int32(714)
		dst.MapFixed32Sint32[k] = v
	}
	if include[102] {
		k := gen_uint32(615)
		v := gen_int32(715)
		dst.MapFixed32Sint32[k] = v
	}
	if include[102] {
		k := gen_uint32(616)
		v := gen_int32(716)
		dst.MapFixed32Sint32[k] = v
	}
	if include[102] {
		k := gen_uint32(617)
		v := gen_int32(717)
		dst.MapFixed32Sint32[k] = v
	}
	if include[102] {
		k := gen_uint32(618)
		v := gen_int32(718)
		dst.MapFixed32Sint32[k] = v
	}
	dst.MapFixed32Sint64 = make(map[uint32]int64)
	if include[103] {
		k := gen_uint32(619)
		v := gen_int64(719)
		dst.MapFixed32Sint64[k] = v
	}
	if include[103] {
		k := gen_uint32(620)
		v := gen_int64(720)
		dst.MapFixed32Sint64[k] = v
	}
	if include[103] {
		k := gen_uint32(621)
		v := gen_int64(721)
		dst.MapFixed32Sint64[k] = v
	}
	if include[103] {
		k := gen_uint32(622)
		v := gen_int64(722)
		dst.MapFixed32Sint64[k] = v
	}
	if include[103] {
		k := gen_uint32(623)
		v := gen_int64(723)
		dst.MapFixed32Sint64[k] = v
	}
	if include[103] {
		k := gen_uint32(624)
		v := gen_int64(724)
		dst.MapFixed32Sint64[k] = v
	}
	dst.MapFixed32Fixed32 = make(map[uint32]uint32)
	if include[104] {
		k := gen_uint32(625)
		v := gen_uint32(725)
		dst.MapFixed32Fixed32[k] = v
	}
	if include[104] {
		k := gen_uint32(626)
		v := gen_uint32(726)
		dst.MapFixed32Fixed32[k] = v
	}
	if include[104] {
		k := gen_uint32(627)
		v := gen_uint32(727)
		dst.MapFixed32Fixed32[k] = v
	}
	if include[104] {
		k := gen_uint32(628)
		v := gen_uint32(728)
		dst.MapFixed32Fixed32[k] = v
	}
	if include[104] {
		k := gen_uint32(629)
		v := gen_uint32(729)
		dst.MapFixed32Fixed32[k] = v
	}
	if include[104] {
		k := gen_uint32(630)
		v := gen_uint32(730)
		dst.MapFixed32Fixed32[k] = v
	}
	dst.MapFixed32Fixed64 = make(map[uint32]uint64)
	if include[105] {
		k := gen_uint32(631)
		v := gen_uint64(731)
		dst.MapFixed32Fixed64[k] = v
	}
	if include[105] {
		k := gen_uint32(632)
		v := gen_uint64(732)
		dst.MapFixed32Fixed64[k] = v
	}
	if include[105] {
		k := gen_uint32(633)
		v := gen_uint64(733)
		dst.MapFixed32Fixed64[k] = v
	}
	if include[105] {
		k := gen_uint32(634)
		v := gen_uint64(734)
		dst.MapFixed32Fixed64[k] = v
	}
	if include[105] {
		k := gen_uint32(635)
		v := gen_uint64(735)
		dst.MapFixed32Fixed64[k] = v
	}
	if include[105] {
		k := gen_uint32(636)
		v := gen_uint64(736)
		dst.MapFixed32Fixed64[k] = v
	}
	dst.MapFixed32Sfixed32 = make(map[uint32]int32)
	if include[106] {
		k := gen_uint32(637)
		v := gen_int32(737)
		dst.MapFixed32Sfixed32[k] = v
	}
	if include[106] {
		k := gen_uint32(638)
		v := gen_int32(738)
		dst.MapFixed32Sfixed32[k] = v
	}
	if include[106] {
		k := gen_uint32(639)
		v := gen_int32(739)
		dst.MapFixed32Sfixed32[k] = v
	}
	if include[106] {
		k := gen_uint32(640)
		v := gen_int32(740)
		dst.MapFixed32Sfixed32[k] = v
	}
	if include[106] {
		k := gen_uint32(641)
		v := gen_int32(741)
		dst.MapFixed32Sfixed32[k] = v
	}
	if include[106] {
		k := gen_uint32(642)
		v := gen_int32(742)
		dst.MapFixed32Sfixed32[k] = v
	}
	dst.MapFixed32Sfixed64 = make(map[uint32]int64)
	if include[107] {
		k := gen_uint32(643)
		v := gen_int64(743)
		dst.MapFixed32Sfixed64[k] = v
	}
	if include[107] {
		k := gen_uint32(644)
		v := gen_int64(744)
		dst.MapFixed32Sfixed64[k] = v
	}
	if include[107] {
		k := gen_uint32(645)
		v := gen_int64(745)
		dst.MapFixed32Sfixed64[k] = v
	}
	if include[107] {
		k := gen_uint32(646)
		v := gen_int64(746)
		dst.MapFixed32Sfixed64[k] = v
	}
	if include[107] {
		k := gen_uint32(647)
		v := gen_int64(747)
		dst.MapFixed32Sfixed64[k] = v
	}
	if include[107] {
		k := gen_uint32(648)
		v := gen_int64(748)
		dst.MapFixed32Sfixed64[k] = v
	}
	dst.MapFixed32Bool = make(map[uint32]bool)
	if include[108] {
		k := gen_uint32(649)
		v := gen_bool(749)
		dst.MapFixed32Bool[k] = v
	}
	if include[108] {
		k := gen_uint32(650)
		v := gen_bool(750)
		dst.MapFixed32Bool[k] = v
	}
	if include[108] {
		k := gen_uint32(651)
		v := gen_bool(751)
		dst.MapFixed32Bool[k] = v
	}
	if include[108] {
		k := gen_uint32(652)
		v := gen_bool(752)
		dst.MapFixed32Bool[k] = v
	}
	if include[108] {
		k := gen_uint32(653)
		v := gen_bool(753)
		dst.MapFixed32Bool[k] = v
	}
	if include[108] {
		k := gen_uint32(654)
		v := gen_bool(754)
		dst.MapFixed32Bool[k] = v
	}
	dst.MapFixed32String = make(map[uint32]string)
	if include[109] {
		k := gen_uint32(655)
		v := gen_string(755)
		dst.MapFixed32String[k] = v
	}
	if include[109] {
		k := gen_uint32(656)
		v := gen_string(756)
		dst.MapFixed32String[k] = v
	}
	if include[109] {
		k := gen_uint32(657)
		v := gen_string(757)
		dst.MapFixed32String[k] = v
	}
	if include[109] {
		k := gen_uint32(658)
		v := gen_string(758)
		dst.MapFixed32String[k] = v
	}
	if include[109] {
		k := gen_uint32(659)
		v := gen_string(759)
		dst.MapFixed32String[k] = v
	}
	if include[109] {
		k := gen_uint32(660)
		v := gen_string(760)
		dst.MapFixed32String[k] = v
	}
	dst.MapFixed32Bytes = make(map[uint32][]byte)
	if include[110] {
		k := gen_uint32(661)
		v := gen_bytes(761)
		dst.MapFixed32Bytes[k] = v
	}
	if include[110] {
		k := gen_uint32(662)
		v := gen_bytes(762)
		dst.MapFixed32Bytes[k] = v
	}
	if include[110] {
		k := gen_uint32(663)
		v := gen_bytes(763)
		dst.MapFixed32Bytes[k] = v
	}
	if include[110] {
		k := gen_uint32(664)
		v := gen_bytes(764)
		dst.MapFixed32Bytes[k] = v
	}
	if include[110] {
		k := gen_uint32(665)
		v := gen_bytes(765)
		dst.MapFixed32Bytes[k] = v
	}
	if include[110] {
		k := gen_uint32(666)
		v := gen_bytes(766)
		dst.MapFixed32Bytes[k] = v
	}
	dst.MapFixed32_Item = make(map[uint32]*Item)
	if include[111] {
		k := gen_uint32(667)
		v := gen_Item(767)
		dst.MapFixed32_Item[k] = v
	}
	if include[111] {
		k := gen_uint32(668)
		v := gen_Item(768)
		dst.MapFixed32_Item[k] = v
	}
	if include[111] {
		k := gen_uint32(669)
		v := gen_Item(769)
		dst.MapFixed32_Item[k] = v
	}
	if include[111] {
		k := gen_uint32(670)
		v := gen_Item(770)
		dst.MapFixed32_Item[k] = v
	}
	if include[111] {
		k := gen_uint32(671)
		v := gen_Item(771)
		dst.MapFixed32_Item[k] = v
	}
	if include[111] {
		k := gen_uint32(672)
		v := gen_Item(772)
		dst.MapFixed32_Item[k] = v
	}
	dst.MapFixed64Double = make(map[uint64]float64)
	if include[112] {
		k := gen_uint64(673)
		v := gen_float64(773)
		dst.MapFixed64Double[k] = v
	}
	if include[112] {
		k := gen_uint64(674)
		v := gen_float64(774)
		dst.MapFixed64Double[k] = v
	}
	if include[112] {
		k := gen_uint64(675)
		v := gen_float64(775)
		dst.MapFixed64Double[k] = v
	}
	if include[112] {
		k := gen_uint64(676)
		v := gen_float64(776)
		dst.MapFixed64Double[k] = v
	}
	if include[112] {
		k := gen_uint64(677)
		v := gen_float64(777)
		dst.MapFixed64Double[k] = v
	}
	if include[112] {
		k := gen_uint64(678)
		v := gen_float64(778)
		dst.MapFixed64Double[k] = v
	}
	dst.MapFixed64Float = make(map[uint64]float32)
	if include[113] {
		k := gen_uint64(679)
		v := gen_float32(779)
		dst.MapFixed64Float[k] = v
	}
	if include[113] {
		k := gen_uint64(680)
		v := gen_float32(780)
		dst.MapFixed64Float[k] = v
	}
	if include[113] {
		k := gen_uint64(681)
		v := gen_float32(781)
		dst.MapFixed64Float[k] = v
	}
	if include[113] {
		k := gen_uint64(682)
		v := gen_float32(782)
		dst.MapFixed64Float[k] = v
	}
	if include[113] {
		k := gen_uint64(683)
		v := gen_float32(783)
		dst.MapFixed64Float[k] = v
	}
	if include[113] {
		k := gen_uint64(684)
		v := gen_float32(784)
		dst.MapFixed64Float[k] = v
	}
	dst.MapFixed64Int32 = make(map[uint64]int32)
	if include[114] {
		k := gen_uint64(685)
		v := gen_int32(785)
		dst.MapFixed64Int32[k] = v
	}
	if include[114] {
		k := gen_uint64(686)
		v := gen_int32(786)
		dst.MapFixed64Int32[k] = v
	}
	if include[114] {
		k := gen_uint64(687)
		v := gen_int32(787)
		dst.MapFixed64Int32[k] = v
	}
	if include[114] {
		k := gen_uint64(688)
		v := gen_int32(788)
		dst.MapFixed64Int32[k] = v
	}
	if include[114] {
		k := gen_uint64(689)
		v := gen_int32(789)
		dst.MapFixed64Int32[k] = v
	}
	if include[114] {
		k := gen_uint64(690)
		v := gen_int32(790)
		dst.MapFixed64Int32[k] = v
	}
	dst.MapFixed64Int64 = make(map[uint64]int64)
	if include[115] {
		k := gen_uint64(691)
		v := gen_int64(791)
		dst.MapFixed64Int64[k] = v
	}
	if include[115] {
		k := gen_uint64(692)
		v := gen_int64(792)
		dst.MapFixed64Int64[k] = v
	}
	if include[115] {
		k := gen_uint64(693)
		v := gen_int64(793)
		dst.MapFixed64Int64[k] = v
	}
	if include[115] {
		k := gen_uint64(694)
		v := gen_int64(794)
		dst.MapFixed64Int64[k] = v
	}
	if include[115] {
		k := gen_uint64(695)
		v := gen_int64(795)
		dst.MapFixed64Int64[k] = v
	}
	if include[115] {
		k := gen_uint64(696)
		v := gen_int64(796)
		dst.MapFixed64Int64[k] = v
	}
	dst.MapFixed64Uint32 = make(map[uint64]uint32)
	if include[116] {
		k := gen_uint64(697)
		v := gen_uint32(797)
		dst.MapFixed64Uint32[k] = v
	}
	if include[116] {
		k := gen_uint64(698)
		v := gen_uint32(798)
		dst.MapFixed64Uint32[k] = v
	}
	if include[116] {
		k := gen_uint64(699)
		v := gen_uint32(799)
		dst.MapFixed64Uint32[k] = v
	}
	if include[116] {
		k := gen_uint64(700)
		v := gen_uint32(800)
		dst.MapFixed64Uint32[k] = v
	}
	if include[116] {
		k := gen_uint64(701)
		v := gen_uint32(801)
		dst.MapFixed64Uint32[k] = v
	}
	if include[116] {
		k := gen_uint64(702)
		v := gen_uint32(802)
		dst.MapFixed64Uint32[k] = v
	}
	dst.MapFixed64Uint64 = make(map[uint64]uint64)
	if include[117] {
		k := gen_uint64(703)
		v := gen_uint64(803)
		dst.MapFixed64Uint64[k] = v
	}
	if include[117] {
		k := gen_uint64(704)
		v := gen_uint64(804)
		dst.MapFixed64Uint64[k] = v
	}
	if include[117] {
		k := gen_uint64(705)
		v := gen_uint64(805)
		dst.MapFixed64Uint64[k] = v
	}
	if include[117] {
		k := gen_uint64(706)
		v := gen_uint64(806)
		dst.MapFixed64Uint64[k] = v
	}
	if include[117] {
		k := gen_uint64(707)
		v := gen_uint64(807)
		dst.MapFixed64Uint64[k] = v
	}
	if include[117] {
		k := gen_uint64(708)
		v := gen_uint64(808)
		dst.MapFixed64Uint64[k] = v
	}
	dst.MapFixed64Sint32 = make(map[uint64]int32)
	if include[118] {
		k := gen_uint64(709)
		v := gen_int32(809)
		dst.MapFixed64Sint32[k] = v
	}
	if include[118] {
		k := gen_uint64(710)
		v := gen_int32(810)
		dst.MapFixed64Sint32[k] = v
	}
	if include[118] {
		k := gen_uint64(711)
		v := gen_int32(811)
		dst.MapFixed64Sint32[k] = v
	}
	if include[118] {
		k := gen_uint64(712)
		v := gen_int32(812)
		dst.MapFixed64Sint32[k] = v
	}
	if include[118] {
		k := gen_uint64(713)
		v := gen_int32(813)
		dst.MapFixed64Sint32[k] = v
	}
	if include[118] {
		k := gen_uint64(714)
		v := gen_int32(814)
		dst.MapFixed64Sint32[k] = v
	}
	dst.MapFixed64Sint64 = make(map[uint64]int64)
	if include[119] {
		k := gen_uint64(715)
		v := gen_int64(815)
		dst.MapFixed64Sint64[k] = v
	}
	if include[119] {
		k := gen_uint64(716)
		v := gen_int64(816)
		dst.MapFixed64Sint64[k] = v
	}
	if include[119] {
		k := gen_uint64(717)
		v := gen_int64(817)
		dst.MapFixed64Sint64[k] = v
	}
	if include[119] {
		k := gen_uint64(718)
		v := gen_int64(818)
		dst.MapFixed64Sint64[k] = v
	}
	if include[119] {
		k := gen_uint64(719)
		v := gen_int64(819)
		dst.MapFixed64Sint64[k] = v
	}
	if include[119] {
		k := gen_uint64(720)
		v := gen_int64(820)
		dst.MapFixed64Sint64[k] = v
	}
	dst.MapFixed64Fixed32 = make(map[uint64]uint32)
	if include[120] {
		k := gen_uint64(721)
		v := gen_uint32(821)
		dst.MapFixed64Fixed32[k] = v
	}
	if include[120] {
		k := gen_uint64(722)
		v := gen_uint32(822)
		dst.MapFixed64Fixed32[k] = v
	}
	if include[120] {
		k := gen_uint64(723)
		v := gen_uint32(823)
		dst.MapFixed64Fixed32[k] = v
	}
	if include[120] {
		k := gen_uint64(724)
		v := gen_uint32(824)
		dst.MapFixed64Fixed32[k] = v
	}
	if include[120] {
		k := gen_uint64(725)
		v := gen_uint32(825)
		dst.MapFixed64Fixed32[k] = v
	}
	if include[120] {
		k := gen_uint64(726)
		v := gen_uint32(826)
		dst.MapFixed64Fixed32[k] = v
	}
	dst.MapFixed64Fixed64 = make(map[uint64]uint64)
	if include[121] {
		k := gen_uint64(727)
		v := gen_uint64(827)
		dst.MapFixed64Fixed64[k] = v
	}
	if include[121] {
		k := gen_uint64(728)
		v := gen_uint64(828)
		dst.MapFixed64Fixed64[k] = v
	}
	if include[121] {
		k := gen_uint64(729)
		v := gen_uint64(829)
		dst.MapFixed64Fixed64[k] = v
	}
	if include[121] {
		k := gen_uint64(730)
		v := gen_uint64(830)
		dst.MapFixed64Fixed64[k] = v
	}
	if include[121] {
		k := gen_uint64(731)
		v := gen_uint64(831)
		dst.MapFixed64Fixed64[k] = v
	}
	if include[121] {
		k := gen_uint64(732)
		v := gen_uint64(832)
		dst.MapFixed64Fixed64[k] = v
	}
	dst.MapFixed64Sfixed32 = make(map[uint64]int32)
	if include[122] {
		k := gen_uint64(733)
		v := gen_int32(833)
		dst.MapFixed64Sfixed32[k] = v
	}
	if include[122] {
		k := gen_uint64(734)
		v := gen_int32(834)
		dst.MapFixed64Sfixed32[k] = v
	}
	if include[122] {
		k := gen_uint64(735)
		v := gen_int32(835)
		dst.MapFixed64Sfixed32[k] = v
	}
	if include[122] {
		k := gen_uint64(736)
		v := gen_int32(836)
		dst.MapFixed64Sfixed32[k] = v
	}
	if include[122] {
		k := gen_uint64(737)
		v := gen_int32(837)
		dst.MapFixed64Sfixed32[k] = v
	}
	if include[122] {
		k := gen_uint64(738)
		v := gen_int32(838)
		dst.MapFixed64Sfixed32[k] = v
	}
	dst.MapFixed64Sfixed64 = make(map[uint64]int64)
	if include[123] {
		k := gen_uint64(739)
		v := gen_int64(839)
		dst.MapFixed64Sfixed64[k] = v
	}
	if include[123] {
		k := gen_uint64(740)
		v := gen_int64(840)
		dst.MapFixed64Sfixed64[k] = v
	}
	if include[123] {
		k := gen_uint64(741)
		v := gen_int64(841)
		dst.MapFixed64Sfixed64[k] = v
	}
	if include[123] {
		k := gen_uint64(742)
		v := gen_int64(842)
		dst.MapFixed64Sfixed64[k] = v
	}
	if include[123] {
		k := gen_uint64(743)
		v := gen_int64(843)
		dst.MapFixed64Sfixed64[k] = v
	}
	if include[123] {
		k := gen_uint64(744)
		v := gen_int64(844)
		dst.MapFixed64Sfixed64[k] = v
	}
	dst.MapFixed64Bool = make(map[uint64]bool)
	if include[124] {
		k := gen_uint64(745)
		v := gen_bool(845)
		dst.MapFixed64Bool[k] = v
	}
	if include[124] {
		k := gen_uint64(746)
		v := gen_bool(846)
		dst.MapFixed64Bool[k] = v
	}
	if include[124] {
		k := gen_uint64(747)
		v := gen_bool(847)
		dst.MapFixed64Bool[k] = v
	}
	if include[124] {
		k := gen_uint64(748)
		v := gen_bool(848)
		dst.MapFixed64Bool[k] = v
	}
	if include[124] {
		k := gen_uint64(749)
		v := gen_bool(849)
		dst.MapFixed64Bool[k] = v
	}
	if include[124] {
		k := gen_uint64(750)
		v := gen_bool(850)
		dst.MapFixed64Bool[k] = v
	}
	dst.MapFixed64String = make(map[uint64]string)
	if include[125] {
		k := gen_uint64(751)
		v := gen_string(851)
		dst.MapFixed64String[k] = v
	}
	if include[125] {
		k := gen_uint64(752)
		v := gen_string(852)
		dst.MapFixed64String[k] = v
	}
	if include[125] {
		k := gen_uint64(753)
		v := gen_string(853)
		dst.MapFixed64String[k] = v
	}
	if include[125] {
		k := gen_uint64(754)
		v := gen_string(854)
		dst.MapFixed64String[k] = v
	}
	if include[125] {
		k := gen_uint64(755)
		v := gen_string(855)
		dst.MapFixed64String[k] = v
	}
	if include[125] {
		k := gen_uint64(756)
		v := gen_string(856)
		dst.MapFixed64String[k] = v
	}
	dst.MapFixed64Bytes = make(map[uint64][]byte)
	if include[126] {
		k := gen_uint64(757)
		v := gen_bytes(857)
		dst.MapFixed64Bytes[k] = v
	}
	if include[126] {
		k := gen_uint64(758)
		v := gen_bytes(858)
		dst.MapFixed64Bytes[k] = v
	}
	if include[126] {
		k := gen_uint64(759)
		v := gen_bytes(859)
		dst.MapFixed64Bytes[k] = v
	}
	if include[126] {
		k := gen_uint64(760)
		v := gen_bytes(860)
		dst.MapFixed64Bytes[k] = v
	}
	if include[126] {
		k := gen_uint64(761)
		v := gen_bytes(861)
		dst.MapFixed64Bytes[k] = v
	}
	if include[126] {
		k := gen_uint64(762)
		v := gen_bytes(862)
		dst.MapFixed64Bytes[k] = v
	}
	dst.MapFixed64_Item = make(map[uint64]*Item)
	if include[127] {
		k := gen_uint64(763)
		v := gen_Item(863)
		dst.MapFixed64_Item[k] = v
	}
	if include[127] {
		k := gen_uint64(764)
		v := gen_Item(864)
		dst.MapFixed64_Item[k] = v
	}
	if include[127] {
		k := gen_uint64(765)
		v := gen_Item(865)
		dst.MapFixed64_Item[k] = v
	}
	if include[127] {
		k := gen_uint64(766)
		v := gen_Item(866)
		dst.MapFixed64_Item[k] = v
	}
	if include[127] {
		k := gen_uint64(767)
		v := gen_Item(867)
		dst.MapFixed64_Item[k] = v
	}
	if include[127] {
		k := gen_uint64(768)
		v := gen_Item(868)
		dst.MapFixed64_Item[k] = v
	}
	dst.MapSfixed32Double = make(map[int32]float64)
	if include[128] {
		k := gen_int32(769)
		v := gen_float64(869)
		dst.MapSfixed32Double[k] = v
	}
	if include[128] {
		k := gen_int32(770)
		v := gen_float64(870)
		dst.MapSfixed32Double[k] = v
	}
	if include[128] {
		k := gen_int32(771)
		v := gen_float64(871)
		dst.MapSfixed32Double[k] = v
	}
	if include[128] {
		k := gen_int32(772)
		v := gen_float64(872)
		dst.MapSfixed32Double[k] = v
	}
	if include[128] {
		k := gen_int32(773)
		v := gen_float64(873)
		dst.MapSfixed32Double[k] = v
	}
	if include[128] {
		k := gen_int32(774)
		v := gen_float64(874)
		dst.MapSfixed32Double[k] = v
	}
	dst.MapSfixed32Float = make(map[int32]float32)
	if include[129] {
		k := gen_int32(775)
		v := gen_float32(875)
		dst.MapSfixed32Float[k] = v
	}
	if include[129] {
		k := gen_int32(776)
		v := gen_float32(876)
		dst.MapSfixed32Float[k] = v
	}
	if include[129] {
		k := gen_int32(777)
		v := gen_float32(877)
		dst.MapSfixed32Float[k] = v
	}
	if include[129] {
		k := gen_int32(778)
		v := gen_float32(878)
		dst.MapSfixed32Float[k] = v
	}
	if include[129] {
		k := gen_int32(779)
		v := gen_float32(879)
		dst.MapSfixed32Float[k] = v
	}
	if include[129] {
		k := gen_int32(780)
		v := gen_float32(880)
		dst.MapSfixed32Float[k] = v
	}
	dst.MapSfixed32Int32 = make(map[int32]int32)
	if include[130] {
		k := gen_int32(781)
		v := gen_int32(881)
		dst.MapSfixed32Int32[k] = v
	}
	if include[130] {
		k := gen_int32(782)
		v := gen_int32(882)
		dst.MapSfixed32Int32[k] = v
	}
	if include[130] {
		k := gen_int32(783)
		v := gen_int32(883)
		dst.MapSfixed32Int32[k] = v
	}
	if include[130] {
		k := gen_int32(784)
		v := gen_int32(884)
		dst.MapSfixed32Int32[k] = v
	}
	if include[130] {
		k := gen_int32(785)
		v := gen_int32(885)
		dst.MapSfixed32Int32[k] = v
	}
	if include[130] {
		k := gen_int32(786)
		v := gen_int32(886)
		dst.MapSfixed32Int32[k] = v
	}
	dst.MapSfixed32Int64 = make(map[int32]int64)
	if include[131] {
		k := gen_int32(787)
		v := gen_int64(887)
		dst.MapSfixed32Int64[k] = v
	}
	if include[131] {
		k := gen_int32(788)
		v := gen_int64(888)
		dst.MapSfixed32Int64[k] = v
	}
	if include[131] {
		k := gen_int32(789)
		v := gen_int64(889)
		dst.MapSfixed32Int64[k] = v
	}
	if include[131] {
		k := gen_int32(790)
		v := gen_int64(890)
		dst.MapSfixed32Int64[k] = v
	}
	if include[131] {
		k := gen_int32(791)
		v := gen_int64(891)
		dst.MapSfixed32Int64[k] = v
	}
	if include[131] {
		k := gen_int32(792)
		v := gen_int64(892)
		dst.MapSfixed32Int64[k] = v
	}
	dst.MapSfixed32Uint32 = make(map[int32]uint32)
	if include[132] {
		k := gen_int32(793)
		v := gen_uint32(893)
		dst.MapSfixed32Uint32[k] = v
	}
	if include[132] {
		k := gen_int32(794)
		v := gen_uint32(894)
		dst.MapSfixed32Uint32[k] = v
	}
	if include[132] {
		k := gen_int32(795)
		v := gen_uint32(895)
		dst.MapSfixed32Uint32[k] = v
	}
	if include[132] {
		k := gen_int32(796)
		v := gen_uint32(896)
		dst.MapSfixed32Uint32[k] = v
	}
	if include[132] {
		k := gen_int32(797)
		v := gen_uint32(897)
		dst.MapSfixed32Uint32[k] = v
	}
	if include[132] {
		k := gen_int32(798)
		v := gen_uint32(898)
		dst.MapSfixed32Uint32[k] = v
	}
	dst.MapSfixed32Uint64 = make(map[int32]uint64)
	if include[133] {
		k := gen_int32(799)
		v := gen_uint64(899)
		dst.MapSfixed32Uint64[k] = v
	}
	if include[133] {
		k := gen_int32(800)
		v := gen_uint64(900)
		dst.MapSfixed32Uint64[k] = v
	}
	if include[133] {
		k := gen_int32(801)
		v := gen_uint64(901)
		dst.MapSfixed32Uint64[k] = v
	}
	if include[133] {
		k := gen_int32(802)
		v := gen_uint64(902)
		dst.MapSfixed32Uint64[k] = v
	}
	if include[133] {
		k := gen_int32(803)
		v := gen_uint64(903)
		dst.MapSfixed32Uint64[k] = v
	}
	if include[133] {
		k := gen_int32(804)
		v := gen_uint64(904)
		dst.MapSfixed32Uint64[k] = v
	}
	dst.MapSfixed32Sint32 = make(map[int32]int32)
	if include[134] {
		k := gen_int32(805)
		v := gen_int32(905)
		dst.MapSfixed32Sint32[k] = v
	}
	if include[134] {
		k := gen_int32(806)
		v := gen_int32(906)
		dst.MapSfixed32Sint32[k] = v
	}
	if include[134] {
		k := gen_int32(807)
		v := gen_int32(907)
		dst.MapSfixed32Sint32[k] = v
	}
	if include[134] {
		k := gen_int32(808)
		v := gen_int32(908)
		dst.MapSfixed32Sint32[k] = v
	}
	if include[134] {
		k := gen_int32(809)
		v := gen_int32(909)
		dst.MapSfixed32Sint32[k] = v
	}
	if include[134] {
		k := gen_int32(810)
		v := gen_int32(910)
		dst.MapSfixed32Sint32[k] = v
	}
	dst.MapSfixed32Sint64 = make(map[int32]int64)
	if include[135] {
		k := gen_int32(811)
		v := gen_int64(911)
		dst.MapSfixed32Sint64[k] = v
	}
	if include[135] {
		k := gen_int32(812)
		v := gen_int64(912)
		dst.MapSfixed32Sint64[k] = v
	}
	if include[135] {
		k := gen_int32(813)
		v := gen_int64(913)
		dst.MapSfixed32Sint64[k] = v
	}
	if include[135] {
		k := gen_int32(814)
		v := gen_int64(914)
		dst.MapSfixed32Sint64[k] = v
	}
	if include[135] {
		k := gen_int32(815)
		v := gen_int64(915)
		dst.MapSfixed32Sint64[k] = v
	}
	if include[135] {
		k := gen_int32(816)
		v := gen_int64(916)
		dst.MapSfixed32Sint64[k] = v
	}
	dst.MapSfixed32Fixed32 = make(map[int32]uint32)
	if include[136] {
		k := gen_int32(817)
		v := gen_uint32(917)
		dst.MapSfixed32Fixed32[k] = v
	}
	if include[136] {
		k := gen_int32(818)
		v := gen_uint32(918)
		dst.MapSfixed32Fixed32[k] = v
	}
	if include[136] {
		k := gen_int32(819)
		v := gen_uint32(919)
		dst.MapSfixed32Fixed32[k] = v
	}
	if include[136] {
		k := gen_int32(820)
		v := gen_uint32(920)
		dst.MapSfixed32Fixed32[k] = v
	}
	if include[136] {
		k := gen_int32(821)
		v := gen_uint32(921)
		dst.MapSfixed32Fixed32[k] = v
	}
	if include[136] {
		k := gen_int32(822)
		v := gen_uint32(922)
		dst.MapSfixed32Fixed32[k] = v
	}
	dst.MapSfixed32Fixed64 = make(map[int32]uint64)
	if include[137] {
		k := gen_int32(823)
		v := gen_uint64(923)
		dst.MapSfixed32Fixed64[k] = v
	}
	if include[137] {
		k := gen_int32(824)
		v := gen_uint64(924)
		dst.MapSfixed32Fixed64[k] = v
	}
	if include[137] {
		k := gen_int32(825)
		v := gen_uint64(925)
		dst.MapSfixed32Fixed64[k] = v
	}
	if include[137] {
		k := gen_int32(826)
		v := gen_uint64(926)
		dst.MapSfixed32Fixed64[k] = v
	}
	if include[137] {
		k := gen_int32(827)
		v := gen_uint64(927)
		dst.MapSfixed32Fixed64[k] = v
	}
	if include[137] {
		k := gen_int32(828)
		v := gen_uint64(928)
		dst.MapSfixed32Fixed64[k] = v
	}
	dst.MapSfixed32Sfixed32 = make(map[int32]int32)
	if include[138] {
		k := gen_int32(829)
		v := gen_int32(929)
		dst.MapSfixed32Sfixed32[k] = v
	}
	if include[138] {
		k := gen_int32(830)
		v := gen_int32(930)
		dst.MapSfixed32Sfixed32[k] = v
	}
	if include[138] {
		k := gen_int32(831)
		v := gen_int32(931)
		dst.MapSfixed32Sfixed32[k] = v
	}
	if include[138] {
		k := gen_int32(832)
		v := gen_int32(932)
		dst.MapSfixed32Sfixed32[k] = v
	}
	if include[138] {
		k := gen_int32(833)
		v := gen_int32(933)
		dst.MapSfixed32Sfixed32[k] = v
	}
	if include[138] {
		k := gen_int32(834)
		v := gen_int32(934)
		dst.MapSfixed32Sfixed32[k] = v
	}
	dst.MapSfixed32Sfixed64 = make(map[int32]int64)
	if include[139] {
		k := gen_int32(835)
		v := gen_int64(935)
		dst.MapSfixed32Sfixed64[k] = v
	}
	if include[139] {
		k := gen_int32(836)
		v := gen_int64(936)
		dst.MapSfixed32Sfixed64[k] = v
	}
	if include[139] {
		k := gen_int32(837)
		v := gen_int64(937)
		dst.MapSfixed32Sfixed64[k] = v
	}
	if include[139] {
		k := gen_int32(838)
		v := gen_int64(938)
		dst.MapSfixed32Sfixed64[k] = v
	}
	if include[139] {
		k := gen_int32(839)
		v := gen_int64(939)
		dst.MapSfixed32Sfixed64[k] = v
	}
	if include[139] {
		k := gen_int32(840)
		v := gen_int64(940)
		dst.MapSfixed32Sfixed64[k] = v
	}
	dst.MapSfixed32Bool = make(map[int32]bool)
	if include[140] {
		k := gen_int32(841)
		v := gen_bool(941)
		dst.MapSfixed32Bool[k] = v
	}
	if include[140] {
		k := gen_int32(842)
		v := gen_bool(942)
		dst.MapSfixed32Bool[k] = v
	}
	if include[140] {
		k := gen_int32(843)
		v := gen_bool(943)
		dst.MapSfixed32Bool[k] = v
	}
	if include[140] {
		k := gen_int32(844)
		v := gen_bool(944)
		dst.MapSfixed32Bool[k] = v
	}
	if include[140] {
		k := gen_int32(845)
		v := gen_bool(945)
		dst.MapSfixed32Bool[k] = v
	}
	if include[140] {
		k := gen_int32(846)
		v := gen_bool(946)
		dst.MapSfixed32Bool[k] = v
	}
	dst.MapSfixed32String = make(map[int32]string)
	if include[141] {
		k := gen_int32(847)
		v := gen_string(947)
		dst.MapSfixed32String[k] = v
	}
	if include[141] {
		k := gen_int32(848)
		v := gen_string(948)
		dst.MapSfixed32String[k] = v
	}
	if include[141] {
		k := gen_int32(849)
		v := gen_string(949)
		dst.MapSfixed32String[k] = v
	}
	if include[141] {
		k := gen_int32(850)
		v := gen_string(950)
		dst.MapSfixed32String[k] = v
	}
	if include[141] {
		k := gen_int32(851)
		v := gen_string(951)
		dst.MapSfixed32String[k] = v
	}
	if include[141] {
		k := gen_int32(852)
		v := gen_string(952)
		dst.MapSfixed32String[k] = v
	}
	dst.MapSfixed32Bytes = make(map[int32][]byte)
	if include[142] {
		k := gen_int32(853)
		v := gen_bytes(953)
		dst.MapSfixed32Bytes[k] = v
	}
	if include[142] {
		k := gen_int32(854)
		v := gen_bytes(954)
		dst.MapSfixed32Bytes[k] = v
	}
	if include[142] {
		k := gen_int32(855)
		v := gen_bytes(955)
		dst.MapSfixed32Bytes[k] = v
	}
	if include[142] {
		k := gen_int32(856)
		v := gen_bytes(956)
		dst.MapSfixed32Bytes[k] = v
	}
	if include[142] {
		k := gen_int32(857)
		v := gen_bytes(957)
		dst.MapSfixed32Bytes[k] = v
	}
	if include[142] {
		k := gen_int32(858)
		v := gen_bytes(958)
		dst.MapSfixed32Bytes[k] = v
	}
	dst.MapSfixed32_Item = make(map[int32]*Item)
	if include[143] {
		k := gen_int32(859)
		v := gen_Item(959)
		dst.MapSfixed32_Item[k] = v
	}
	if include[143] {
		k := gen_int32(860)
		v := gen_Item(960)
		dst.MapSfixed32_Item[k] = v
	}
	if include[143] {
		k := gen_int32(861)
		v := gen_Item(961)
		dst.MapSfixed32_Item[k] = v
	}
	if include[143] {
		k := gen_int32(862)
		v := gen_Item(962)
		dst.MapSfixed32_Item[k] = v
	}
	if include[143] {
		k := gen_int32(863)
		v := gen_Item(963)
		dst.MapSfixed32_Item[k] = v
	}
	if include[143] {
		k := gen_int32(864)
		v := gen_Item(964)
		dst.MapSfixed32_Item[k] = v
	}
	dst.MapSfixed64Double = make(map[int64]float64)
	if include[144] {
		k := gen_int64(865)
		v := gen_float64(965)
		dst.MapSfixed64Double[k] = v
	}
	if include[144] {
		k := gen_int64(866)
		v := gen_float64(966)
		dst.MapSfixed64Double[k] = v
	}
	if include[144] {
		k := gen_int64(867)
		v := gen_float64(967)
		dst.MapSfixed64Double[k] = v
	}
	if include[144] {
		k := gen_int64(868)
		v := gen_float64(968)
		dst.MapSfixed64Double[k] = v
	}
	if include[144] {
		k := gen_int64(869)
		v := gen_float64(969)
		dst.MapSfixed64Double[k] = v
	}
	if include[144] {
		k := gen_int64(870)
		v := gen_float64(970)
		dst.MapSfixed64Double[k] = v
	}
	dst.MapSfixed64Float = make(map[int64]float32)
	if include[145] {
		k := gen_int64(871)
		v := gen_float32(971)
		dst.MapSfixed64Float[k] = v
	}
	if include[145] {
		k := gen_int64(872)
		v := gen_float32(972)
		dst.MapSfixed64Float[k] = v
	}
	if include[145] {
		k := gen_int64(873)
		v := gen_float32(973)
		dst.MapSfixed64Float[k] = v
	}
	if include[145] {
		k := gen_int64(874)
		v := gen_float32(974)
		dst.MapSfixed64Float[k] = v
	}
	if include[145] {
		k := gen_int64(875)
		v := gen_float32(975)
		dst.MapSfixed64Float[k] = v
	}
	if include[145] {
		k := gen_int64(876)
		v := gen_float32(976)
		dst.MapSfixed64Float[k] = v
	}
	dst.MapSfixed64Int32 = make(map[int64]int32)
	if include[146] {
		k := gen_int64(877)
		v := gen_int32(977)
		dst.MapSfixed64Int32[k] = v
	}
	if include[146] {
		k := gen_int64(878)
		v := gen_int32(978)
		dst.MapSfixed64Int32[k] = v
	}
	if include[146] {
		k := gen_int64(879)
		v := gen_int32(979)
		dst.MapSfixed64Int32[k] = v
	}
	if include[146] {
		k := gen_int64(880)
		v := gen_int32(980)
		dst.MapSfixed64Int32[k] = v
	}
	if include[146] {
		k := gen_int64(881)
		v := gen_int32(981)
		dst.MapSfixed64Int32[k] = v
	}
	if include[146] {
		k := gen_int64(882)
		v := gen_int32(982)
		dst.MapSfixed64Int32[k] = v
	}
	dst.MapSfixed64Int64 = make(map[int64]int64)
	if include[147] {
		k := gen_int64(883)
		v := gen_int64(983)
		dst.MapSfixed64Int64[k] = v
	}
	if include[147] {
		k := gen_int64(884)
		v := gen_int64(984)
		dst.MapSfixed64Int64[k] = v
	}
	if include[147] {
		k := gen_int64(885)
		v := gen_int64(985)
		dst.MapSfixed64Int64[k] = v
	}
	if include[147] {
		k := gen_int64(886)
		v := gen_int64(986)
		dst.MapSfixed64Int64[k] = v
	}
	if include[147] {
		k := gen_int64(887)
		v := gen_int64(987)
		dst.MapSfixed64Int64[k] = v
	}
	if include[147] {
		k := gen_int64(888)
		v := gen_int64(988)
		dst.MapSfixed64Int64[k] = v
	}
	dst.MapSfixed64Uint32 = make(map[int64]uint32)
	if include[148] {
		k := gen_int64(889)
		v := gen_uint32(989)
		dst.MapSfixed64Uint32[k] = v
	}
	if include[148] {
		k := gen_int64(890)
		v := gen_uint32(990)
		dst.MapSfixed64Uint32[k] = v
	}
	if include[148] {
		k := gen_int64(891)
		v := gen_uint32(991)
		dst.MapSfixed64Uint32[k] = v
	}
	if include[148] {
		k := gen_int64(892)
		v := gen_uint32(992)
		dst.MapSfixed64Uint32[k] = v
	}
	if include[148] {
		k := gen_int64(893)
		v := gen_uint32(993)
		dst.MapSfixed64Uint32[k] = v
	}
	if include[148] {
		k := gen_int64(894)
		v := gen_uint32(994)
		dst.MapSfixed64Uint32[k] = v
	}
	dst.MapSfixed64Uint64 = make(map[int64]uint64)
	if include[149] {
		k := gen_int64(895)
		v := gen_uint64(995)
		dst.MapSfixed64Uint64[k] = v
	}
	if include[149] {
		k := gen_int64(896)
		v := gen_uint64(996)
		dst.MapSfixed64Uint64[k] = v
	}
	if include[149] {
		k := gen_int64(897)
		v := gen_uint64(997)
		dst.MapSfixed64Uint64[k] = v
	}
	if include[149] {
		k := gen_int64(898)
		v := gen_uint64(998)
		dst.MapSfixed64Uint64[k] = v
	}
	if include[149] {
		k := gen_int64(899)
		v := gen_uint64(999)
		dst.MapSfixed64Uint64[k] = v
	}
	if include[149] {
		k := gen_int64(900)
		v := gen_uint64(1000)
		dst.MapSfixed64Uint64[k] = v
	}
	dst.MapSfixed64Sint32 = make(map[int64]int32)
	if include[150] {
		k := gen_int64(901)
		v := gen_int32(1001)
		dst.MapSfixed64Sint32[k] = v
	}
	if include[150] {
		k := gen_int64(902)
		v := gen_int32(1002)
		dst.MapSfixed64Sint32[k] = v
	}
	if include[150] {
		k := gen_int64(903)
		v := gen_int32(1003)
		dst.MapSfixed64Sint32[k] = v
	}
	if include[150] {
		k := gen_int64(904)
		v := gen_int32(1004)
		dst.MapSfixed64Sint32[k] = v
	}
	if include[150] {
		k := gen_int64(905)
		v := gen_int32(1005)
		dst.MapSfixed64Sint32[k] = v
	}
	if include[150] {
		k := gen_int64(906)
		v := gen_int32(1006)
		dst.MapSfixed64Sint32[k] = v
	}
	dst.MapSfixed64Sint64 = make(map[int64]int64)
	if include[151] {
		k := gen_int64(907)
		v := gen_int64(1007)
		dst.MapSfixed64Sint64[k] = v
	}
	if include[151] {
		k := gen_int64(908)
		v := gen_int64(1008)
		dst.MapSfixed64Sint64[k] = v
	}
	if include[151] {
		k := gen_int64(909)
		v := gen_int64(1009)
		dst.MapSfixed64Sint64[k] = v
	}
	if include[151] {
		k := gen_int64(910)
		v := gen_int64(1010)
		dst.MapSfixed64Sint64[k] = v
	}
	if include[151] {
		k := gen_int64(911)
		v := gen_int64(1011)
		dst.MapSfixed64Sint64[k] = v
	}
	if include[151] {
		k := gen_int64(912)
		v := gen_int64(1012)
		dst.MapSfixed64Sint64[k] = v
	}
	dst.MapSfixed64Fixed32 = make(map[int64]uint32)
	if include[152] {
		k := gen_int64(913)
		v := gen_uint32(1013)
		dst.MapSfixed64Fixed32[k] = v
	}
	if include[152] {
		k := gen_int64(914)
		v := gen_uint32(1014)
		dst.MapSfixed64Fixed32[k] = v
	}
	if include[152] {
		k := gen_int64(915)
		v := gen_uint32(1015)
		dst.MapSfixed64Fixed32[k] = v
	}
	if include[152] {
		k := gen_int64(916)
		v := gen_uint32(1016)
		dst.MapSfixed64Fixed32[k] = v
	}
	if include[152] {
		k := gen_int64(917)
		v := gen_uint32(1017)
		dst.MapSfixed64Fixed32[k] = v
	}
	if include[152] {
		k := gen_int64(918)
		v := gen_uint32(1018)
		dst.MapSfixed64Fixed32[k] = v
	}
	dst.MapSfixed64Fixed64 = make(map[int64]uint64)
	if include[153] {
		k := gen_int64(919)
		v := gen_uint64(1019)
		dst.MapSfixed64Fixed64[k] = v
	}
	if include[153] {
		k := gen_int64(920)
		v := gen_uint64(1020)
		dst.MapSfixed64Fixed64[k] = v
	}
	if include[153] {
		k := gen_int64(921)
		v := gen_uint64(1021)
		dst.MapSfixed64Fixed64[k] = v
	}
	if include[153] {
		k := gen_int64(922)
		v := gen_uint64(1022)
		dst.MapSfixed64Fixed64[k] = v
	}
	if include[153] {
		k := gen_int64(923)
		v := gen_uint64(1023)
		dst.MapSfixed64Fixed64[k] = v
	}
	if include[153] {
		k := gen_int64(924)
		v := gen_uint64(1024)
		dst.MapSfixed64Fixed64[k] = v
	}
	dst.MapSfixed64Sfixed32 = make(map[int64]int32)
	if include[154] {
		k := gen_int64(925)
		v := gen_int32(1025)
		dst.MapSfixed64Sfixed32[k] = v
	}
	if include[154] {
		k := gen_int64(926)
		v := gen_int32(1026)
		dst.MapSfixed64Sfixed32[k] = v
	}
	if include[154] {
		k := gen_int64(927)
		v := gen_int32(1027)
		dst.MapSfixed64Sfixed32[k] = v
	}
	if include[154] {
		k := gen_int64(928)
		v := gen_int32(1028)
		dst.MapSfixed64Sfixed32[k] = v
	}
	if include[154] {
		k := gen_int64(929)
		v := gen_int32(1029)
		dst.MapSfixed64Sfixed32[k] = v
	}
	if include[154] {
		k := gen_int64(930)
		v := gen_int32(1030)
		dst.MapSfixed64Sfixed32[k] = v
	}
	dst.MapSfixed64Sfixed64 = make(map[int64]int64)
	if include[155] {
		k := gen_int64(931)
		v := gen_int64(1031)
		dst.MapSfixed64Sfixed64[k] = v
	}
	if include[155] {
		k := gen_int64(932)
		v := gen_int64(1032)
		dst.MapSfixed64Sfixed64[k] = v
	}
	if include[155] {
		k := gen_int64(933)
		v := gen_int64(1033)
		dst.MapSfixed64Sfixed64[k] = v
	}
	if include[155] {
		k := gen_int64(934)
		v := gen_int64(1034)
		dst.MapSfixed64Sfixed64[k] = v
	}
	if include[155] {
		k := gen_int64(935)
		v := gen_int64(1035)
		dst.MapSfixed64Sfixed64[k] = v
	}
	if include[155] {
		k := gen_int64(936)
		v := gen_int64(1036)
		dst.MapSfixed64Sfixed64[k] = v
	}
	dst.MapSfixed64Bool = make(map[int64]bool)
	if include[156] {
		k := gen_int64(937)
		v := gen_bool(1037)
		dst.MapSfixed64Bool[k] = v
	}
	if include[156] {
		k := gen_int64(938)
		v := gen_bool(1038)
		dst.MapSfixed64Bool[k] = v
	}
	if include[156] {
		k := gen_int64(939)
		v := gen_bool(1039)
		dst.MapSfixed64Bool[k] = v
	}
	if include[156] {
		k := gen_int64(940)
		v := gen_bool(1040)
		dst.MapSfixed64Bool[k] = v
	}
	if include[156] {
		k := gen_int64(941)
		v := gen_bool(1041)
		dst.MapSfixed64Bool[k] = v
	}
	if include[156] {
		k := gen_int64(942)
		v := gen_bool(1042)
		dst.MapSfixed64Bool[k] = v
	}
	dst.MapSfixed64String = make(map[int64]string)
	if include[157] {
		k := gen_int64(943)
		v := gen_string(1043)
		dst.MapSfixed64String[k] = v
	}
	if include[157] {
		k := gen_int64(944)
		v := gen_string(1044)
		dst.MapSfixed64String[k] = v
	}
	if include[157] {
		k := gen_int64(945)
		v := gen_string(1045)
		dst.MapSfixed64String[k] = v
	}
	if include[157] {
		k := gen_int64(946)
		v := gen_string(1046)
		dst.MapSfixed64String[k] = v
	}
	if include[157] {
		k := gen_int64(947)
		v := gen_string(1047)
		dst.MapSfixed64String[k] = v
	}
	if include[157] {
		k := gen_int64(948)
		v := gen_string(1048)
		dst.MapSfixed64String[k] = v
	}
	dst.MapSfixed64Bytes = make(map[int64][]byte)
	if include[158] {
		k := gen_int64(949)
		v := gen_bytes(1049)
		dst.MapSfixed64Bytes[k] = v
	}
	if include[158] {
		k := gen_int64(950)
		v := gen_bytes(1050)
		dst.MapSfixed64Bytes[k] = v
	}
	if include[158] {
		k := gen_int64(951)
		v := gen_bytes(1051)
		dst.MapSfixed64Bytes[k] = v
	}
	if include[158] {
		k := gen_int64(952)
		v := gen_bytes(1052)
		dst.MapSfixed64Bytes[k] = v
	}
	if include[158] {
		k := gen_int64(953)
		v := gen_bytes(1053)
		dst.MapSfixed64Bytes[k] = v
	}
	if include[158] {
		k := gen_int64(954)
		v := gen_bytes(1054)
		dst.MapSfixed64Bytes[k] = v
	}
	dst.MapSfixed64_Item = make(map[int64]*Item)
	if include[159] {
		k := gen_int64(955)
		v := gen_Item(1055)
		dst.MapSfixed64_Item[k] = v
	}
	if include[159] {
		k := gen_int64(956)
		v := gen_Item(1056)
		dst.MapSfixed64_Item[k] = v
	}
	if include[159] {
		k := gen_int64(957)
		v := gen_Item(1057)
		dst.MapSfixed64_Item[k] = v
	}
	if include[159] {
		k := gen_int64(958)
		v := gen_Item(1058)
		dst.MapSfixed64_Item[k] = v
	}
	if include[159] {
		k := gen_int64(959)
		v := gen_Item(1059)
		dst.MapSfixed64_Item[k] = v
	}
	if include[159] {
		k := gen_int64(960)
		v := gen_Item(1060)
		dst.MapSfixed64_Item[k] = v
	}
	dst.MapBoolDouble = make(map[bool]float64)
	if include[160] {
		k := gen_bool(961)
		v := gen_float64(1061)
		dst.MapBoolDouble[k] = v
	}
	if include[160] {
		k := gen_bool(962)
		v := gen_float64(1062)
		dst.MapBoolDouble[k] = v
	}
	dst.MapBoolFloat = make(map[bool]float32)
	if include[161] {
		k := gen_bool(963)
		v := gen_float32(1063)
		dst.MapBoolFloat[k] = v
	}
	if include[161] {
		k := gen_bool(964)
		v := gen_float32(1064)
		dst.MapBoolFloat[k] = v
	}
	dst.MapBoolInt32 = make(map[bool]int32)
	if include[162] {
		k := gen_bool(965)
		v := gen_int32(1065)
		dst.MapBoolInt32[k] = v
	}
	if include[162] {
		k := gen_bool(966)
		v := gen_int32(1066)
		dst.MapBoolInt32[k] = v
	}
	dst.MapBoolInt64 = make(map[bool]int64)
	if include[163] {
		k := gen_bool(967)
		v := gen_int64(1067)
		dst.MapBoolInt64[k] = v
	}
	if include[163] {
		k := gen_bool(968)
		v := gen_int64(1068)
		dst.MapBoolInt64[k] = v
	}
	dst.MapBoolUint32 = make(map[bool]uint32)
	if include[164] {
		k := gen_bool(969)
		v := gen_uint32(1069)
		dst.MapBoolUint32[k] = v
	}
	if include[164] {
		k := gen_bool(970)
		v := gen_uint32(1070)
		dst.MapBoolUint32[k] = v
	}
	dst.MapBoolUint64 = make(map[bool]uint64)
	if include[165] {
		k := gen_bool(971)
		v := gen_uint64(1071)
		dst.MapBoolUint64[k] = v
	}
	if include[165] {
		k := gen_bool(972)
		v := gen_uint64(1072)
		dst.MapBoolUint64[k] = v
	}
	dst.MapBoolSint32 = make(map[bool]int32)
	if include[166] {
		k := gen_bool(973)
		v := gen_int32(1073)
		dst.MapBoolSint32[k] = v
	}
	if include[166] {
		k := gen_bool(974)
		v := gen_int32(1074)
		dst.MapBoolSint32[k] = v
	}
	dst.MapBoolSint64 = make(map[bool]int64)
	if include[167] {
		k := gen_bool(975)
		v := gen_int64(1075)
		dst.MapBoolSint64[k] = v
	}
	if include[167] {
		k := gen_bool(976)
		v := gen_int64(1076)
		dst.MapBoolSint64[k] = v
	}
	dst.MapBoolFixed32 = make(map[bool]uint32)
	if include[168] {
		k := gen_bool(977)
		v := gen_uint32(1077)
		dst.MapBoolFixed32[k] = v
	}
	if include[168] {
		k := gen_bool(978)
		v := gen_uint32(1078)
		dst.MapBoolFixed32[k] = v
	}
	dst.MapBoolFixed64 = make(map[bool]uint64)
	if include[169] {
		k := gen_bool(979)
		v := gen_uint64(1079)
		dst.MapBoolFixed64[k] = v
	}
	if include[169] {
		k := gen_bool(980)
		v := gen_uint64(1080)
		dst.MapBoolFixed64[k] = v
	}
	dst.MapBoolSfixed32 = make(map[bool]int32)
	if include[170] {
		k := gen_bool(981)
		v := gen_int32(1081)
		dst.MapBoolSfixed32[k] = v
	}
	if include[170] {
		k := gen_bool(982)
		v := gen_int32(1082)
		dst.MapBoolSfixed32[k] = v
	}
	dst.MapBoolSfixed64 = make(map[bool]int64)
	if include[171] {
		k := gen_bool(983)
		v := gen_int64(1083)
		dst.MapBoolSfixed64[k] = v
	}
	if include[171] {
		k := gen_bool(984)
		v := gen_int64(1084)
		dst.MapBoolSfixed64[k] = v
	}
	dst.MapBoolBool = make(map[bool]bool)
	if include[172] {
		k := gen_bool(985)
		v := gen_bool(1085)
		dst.MapBoolBool[k] = v
	}
	if include[172] {
		k := gen_bool(986)
		v := gen_bool(1086)
		dst.MapBoolBool[k] = v
	}
	dst.MapBoolString = make(map[bool]string)
	if include[173] {
		k := gen_bool(987)
		v := gen_string(1087)
		dst.MapBoolString[k] = v
	}
	if include[173] {
		k := gen_bool(988)
		v := gen_string(1088)
		dst.MapBoolString[k] = v
	}
	dst.MapBoolBytes = make(map[bool][]byte)
	if include[174] {
		k := gen_bool(989)
		v := gen_bytes(1089)
		dst.MapBoolBytes[k] = v
	}
	if include[174] {
		k := gen_bool(990)
		v := gen_bytes(1090)
		dst.MapBoolBytes[k] = v
	}
	dst.MapBool_Item = make(map[bool]*Item)
	if include[175] {
		k := gen_bool(991)
		v := gen_Item(1091)
		dst.MapBool_Item[k] = v
	}
	if include[175] {
		k := gen_bool(992)
		v := gen_Item(1092)
		dst.MapBool_Item[k] = v
	}
	dst.MapStringDouble = make(map[string]float64)
	if include[176] {
		k := gen_string(993)
		v := gen_float64(1093)
		dst.MapStringDouble[k] = v
	}
	if include[176] {
		k := gen_string(994)
		v := gen_float64(1094)
		dst.MapStringDouble[k] = v
	}
	if include[176] {
		k := gen_string(995)
		v := gen_float64(1095)
		dst.MapStringDouble[k] = v
	}
	if include[176] {
		k := gen_string(996)
		v := gen_float64(1096)
		dst.MapStringDouble[k] = v
	}
	if include[176] {
		k := gen_string(997)
		v := gen_float64(1097)
		dst.MapStringDouble[k] = v
	}
	if include[176] {
		k := gen_string(998)
		v := gen_float64(1098)
		dst.MapStringDouble[k] = v
	}
	dst.MapStringFloat = make(map[string]float32)
	if include[177] {
		k := gen_string(999)
		v := gen_float32(1099)
		dst.MapStringFloat[k] = v
	}
	if include[177] {
		k := gen_string(1000)
		v := gen_float32(1100)
		dst.MapStringFloat[k] = v
	}
	if include[177] {
		k := gen_string(1001)
		v := gen_float32(1101)
		dst.MapStringFloat[k] = v
	}
	if include[177] {
		k := gen_string(1002)
		v := gen_float32(1102)
		dst.MapStringFloat[k] = v
	}
	if include[177] {
		k := gen_string(1003)
		v := gen_float32(1103)
		dst.MapStringFloat[k] = v
	}
	if include[177] {
		k := gen_string(1004)
		v := gen_float32(1104)
		dst.MapStringFloat[k] = v
	}
	dst.MapStringInt32 = make(map[string]int32)
	if include[178] {
		k := gen_string(1005)
		v := gen_int32(1105)
		dst.MapStringInt32[k] = v
	}
	if include[178] {
		k := gen_string(1006)
		v := gen_int32(1106)
		dst.MapStringInt32[k] = v
	}
	if include[178] {
		k := gen_string(1007)
		v := gen_int32(1107)
		dst.MapStringInt32[k] = v
	}
	if include[178] {
		k := gen_string(1008)
		v := gen_int32(1108)
		dst.MapStringInt32[k] = v
	}
	if include[178] {
		k := gen_string(1009)
		v := gen_int32(1109)
		dst.MapStringInt32[k] = v
	}
	if include[178] {
		k := gen_string(1010)
		v := gen_int32(1110)
		dst.MapStringInt32[k] = v
	}
	dst.MapStringInt64 = make(map[string]int64)
	if include[179] {
		k := gen_string(1011)
		v := gen_int64(1111)
		dst.MapStringInt64[k] = v
	}
	if include[179] {
		k := gen_string(1012)
		v := gen_int64(1112)
		dst.MapStringInt64[k] = v
	}
	if include[179] {
		k := gen_string(1013)
		v := gen_int64(1113)
		dst.MapStringInt64[k] = v
	}
	if include[179] {
		k := gen_string(1014)
		v := gen_int64(1114)
		dst.MapStringInt64[k] = v
	}
	if include[179] {
		k := gen_string(1015)
		v := gen_int64(1115)
		dst.MapStringInt64[k] = v
	}
	if include[179] {
		k := gen_string(1016)
		v := gen_int64(1116)
		dst.MapStringInt64[k] = v
	}
	dst.MapStringUint32 = make(map[string]uint32)
	if include[180] {
		k := gen_string(1017)
		v := gen_uint32(1117)
		dst.MapStringUint32[k] = v
	}
	if include[180] {
		k := gen_string(1018)
		v := gen_uint32(1118)
		dst.MapStringUint32[k] = v
	}
	if include[180] {
		k := gen_string(1019)
		v := gen_uint32(1119)
		dst.MapStringUint32[k] = v
	}
	if include[180] {
		k := gen_string(1020)
		v := gen_uint32(1120)
		dst.MapStringUint32[k] = v
	}
	if include[180] {
		k := gen_string(1021)
		v := gen_uint32(1121)
		dst.MapStringUint32[k] = v
	}
	if include[180] {
		k := gen_string(1022)
		v := gen_uint32(1122)
		dst.MapStringUint32[k] = v
	}
	dst.MapStringUint64 = make(map[string]uint64)
	if include[181] {
		k := gen_string(1023)
		v := gen_uint64(1123)
		dst.MapStringUint64[k] = v
	}
	if include[181] {
		k := gen_string(1024)
		v := gen_uint64(1124)
		dst.MapStringUint64[k] = v
	}
	if include[181] {
		k := gen_string(1025)
		v := gen_uint64(1125)
		dst.MapStringUint64[k] = v
	}
	if include[181] {
		k := gen_string(1026)
		v := gen_uint64(1126)
		dst.MapStringUint64[k] = v
	}
	if include[181] {
		k := gen_string(1027)
		v := gen_uint64(1127)
		dst.MapStringUint64[k] = v
	}
	if include[181] {
		k := gen_string(1028)
		v := gen_uint64(1128)
		dst.MapStringUint64[k] = v
	}
	dst.MapStringSint32 = make(map[string]int32)
	if include[182] {
		k := gen_string(1029)
		v := gen_int32(1129)
		dst.MapStringSint32[k] = v
	}
	if include[182] {
		k := gen_string(1030)
		v := gen_int32(1130)
		dst.MapStringSint32[k] = v
	}
	if include[182] {
		k := gen_string(1031)
		v := gen_int32(1131)
		dst.MapStringSint32[k] = v
	}
	if include[182] {
		k := gen_string(1032)
		v := gen_int32(1132)
		dst.MapStringSint32[k] = v
	}
	if include[182] {
		k := gen_string(1033)
		v := gen_int32(1133)
		dst.MapStringSint32[k] = v
	}
	if include[182] {
		k := gen_string(1034)
		v := gen_int32(1134)
		dst.MapStringSint32[k] = v
	}
	dst.MapStringSint64 = make(map[string]int64)
	if include[183] {
		k := gen_string(1035)
		v := gen_int64(1135)
		dst.MapStringSint64[k] = v
	}
	if include[183] {
		k := gen_string(1036)
		v := gen_int64(1136)
		dst.MapStringSint64[k] = v
	}
	if include[183] {
		k := gen_string(1037)
		v := gen_int64(1137)
		dst.MapStringSint64[k] = v
	}
	if include[183] {
		k := gen_string(1038)
		v := gen_int64(1138)
		dst.MapStringSint64[k] = v
	}
	if include[183] {
		k := gen_string(1039)
		v := gen_int64(1139)
		dst.MapStringSint64[k] = v
	}
	if include[183] {
		k := gen_string(1040)
		v := gen_int64(1140)
		dst.MapStringSint64[k] = v
	}
	dst.MapStringFixed32 = make(map[string]uint32)
	if include[184] {
		k := gen_string(1041)
		v := gen_uint32(1141)
		dst.MapStringFixed32[k] = v
	}
	if include[184] {
		k := gen_string(1042)
		v := gen_uint32(1142)
		dst.MapStringFixed32[k] = v
	}
	if include[184] {
		k := gen_string(1043)
		v := gen_uint32(1143)
		dst.MapStringFixed32[k] = v
	}
	if include[184] {
		k := gen_string(1044)
		v := gen_uint32(1144)
		dst.MapStringFixed32[k] = v
	}
	if include[184] {
		k := gen_string(1045)
		v := gen_uint32(1145)
		dst.MapStringFixed32[k] = v
	}
	if include[184] {
		k := gen_string(1046)
		v := gen_uint32(1146)
		dst.MapStringFixed32[k] = v
	}
	dst.MapStringFixed64 = make(map[string]uint64)
	if include[185] {
		k := gen_string(1047)
		v := gen_uint64(1147)
		dst.MapStringFixed64[k] = v
	}
	if include[185] {
		k := gen_string(1048)
		v := gen_uint64(1148)
		dst.MapStringFixed64[k] = v
	}
	if include[185] {
		k := gen_string(1049)
		v := gen_uint64(1149)
		dst.MapStringFixed64[k] = v
	}
	if include[185] {
		k := gen_string(1050)
		v := gen_uint64(1150)
		dst.MapStringFixed64[k] = v
	}
	if include[185] {
		k := gen_string(1051)
		v := gen_uint64(1151)
		dst.MapStringFixed64[k] = v
	}
	if include[185] {
		k := gen_string(1052)
		v := gen_uint64(1152)
		dst.MapStringFixed64[k] = v
	}
	dst.MapStringSfixed32 = make(map[string]int32)
	if include[186] {
		k := gen_string(1053)
		v := gen_int32(1153)
		dst.MapStringSfixed32[k] = v
	}
	if include[186] {
		k := gen_string(1054)
		v := gen_int32(1154)
		dst.MapStringSfixed32[k] = v
	}
	if include[186] {
		k := gen_string(1055)
		v := gen_int32(1155)
		dst.MapStringSfixed32[k] = v
	}
	if include[186] {
		k := gen_string(1056)
		v := gen_int32(1156)
		dst.MapStringSfixed32[k] = v
	}
	if include[186] {
		k := gen_string(1057)
		v := gen_int32(1157)
		dst.MapStringSfixed32[k] = v
	}
	if include[186] {
		k := gen_string(1058)
		v := gen_int32(1158)
		dst.MapStringSfixed32[k] = v
	}
	dst.MapStringSfixed64 = make(map[string]int64)
	if include[187] {
		k := gen_string(1059)
		v := gen_int64(1159)
		dst.MapStringSfixed64[k] = v
	}
	if include[187] {
		k := gen_string(1060)
		v := gen_int64(1160)
		dst.MapStringSfixed64[k] = v
	}
	if include[187] {
		k := gen_string(1061)
		v := gen_int64(1161)
		dst.MapStringSfixed64[k] = v
	}
	if include[187] {
		k := gen_string(1062)
		v := gen_int64(1162)
		dst.MapStringSfixed64[k] = v
	}
	if include[187] {
		k := gen_string(1063)
		v := gen_int64(1163)
		dst.MapStringSfixed64[k] = v
	}
	if include[187] {
		k := gen_string(1064)
		v := gen_int64(1164)
		dst.MapStringSfixed64[k] = v
	}
	dst.MapStringBool = make(map[string]bool)
	if include[188] {
		k := gen_string(1065)
		v := gen_bool(1165)
		dst.MapStringBool[k] = v
	}
	if include[188] {
		k := gen_string(1066)
		v := gen_bool(1166)
		dst.MapStringBool[k] = v
	}
	if include[188] {
		k := gen_string(1067)
		v := gen_bool(1167)
		dst.MapStringBool[k] = v
	}
	if include[188] {
		k := gen_string(1068)
		v := gen_bool(1168)
		dst.MapStringBool[k] = v
	}
	if include[188] {
		k := gen_string(1069)
		v := gen_bool(1169)
		dst.MapStringBool[k] = v
	}
	if include[188] {
		k := gen_string(1070)
		v := gen_bool(1170)
		dst.MapStringBool[k] = v
	}
	dst.MapStringString = make(map[string]string)
	if include[189] {
		k := gen_string(1071)
		v := gen_string(1171)
		dst.MapStringString[k] = v
	}
	if include[189] {
		k := gen_string(1072)
		v := gen_string(1172)
		dst.MapStringString[k] = v
	}
	if include[189] {
		k := gen_string(1073)
		v := gen_string(1173)
		dst.MapStringString[k] = v
	}
	if include[189] {
		k := gen_string(1074)
		v := gen_string(1174)
		dst.MapStringString[k] = v
	}
	if include[189] {
		k := gen_string(1075)
		v := gen_string(1175)
		dst.MapStringString[k] = v
	}
	if include[189] {
		k := gen_string(1076)
		v := gen_string(1176)
		dst.MapStringString[k] = v
	}
	dst.MapStringBytes = make(map[string][]byte)
	if include[190] {
		k := gen_string(1077)
		v := gen_bytes(1177)
		dst.MapStringBytes[k] = v
	}
	if include[190] {
		k := gen_string(1078)
		v := gen_bytes(1178)
		dst.MapStringBytes[k] = v
	}
	if include[190] {
		k := gen_string(1079)
		v := gen_bytes(1179)
		dst.MapStringBytes[k] = v
	}
	if include[190] {
		k := gen_string(1080)
		v := gen_bytes(1180)
		dst.MapStringBytes[k] = v
	}
	if include[190] {
		k := gen_string(1081)
		v := gen_bytes(1181)
		dst.MapStringBytes[k] = v
	}
	if include[190] {
		k := gen_string(1082)
		v := gen_bytes(1182)
		dst.MapStringBytes[k] = v
	}
	dst.MapString_Item = make(map[string]*Item)
	if include[191] {
		k := gen_string(1083)
		v := gen_Item(1183)
		dst.MapString_Item[k] = v
	}
	if include[191] {
		k := gen_string(1084)
		v := gen_Item(1184)
		dst.MapString_Item[k] = v
	}
	if include[191] {
		k := gen_string(1085)
		v := gen_Item(1185)
		dst.MapString_Item[k] = v
	}
	if include[191] {
		k := gen_string(1086)
		v := gen_Item(1186)
		dst.MapString_Item[k] = v
	}
	if include[191] {
		k := gen_string(1087)
		v := gen_Item(1187)
		dst.MapString_Item[k] = v
	}
	if include[191] {
		k := gen_string(1088)
		v := gen_Item(1188)
		dst.MapString_Item[k] = v
	}
}

var hotnames = []string{
	"Map_Int32_Double",      // hot_0
	"Map_Int32_Float",       // hot_1
	"Map_Int32_Int32",       // hot_2
	"Map_Int32_Int64",       // hot_3
	"Map_Int32_Uint32",      // hot_4
	"Map_Int32_Uint64",      // hot_5
	"Map_Int32_Sint32",      // hot_6
	"Map_Int32_Sint64",      // hot_7
	"Map_Int32_Fixed32",     // hot_8
	"Map_Int32_Fixed64",     // hot_9
	"Map_Int32_Sfixed32",    // hot_10
	"Map_Int32_Sfixed64",    // hot_11
	"Map_Int32_Bool",        // hot_12
	"Map_Int32_String",      // hot_13
	"Map_Int32_Bytes",       // hot_14
	"Map_Int32__Item",       // hot_15
	"Map_Int64_Double",      // hot_16
	"Map_Int64_Float",       // hot_17
	"Map_Int64_Int32",       // hot_18
	"Map_Int64_Int64",       // hot_19
	"Map_Int64_Uint32",      // hot_20
	"Map_Int64_Uint64",      // hot_21
	"Map_Int64_Sint32",      // hot_22
	"Map_Int64_Sint64",      // hot_23
	"Map_Int64_Fixed32",     // hot_24
	"Map_Int64_Fixed64",     // hot_25
	"Map_Int64_Sfixed32",    // hot_26
	"Map_Int64_Sfixed64",    // hot_27
	"Map_Int64_Bool",        // hot_28
	"Map_Int64_String",      // hot_29
	"Map_Int64_Bytes",       // hot_30
	"Map_Int64__Item",       // hot_31
	"Map_Uint32_Double",     // hot_32
	"Map_Uint32_Float",      // hot_33
	"Map_Uint32_Int32",      // hot_34
	"Map_Uint32_Int64",      // hot_35
	"Map_Uint32_Uint32",     // hot_36
	"Map_Uint32_Uint64",     // hot_37
	"Map_Uint32_Sint32",     // hot_38
	"Map_Uint32_Sint64",     // hot_39
	"Map_Uint32_Fixed32",    // hot_40
	"Map_Uint32_Fixed64",    // hot_41
	"Map_Uint32_Sfixed32",   // hot_42
	"Map_Uint32_Sfixed64",   // hot_43
	"Map_Uint32_Bool",       // hot_44
	"Map_Uint32_String",     // hot_45
	"Map_Uint32_Bytes",      // hot_46
	"Map_Uint32__Item",      // hot_47
	"Map_Uint64_Double",     // hot_48
	"Map_Uint64_Float",      // hot_49
	"Map_Uint64_Int32",      // hot_50
	"Map_Uint64_Int64",      // hot_51
	"Map_Uint64_Uint32",     // hot_52
	"Map_Uint64_Uint64",     // hot_53
	"Map_Uint64_Sint32",     // hot_54
	"Map_Uint64_Sint64",     // hot_55
	"Map_Uint64_Fixed32",    // hot_56
	"Map_Uint64_Fixed64",    // hot_57
	"Map_Uint64_Sfixed32",   // hot_58
	"Map_Uint64_Sfixed64",   // hot_59
	"Map_Uint64_Bool",       // hot_60
	"Map_Uint64_String",     // hot_61
	"Map_Uint64_Bytes",      // hot_62
	"Map_Uint64__Item",      // hot_63
	"Map_Sint32_Double",     // hot_64
	"Map_Sint32_Float",      // hot_65
	"Map_Sint32_Int32",      // hot_66
	"Map_Sint32_Int64",      // hot_67
	"Map_Sint32_Uint32",     // hot_68
	"Map_Sint32_Uint64",     // hot_69
	"Map_Sint32_Sint32",     // hot_70
	"Map_Sint32_Sint64",     // hot_71
	"Map_Sint32_Fixed32",    // hot_72
	"Map_Sint32_Fixed64",    // hot_73
	"Map_Sint32_Sfixed32",   // hot_74
	"Map_Sint32_Sfixed64",   // hot_75
	"Map_Sint32_Bool",       // hot_76
	"Map_Sint32_String",     // hot_77
	"Map_Sint32_Bytes",      // hot_78
	"Map_Sint32__Item",      // hot_79
	"Map_Sint64_Double",     // hot_80
	"Map_Sint64_Float",      // hot_81
	"Map_Sint64_Int32",      // hot_82
	"Map_Sint64_Int64",      // hot_83
	"Map_Sint64_Uint32",     // hot_84
	"Map_Sint64_Uint64",     // hot_85
	"Map_Sint64_Sint32",     // hot_86
	"Map_Sint64_Sint64",     // hot_87
	"Map_Sint64_Fixed32",    // hot_88
	"Map_Sint64_Fixed64",    // hot_89
	"Map_Sint64_Sfixed32",   // hot_90
	"Map_Sint64_Sfixed64",   // hot_91
	"Map_Sint64_Bool",       // hot_92
	"Map_Sint64_String",     // hot_93
	"Map_Sint64_Bytes",      // hot_94
	"Map_Sint64__Item",      // hot_95
	"Map_Fixed32_Double",    // hot_96
	"Map_Fixed32_Float",     // hot_97
	"Map_Fixed32_Int32",     // hot_98
	"Map_Fixed32_Int64",     // hot_99
	"Map_Fixed32_Uint32",    // hot_100
	"Map_Fixed32_Uint64",    // hot_101
	"Map_Fixed32_Sint32",    // hot_102
	"Map_Fixed32_Sint64",    // hot_103
	"Map_Fixed32_Fixed32",   // hot_104
	"Map_Fixed32_Fixed64",   // hot_105
	"Map_Fixed32_Sfixed32",  // hot_106
	"Map_Fixed32_Sfixed64",  // hot_107
	"Map_Fixed32_Bool",      // hot_108
	"Map_Fixed32_String",    // hot_109
	"Map_Fixed32_Bytes",     // hot_110
	"Map_Fixed32__Item",     // hot_111
	"Map_Fixed64_Double",    // hot_112
	"Map_Fixed64_Float",     // hot_113
	"Map_Fixed64_Int32",     // hot_114
	"Map_Fixed64_Int64",     // hot_115
	"Map_Fixed64_Uint32",    // hot_116
	"Map_Fixed64_Uint64",    // hot_117
	"Map_Fixed64_Sint32",    // hot_118
	"Map_Fixed64_Sint64",    // hot_119
	"Map_Fixed64_Fixed32",   // hot_120
	"Map_Fixed64_Fixed64",   // hot_121
	"Map_Fixed64_Sfixed32",  // hot_122
	"Map_Fixed64_Sfixed64",  // hot_123
	"Map_Fixed64_Bool",      // hot_124
	"Map_Fixed64_String",    // hot_125
	"Map_Fixed64_Bytes",     // hot_126
	"Map_Fixed64__Item",     // hot_127
	"Map_Sfixed32_Double",   // hot_128
	"Map_Sfixed32_Float",    // hot_129
	"Map_Sfixed32_Int32",    // hot_130
	"Map_Sfixed32_Int64",    // hot_131
	"Map_Sfixed32_Uint32",   // hot_132
	"Map_Sfixed32_Uint64",   // hot_133
	"Map_Sfixed32_Sint32",   // hot_134
	"Map_Sfixed32_Sint64",   // hot_135
	"Map_Sfixed32_Fixed32",  // hot_136
	"Map_Sfixed32_Fixed64",  // hot_137
	"Map_Sfixed32_Sfixed32", // hot_138
	"Map_Sfixed32_Sfixed64", // hot_139
	"Map_Sfixed32_Bool",     // hot_140
	"Map_Sfixed32_String",   // hot_141
	"Map_Sfixed32_Bytes",    // hot_142
	"Map_Sfixed32__Item",    // hot_143
	"Map_Sfixed64_Double",   // hot_144
	"Map_Sfixed64_Float",    // hot_145
	"Map_Sfixed64_Int32",    // hot_146
	"Map_Sfixed64_Int64",    // hot_147
	"Map_Sfixed64_Uint32",   // hot_148
	"Map_Sfixed64_Uint64",   // hot_149
	"Map_Sfixed64_Sint32",   // hot_150
	"Map_Sfixed64_Sint64",   // hot_151
	"Map_Sfixed64_Fixed32",  // hot_152
	"Map_Sfixed64_Fixed64",  // hot_153
	"Map_Sfixed64_Sfixed32", // hot_154
	"Map_Sfixed64_Sfixed64", // hot_155
	"Map_Sfixed64_Bool",     // hot_156
	"Map_Sfixed64_String",   // hot_157
	"Map_Sfixed64_Bytes",    // hot_158
	"Map_Sfixed64__Item",    // hot_159
	"Map_Bool_Double",       // hot_160
	"Map_Bool_Float",        // hot_161
	"Map_Bool_Int32",        // hot_162
	"Map_Bool_Int64",        // hot_163
	"Map_Bool_Uint32",       // hot_164
	"Map_Bool_Uint64",       // hot_165
	"Map_Bool_Sint32",       // hot_166
	"Map_Bool_Sint64",       // hot_167
	"Map_Bool_Fixed32",      // hot_168
	"Map_Bool_Fixed64",      // hot_169
	"Map_Bool_Sfixed32",     // hot_170
	"Map_Bool_Sfixed64",     // hot_171
	"Map_Bool_Bool",         // hot_172
	"Map_Bool_String",       // hot_173
	"Map_Bool_Bytes",        // hot_174
	"Map_Bool__Item",        // hot_175
	"Map_String_Double",     // hot_176
	"Map_String_Float",      // hot_177
	"Map_String_Int32",      // hot_178
	"Map_String_Int64",      // hot_179
	"Map_String_Uint32",     // hot_180
	"Map_String_Uint64",     // hot_181
	"Map_String_Sint32",     // hot_182
	"Map_String_Sint64",     // hot_183
	"Map_String_Fixed32",    // hot_184
	"Map_String_Fixed64",    // hot_185
	"Map_String_Sfixed32",   // hot_186
	"Map_String_Sfixed64",   // hot_187
	"Map_String_Bool",       // hot_188
	"Map_String_String",     // hot_189
	"Map_String_Bytes",      // hot_190
	"Map_String__Item",      // hot_191
}

func checkMaps(t *testing.T, src *Maps, include []bool) {
	if include[0] {
		k := gen_int32(1)
		vx := gen_float64(101)
		v, ok := src.MapInt32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[0] {
		k := gen_int32(2)
		vx := gen_float64(102)
		v, ok := src.MapInt32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[0] {
		k := gen_int32(3)
		vx := gen_float64(103)
		v, ok := src.MapInt32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[0] {
		k := gen_int32(4)
		vx := gen_float64(104)
		v, ok := src.MapInt32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[0] {
		k := gen_int32(5)
		vx := gen_float64(105)
		v, ok := src.MapInt32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[0] {
		k := gen_int32(6)
		vx := gen_float64(106)
		v, ok := src.MapInt32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[1] {
		k := gen_int32(7)
		vx := gen_float32(107)
		v, ok := src.MapInt32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[1] {
		k := gen_int32(8)
		vx := gen_float32(108)
		v, ok := src.MapInt32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[1] {
		k := gen_int32(9)
		vx := gen_float32(109)
		v, ok := src.MapInt32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[1] {
		k := gen_int32(10)
		vx := gen_float32(110)
		v, ok := src.MapInt32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[1] {
		k := gen_int32(11)
		vx := gen_float32(111)
		v, ok := src.MapInt32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[1] {
		k := gen_int32(12)
		vx := gen_float32(112)
		v, ok := src.MapInt32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[2] {
		k := gen_int32(13)
		vx := gen_int32(113)
		v, ok := src.MapInt32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[2] {
		k := gen_int32(14)
		vx := gen_int32(114)
		v, ok := src.MapInt32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[2] {
		k := gen_int32(15)
		vx := gen_int32(115)
		v, ok := src.MapInt32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[2] {
		k := gen_int32(16)
		vx := gen_int32(116)
		v, ok := src.MapInt32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[2] {
		k := gen_int32(17)
		vx := gen_int32(117)
		v, ok := src.MapInt32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[2] {
		k := gen_int32(18)
		vx := gen_int32(118)
		v, ok := src.MapInt32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[3] {
		k := gen_int32(19)
		vx := gen_int64(119)
		v, ok := src.MapInt32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[3] {
		k := gen_int32(20)
		vx := gen_int64(120)
		v, ok := src.MapInt32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[3] {
		k := gen_int32(21)
		vx := gen_int64(121)
		v, ok := src.MapInt32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[3] {
		k := gen_int32(22)
		vx := gen_int64(122)
		v, ok := src.MapInt32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[3] {
		k := gen_int32(23)
		vx := gen_int64(123)
		v, ok := src.MapInt32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[3] {
		k := gen_int32(24)
		vx := gen_int64(124)
		v, ok := src.MapInt32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[4] {
		k := gen_int32(25)
		vx := gen_uint32(125)
		v, ok := src.MapInt32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[4] {
		k := gen_int32(26)
		vx := gen_uint32(126)
		v, ok := src.MapInt32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[4] {
		k := gen_int32(27)
		vx := gen_uint32(127)
		v, ok := src.MapInt32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[4] {
		k := gen_int32(28)
		vx := gen_uint32(128)
		v, ok := src.MapInt32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[4] {
		k := gen_int32(29)
		vx := gen_uint32(129)
		v, ok := src.MapInt32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[4] {
		k := gen_int32(30)
		vx := gen_uint32(130)
		v, ok := src.MapInt32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[5] {
		k := gen_int32(31)
		vx := gen_uint64(131)
		v, ok := src.MapInt32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[5] {
		k := gen_int32(32)
		vx := gen_uint64(132)
		v, ok := src.MapInt32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[5] {
		k := gen_int32(33)
		vx := gen_uint64(133)
		v, ok := src.MapInt32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[5] {
		k := gen_int32(34)
		vx := gen_uint64(134)
		v, ok := src.MapInt32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[5] {
		k := gen_int32(35)
		vx := gen_uint64(135)
		v, ok := src.MapInt32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[5] {
		k := gen_int32(36)
		vx := gen_uint64(136)
		v, ok := src.MapInt32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[6] {
		k := gen_int32(37)
		vx := gen_int32(137)
		v, ok := src.MapInt32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[6] {
		k := gen_int32(38)
		vx := gen_int32(138)
		v, ok := src.MapInt32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[6] {
		k := gen_int32(39)
		vx := gen_int32(139)
		v, ok := src.MapInt32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[6] {
		k := gen_int32(40)
		vx := gen_int32(140)
		v, ok := src.MapInt32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[6] {
		k := gen_int32(41)
		vx := gen_int32(141)
		v, ok := src.MapInt32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[6] {
		k := gen_int32(42)
		vx := gen_int32(142)
		v, ok := src.MapInt32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[7] {
		k := gen_int32(43)
		vx := gen_int64(143)
		v, ok := src.MapInt32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[7] {
		k := gen_int32(44)
		vx := gen_int64(144)
		v, ok := src.MapInt32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[7] {
		k := gen_int32(45)
		vx := gen_int64(145)
		v, ok := src.MapInt32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[7] {
		k := gen_int32(46)
		vx := gen_int64(146)
		v, ok := src.MapInt32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[7] {
		k := gen_int32(47)
		vx := gen_int64(147)
		v, ok := src.MapInt32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[7] {
		k := gen_int32(48)
		vx := gen_int64(148)
		v, ok := src.MapInt32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[8] {
		k := gen_int32(49)
		vx := gen_uint32(149)
		v, ok := src.MapInt32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[8] {
		k := gen_int32(50)
		vx := gen_uint32(150)
		v, ok := src.MapInt32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[8] {
		k := gen_int32(51)
		vx := gen_uint32(151)
		v, ok := src.MapInt32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[8] {
		k := gen_int32(52)
		vx := gen_uint32(152)
		v, ok := src.MapInt32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[8] {
		k := gen_int32(53)
		vx := gen_uint32(153)
		v, ok := src.MapInt32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[8] {
		k := gen_int32(54)
		vx := gen_uint32(154)
		v, ok := src.MapInt32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[9] {
		k := gen_int32(55)
		vx := gen_uint64(155)
		v, ok := src.MapInt32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[9] {
		k := gen_int32(56)
		vx := gen_uint64(156)
		v, ok := src.MapInt32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[9] {
		k := gen_int32(57)
		vx := gen_uint64(157)
		v, ok := src.MapInt32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[9] {
		k := gen_int32(58)
		vx := gen_uint64(158)
		v, ok := src.MapInt32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[9] {
		k := gen_int32(59)
		vx := gen_uint64(159)
		v, ok := src.MapInt32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[9] {
		k := gen_int32(60)
		vx := gen_uint64(160)
		v, ok := src.MapInt32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[10] {
		k := gen_int32(61)
		vx := gen_int32(161)
		v, ok := src.MapInt32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[10] {
		k := gen_int32(62)
		vx := gen_int32(162)
		v, ok := src.MapInt32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[10] {
		k := gen_int32(63)
		vx := gen_int32(163)
		v, ok := src.MapInt32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[10] {
		k := gen_int32(64)
		vx := gen_int32(164)
		v, ok := src.MapInt32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[10] {
		k := gen_int32(65)
		vx := gen_int32(165)
		v, ok := src.MapInt32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[10] {
		k := gen_int32(66)
		vx := gen_int32(166)
		v, ok := src.MapInt32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[11] {
		k := gen_int32(67)
		vx := gen_int64(167)
		v, ok := src.MapInt32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[11] {
		k := gen_int32(68)
		vx := gen_int64(168)
		v, ok := src.MapInt32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[11] {
		k := gen_int32(69)
		vx := gen_int64(169)
		v, ok := src.MapInt32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[11] {
		k := gen_int32(70)
		vx := gen_int64(170)
		v, ok := src.MapInt32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[11] {
		k := gen_int32(71)
		vx := gen_int64(171)
		v, ok := src.MapInt32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[11] {
		k := gen_int32(72)
		vx := gen_int64(172)
		v, ok := src.MapInt32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[12] {
		k := gen_int32(73)
		vx := gen_bool(173)
		v, ok := src.MapInt32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[12] {
		k := gen_int32(74)
		vx := gen_bool(174)
		v, ok := src.MapInt32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[12] {
		k := gen_int32(75)
		vx := gen_bool(175)
		v, ok := src.MapInt32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[12] {
		k := gen_int32(76)
		vx := gen_bool(176)
		v, ok := src.MapInt32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[12] {
		k := gen_int32(77)
		vx := gen_bool(177)
		v, ok := src.MapInt32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[12] {
		k := gen_int32(78)
		vx := gen_bool(178)
		v, ok := src.MapInt32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[13] {
		k := gen_int32(79)
		vx := gen_string(179)
		v, ok := src.MapInt32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[13] {
		k := gen_int32(80)
		vx := gen_string(180)
		v, ok := src.MapInt32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[13] {
		k := gen_int32(81)
		vx := gen_string(181)
		v, ok := src.MapInt32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[13] {
		k := gen_int32(82)
		vx := gen_string(182)
		v, ok := src.MapInt32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[13] {
		k := gen_int32(83)
		vx := gen_string(183)
		v, ok := src.MapInt32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[13] {
		k := gen_int32(84)
		vx := gen_string(184)
		v, ok := src.MapInt32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[14] {
		k := gen_int32(85)
		vx := gen_bytes(185)
		v, ok := src.MapInt32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[14] {
		k := gen_int32(86)
		vx := gen_bytes(186)
		v, ok := src.MapInt32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[14] {
		k := gen_int32(87)
		vx := gen_bytes(187)
		v, ok := src.MapInt32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[14] {
		k := gen_int32(88)
		vx := gen_bytes(188)
		v, ok := src.MapInt32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[14] {
		k := gen_int32(89)
		vx := gen_bytes(189)
		v, ok := src.MapInt32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[14] {
		k := gen_int32(90)
		vx := gen_bytes(190)
		v, ok := src.MapInt32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[15] {
		k := gen_int32(91)
		vx := gen_Item(191)
		v, ok := src.MapInt32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[15] {
		k := gen_int32(92)
		vx := gen_Item(192)
		v, ok := src.MapInt32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[15] {
		k := gen_int32(93)
		vx := gen_Item(193)
		v, ok := src.MapInt32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[15] {
		k := gen_int32(94)
		vx := gen_Item(194)
		v, ok := src.MapInt32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[15] {
		k := gen_int32(95)
		vx := gen_Item(195)
		v, ok := src.MapInt32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[15] {
		k := gen_int32(96)
		vx := gen_Item(196)
		v, ok := src.MapInt32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[16] {
		k := gen_int64(97)
		vx := gen_float64(197)
		v, ok := src.MapInt64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[16] {
		k := gen_int64(98)
		vx := gen_float64(198)
		v, ok := src.MapInt64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[16] {
		k := gen_int64(99)
		vx := gen_float64(199)
		v, ok := src.MapInt64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[16] {
		k := gen_int64(100)
		vx := gen_float64(200)
		v, ok := src.MapInt64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[16] {
		k := gen_int64(101)
		vx := gen_float64(201)
		v, ok := src.MapInt64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[16] {
		k := gen_int64(102)
		vx := gen_float64(202)
		v, ok := src.MapInt64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[17] {
		k := gen_int64(103)
		vx := gen_float32(203)
		v, ok := src.MapInt64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[17] {
		k := gen_int64(104)
		vx := gen_float32(204)
		v, ok := src.MapInt64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[17] {
		k := gen_int64(105)
		vx := gen_float32(205)
		v, ok := src.MapInt64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[17] {
		k := gen_int64(106)
		vx := gen_float32(206)
		v, ok := src.MapInt64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[17] {
		k := gen_int64(107)
		vx := gen_float32(207)
		v, ok := src.MapInt64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[17] {
		k := gen_int64(108)
		vx := gen_float32(208)
		v, ok := src.MapInt64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[18] {
		k := gen_int64(109)
		vx := gen_int32(209)
		v, ok := src.MapInt64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[18] {
		k := gen_int64(110)
		vx := gen_int32(210)
		v, ok := src.MapInt64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[18] {
		k := gen_int64(111)
		vx := gen_int32(211)
		v, ok := src.MapInt64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[18] {
		k := gen_int64(112)
		vx := gen_int32(212)
		v, ok := src.MapInt64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[18] {
		k := gen_int64(113)
		vx := gen_int32(213)
		v, ok := src.MapInt64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[18] {
		k := gen_int64(114)
		vx := gen_int32(214)
		v, ok := src.MapInt64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[19] {
		k := gen_int64(115)
		vx := gen_int64(215)
		v, ok := src.MapInt64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[19] {
		k := gen_int64(116)
		vx := gen_int64(216)
		v, ok := src.MapInt64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[19] {
		k := gen_int64(117)
		vx := gen_int64(217)
		v, ok := src.MapInt64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[19] {
		k := gen_int64(118)
		vx := gen_int64(218)
		v, ok := src.MapInt64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[19] {
		k := gen_int64(119)
		vx := gen_int64(219)
		v, ok := src.MapInt64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[19] {
		k := gen_int64(120)
		vx := gen_int64(220)
		v, ok := src.MapInt64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[20] {
		k := gen_int64(121)
		vx := gen_uint32(221)
		v, ok := src.MapInt64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[20] {
		k := gen_int64(122)
		vx := gen_uint32(222)
		v, ok := src.MapInt64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[20] {
		k := gen_int64(123)
		vx := gen_uint32(223)
		v, ok := src.MapInt64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[20] {
		k := gen_int64(124)
		vx := gen_uint32(224)
		v, ok := src.MapInt64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[20] {
		k := gen_int64(125)
		vx := gen_uint32(225)
		v, ok := src.MapInt64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[20] {
		k := gen_int64(126)
		vx := gen_uint32(226)
		v, ok := src.MapInt64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[21] {
		k := gen_int64(127)
		vx := gen_uint64(227)
		v, ok := src.MapInt64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[21] {
		k := gen_int64(128)
		vx := gen_uint64(228)
		v, ok := src.MapInt64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[21] {
		k := gen_int64(129)
		vx := gen_uint64(229)
		v, ok := src.MapInt64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[21] {
		k := gen_int64(130)
		vx := gen_uint64(230)
		v, ok := src.MapInt64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[21] {
		k := gen_int64(131)
		vx := gen_uint64(231)
		v, ok := src.MapInt64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[21] {
		k := gen_int64(132)
		vx := gen_uint64(232)
		v, ok := src.MapInt64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[22] {
		k := gen_int64(133)
		vx := gen_int32(233)
		v, ok := src.MapInt64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[22] {
		k := gen_int64(134)
		vx := gen_int32(234)
		v, ok := src.MapInt64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[22] {
		k := gen_int64(135)
		vx := gen_int32(235)
		v, ok := src.MapInt64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[22] {
		k := gen_int64(136)
		vx := gen_int32(236)
		v, ok := src.MapInt64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[22] {
		k := gen_int64(137)
		vx := gen_int32(237)
		v, ok := src.MapInt64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[22] {
		k := gen_int64(138)
		vx := gen_int32(238)
		v, ok := src.MapInt64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[23] {
		k := gen_int64(139)
		vx := gen_int64(239)
		v, ok := src.MapInt64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[23] {
		k := gen_int64(140)
		vx := gen_int64(240)
		v, ok := src.MapInt64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[23] {
		k := gen_int64(141)
		vx := gen_int64(241)
		v, ok := src.MapInt64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[23] {
		k := gen_int64(142)
		vx := gen_int64(242)
		v, ok := src.MapInt64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[23] {
		k := gen_int64(143)
		vx := gen_int64(243)
		v, ok := src.MapInt64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[23] {
		k := gen_int64(144)
		vx := gen_int64(244)
		v, ok := src.MapInt64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[24] {
		k := gen_int64(145)
		vx := gen_uint32(245)
		v, ok := src.MapInt64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[24] {
		k := gen_int64(146)
		vx := gen_uint32(246)
		v, ok := src.MapInt64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[24] {
		k := gen_int64(147)
		vx := gen_uint32(247)
		v, ok := src.MapInt64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[24] {
		k := gen_int64(148)
		vx := gen_uint32(248)
		v, ok := src.MapInt64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[24] {
		k := gen_int64(149)
		vx := gen_uint32(249)
		v, ok := src.MapInt64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[24] {
		k := gen_int64(150)
		vx := gen_uint32(250)
		v, ok := src.MapInt64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[25] {
		k := gen_int64(151)
		vx := gen_uint64(251)
		v, ok := src.MapInt64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[25] {
		k := gen_int64(152)
		vx := gen_uint64(252)
		v, ok := src.MapInt64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[25] {
		k := gen_int64(153)
		vx := gen_uint64(253)
		v, ok := src.MapInt64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[25] {
		k := gen_int64(154)
		vx := gen_uint64(254)
		v, ok := src.MapInt64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[25] {
		k := gen_int64(155)
		vx := gen_uint64(255)
		v, ok := src.MapInt64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[25] {
		k := gen_int64(156)
		vx := gen_uint64(256)
		v, ok := src.MapInt64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[26] {
		k := gen_int64(157)
		vx := gen_int32(257)
		v, ok := src.MapInt64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[26] {
		k := gen_int64(158)
		vx := gen_int32(258)
		v, ok := src.MapInt64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[26] {
		k := gen_int64(159)
		vx := gen_int32(259)
		v, ok := src.MapInt64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[26] {
		k := gen_int64(160)
		vx := gen_int32(260)
		v, ok := src.MapInt64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[26] {
		k := gen_int64(161)
		vx := gen_int32(261)
		v, ok := src.MapInt64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[26] {
		k := gen_int64(162)
		vx := gen_int32(262)
		v, ok := src.MapInt64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[27] {
		k := gen_int64(163)
		vx := gen_int64(263)
		v, ok := src.MapInt64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[27] {
		k := gen_int64(164)
		vx := gen_int64(264)
		v, ok := src.MapInt64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[27] {
		k := gen_int64(165)
		vx := gen_int64(265)
		v, ok := src.MapInt64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[27] {
		k := gen_int64(166)
		vx := gen_int64(266)
		v, ok := src.MapInt64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[27] {
		k := gen_int64(167)
		vx := gen_int64(267)
		v, ok := src.MapInt64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[27] {
		k := gen_int64(168)
		vx := gen_int64(268)
		v, ok := src.MapInt64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[28] {
		k := gen_int64(169)
		vx := gen_bool(269)
		v, ok := src.MapInt64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[28] {
		k := gen_int64(170)
		vx := gen_bool(270)
		v, ok := src.MapInt64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[28] {
		k := gen_int64(171)
		vx := gen_bool(271)
		v, ok := src.MapInt64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[28] {
		k := gen_int64(172)
		vx := gen_bool(272)
		v, ok := src.MapInt64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[28] {
		k := gen_int64(173)
		vx := gen_bool(273)
		v, ok := src.MapInt64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[28] {
		k := gen_int64(174)
		vx := gen_bool(274)
		v, ok := src.MapInt64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[29] {
		k := gen_int64(175)
		vx := gen_string(275)
		v, ok := src.MapInt64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[29] {
		k := gen_int64(176)
		vx := gen_string(276)
		v, ok := src.MapInt64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[29] {
		k := gen_int64(177)
		vx := gen_string(277)
		v, ok := src.MapInt64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[29] {
		k := gen_int64(178)
		vx := gen_string(278)
		v, ok := src.MapInt64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[29] {
		k := gen_int64(179)
		vx := gen_string(279)
		v, ok := src.MapInt64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[29] {
		k := gen_int64(180)
		vx := gen_string(280)
		v, ok := src.MapInt64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[30] {
		k := gen_int64(181)
		vx := gen_bytes(281)
		v, ok := src.MapInt64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[30] {
		k := gen_int64(182)
		vx := gen_bytes(282)
		v, ok := src.MapInt64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[30] {
		k := gen_int64(183)
		vx := gen_bytes(283)
		v, ok := src.MapInt64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[30] {
		k := gen_int64(184)
		vx := gen_bytes(284)
		v, ok := src.MapInt64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[30] {
		k := gen_int64(185)
		vx := gen_bytes(285)
		v, ok := src.MapInt64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[30] {
		k := gen_int64(186)
		vx := gen_bytes(286)
		v, ok := src.MapInt64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[31] {
		k := gen_int64(187)
		vx := gen_Item(287)
		v, ok := src.MapInt64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[31] {
		k := gen_int64(188)
		vx := gen_Item(288)
		v, ok := src.MapInt64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[31] {
		k := gen_int64(189)
		vx := gen_Item(289)
		v, ok := src.MapInt64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[31] {
		k := gen_int64(190)
		vx := gen_Item(290)
		v, ok := src.MapInt64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[31] {
		k := gen_int64(191)
		vx := gen_Item(291)
		v, ok := src.MapInt64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[31] {
		k := gen_int64(192)
		vx := gen_Item(292)
		v, ok := src.MapInt64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[32] {
		k := gen_uint32(193)
		vx := gen_float64(293)
		v, ok := src.MapUint32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[32] {
		k := gen_uint32(194)
		vx := gen_float64(294)
		v, ok := src.MapUint32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[32] {
		k := gen_uint32(195)
		vx := gen_float64(295)
		v, ok := src.MapUint32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[32] {
		k := gen_uint32(196)
		vx := gen_float64(296)
		v, ok := src.MapUint32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[32] {
		k := gen_uint32(197)
		vx := gen_float64(297)
		v, ok := src.MapUint32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[32] {
		k := gen_uint32(198)
		vx := gen_float64(298)
		v, ok := src.MapUint32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[33] {
		k := gen_uint32(199)
		vx := gen_float32(299)
		v, ok := src.MapUint32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[33] {
		k := gen_uint32(200)
		vx := gen_float32(300)
		v, ok := src.MapUint32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[33] {
		k := gen_uint32(201)
		vx := gen_float32(301)
		v, ok := src.MapUint32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[33] {
		k := gen_uint32(202)
		vx := gen_float32(302)
		v, ok := src.MapUint32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[33] {
		k := gen_uint32(203)
		vx := gen_float32(303)
		v, ok := src.MapUint32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[33] {
		k := gen_uint32(204)
		vx := gen_float32(304)
		v, ok := src.MapUint32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[34] {
		k := gen_uint32(205)
		vx := gen_int32(305)
		v, ok := src.MapUint32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[34] {
		k := gen_uint32(206)
		vx := gen_int32(306)
		v, ok := src.MapUint32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[34] {
		k := gen_uint32(207)
		vx := gen_int32(307)
		v, ok := src.MapUint32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[34] {
		k := gen_uint32(208)
		vx := gen_int32(308)
		v, ok := src.MapUint32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[34] {
		k := gen_uint32(209)
		vx := gen_int32(309)
		v, ok := src.MapUint32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[34] {
		k := gen_uint32(210)
		vx := gen_int32(310)
		v, ok := src.MapUint32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[35] {
		k := gen_uint32(211)
		vx := gen_int64(311)
		v, ok := src.MapUint32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[35] {
		k := gen_uint32(212)
		vx := gen_int64(312)
		v, ok := src.MapUint32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[35] {
		k := gen_uint32(213)
		vx := gen_int64(313)
		v, ok := src.MapUint32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[35] {
		k := gen_uint32(214)
		vx := gen_int64(314)
		v, ok := src.MapUint32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[35] {
		k := gen_uint32(215)
		vx := gen_int64(315)
		v, ok := src.MapUint32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[35] {
		k := gen_uint32(216)
		vx := gen_int64(316)
		v, ok := src.MapUint32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[36] {
		k := gen_uint32(217)
		vx := gen_uint32(317)
		v, ok := src.MapUint32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[36] {
		k := gen_uint32(218)
		vx := gen_uint32(318)
		v, ok := src.MapUint32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[36] {
		k := gen_uint32(219)
		vx := gen_uint32(319)
		v, ok := src.MapUint32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[36] {
		k := gen_uint32(220)
		vx := gen_uint32(320)
		v, ok := src.MapUint32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[36] {
		k := gen_uint32(221)
		vx := gen_uint32(321)
		v, ok := src.MapUint32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[36] {
		k := gen_uint32(222)
		vx := gen_uint32(322)
		v, ok := src.MapUint32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[37] {
		k := gen_uint32(223)
		vx := gen_uint64(323)
		v, ok := src.MapUint32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[37] {
		k := gen_uint32(224)
		vx := gen_uint64(324)
		v, ok := src.MapUint32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[37] {
		k := gen_uint32(225)
		vx := gen_uint64(325)
		v, ok := src.MapUint32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[37] {
		k := gen_uint32(226)
		vx := gen_uint64(326)
		v, ok := src.MapUint32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[37] {
		k := gen_uint32(227)
		vx := gen_uint64(327)
		v, ok := src.MapUint32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[37] {
		k := gen_uint32(228)
		vx := gen_uint64(328)
		v, ok := src.MapUint32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[38] {
		k := gen_uint32(229)
		vx := gen_int32(329)
		v, ok := src.MapUint32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[38] {
		k := gen_uint32(230)
		vx := gen_int32(330)
		v, ok := src.MapUint32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[38] {
		k := gen_uint32(231)
		vx := gen_int32(331)
		v, ok := src.MapUint32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[38] {
		k := gen_uint32(232)
		vx := gen_int32(332)
		v, ok := src.MapUint32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[38] {
		k := gen_uint32(233)
		vx := gen_int32(333)
		v, ok := src.MapUint32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[38] {
		k := gen_uint32(234)
		vx := gen_int32(334)
		v, ok := src.MapUint32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[39] {
		k := gen_uint32(235)
		vx := gen_int64(335)
		v, ok := src.MapUint32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[39] {
		k := gen_uint32(236)
		vx := gen_int64(336)
		v, ok := src.MapUint32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[39] {
		k := gen_uint32(237)
		vx := gen_int64(337)
		v, ok := src.MapUint32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[39] {
		k := gen_uint32(238)
		vx := gen_int64(338)
		v, ok := src.MapUint32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[39] {
		k := gen_uint32(239)
		vx := gen_int64(339)
		v, ok := src.MapUint32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[39] {
		k := gen_uint32(240)
		vx := gen_int64(340)
		v, ok := src.MapUint32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[40] {
		k := gen_uint32(241)
		vx := gen_uint32(341)
		v, ok := src.MapUint32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[40] {
		k := gen_uint32(242)
		vx := gen_uint32(342)
		v, ok := src.MapUint32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[40] {
		k := gen_uint32(243)
		vx := gen_uint32(343)
		v, ok := src.MapUint32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[40] {
		k := gen_uint32(244)
		vx := gen_uint32(344)
		v, ok := src.MapUint32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[40] {
		k := gen_uint32(245)
		vx := gen_uint32(345)
		v, ok := src.MapUint32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[40] {
		k := gen_uint32(246)
		vx := gen_uint32(346)
		v, ok := src.MapUint32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[41] {
		k := gen_uint32(247)
		vx := gen_uint64(347)
		v, ok := src.MapUint32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[41] {
		k := gen_uint32(248)
		vx := gen_uint64(348)
		v, ok := src.MapUint32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[41] {
		k := gen_uint32(249)
		vx := gen_uint64(349)
		v, ok := src.MapUint32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[41] {
		k := gen_uint32(250)
		vx := gen_uint64(350)
		v, ok := src.MapUint32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[41] {
		k := gen_uint32(251)
		vx := gen_uint64(351)
		v, ok := src.MapUint32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[41] {
		k := gen_uint32(252)
		vx := gen_uint64(352)
		v, ok := src.MapUint32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[42] {
		k := gen_uint32(253)
		vx := gen_int32(353)
		v, ok := src.MapUint32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[42] {
		k := gen_uint32(254)
		vx := gen_int32(354)
		v, ok := src.MapUint32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[42] {
		k := gen_uint32(255)
		vx := gen_int32(355)
		v, ok := src.MapUint32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[42] {
		k := gen_uint32(256)
		vx := gen_int32(356)
		v, ok := src.MapUint32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[42] {
		k := gen_uint32(257)
		vx := gen_int32(357)
		v, ok := src.MapUint32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[42] {
		k := gen_uint32(258)
		vx := gen_int32(358)
		v, ok := src.MapUint32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[43] {
		k := gen_uint32(259)
		vx := gen_int64(359)
		v, ok := src.MapUint32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[43] {
		k := gen_uint32(260)
		vx := gen_int64(360)
		v, ok := src.MapUint32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[43] {
		k := gen_uint32(261)
		vx := gen_int64(361)
		v, ok := src.MapUint32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[43] {
		k := gen_uint32(262)
		vx := gen_int64(362)
		v, ok := src.MapUint32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[43] {
		k := gen_uint32(263)
		vx := gen_int64(363)
		v, ok := src.MapUint32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[43] {
		k := gen_uint32(264)
		vx := gen_int64(364)
		v, ok := src.MapUint32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[44] {
		k := gen_uint32(265)
		vx := gen_bool(365)
		v, ok := src.MapUint32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[44] {
		k := gen_uint32(266)
		vx := gen_bool(366)
		v, ok := src.MapUint32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[44] {
		k := gen_uint32(267)
		vx := gen_bool(367)
		v, ok := src.MapUint32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[44] {
		k := gen_uint32(268)
		vx := gen_bool(368)
		v, ok := src.MapUint32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[44] {
		k := gen_uint32(269)
		vx := gen_bool(369)
		v, ok := src.MapUint32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[44] {
		k := gen_uint32(270)
		vx := gen_bool(370)
		v, ok := src.MapUint32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[45] {
		k := gen_uint32(271)
		vx := gen_string(371)
		v, ok := src.MapUint32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[45] {
		k := gen_uint32(272)
		vx := gen_string(372)
		v, ok := src.MapUint32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[45] {
		k := gen_uint32(273)
		vx := gen_string(373)
		v, ok := src.MapUint32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[45] {
		k := gen_uint32(274)
		vx := gen_string(374)
		v, ok := src.MapUint32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[45] {
		k := gen_uint32(275)
		vx := gen_string(375)
		v, ok := src.MapUint32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[45] {
		k := gen_uint32(276)
		vx := gen_string(376)
		v, ok := src.MapUint32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[46] {
		k := gen_uint32(277)
		vx := gen_bytes(377)
		v, ok := src.MapUint32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[46] {
		k := gen_uint32(278)
		vx := gen_bytes(378)
		v, ok := src.MapUint32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[46] {
		k := gen_uint32(279)
		vx := gen_bytes(379)
		v, ok := src.MapUint32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[46] {
		k := gen_uint32(280)
		vx := gen_bytes(380)
		v, ok := src.MapUint32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[46] {
		k := gen_uint32(281)
		vx := gen_bytes(381)
		v, ok := src.MapUint32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[46] {
		k := gen_uint32(282)
		vx := gen_bytes(382)
		v, ok := src.MapUint32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[47] {
		k := gen_uint32(283)
		vx := gen_Item(383)
		v, ok := src.MapUint32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[47] {
		k := gen_uint32(284)
		vx := gen_Item(384)
		v, ok := src.MapUint32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[47] {
		k := gen_uint32(285)
		vx := gen_Item(385)
		v, ok := src.MapUint32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[47] {
		k := gen_uint32(286)
		vx := gen_Item(386)
		v, ok := src.MapUint32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[47] {
		k := gen_uint32(287)
		vx := gen_Item(387)
		v, ok := src.MapUint32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[47] {
		k := gen_uint32(288)
		vx := gen_Item(388)
		v, ok := src.MapUint32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[48] {
		k := gen_uint64(289)
		vx := gen_float64(389)
		v, ok := src.MapUint64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[48] {
		k := gen_uint64(290)
		vx := gen_float64(390)
		v, ok := src.MapUint64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[48] {
		k := gen_uint64(291)
		vx := gen_float64(391)
		v, ok := src.MapUint64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[48] {
		k := gen_uint64(292)
		vx := gen_float64(392)
		v, ok := src.MapUint64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[48] {
		k := gen_uint64(293)
		vx := gen_float64(393)
		v, ok := src.MapUint64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[48] {
		k := gen_uint64(294)
		vx := gen_float64(394)
		v, ok := src.MapUint64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[49] {
		k := gen_uint64(295)
		vx := gen_float32(395)
		v, ok := src.MapUint64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[49] {
		k := gen_uint64(296)
		vx := gen_float32(396)
		v, ok := src.MapUint64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[49] {
		k := gen_uint64(297)
		vx := gen_float32(397)
		v, ok := src.MapUint64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[49] {
		k := gen_uint64(298)
		vx := gen_float32(398)
		v, ok := src.MapUint64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[49] {
		k := gen_uint64(299)
		vx := gen_float32(399)
		v, ok := src.MapUint64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[49] {
		k := gen_uint64(300)
		vx := gen_float32(400)
		v, ok := src.MapUint64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[50] {
		k := gen_uint64(301)
		vx := gen_int32(401)
		v, ok := src.MapUint64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[50] {
		k := gen_uint64(302)
		vx := gen_int32(402)
		v, ok := src.MapUint64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[50] {
		k := gen_uint64(303)
		vx := gen_int32(403)
		v, ok := src.MapUint64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[50] {
		k := gen_uint64(304)
		vx := gen_int32(404)
		v, ok := src.MapUint64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[50] {
		k := gen_uint64(305)
		vx := gen_int32(405)
		v, ok := src.MapUint64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[50] {
		k := gen_uint64(306)
		vx := gen_int32(406)
		v, ok := src.MapUint64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[51] {
		k := gen_uint64(307)
		vx := gen_int64(407)
		v, ok := src.MapUint64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[51] {
		k := gen_uint64(308)
		vx := gen_int64(408)
		v, ok := src.MapUint64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[51] {
		k := gen_uint64(309)
		vx := gen_int64(409)
		v, ok := src.MapUint64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[51] {
		k := gen_uint64(310)
		vx := gen_int64(410)
		v, ok := src.MapUint64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[51] {
		k := gen_uint64(311)
		vx := gen_int64(411)
		v, ok := src.MapUint64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[51] {
		k := gen_uint64(312)
		vx := gen_int64(412)
		v, ok := src.MapUint64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[52] {
		k := gen_uint64(313)
		vx := gen_uint32(413)
		v, ok := src.MapUint64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[52] {
		k := gen_uint64(314)
		vx := gen_uint32(414)
		v, ok := src.MapUint64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[52] {
		k := gen_uint64(315)
		vx := gen_uint32(415)
		v, ok := src.MapUint64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[52] {
		k := gen_uint64(316)
		vx := gen_uint32(416)
		v, ok := src.MapUint64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[52] {
		k := gen_uint64(317)
		vx := gen_uint32(417)
		v, ok := src.MapUint64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[52] {
		k := gen_uint64(318)
		vx := gen_uint32(418)
		v, ok := src.MapUint64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[53] {
		k := gen_uint64(319)
		vx := gen_uint64(419)
		v, ok := src.MapUint64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[53] {
		k := gen_uint64(320)
		vx := gen_uint64(420)
		v, ok := src.MapUint64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[53] {
		k := gen_uint64(321)
		vx := gen_uint64(421)
		v, ok := src.MapUint64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[53] {
		k := gen_uint64(322)
		vx := gen_uint64(422)
		v, ok := src.MapUint64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[53] {
		k := gen_uint64(323)
		vx := gen_uint64(423)
		v, ok := src.MapUint64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[53] {
		k := gen_uint64(324)
		vx := gen_uint64(424)
		v, ok := src.MapUint64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[54] {
		k := gen_uint64(325)
		vx := gen_int32(425)
		v, ok := src.MapUint64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[54] {
		k := gen_uint64(326)
		vx := gen_int32(426)
		v, ok := src.MapUint64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[54] {
		k := gen_uint64(327)
		vx := gen_int32(427)
		v, ok := src.MapUint64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[54] {
		k := gen_uint64(328)
		vx := gen_int32(428)
		v, ok := src.MapUint64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[54] {
		k := gen_uint64(329)
		vx := gen_int32(429)
		v, ok := src.MapUint64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[54] {
		k := gen_uint64(330)
		vx := gen_int32(430)
		v, ok := src.MapUint64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[55] {
		k := gen_uint64(331)
		vx := gen_int64(431)
		v, ok := src.MapUint64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[55] {
		k := gen_uint64(332)
		vx := gen_int64(432)
		v, ok := src.MapUint64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[55] {
		k := gen_uint64(333)
		vx := gen_int64(433)
		v, ok := src.MapUint64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[55] {
		k := gen_uint64(334)
		vx := gen_int64(434)
		v, ok := src.MapUint64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[55] {
		k := gen_uint64(335)
		vx := gen_int64(435)
		v, ok := src.MapUint64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[55] {
		k := gen_uint64(336)
		vx := gen_int64(436)
		v, ok := src.MapUint64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[56] {
		k := gen_uint64(337)
		vx := gen_uint32(437)
		v, ok := src.MapUint64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[56] {
		k := gen_uint64(338)
		vx := gen_uint32(438)
		v, ok := src.MapUint64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[56] {
		k := gen_uint64(339)
		vx := gen_uint32(439)
		v, ok := src.MapUint64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[56] {
		k := gen_uint64(340)
		vx := gen_uint32(440)
		v, ok := src.MapUint64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[56] {
		k := gen_uint64(341)
		vx := gen_uint32(441)
		v, ok := src.MapUint64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[56] {
		k := gen_uint64(342)
		vx := gen_uint32(442)
		v, ok := src.MapUint64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[57] {
		k := gen_uint64(343)
		vx := gen_uint64(443)
		v, ok := src.MapUint64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[57] {
		k := gen_uint64(344)
		vx := gen_uint64(444)
		v, ok := src.MapUint64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[57] {
		k := gen_uint64(345)
		vx := gen_uint64(445)
		v, ok := src.MapUint64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[57] {
		k := gen_uint64(346)
		vx := gen_uint64(446)
		v, ok := src.MapUint64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[57] {
		k := gen_uint64(347)
		vx := gen_uint64(447)
		v, ok := src.MapUint64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[57] {
		k := gen_uint64(348)
		vx := gen_uint64(448)
		v, ok := src.MapUint64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[58] {
		k := gen_uint64(349)
		vx := gen_int32(449)
		v, ok := src.MapUint64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[58] {
		k := gen_uint64(350)
		vx := gen_int32(450)
		v, ok := src.MapUint64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[58] {
		k := gen_uint64(351)
		vx := gen_int32(451)
		v, ok := src.MapUint64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[58] {
		k := gen_uint64(352)
		vx := gen_int32(452)
		v, ok := src.MapUint64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[58] {
		k := gen_uint64(353)
		vx := gen_int32(453)
		v, ok := src.MapUint64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[58] {
		k := gen_uint64(354)
		vx := gen_int32(454)
		v, ok := src.MapUint64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[59] {
		k := gen_uint64(355)
		vx := gen_int64(455)
		v, ok := src.MapUint64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[59] {
		k := gen_uint64(356)
		vx := gen_int64(456)
		v, ok := src.MapUint64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[59] {
		k := gen_uint64(357)
		vx := gen_int64(457)
		v, ok := src.MapUint64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[59] {
		k := gen_uint64(358)
		vx := gen_int64(458)
		v, ok := src.MapUint64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[59] {
		k := gen_uint64(359)
		vx := gen_int64(459)
		v, ok := src.MapUint64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[59] {
		k := gen_uint64(360)
		vx := gen_int64(460)
		v, ok := src.MapUint64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[60] {
		k := gen_uint64(361)
		vx := gen_bool(461)
		v, ok := src.MapUint64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[60] {
		k := gen_uint64(362)
		vx := gen_bool(462)
		v, ok := src.MapUint64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[60] {
		k := gen_uint64(363)
		vx := gen_bool(463)
		v, ok := src.MapUint64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[60] {
		k := gen_uint64(364)
		vx := gen_bool(464)
		v, ok := src.MapUint64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[60] {
		k := gen_uint64(365)
		vx := gen_bool(465)
		v, ok := src.MapUint64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[60] {
		k := gen_uint64(366)
		vx := gen_bool(466)
		v, ok := src.MapUint64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[61] {
		k := gen_uint64(367)
		vx := gen_string(467)
		v, ok := src.MapUint64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[61] {
		k := gen_uint64(368)
		vx := gen_string(468)
		v, ok := src.MapUint64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[61] {
		k := gen_uint64(369)
		vx := gen_string(469)
		v, ok := src.MapUint64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[61] {
		k := gen_uint64(370)
		vx := gen_string(470)
		v, ok := src.MapUint64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[61] {
		k := gen_uint64(371)
		vx := gen_string(471)
		v, ok := src.MapUint64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[61] {
		k := gen_uint64(372)
		vx := gen_string(472)
		v, ok := src.MapUint64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[62] {
		k := gen_uint64(373)
		vx := gen_bytes(473)
		v, ok := src.MapUint64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[62] {
		k := gen_uint64(374)
		vx := gen_bytes(474)
		v, ok := src.MapUint64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[62] {
		k := gen_uint64(375)
		vx := gen_bytes(475)
		v, ok := src.MapUint64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[62] {
		k := gen_uint64(376)
		vx := gen_bytes(476)
		v, ok := src.MapUint64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[62] {
		k := gen_uint64(377)
		vx := gen_bytes(477)
		v, ok := src.MapUint64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[62] {
		k := gen_uint64(378)
		vx := gen_bytes(478)
		v, ok := src.MapUint64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[63] {
		k := gen_uint64(379)
		vx := gen_Item(479)
		v, ok := src.MapUint64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[63] {
		k := gen_uint64(380)
		vx := gen_Item(480)
		v, ok := src.MapUint64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[63] {
		k := gen_uint64(381)
		vx := gen_Item(481)
		v, ok := src.MapUint64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[63] {
		k := gen_uint64(382)
		vx := gen_Item(482)
		v, ok := src.MapUint64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[63] {
		k := gen_uint64(383)
		vx := gen_Item(483)
		v, ok := src.MapUint64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[63] {
		k := gen_uint64(384)
		vx := gen_Item(484)
		v, ok := src.MapUint64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[64] {
		k := gen_int32(385)
		vx := gen_float64(485)
		v, ok := src.MapSint32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[64] {
		k := gen_int32(386)
		vx := gen_float64(486)
		v, ok := src.MapSint32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[64] {
		k := gen_int32(387)
		vx := gen_float64(487)
		v, ok := src.MapSint32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[64] {
		k := gen_int32(388)
		vx := gen_float64(488)
		v, ok := src.MapSint32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[64] {
		k := gen_int32(389)
		vx := gen_float64(489)
		v, ok := src.MapSint32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[64] {
		k := gen_int32(390)
		vx := gen_float64(490)
		v, ok := src.MapSint32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[65] {
		k := gen_int32(391)
		vx := gen_float32(491)
		v, ok := src.MapSint32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[65] {
		k := gen_int32(392)
		vx := gen_float32(492)
		v, ok := src.MapSint32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[65] {
		k := gen_int32(393)
		vx := gen_float32(493)
		v, ok := src.MapSint32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[65] {
		k := gen_int32(394)
		vx := gen_float32(494)
		v, ok := src.MapSint32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[65] {
		k := gen_int32(395)
		vx := gen_float32(495)
		v, ok := src.MapSint32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[65] {
		k := gen_int32(396)
		vx := gen_float32(496)
		v, ok := src.MapSint32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[66] {
		k := gen_int32(397)
		vx := gen_int32(497)
		v, ok := src.MapSint32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[66] {
		k := gen_int32(398)
		vx := gen_int32(498)
		v, ok := src.MapSint32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[66] {
		k := gen_int32(399)
		vx := gen_int32(499)
		v, ok := src.MapSint32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[66] {
		k := gen_int32(400)
		vx := gen_int32(500)
		v, ok := src.MapSint32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[66] {
		k := gen_int32(401)
		vx := gen_int32(501)
		v, ok := src.MapSint32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[66] {
		k := gen_int32(402)
		vx := gen_int32(502)
		v, ok := src.MapSint32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[67] {
		k := gen_int32(403)
		vx := gen_int64(503)
		v, ok := src.MapSint32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[67] {
		k := gen_int32(404)
		vx := gen_int64(504)
		v, ok := src.MapSint32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[67] {
		k := gen_int32(405)
		vx := gen_int64(505)
		v, ok := src.MapSint32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[67] {
		k := gen_int32(406)
		vx := gen_int64(506)
		v, ok := src.MapSint32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[67] {
		k := gen_int32(407)
		vx := gen_int64(507)
		v, ok := src.MapSint32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[67] {
		k := gen_int32(408)
		vx := gen_int64(508)
		v, ok := src.MapSint32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[68] {
		k := gen_int32(409)
		vx := gen_uint32(509)
		v, ok := src.MapSint32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[68] {
		k := gen_int32(410)
		vx := gen_uint32(510)
		v, ok := src.MapSint32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[68] {
		k := gen_int32(411)
		vx := gen_uint32(511)
		v, ok := src.MapSint32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[68] {
		k := gen_int32(412)
		vx := gen_uint32(512)
		v, ok := src.MapSint32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[68] {
		k := gen_int32(413)
		vx := gen_uint32(513)
		v, ok := src.MapSint32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[68] {
		k := gen_int32(414)
		vx := gen_uint32(514)
		v, ok := src.MapSint32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[69] {
		k := gen_int32(415)
		vx := gen_uint64(515)
		v, ok := src.MapSint32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[69] {
		k := gen_int32(416)
		vx := gen_uint64(516)
		v, ok := src.MapSint32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[69] {
		k := gen_int32(417)
		vx := gen_uint64(517)
		v, ok := src.MapSint32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[69] {
		k := gen_int32(418)
		vx := gen_uint64(518)
		v, ok := src.MapSint32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[69] {
		k := gen_int32(419)
		vx := gen_uint64(519)
		v, ok := src.MapSint32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[69] {
		k := gen_int32(420)
		vx := gen_uint64(520)
		v, ok := src.MapSint32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[70] {
		k := gen_int32(421)
		vx := gen_int32(521)
		v, ok := src.MapSint32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[70] {
		k := gen_int32(422)
		vx := gen_int32(522)
		v, ok := src.MapSint32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[70] {
		k := gen_int32(423)
		vx := gen_int32(523)
		v, ok := src.MapSint32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[70] {
		k := gen_int32(424)
		vx := gen_int32(524)
		v, ok := src.MapSint32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[70] {
		k := gen_int32(425)
		vx := gen_int32(525)
		v, ok := src.MapSint32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[70] {
		k := gen_int32(426)
		vx := gen_int32(526)
		v, ok := src.MapSint32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[71] {
		k := gen_int32(427)
		vx := gen_int64(527)
		v, ok := src.MapSint32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[71] {
		k := gen_int32(428)
		vx := gen_int64(528)
		v, ok := src.MapSint32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[71] {
		k := gen_int32(429)
		vx := gen_int64(529)
		v, ok := src.MapSint32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[71] {
		k := gen_int32(430)
		vx := gen_int64(530)
		v, ok := src.MapSint32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[71] {
		k := gen_int32(431)
		vx := gen_int64(531)
		v, ok := src.MapSint32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[71] {
		k := gen_int32(432)
		vx := gen_int64(532)
		v, ok := src.MapSint32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[72] {
		k := gen_int32(433)
		vx := gen_uint32(533)
		v, ok := src.MapSint32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[72] {
		k := gen_int32(434)
		vx := gen_uint32(534)
		v, ok := src.MapSint32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[72] {
		k := gen_int32(435)
		vx := gen_uint32(535)
		v, ok := src.MapSint32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[72] {
		k := gen_int32(436)
		vx := gen_uint32(536)
		v, ok := src.MapSint32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[72] {
		k := gen_int32(437)
		vx := gen_uint32(537)
		v, ok := src.MapSint32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[72] {
		k := gen_int32(438)
		vx := gen_uint32(538)
		v, ok := src.MapSint32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[73] {
		k := gen_int32(439)
		vx := gen_uint64(539)
		v, ok := src.MapSint32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[73] {
		k := gen_int32(440)
		vx := gen_uint64(540)
		v, ok := src.MapSint32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[73] {
		k := gen_int32(441)
		vx := gen_uint64(541)
		v, ok := src.MapSint32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[73] {
		k := gen_int32(442)
		vx := gen_uint64(542)
		v, ok := src.MapSint32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[73] {
		k := gen_int32(443)
		vx := gen_uint64(543)
		v, ok := src.MapSint32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[73] {
		k := gen_int32(444)
		vx := gen_uint64(544)
		v, ok := src.MapSint32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[74] {
		k := gen_int32(445)
		vx := gen_int32(545)
		v, ok := src.MapSint32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[74] {
		k := gen_int32(446)
		vx := gen_int32(546)
		v, ok := src.MapSint32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[74] {
		k := gen_int32(447)
		vx := gen_int32(547)
		v, ok := src.MapSint32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[74] {
		k := gen_int32(448)
		vx := gen_int32(548)
		v, ok := src.MapSint32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[74] {
		k := gen_int32(449)
		vx := gen_int32(549)
		v, ok := src.MapSint32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[74] {
		k := gen_int32(450)
		vx := gen_int32(550)
		v, ok := src.MapSint32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[75] {
		k := gen_int32(451)
		vx := gen_int64(551)
		v, ok := src.MapSint32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[75] {
		k := gen_int32(452)
		vx := gen_int64(552)
		v, ok := src.MapSint32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[75] {
		k := gen_int32(453)
		vx := gen_int64(553)
		v, ok := src.MapSint32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[75] {
		k := gen_int32(454)
		vx := gen_int64(554)
		v, ok := src.MapSint32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[75] {
		k := gen_int32(455)
		vx := gen_int64(555)
		v, ok := src.MapSint32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[75] {
		k := gen_int32(456)
		vx := gen_int64(556)
		v, ok := src.MapSint32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[76] {
		k := gen_int32(457)
		vx := gen_bool(557)
		v, ok := src.MapSint32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[76] {
		k := gen_int32(458)
		vx := gen_bool(558)
		v, ok := src.MapSint32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[76] {
		k := gen_int32(459)
		vx := gen_bool(559)
		v, ok := src.MapSint32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[76] {
		k := gen_int32(460)
		vx := gen_bool(560)
		v, ok := src.MapSint32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[76] {
		k := gen_int32(461)
		vx := gen_bool(561)
		v, ok := src.MapSint32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[76] {
		k := gen_int32(462)
		vx := gen_bool(562)
		v, ok := src.MapSint32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[77] {
		k := gen_int32(463)
		vx := gen_string(563)
		v, ok := src.MapSint32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[77] {
		k := gen_int32(464)
		vx := gen_string(564)
		v, ok := src.MapSint32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[77] {
		k := gen_int32(465)
		vx := gen_string(565)
		v, ok := src.MapSint32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[77] {
		k := gen_int32(466)
		vx := gen_string(566)
		v, ok := src.MapSint32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[77] {
		k := gen_int32(467)
		vx := gen_string(567)
		v, ok := src.MapSint32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[77] {
		k := gen_int32(468)
		vx := gen_string(568)
		v, ok := src.MapSint32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[78] {
		k := gen_int32(469)
		vx := gen_bytes(569)
		v, ok := src.MapSint32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[78] {
		k := gen_int32(470)
		vx := gen_bytes(570)
		v, ok := src.MapSint32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[78] {
		k := gen_int32(471)
		vx := gen_bytes(571)
		v, ok := src.MapSint32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[78] {
		k := gen_int32(472)
		vx := gen_bytes(572)
		v, ok := src.MapSint32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[78] {
		k := gen_int32(473)
		vx := gen_bytes(573)
		v, ok := src.MapSint32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[78] {
		k := gen_int32(474)
		vx := gen_bytes(574)
		v, ok := src.MapSint32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[79] {
		k := gen_int32(475)
		vx := gen_Item(575)
		v, ok := src.MapSint32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[79] {
		k := gen_int32(476)
		vx := gen_Item(576)
		v, ok := src.MapSint32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[79] {
		k := gen_int32(477)
		vx := gen_Item(577)
		v, ok := src.MapSint32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[79] {
		k := gen_int32(478)
		vx := gen_Item(578)
		v, ok := src.MapSint32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[79] {
		k := gen_int32(479)
		vx := gen_Item(579)
		v, ok := src.MapSint32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[79] {
		k := gen_int32(480)
		vx := gen_Item(580)
		v, ok := src.MapSint32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[80] {
		k := gen_int64(481)
		vx := gen_float64(581)
		v, ok := src.MapSint64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[80] {
		k := gen_int64(482)
		vx := gen_float64(582)
		v, ok := src.MapSint64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[80] {
		k := gen_int64(483)
		vx := gen_float64(583)
		v, ok := src.MapSint64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[80] {
		k := gen_int64(484)
		vx := gen_float64(584)
		v, ok := src.MapSint64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[80] {
		k := gen_int64(485)
		vx := gen_float64(585)
		v, ok := src.MapSint64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[80] {
		k := gen_int64(486)
		vx := gen_float64(586)
		v, ok := src.MapSint64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[81] {
		k := gen_int64(487)
		vx := gen_float32(587)
		v, ok := src.MapSint64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[81] {
		k := gen_int64(488)
		vx := gen_float32(588)
		v, ok := src.MapSint64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[81] {
		k := gen_int64(489)
		vx := gen_float32(589)
		v, ok := src.MapSint64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[81] {
		k := gen_int64(490)
		vx := gen_float32(590)
		v, ok := src.MapSint64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[81] {
		k := gen_int64(491)
		vx := gen_float32(591)
		v, ok := src.MapSint64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[81] {
		k := gen_int64(492)
		vx := gen_float32(592)
		v, ok := src.MapSint64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[82] {
		k := gen_int64(493)
		vx := gen_int32(593)
		v, ok := src.MapSint64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[82] {
		k := gen_int64(494)
		vx := gen_int32(594)
		v, ok := src.MapSint64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[82] {
		k := gen_int64(495)
		vx := gen_int32(595)
		v, ok := src.MapSint64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[82] {
		k := gen_int64(496)
		vx := gen_int32(596)
		v, ok := src.MapSint64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[82] {
		k := gen_int64(497)
		vx := gen_int32(597)
		v, ok := src.MapSint64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[82] {
		k := gen_int64(498)
		vx := gen_int32(598)
		v, ok := src.MapSint64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[83] {
		k := gen_int64(499)
		vx := gen_int64(599)
		v, ok := src.MapSint64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[83] {
		k := gen_int64(500)
		vx := gen_int64(600)
		v, ok := src.MapSint64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[83] {
		k := gen_int64(501)
		vx := gen_int64(601)
		v, ok := src.MapSint64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[83] {
		k := gen_int64(502)
		vx := gen_int64(602)
		v, ok := src.MapSint64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[83] {
		k := gen_int64(503)
		vx := gen_int64(603)
		v, ok := src.MapSint64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[83] {
		k := gen_int64(504)
		vx := gen_int64(604)
		v, ok := src.MapSint64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[84] {
		k := gen_int64(505)
		vx := gen_uint32(605)
		v, ok := src.MapSint64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[84] {
		k := gen_int64(506)
		vx := gen_uint32(606)
		v, ok := src.MapSint64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[84] {
		k := gen_int64(507)
		vx := gen_uint32(607)
		v, ok := src.MapSint64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[84] {
		k := gen_int64(508)
		vx := gen_uint32(608)
		v, ok := src.MapSint64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[84] {
		k := gen_int64(509)
		vx := gen_uint32(609)
		v, ok := src.MapSint64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[84] {
		k := gen_int64(510)
		vx := gen_uint32(610)
		v, ok := src.MapSint64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[85] {
		k := gen_int64(511)
		vx := gen_uint64(611)
		v, ok := src.MapSint64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[85] {
		k := gen_int64(512)
		vx := gen_uint64(612)
		v, ok := src.MapSint64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[85] {
		k := gen_int64(513)
		vx := gen_uint64(613)
		v, ok := src.MapSint64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[85] {
		k := gen_int64(514)
		vx := gen_uint64(614)
		v, ok := src.MapSint64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[85] {
		k := gen_int64(515)
		vx := gen_uint64(615)
		v, ok := src.MapSint64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[85] {
		k := gen_int64(516)
		vx := gen_uint64(616)
		v, ok := src.MapSint64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[86] {
		k := gen_int64(517)
		vx := gen_int32(617)
		v, ok := src.MapSint64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[86] {
		k := gen_int64(518)
		vx := gen_int32(618)
		v, ok := src.MapSint64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[86] {
		k := gen_int64(519)
		vx := gen_int32(619)
		v, ok := src.MapSint64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[86] {
		k := gen_int64(520)
		vx := gen_int32(620)
		v, ok := src.MapSint64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[86] {
		k := gen_int64(521)
		vx := gen_int32(621)
		v, ok := src.MapSint64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[86] {
		k := gen_int64(522)
		vx := gen_int32(622)
		v, ok := src.MapSint64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[87] {
		k := gen_int64(523)
		vx := gen_int64(623)
		v, ok := src.MapSint64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[87] {
		k := gen_int64(524)
		vx := gen_int64(624)
		v, ok := src.MapSint64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[87] {
		k := gen_int64(525)
		vx := gen_int64(625)
		v, ok := src.MapSint64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[87] {
		k := gen_int64(526)
		vx := gen_int64(626)
		v, ok := src.MapSint64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[87] {
		k := gen_int64(527)
		vx := gen_int64(627)
		v, ok := src.MapSint64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[87] {
		k := gen_int64(528)
		vx := gen_int64(628)
		v, ok := src.MapSint64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[88] {
		k := gen_int64(529)
		vx := gen_uint32(629)
		v, ok := src.MapSint64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[88] {
		k := gen_int64(530)
		vx := gen_uint32(630)
		v, ok := src.MapSint64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[88] {
		k := gen_int64(531)
		vx := gen_uint32(631)
		v, ok := src.MapSint64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[88] {
		k := gen_int64(532)
		vx := gen_uint32(632)
		v, ok := src.MapSint64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[88] {
		k := gen_int64(533)
		vx := gen_uint32(633)
		v, ok := src.MapSint64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[88] {
		k := gen_int64(534)
		vx := gen_uint32(634)
		v, ok := src.MapSint64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[89] {
		k := gen_int64(535)
		vx := gen_uint64(635)
		v, ok := src.MapSint64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[89] {
		k := gen_int64(536)
		vx := gen_uint64(636)
		v, ok := src.MapSint64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[89] {
		k := gen_int64(537)
		vx := gen_uint64(637)
		v, ok := src.MapSint64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[89] {
		k := gen_int64(538)
		vx := gen_uint64(638)
		v, ok := src.MapSint64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[89] {
		k := gen_int64(539)
		vx := gen_uint64(639)
		v, ok := src.MapSint64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[89] {
		k := gen_int64(540)
		vx := gen_uint64(640)
		v, ok := src.MapSint64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[90] {
		k := gen_int64(541)
		vx := gen_int32(641)
		v, ok := src.MapSint64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[90] {
		k := gen_int64(542)
		vx := gen_int32(642)
		v, ok := src.MapSint64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[90] {
		k := gen_int64(543)
		vx := gen_int32(643)
		v, ok := src.MapSint64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[90] {
		k := gen_int64(544)
		vx := gen_int32(644)
		v, ok := src.MapSint64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[90] {
		k := gen_int64(545)
		vx := gen_int32(645)
		v, ok := src.MapSint64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[90] {
		k := gen_int64(546)
		vx := gen_int32(646)
		v, ok := src.MapSint64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[91] {
		k := gen_int64(547)
		vx := gen_int64(647)
		v, ok := src.MapSint64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[91] {
		k := gen_int64(548)
		vx := gen_int64(648)
		v, ok := src.MapSint64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[91] {
		k := gen_int64(549)
		vx := gen_int64(649)
		v, ok := src.MapSint64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[91] {
		k := gen_int64(550)
		vx := gen_int64(650)
		v, ok := src.MapSint64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[91] {
		k := gen_int64(551)
		vx := gen_int64(651)
		v, ok := src.MapSint64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[91] {
		k := gen_int64(552)
		vx := gen_int64(652)
		v, ok := src.MapSint64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[92] {
		k := gen_int64(553)
		vx := gen_bool(653)
		v, ok := src.MapSint64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[92] {
		k := gen_int64(554)
		vx := gen_bool(654)
		v, ok := src.MapSint64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[92] {
		k := gen_int64(555)
		vx := gen_bool(655)
		v, ok := src.MapSint64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[92] {
		k := gen_int64(556)
		vx := gen_bool(656)
		v, ok := src.MapSint64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[92] {
		k := gen_int64(557)
		vx := gen_bool(657)
		v, ok := src.MapSint64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[92] {
		k := gen_int64(558)
		vx := gen_bool(658)
		v, ok := src.MapSint64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[93] {
		k := gen_int64(559)
		vx := gen_string(659)
		v, ok := src.MapSint64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[93] {
		k := gen_int64(560)
		vx := gen_string(660)
		v, ok := src.MapSint64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[93] {
		k := gen_int64(561)
		vx := gen_string(661)
		v, ok := src.MapSint64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[93] {
		k := gen_int64(562)
		vx := gen_string(662)
		v, ok := src.MapSint64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[93] {
		k := gen_int64(563)
		vx := gen_string(663)
		v, ok := src.MapSint64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[93] {
		k := gen_int64(564)
		vx := gen_string(664)
		v, ok := src.MapSint64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[94] {
		k := gen_int64(565)
		vx := gen_bytes(665)
		v, ok := src.MapSint64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[94] {
		k := gen_int64(566)
		vx := gen_bytes(666)
		v, ok := src.MapSint64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[94] {
		k := gen_int64(567)
		vx := gen_bytes(667)
		v, ok := src.MapSint64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[94] {
		k := gen_int64(568)
		vx := gen_bytes(668)
		v, ok := src.MapSint64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[94] {
		k := gen_int64(569)
		vx := gen_bytes(669)
		v, ok := src.MapSint64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[94] {
		k := gen_int64(570)
		vx := gen_bytes(670)
		v, ok := src.MapSint64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[95] {
		k := gen_int64(571)
		vx := gen_Item(671)
		v, ok := src.MapSint64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[95] {
		k := gen_int64(572)
		vx := gen_Item(672)
		v, ok := src.MapSint64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[95] {
		k := gen_int64(573)
		vx := gen_Item(673)
		v, ok := src.MapSint64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[95] {
		k := gen_int64(574)
		vx := gen_Item(674)
		v, ok := src.MapSint64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[95] {
		k := gen_int64(575)
		vx := gen_Item(675)
		v, ok := src.MapSint64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[95] {
		k := gen_int64(576)
		vx := gen_Item(676)
		v, ok := src.MapSint64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[96] {
		k := gen_uint32(577)
		vx := gen_float64(677)
		v, ok := src.MapFixed32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[96] {
		k := gen_uint32(578)
		vx := gen_float64(678)
		v, ok := src.MapFixed32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[96] {
		k := gen_uint32(579)
		vx := gen_float64(679)
		v, ok := src.MapFixed32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[96] {
		k := gen_uint32(580)
		vx := gen_float64(680)
		v, ok := src.MapFixed32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[96] {
		k := gen_uint32(581)
		vx := gen_float64(681)
		v, ok := src.MapFixed32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[96] {
		k := gen_uint32(582)
		vx := gen_float64(682)
		v, ok := src.MapFixed32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[97] {
		k := gen_uint32(583)
		vx := gen_float32(683)
		v, ok := src.MapFixed32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[97] {
		k := gen_uint32(584)
		vx := gen_float32(684)
		v, ok := src.MapFixed32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[97] {
		k := gen_uint32(585)
		vx := gen_float32(685)
		v, ok := src.MapFixed32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[97] {
		k := gen_uint32(586)
		vx := gen_float32(686)
		v, ok := src.MapFixed32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[97] {
		k := gen_uint32(587)
		vx := gen_float32(687)
		v, ok := src.MapFixed32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[97] {
		k := gen_uint32(588)
		vx := gen_float32(688)
		v, ok := src.MapFixed32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[98] {
		k := gen_uint32(589)
		vx := gen_int32(689)
		v, ok := src.MapFixed32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[98] {
		k := gen_uint32(590)
		vx := gen_int32(690)
		v, ok := src.MapFixed32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[98] {
		k := gen_uint32(591)
		vx := gen_int32(691)
		v, ok := src.MapFixed32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[98] {
		k := gen_uint32(592)
		vx := gen_int32(692)
		v, ok := src.MapFixed32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[98] {
		k := gen_uint32(593)
		vx := gen_int32(693)
		v, ok := src.MapFixed32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[98] {
		k := gen_uint32(594)
		vx := gen_int32(694)
		v, ok := src.MapFixed32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[99] {
		k := gen_uint32(595)
		vx := gen_int64(695)
		v, ok := src.MapFixed32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[99] {
		k := gen_uint32(596)
		vx := gen_int64(696)
		v, ok := src.MapFixed32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[99] {
		k := gen_uint32(597)
		vx := gen_int64(697)
		v, ok := src.MapFixed32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[99] {
		k := gen_uint32(598)
		vx := gen_int64(698)
		v, ok := src.MapFixed32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[99] {
		k := gen_uint32(599)
		vx := gen_int64(699)
		v, ok := src.MapFixed32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[99] {
		k := gen_uint32(600)
		vx := gen_int64(700)
		v, ok := src.MapFixed32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[100] {
		k := gen_uint32(601)
		vx := gen_uint32(701)
		v, ok := src.MapFixed32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[100] {
		k := gen_uint32(602)
		vx := gen_uint32(702)
		v, ok := src.MapFixed32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[100] {
		k := gen_uint32(603)
		vx := gen_uint32(703)
		v, ok := src.MapFixed32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[100] {
		k := gen_uint32(604)
		vx := gen_uint32(704)
		v, ok := src.MapFixed32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[100] {
		k := gen_uint32(605)
		vx := gen_uint32(705)
		v, ok := src.MapFixed32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[100] {
		k := gen_uint32(606)
		vx := gen_uint32(706)
		v, ok := src.MapFixed32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[101] {
		k := gen_uint32(607)
		vx := gen_uint64(707)
		v, ok := src.MapFixed32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[101] {
		k := gen_uint32(608)
		vx := gen_uint64(708)
		v, ok := src.MapFixed32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[101] {
		k := gen_uint32(609)
		vx := gen_uint64(709)
		v, ok := src.MapFixed32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[101] {
		k := gen_uint32(610)
		vx := gen_uint64(710)
		v, ok := src.MapFixed32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[101] {
		k := gen_uint32(611)
		vx := gen_uint64(711)
		v, ok := src.MapFixed32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[101] {
		k := gen_uint32(612)
		vx := gen_uint64(712)
		v, ok := src.MapFixed32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[102] {
		k := gen_uint32(613)
		vx := gen_int32(713)
		v, ok := src.MapFixed32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[102] {
		k := gen_uint32(614)
		vx := gen_int32(714)
		v, ok := src.MapFixed32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[102] {
		k := gen_uint32(615)
		vx := gen_int32(715)
		v, ok := src.MapFixed32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[102] {
		k := gen_uint32(616)
		vx := gen_int32(716)
		v, ok := src.MapFixed32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[102] {
		k := gen_uint32(617)
		vx := gen_int32(717)
		v, ok := src.MapFixed32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[102] {
		k := gen_uint32(618)
		vx := gen_int32(718)
		v, ok := src.MapFixed32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[103] {
		k := gen_uint32(619)
		vx := gen_int64(719)
		v, ok := src.MapFixed32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[103] {
		k := gen_uint32(620)
		vx := gen_int64(720)
		v, ok := src.MapFixed32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[103] {
		k := gen_uint32(621)
		vx := gen_int64(721)
		v, ok := src.MapFixed32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[103] {
		k := gen_uint32(622)
		vx := gen_int64(722)
		v, ok := src.MapFixed32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[103] {
		k := gen_uint32(623)
		vx := gen_int64(723)
		v, ok := src.MapFixed32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[103] {
		k := gen_uint32(624)
		vx := gen_int64(724)
		v, ok := src.MapFixed32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[104] {
		k := gen_uint32(625)
		vx := gen_uint32(725)
		v, ok := src.MapFixed32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[104] {
		k := gen_uint32(626)
		vx := gen_uint32(726)
		v, ok := src.MapFixed32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[104] {
		k := gen_uint32(627)
		vx := gen_uint32(727)
		v, ok := src.MapFixed32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[104] {
		k := gen_uint32(628)
		vx := gen_uint32(728)
		v, ok := src.MapFixed32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[104] {
		k := gen_uint32(629)
		vx := gen_uint32(729)
		v, ok := src.MapFixed32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[104] {
		k := gen_uint32(630)
		vx := gen_uint32(730)
		v, ok := src.MapFixed32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[105] {
		k := gen_uint32(631)
		vx := gen_uint64(731)
		v, ok := src.MapFixed32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[105] {
		k := gen_uint32(632)
		vx := gen_uint64(732)
		v, ok := src.MapFixed32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[105] {
		k := gen_uint32(633)
		vx := gen_uint64(733)
		v, ok := src.MapFixed32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[105] {
		k := gen_uint32(634)
		vx := gen_uint64(734)
		v, ok := src.MapFixed32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[105] {
		k := gen_uint32(635)
		vx := gen_uint64(735)
		v, ok := src.MapFixed32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[105] {
		k := gen_uint32(636)
		vx := gen_uint64(736)
		v, ok := src.MapFixed32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[106] {
		k := gen_uint32(637)
		vx := gen_int32(737)
		v, ok := src.MapFixed32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[106] {
		k := gen_uint32(638)
		vx := gen_int32(738)
		v, ok := src.MapFixed32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[106] {
		k := gen_uint32(639)
		vx := gen_int32(739)
		v, ok := src.MapFixed32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[106] {
		k := gen_uint32(640)
		vx := gen_int32(740)
		v, ok := src.MapFixed32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[106] {
		k := gen_uint32(641)
		vx := gen_int32(741)
		v, ok := src.MapFixed32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[106] {
		k := gen_uint32(642)
		vx := gen_int32(742)
		v, ok := src.MapFixed32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[107] {
		k := gen_uint32(643)
		vx := gen_int64(743)
		v, ok := src.MapFixed32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[107] {
		k := gen_uint32(644)
		vx := gen_int64(744)
		v, ok := src.MapFixed32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[107] {
		k := gen_uint32(645)
		vx := gen_int64(745)
		v, ok := src.MapFixed32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[107] {
		k := gen_uint32(646)
		vx := gen_int64(746)
		v, ok := src.MapFixed32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[107] {
		k := gen_uint32(647)
		vx := gen_int64(747)
		v, ok := src.MapFixed32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[107] {
		k := gen_uint32(648)
		vx := gen_int64(748)
		v, ok := src.MapFixed32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[108] {
		k := gen_uint32(649)
		vx := gen_bool(749)
		v, ok := src.MapFixed32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[108] {
		k := gen_uint32(650)
		vx := gen_bool(750)
		v, ok := src.MapFixed32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[108] {
		k := gen_uint32(651)
		vx := gen_bool(751)
		v, ok := src.MapFixed32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[108] {
		k := gen_uint32(652)
		vx := gen_bool(752)
		v, ok := src.MapFixed32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[108] {
		k := gen_uint32(653)
		vx := gen_bool(753)
		v, ok := src.MapFixed32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[108] {
		k := gen_uint32(654)
		vx := gen_bool(754)
		v, ok := src.MapFixed32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[109] {
		k := gen_uint32(655)
		vx := gen_string(755)
		v, ok := src.MapFixed32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[109] {
		k := gen_uint32(656)
		vx := gen_string(756)
		v, ok := src.MapFixed32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[109] {
		k := gen_uint32(657)
		vx := gen_string(757)
		v, ok := src.MapFixed32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[109] {
		k := gen_uint32(658)
		vx := gen_string(758)
		v, ok := src.MapFixed32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[109] {
		k := gen_uint32(659)
		vx := gen_string(759)
		v, ok := src.MapFixed32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[109] {
		k := gen_uint32(660)
		vx := gen_string(760)
		v, ok := src.MapFixed32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[110] {
		k := gen_uint32(661)
		vx := gen_bytes(761)
		v, ok := src.MapFixed32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[110] {
		k := gen_uint32(662)
		vx := gen_bytes(762)
		v, ok := src.MapFixed32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[110] {
		k := gen_uint32(663)
		vx := gen_bytes(763)
		v, ok := src.MapFixed32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[110] {
		k := gen_uint32(664)
		vx := gen_bytes(764)
		v, ok := src.MapFixed32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[110] {
		k := gen_uint32(665)
		vx := gen_bytes(765)
		v, ok := src.MapFixed32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[110] {
		k := gen_uint32(666)
		vx := gen_bytes(766)
		v, ok := src.MapFixed32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[111] {
		k := gen_uint32(667)
		vx := gen_Item(767)
		v, ok := src.MapFixed32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[111] {
		k := gen_uint32(668)
		vx := gen_Item(768)
		v, ok := src.MapFixed32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[111] {
		k := gen_uint32(669)
		vx := gen_Item(769)
		v, ok := src.MapFixed32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[111] {
		k := gen_uint32(670)
		vx := gen_Item(770)
		v, ok := src.MapFixed32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[111] {
		k := gen_uint32(671)
		vx := gen_Item(771)
		v, ok := src.MapFixed32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[111] {
		k := gen_uint32(672)
		vx := gen_Item(772)
		v, ok := src.MapFixed32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[112] {
		k := gen_uint64(673)
		vx := gen_float64(773)
		v, ok := src.MapFixed64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[112] {
		k := gen_uint64(674)
		vx := gen_float64(774)
		v, ok := src.MapFixed64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[112] {
		k := gen_uint64(675)
		vx := gen_float64(775)
		v, ok := src.MapFixed64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[112] {
		k := gen_uint64(676)
		vx := gen_float64(776)
		v, ok := src.MapFixed64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[112] {
		k := gen_uint64(677)
		vx := gen_float64(777)
		v, ok := src.MapFixed64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[112] {
		k := gen_uint64(678)
		vx := gen_float64(778)
		v, ok := src.MapFixed64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[113] {
		k := gen_uint64(679)
		vx := gen_float32(779)
		v, ok := src.MapFixed64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[113] {
		k := gen_uint64(680)
		vx := gen_float32(780)
		v, ok := src.MapFixed64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[113] {
		k := gen_uint64(681)
		vx := gen_float32(781)
		v, ok := src.MapFixed64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[113] {
		k := gen_uint64(682)
		vx := gen_float32(782)
		v, ok := src.MapFixed64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[113] {
		k := gen_uint64(683)
		vx := gen_float32(783)
		v, ok := src.MapFixed64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[113] {
		k := gen_uint64(684)
		vx := gen_float32(784)
		v, ok := src.MapFixed64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[114] {
		k := gen_uint64(685)
		vx := gen_int32(785)
		v, ok := src.MapFixed64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[114] {
		k := gen_uint64(686)
		vx := gen_int32(786)
		v, ok := src.MapFixed64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[114] {
		k := gen_uint64(687)
		vx := gen_int32(787)
		v, ok := src.MapFixed64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[114] {
		k := gen_uint64(688)
		vx := gen_int32(788)
		v, ok := src.MapFixed64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[114] {
		k := gen_uint64(689)
		vx := gen_int32(789)
		v, ok := src.MapFixed64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[114] {
		k := gen_uint64(690)
		vx := gen_int32(790)
		v, ok := src.MapFixed64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[115] {
		k := gen_uint64(691)
		vx := gen_int64(791)
		v, ok := src.MapFixed64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[115] {
		k := gen_uint64(692)
		vx := gen_int64(792)
		v, ok := src.MapFixed64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[115] {
		k := gen_uint64(693)
		vx := gen_int64(793)
		v, ok := src.MapFixed64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[115] {
		k := gen_uint64(694)
		vx := gen_int64(794)
		v, ok := src.MapFixed64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[115] {
		k := gen_uint64(695)
		vx := gen_int64(795)
		v, ok := src.MapFixed64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[115] {
		k := gen_uint64(696)
		vx := gen_int64(796)
		v, ok := src.MapFixed64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[116] {
		k := gen_uint64(697)
		vx := gen_uint32(797)
		v, ok := src.MapFixed64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[116] {
		k := gen_uint64(698)
		vx := gen_uint32(798)
		v, ok := src.MapFixed64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[116] {
		k := gen_uint64(699)
		vx := gen_uint32(799)
		v, ok := src.MapFixed64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[116] {
		k := gen_uint64(700)
		vx := gen_uint32(800)
		v, ok := src.MapFixed64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[116] {
		k := gen_uint64(701)
		vx := gen_uint32(801)
		v, ok := src.MapFixed64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[116] {
		k := gen_uint64(702)
		vx := gen_uint32(802)
		v, ok := src.MapFixed64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[117] {
		k := gen_uint64(703)
		vx := gen_uint64(803)
		v, ok := src.MapFixed64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[117] {
		k := gen_uint64(704)
		vx := gen_uint64(804)
		v, ok := src.MapFixed64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[117] {
		k := gen_uint64(705)
		vx := gen_uint64(805)
		v, ok := src.MapFixed64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[117] {
		k := gen_uint64(706)
		vx := gen_uint64(806)
		v, ok := src.MapFixed64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[117] {
		k := gen_uint64(707)
		vx := gen_uint64(807)
		v, ok := src.MapFixed64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[117] {
		k := gen_uint64(708)
		vx := gen_uint64(808)
		v, ok := src.MapFixed64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[118] {
		k := gen_uint64(709)
		vx := gen_int32(809)
		v, ok := src.MapFixed64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[118] {
		k := gen_uint64(710)
		vx := gen_int32(810)
		v, ok := src.MapFixed64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[118] {
		k := gen_uint64(711)
		vx := gen_int32(811)
		v, ok := src.MapFixed64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[118] {
		k := gen_uint64(712)
		vx := gen_int32(812)
		v, ok := src.MapFixed64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[118] {
		k := gen_uint64(713)
		vx := gen_int32(813)
		v, ok := src.MapFixed64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[118] {
		k := gen_uint64(714)
		vx := gen_int32(814)
		v, ok := src.MapFixed64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[119] {
		k := gen_uint64(715)
		vx := gen_int64(815)
		v, ok := src.MapFixed64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[119] {
		k := gen_uint64(716)
		vx := gen_int64(816)
		v, ok := src.MapFixed64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[119] {
		k := gen_uint64(717)
		vx := gen_int64(817)
		v, ok := src.MapFixed64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[119] {
		k := gen_uint64(718)
		vx := gen_int64(818)
		v, ok := src.MapFixed64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[119] {
		k := gen_uint64(719)
		vx := gen_int64(819)
		v, ok := src.MapFixed64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[119] {
		k := gen_uint64(720)
		vx := gen_int64(820)
		v, ok := src.MapFixed64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[120] {
		k := gen_uint64(721)
		vx := gen_uint32(821)
		v, ok := src.MapFixed64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[120] {
		k := gen_uint64(722)
		vx := gen_uint32(822)
		v, ok := src.MapFixed64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[120] {
		k := gen_uint64(723)
		vx := gen_uint32(823)
		v, ok := src.MapFixed64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[120] {
		k := gen_uint64(724)
		vx := gen_uint32(824)
		v, ok := src.MapFixed64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[120] {
		k := gen_uint64(725)
		vx := gen_uint32(825)
		v, ok := src.MapFixed64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[120] {
		k := gen_uint64(726)
		vx := gen_uint32(826)
		v, ok := src.MapFixed64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[121] {
		k := gen_uint64(727)
		vx := gen_uint64(827)
		v, ok := src.MapFixed64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[121] {
		k := gen_uint64(728)
		vx := gen_uint64(828)
		v, ok := src.MapFixed64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[121] {
		k := gen_uint64(729)
		vx := gen_uint64(829)
		v, ok := src.MapFixed64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[121] {
		k := gen_uint64(730)
		vx := gen_uint64(830)
		v, ok := src.MapFixed64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[121] {
		k := gen_uint64(731)
		vx := gen_uint64(831)
		v, ok := src.MapFixed64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[121] {
		k := gen_uint64(732)
		vx := gen_uint64(832)
		v, ok := src.MapFixed64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[122] {
		k := gen_uint64(733)
		vx := gen_int32(833)
		v, ok := src.MapFixed64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[122] {
		k := gen_uint64(734)
		vx := gen_int32(834)
		v, ok := src.MapFixed64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[122] {
		k := gen_uint64(735)
		vx := gen_int32(835)
		v, ok := src.MapFixed64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[122] {
		k := gen_uint64(736)
		vx := gen_int32(836)
		v, ok := src.MapFixed64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[122] {
		k := gen_uint64(737)
		vx := gen_int32(837)
		v, ok := src.MapFixed64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[122] {
		k := gen_uint64(738)
		vx := gen_int32(838)
		v, ok := src.MapFixed64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[123] {
		k := gen_uint64(739)
		vx := gen_int64(839)
		v, ok := src.MapFixed64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[123] {
		k := gen_uint64(740)
		vx := gen_int64(840)
		v, ok := src.MapFixed64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[123] {
		k := gen_uint64(741)
		vx := gen_int64(841)
		v, ok := src.MapFixed64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[123] {
		k := gen_uint64(742)
		vx := gen_int64(842)
		v, ok := src.MapFixed64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[123] {
		k := gen_uint64(743)
		vx := gen_int64(843)
		v, ok := src.MapFixed64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[123] {
		k := gen_uint64(744)
		vx := gen_int64(844)
		v, ok := src.MapFixed64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[124] {
		k := gen_uint64(745)
		vx := gen_bool(845)
		v, ok := src.MapFixed64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[124] {
		k := gen_uint64(746)
		vx := gen_bool(846)
		v, ok := src.MapFixed64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[124] {
		k := gen_uint64(747)
		vx := gen_bool(847)
		v, ok := src.MapFixed64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[124] {
		k := gen_uint64(748)
		vx := gen_bool(848)
		v, ok := src.MapFixed64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[124] {
		k := gen_uint64(749)
		vx := gen_bool(849)
		v, ok := src.MapFixed64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[124] {
		k := gen_uint64(750)
		vx := gen_bool(850)
		v, ok := src.MapFixed64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[125] {
		k := gen_uint64(751)
		vx := gen_string(851)
		v, ok := src.MapFixed64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[125] {
		k := gen_uint64(752)
		vx := gen_string(852)
		v, ok := src.MapFixed64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[125] {
		k := gen_uint64(753)
		vx := gen_string(853)
		v, ok := src.MapFixed64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[125] {
		k := gen_uint64(754)
		vx := gen_string(854)
		v, ok := src.MapFixed64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[125] {
		k := gen_uint64(755)
		vx := gen_string(855)
		v, ok := src.MapFixed64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[125] {
		k := gen_uint64(756)
		vx := gen_string(856)
		v, ok := src.MapFixed64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[126] {
		k := gen_uint64(757)
		vx := gen_bytes(857)
		v, ok := src.MapFixed64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[126] {
		k := gen_uint64(758)
		vx := gen_bytes(858)
		v, ok := src.MapFixed64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[126] {
		k := gen_uint64(759)
		vx := gen_bytes(859)
		v, ok := src.MapFixed64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[126] {
		k := gen_uint64(760)
		vx := gen_bytes(860)
		v, ok := src.MapFixed64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[126] {
		k := gen_uint64(761)
		vx := gen_bytes(861)
		v, ok := src.MapFixed64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[126] {
		k := gen_uint64(762)
		vx := gen_bytes(862)
		v, ok := src.MapFixed64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[127] {
		k := gen_uint64(763)
		vx := gen_Item(863)
		v, ok := src.MapFixed64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[127] {
		k := gen_uint64(764)
		vx := gen_Item(864)
		v, ok := src.MapFixed64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[127] {
		k := gen_uint64(765)
		vx := gen_Item(865)
		v, ok := src.MapFixed64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[127] {
		k := gen_uint64(766)
		vx := gen_Item(866)
		v, ok := src.MapFixed64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[127] {
		k := gen_uint64(767)
		vx := gen_Item(867)
		v, ok := src.MapFixed64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[127] {
		k := gen_uint64(768)
		vx := gen_Item(868)
		v, ok := src.MapFixed64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[128] {
		k := gen_int32(769)
		vx := gen_float64(869)
		v, ok := src.MapSfixed32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[128] {
		k := gen_int32(770)
		vx := gen_float64(870)
		v, ok := src.MapSfixed32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[128] {
		k := gen_int32(771)
		vx := gen_float64(871)
		v, ok := src.MapSfixed32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[128] {
		k := gen_int32(772)
		vx := gen_float64(872)
		v, ok := src.MapSfixed32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[128] {
		k := gen_int32(773)
		vx := gen_float64(873)
		v, ok := src.MapSfixed32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[128] {
		k := gen_int32(774)
		vx := gen_float64(874)
		v, ok := src.MapSfixed32Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[129] {
		k := gen_int32(775)
		vx := gen_float32(875)
		v, ok := src.MapSfixed32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[129] {
		k := gen_int32(776)
		vx := gen_float32(876)
		v, ok := src.MapSfixed32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[129] {
		k := gen_int32(777)
		vx := gen_float32(877)
		v, ok := src.MapSfixed32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[129] {
		k := gen_int32(778)
		vx := gen_float32(878)
		v, ok := src.MapSfixed32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[129] {
		k := gen_int32(779)
		vx := gen_float32(879)
		v, ok := src.MapSfixed32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[129] {
		k := gen_int32(780)
		vx := gen_float32(880)
		v, ok := src.MapSfixed32Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[130] {
		k := gen_int32(781)
		vx := gen_int32(881)
		v, ok := src.MapSfixed32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[130] {
		k := gen_int32(782)
		vx := gen_int32(882)
		v, ok := src.MapSfixed32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[130] {
		k := gen_int32(783)
		vx := gen_int32(883)
		v, ok := src.MapSfixed32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[130] {
		k := gen_int32(784)
		vx := gen_int32(884)
		v, ok := src.MapSfixed32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[130] {
		k := gen_int32(785)
		vx := gen_int32(885)
		v, ok := src.MapSfixed32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[130] {
		k := gen_int32(786)
		vx := gen_int32(886)
		v, ok := src.MapSfixed32Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[131] {
		k := gen_int32(787)
		vx := gen_int64(887)
		v, ok := src.MapSfixed32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[131] {
		k := gen_int32(788)
		vx := gen_int64(888)
		v, ok := src.MapSfixed32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[131] {
		k := gen_int32(789)
		vx := gen_int64(889)
		v, ok := src.MapSfixed32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[131] {
		k := gen_int32(790)
		vx := gen_int64(890)
		v, ok := src.MapSfixed32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[131] {
		k := gen_int32(791)
		vx := gen_int64(891)
		v, ok := src.MapSfixed32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[131] {
		k := gen_int32(792)
		vx := gen_int64(892)
		v, ok := src.MapSfixed32Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[132] {
		k := gen_int32(793)
		vx := gen_uint32(893)
		v, ok := src.MapSfixed32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[132] {
		k := gen_int32(794)
		vx := gen_uint32(894)
		v, ok := src.MapSfixed32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[132] {
		k := gen_int32(795)
		vx := gen_uint32(895)
		v, ok := src.MapSfixed32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[132] {
		k := gen_int32(796)
		vx := gen_uint32(896)
		v, ok := src.MapSfixed32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[132] {
		k := gen_int32(797)
		vx := gen_uint32(897)
		v, ok := src.MapSfixed32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[132] {
		k := gen_int32(798)
		vx := gen_uint32(898)
		v, ok := src.MapSfixed32Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[133] {
		k := gen_int32(799)
		vx := gen_uint64(899)
		v, ok := src.MapSfixed32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[133] {
		k := gen_int32(800)
		vx := gen_uint64(900)
		v, ok := src.MapSfixed32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[133] {
		k := gen_int32(801)
		vx := gen_uint64(901)
		v, ok := src.MapSfixed32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[133] {
		k := gen_int32(802)
		vx := gen_uint64(902)
		v, ok := src.MapSfixed32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[133] {
		k := gen_int32(803)
		vx := gen_uint64(903)
		v, ok := src.MapSfixed32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[133] {
		k := gen_int32(804)
		vx := gen_uint64(904)
		v, ok := src.MapSfixed32Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[134] {
		k := gen_int32(805)
		vx := gen_int32(905)
		v, ok := src.MapSfixed32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[134] {
		k := gen_int32(806)
		vx := gen_int32(906)
		v, ok := src.MapSfixed32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[134] {
		k := gen_int32(807)
		vx := gen_int32(907)
		v, ok := src.MapSfixed32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[134] {
		k := gen_int32(808)
		vx := gen_int32(908)
		v, ok := src.MapSfixed32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[134] {
		k := gen_int32(809)
		vx := gen_int32(909)
		v, ok := src.MapSfixed32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[134] {
		k := gen_int32(810)
		vx := gen_int32(910)
		v, ok := src.MapSfixed32Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[135] {
		k := gen_int32(811)
		vx := gen_int64(911)
		v, ok := src.MapSfixed32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[135] {
		k := gen_int32(812)
		vx := gen_int64(912)
		v, ok := src.MapSfixed32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[135] {
		k := gen_int32(813)
		vx := gen_int64(913)
		v, ok := src.MapSfixed32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[135] {
		k := gen_int32(814)
		vx := gen_int64(914)
		v, ok := src.MapSfixed32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[135] {
		k := gen_int32(815)
		vx := gen_int64(915)
		v, ok := src.MapSfixed32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[135] {
		k := gen_int32(816)
		vx := gen_int64(916)
		v, ok := src.MapSfixed32Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[136] {
		k := gen_int32(817)
		vx := gen_uint32(917)
		v, ok := src.MapSfixed32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[136] {
		k := gen_int32(818)
		vx := gen_uint32(918)
		v, ok := src.MapSfixed32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[136] {
		k := gen_int32(819)
		vx := gen_uint32(919)
		v, ok := src.MapSfixed32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[136] {
		k := gen_int32(820)
		vx := gen_uint32(920)
		v, ok := src.MapSfixed32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[136] {
		k := gen_int32(821)
		vx := gen_uint32(921)
		v, ok := src.MapSfixed32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[136] {
		k := gen_int32(822)
		vx := gen_uint32(922)
		v, ok := src.MapSfixed32Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[137] {
		k := gen_int32(823)
		vx := gen_uint64(923)
		v, ok := src.MapSfixed32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[137] {
		k := gen_int32(824)
		vx := gen_uint64(924)
		v, ok := src.MapSfixed32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[137] {
		k := gen_int32(825)
		vx := gen_uint64(925)
		v, ok := src.MapSfixed32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[137] {
		k := gen_int32(826)
		vx := gen_uint64(926)
		v, ok := src.MapSfixed32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[137] {
		k := gen_int32(827)
		vx := gen_uint64(927)
		v, ok := src.MapSfixed32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[137] {
		k := gen_int32(828)
		vx := gen_uint64(928)
		v, ok := src.MapSfixed32Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[138] {
		k := gen_int32(829)
		vx := gen_int32(929)
		v, ok := src.MapSfixed32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[138] {
		k := gen_int32(830)
		vx := gen_int32(930)
		v, ok := src.MapSfixed32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[138] {
		k := gen_int32(831)
		vx := gen_int32(931)
		v, ok := src.MapSfixed32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[138] {
		k := gen_int32(832)
		vx := gen_int32(932)
		v, ok := src.MapSfixed32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[138] {
		k := gen_int32(833)
		vx := gen_int32(933)
		v, ok := src.MapSfixed32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[138] {
		k := gen_int32(834)
		vx := gen_int32(934)
		v, ok := src.MapSfixed32Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[139] {
		k := gen_int32(835)
		vx := gen_int64(935)
		v, ok := src.MapSfixed32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[139] {
		k := gen_int32(836)
		vx := gen_int64(936)
		v, ok := src.MapSfixed32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[139] {
		k := gen_int32(837)
		vx := gen_int64(937)
		v, ok := src.MapSfixed32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[139] {
		k := gen_int32(838)
		vx := gen_int64(938)
		v, ok := src.MapSfixed32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[139] {
		k := gen_int32(839)
		vx := gen_int64(939)
		v, ok := src.MapSfixed32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[139] {
		k := gen_int32(840)
		vx := gen_int64(940)
		v, ok := src.MapSfixed32Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[140] {
		k := gen_int32(841)
		vx := gen_bool(941)
		v, ok := src.MapSfixed32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[140] {
		k := gen_int32(842)
		vx := gen_bool(942)
		v, ok := src.MapSfixed32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[140] {
		k := gen_int32(843)
		vx := gen_bool(943)
		v, ok := src.MapSfixed32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[140] {
		k := gen_int32(844)
		vx := gen_bool(944)
		v, ok := src.MapSfixed32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[140] {
		k := gen_int32(845)
		vx := gen_bool(945)
		v, ok := src.MapSfixed32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[140] {
		k := gen_int32(846)
		vx := gen_bool(946)
		v, ok := src.MapSfixed32Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[141] {
		k := gen_int32(847)
		vx := gen_string(947)
		v, ok := src.MapSfixed32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[141] {
		k := gen_int32(848)
		vx := gen_string(948)
		v, ok := src.MapSfixed32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[141] {
		k := gen_int32(849)
		vx := gen_string(949)
		v, ok := src.MapSfixed32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[141] {
		k := gen_int32(850)
		vx := gen_string(950)
		v, ok := src.MapSfixed32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[141] {
		k := gen_int32(851)
		vx := gen_string(951)
		v, ok := src.MapSfixed32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[141] {
		k := gen_int32(852)
		vx := gen_string(952)
		v, ok := src.MapSfixed32String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[142] {
		k := gen_int32(853)
		vx := gen_bytes(953)
		v, ok := src.MapSfixed32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[142] {
		k := gen_int32(854)
		vx := gen_bytes(954)
		v, ok := src.MapSfixed32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[142] {
		k := gen_int32(855)
		vx := gen_bytes(955)
		v, ok := src.MapSfixed32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[142] {
		k := gen_int32(856)
		vx := gen_bytes(956)
		v, ok := src.MapSfixed32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[142] {
		k := gen_int32(857)
		vx := gen_bytes(957)
		v, ok := src.MapSfixed32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[142] {
		k := gen_int32(858)
		vx := gen_bytes(958)
		v, ok := src.MapSfixed32Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[143] {
		k := gen_int32(859)
		vx := gen_Item(959)
		v, ok := src.MapSfixed32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[143] {
		k := gen_int32(860)
		vx := gen_Item(960)
		v, ok := src.MapSfixed32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[143] {
		k := gen_int32(861)
		vx := gen_Item(961)
		v, ok := src.MapSfixed32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[143] {
		k := gen_int32(862)
		vx := gen_Item(962)
		v, ok := src.MapSfixed32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[143] {
		k := gen_int32(863)
		vx := gen_Item(963)
		v, ok := src.MapSfixed32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[143] {
		k := gen_int32(864)
		vx := gen_Item(964)
		v, ok := src.MapSfixed32_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[144] {
		k := gen_int64(865)
		vx := gen_float64(965)
		v, ok := src.MapSfixed64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[144] {
		k := gen_int64(866)
		vx := gen_float64(966)
		v, ok := src.MapSfixed64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[144] {
		k := gen_int64(867)
		vx := gen_float64(967)
		v, ok := src.MapSfixed64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[144] {
		k := gen_int64(868)
		vx := gen_float64(968)
		v, ok := src.MapSfixed64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[144] {
		k := gen_int64(869)
		vx := gen_float64(969)
		v, ok := src.MapSfixed64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[144] {
		k := gen_int64(870)
		vx := gen_float64(970)
		v, ok := src.MapSfixed64Double[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[145] {
		k := gen_int64(871)
		vx := gen_float32(971)
		v, ok := src.MapSfixed64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[145] {
		k := gen_int64(872)
		vx := gen_float32(972)
		v, ok := src.MapSfixed64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[145] {
		k := gen_int64(873)
		vx := gen_float32(973)
		v, ok := src.MapSfixed64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[145] {
		k := gen_int64(874)
		vx := gen_float32(974)
		v, ok := src.MapSfixed64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[145] {
		k := gen_int64(875)
		vx := gen_float32(975)
		v, ok := src.MapSfixed64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[145] {
		k := gen_int64(876)
		vx := gen_float32(976)
		v, ok := src.MapSfixed64Float[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[146] {
		k := gen_int64(877)
		vx := gen_int32(977)
		v, ok := src.MapSfixed64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[146] {
		k := gen_int64(878)
		vx := gen_int32(978)
		v, ok := src.MapSfixed64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[146] {
		k := gen_int64(879)
		vx := gen_int32(979)
		v, ok := src.MapSfixed64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[146] {
		k := gen_int64(880)
		vx := gen_int32(980)
		v, ok := src.MapSfixed64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[146] {
		k := gen_int64(881)
		vx := gen_int32(981)
		v, ok := src.MapSfixed64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[146] {
		k := gen_int64(882)
		vx := gen_int32(982)
		v, ok := src.MapSfixed64Int32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[147] {
		k := gen_int64(883)
		vx := gen_int64(983)
		v, ok := src.MapSfixed64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[147] {
		k := gen_int64(884)
		vx := gen_int64(984)
		v, ok := src.MapSfixed64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[147] {
		k := gen_int64(885)
		vx := gen_int64(985)
		v, ok := src.MapSfixed64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[147] {
		k := gen_int64(886)
		vx := gen_int64(986)
		v, ok := src.MapSfixed64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[147] {
		k := gen_int64(887)
		vx := gen_int64(987)
		v, ok := src.MapSfixed64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[147] {
		k := gen_int64(888)
		vx := gen_int64(988)
		v, ok := src.MapSfixed64Int64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[148] {
		k := gen_int64(889)
		vx := gen_uint32(989)
		v, ok := src.MapSfixed64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[148] {
		k := gen_int64(890)
		vx := gen_uint32(990)
		v, ok := src.MapSfixed64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[148] {
		k := gen_int64(891)
		vx := gen_uint32(991)
		v, ok := src.MapSfixed64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[148] {
		k := gen_int64(892)
		vx := gen_uint32(992)
		v, ok := src.MapSfixed64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[148] {
		k := gen_int64(893)
		vx := gen_uint32(993)
		v, ok := src.MapSfixed64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[148] {
		k := gen_int64(894)
		vx := gen_uint32(994)
		v, ok := src.MapSfixed64Uint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[149] {
		k := gen_int64(895)
		vx := gen_uint64(995)
		v, ok := src.MapSfixed64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[149] {
		k := gen_int64(896)
		vx := gen_uint64(996)
		v, ok := src.MapSfixed64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[149] {
		k := gen_int64(897)
		vx := gen_uint64(997)
		v, ok := src.MapSfixed64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[149] {
		k := gen_int64(898)
		vx := gen_uint64(998)
		v, ok := src.MapSfixed64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[149] {
		k := gen_int64(899)
		vx := gen_uint64(999)
		v, ok := src.MapSfixed64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[149] {
		k := gen_int64(900)
		vx := gen_uint64(1000)
		v, ok := src.MapSfixed64Uint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[150] {
		k := gen_int64(901)
		vx := gen_int32(1001)
		v, ok := src.MapSfixed64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[150] {
		k := gen_int64(902)
		vx := gen_int32(1002)
		v, ok := src.MapSfixed64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[150] {
		k := gen_int64(903)
		vx := gen_int32(1003)
		v, ok := src.MapSfixed64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[150] {
		k := gen_int64(904)
		vx := gen_int32(1004)
		v, ok := src.MapSfixed64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[150] {
		k := gen_int64(905)
		vx := gen_int32(1005)
		v, ok := src.MapSfixed64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[150] {
		k := gen_int64(906)
		vx := gen_int32(1006)
		v, ok := src.MapSfixed64Sint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[151] {
		k := gen_int64(907)
		vx := gen_int64(1007)
		v, ok := src.MapSfixed64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[151] {
		k := gen_int64(908)
		vx := gen_int64(1008)
		v, ok := src.MapSfixed64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[151] {
		k := gen_int64(909)
		vx := gen_int64(1009)
		v, ok := src.MapSfixed64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[151] {
		k := gen_int64(910)
		vx := gen_int64(1010)
		v, ok := src.MapSfixed64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[151] {
		k := gen_int64(911)
		vx := gen_int64(1011)
		v, ok := src.MapSfixed64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[151] {
		k := gen_int64(912)
		vx := gen_int64(1012)
		v, ok := src.MapSfixed64Sint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[152] {
		k := gen_int64(913)
		vx := gen_uint32(1013)
		v, ok := src.MapSfixed64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[152] {
		k := gen_int64(914)
		vx := gen_uint32(1014)
		v, ok := src.MapSfixed64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[152] {
		k := gen_int64(915)
		vx := gen_uint32(1015)
		v, ok := src.MapSfixed64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[152] {
		k := gen_int64(916)
		vx := gen_uint32(1016)
		v, ok := src.MapSfixed64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[152] {
		k := gen_int64(917)
		vx := gen_uint32(1017)
		v, ok := src.MapSfixed64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[152] {
		k := gen_int64(918)
		vx := gen_uint32(1018)
		v, ok := src.MapSfixed64Fixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[153] {
		k := gen_int64(919)
		vx := gen_uint64(1019)
		v, ok := src.MapSfixed64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[153] {
		k := gen_int64(920)
		vx := gen_uint64(1020)
		v, ok := src.MapSfixed64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[153] {
		k := gen_int64(921)
		vx := gen_uint64(1021)
		v, ok := src.MapSfixed64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[153] {
		k := gen_int64(922)
		vx := gen_uint64(1022)
		v, ok := src.MapSfixed64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[153] {
		k := gen_int64(923)
		vx := gen_uint64(1023)
		v, ok := src.MapSfixed64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[153] {
		k := gen_int64(924)
		vx := gen_uint64(1024)
		v, ok := src.MapSfixed64Fixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[154] {
		k := gen_int64(925)
		vx := gen_int32(1025)
		v, ok := src.MapSfixed64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[154] {
		k := gen_int64(926)
		vx := gen_int32(1026)
		v, ok := src.MapSfixed64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[154] {
		k := gen_int64(927)
		vx := gen_int32(1027)
		v, ok := src.MapSfixed64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[154] {
		k := gen_int64(928)
		vx := gen_int32(1028)
		v, ok := src.MapSfixed64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[154] {
		k := gen_int64(929)
		vx := gen_int32(1029)
		v, ok := src.MapSfixed64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[154] {
		k := gen_int64(930)
		vx := gen_int32(1030)
		v, ok := src.MapSfixed64Sfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[155] {
		k := gen_int64(931)
		vx := gen_int64(1031)
		v, ok := src.MapSfixed64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[155] {
		k := gen_int64(932)
		vx := gen_int64(1032)
		v, ok := src.MapSfixed64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[155] {
		k := gen_int64(933)
		vx := gen_int64(1033)
		v, ok := src.MapSfixed64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[155] {
		k := gen_int64(934)
		vx := gen_int64(1034)
		v, ok := src.MapSfixed64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[155] {
		k := gen_int64(935)
		vx := gen_int64(1035)
		v, ok := src.MapSfixed64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[155] {
		k := gen_int64(936)
		vx := gen_int64(1036)
		v, ok := src.MapSfixed64Sfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[156] {
		k := gen_int64(937)
		vx := gen_bool(1037)
		v, ok := src.MapSfixed64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[156] {
		k := gen_int64(938)
		vx := gen_bool(1038)
		v, ok := src.MapSfixed64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[156] {
		k := gen_int64(939)
		vx := gen_bool(1039)
		v, ok := src.MapSfixed64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[156] {
		k := gen_int64(940)
		vx := gen_bool(1040)
		v, ok := src.MapSfixed64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[156] {
		k := gen_int64(941)
		vx := gen_bool(1041)
		v, ok := src.MapSfixed64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[156] {
		k := gen_int64(942)
		vx := gen_bool(1042)
		v, ok := src.MapSfixed64Bool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[157] {
		k := gen_int64(943)
		vx := gen_string(1043)
		v, ok := src.MapSfixed64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[157] {
		k := gen_int64(944)
		vx := gen_string(1044)
		v, ok := src.MapSfixed64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[157] {
		k := gen_int64(945)
		vx := gen_string(1045)
		v, ok := src.MapSfixed64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[157] {
		k := gen_int64(946)
		vx := gen_string(1046)
		v, ok := src.MapSfixed64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[157] {
		k := gen_int64(947)
		vx := gen_string(1047)
		v, ok := src.MapSfixed64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[157] {
		k := gen_int64(948)
		vx := gen_string(1048)
		v, ok := src.MapSfixed64String[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[158] {
		k := gen_int64(949)
		vx := gen_bytes(1049)
		v, ok := src.MapSfixed64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[158] {
		k := gen_int64(950)
		vx := gen_bytes(1050)
		v, ok := src.MapSfixed64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[158] {
		k := gen_int64(951)
		vx := gen_bytes(1051)
		v, ok := src.MapSfixed64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[158] {
		k := gen_int64(952)
		vx := gen_bytes(1052)
		v, ok := src.MapSfixed64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[158] {
		k := gen_int64(953)
		vx := gen_bytes(1053)
		v, ok := src.MapSfixed64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[158] {
		k := gen_int64(954)
		vx := gen_bytes(1054)
		v, ok := src.MapSfixed64Bytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[159] {
		k := gen_int64(955)
		vx := gen_Item(1055)
		v, ok := src.MapSfixed64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[159] {
		k := gen_int64(956)
		vx := gen_Item(1056)
		v, ok := src.MapSfixed64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[159] {
		k := gen_int64(957)
		vx := gen_Item(1057)
		v, ok := src.MapSfixed64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[159] {
		k := gen_int64(958)
		vx := gen_Item(1058)
		v, ok := src.MapSfixed64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[159] {
		k := gen_int64(959)
		vx := gen_Item(1059)
		v, ok := src.MapSfixed64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[159] {
		k := gen_int64(960)
		vx := gen_Item(1060)
		v, ok := src.MapSfixed64_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[160] {
		k := gen_bool(961)
		vx := gen_float64(1061)
		v, ok := src.MapBoolDouble[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[160] {
		k := gen_bool(962)
		vx := gen_float64(1062)
		v, ok := src.MapBoolDouble[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[161] {
		k := gen_bool(963)
		vx := gen_float32(1063)
		v, ok := src.MapBoolFloat[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[161] {
		k := gen_bool(964)
		vx := gen_float32(1064)
		v, ok := src.MapBoolFloat[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[162] {
		k := gen_bool(965)
		vx := gen_int32(1065)
		v, ok := src.MapBoolInt32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[162] {
		k := gen_bool(966)
		vx := gen_int32(1066)
		v, ok := src.MapBoolInt32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[163] {
		k := gen_bool(967)
		vx := gen_int64(1067)
		v, ok := src.MapBoolInt64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[163] {
		k := gen_bool(968)
		vx := gen_int64(1068)
		v, ok := src.MapBoolInt64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[164] {
		k := gen_bool(969)
		vx := gen_uint32(1069)
		v, ok := src.MapBoolUint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[164] {
		k := gen_bool(970)
		vx := gen_uint32(1070)
		v, ok := src.MapBoolUint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[165] {
		k := gen_bool(971)
		vx := gen_uint64(1071)
		v, ok := src.MapBoolUint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[165] {
		k := gen_bool(972)
		vx := gen_uint64(1072)
		v, ok := src.MapBoolUint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[166] {
		k := gen_bool(973)
		vx := gen_int32(1073)
		v, ok := src.MapBoolSint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[166] {
		k := gen_bool(974)
		vx := gen_int32(1074)
		v, ok := src.MapBoolSint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[167] {
		k := gen_bool(975)
		vx := gen_int64(1075)
		v, ok := src.MapBoolSint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[167] {
		k := gen_bool(976)
		vx := gen_int64(1076)
		v, ok := src.MapBoolSint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[168] {
		k := gen_bool(977)
		vx := gen_uint32(1077)
		v, ok := src.MapBoolFixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[168] {
		k := gen_bool(978)
		vx := gen_uint32(1078)
		v, ok := src.MapBoolFixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[169] {
		k := gen_bool(979)
		vx := gen_uint64(1079)
		v, ok := src.MapBoolFixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[169] {
		k := gen_bool(980)
		vx := gen_uint64(1080)
		v, ok := src.MapBoolFixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[170] {
		k := gen_bool(981)
		vx := gen_int32(1081)
		v, ok := src.MapBoolSfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[170] {
		k := gen_bool(982)
		vx := gen_int32(1082)
		v, ok := src.MapBoolSfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[171] {
		k := gen_bool(983)
		vx := gen_int64(1083)
		v, ok := src.MapBoolSfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[171] {
		k := gen_bool(984)
		vx := gen_int64(1084)
		v, ok := src.MapBoolSfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[172] {
		k := gen_bool(985)
		vx := gen_bool(1085)
		v, ok := src.MapBoolBool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[172] {
		k := gen_bool(986)
		vx := gen_bool(1086)
		v, ok := src.MapBoolBool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[173] {
		k := gen_bool(987)
		vx := gen_string(1087)
		v, ok := src.MapBoolString[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[173] {
		k := gen_bool(988)
		vx := gen_string(1088)
		v, ok := src.MapBoolString[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[174] {
		k := gen_bool(989)
		vx := gen_bytes(1089)
		v, ok := src.MapBoolBytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[174] {
		k := gen_bool(990)
		vx := gen_bytes(1090)
		v, ok := src.MapBoolBytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[175] {
		k := gen_bool(991)
		vx := gen_Item(1091)
		v, ok := src.MapBool_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[175] {
		k := gen_bool(992)
		vx := gen_Item(1092)
		v, ok := src.MapBool_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[176] {
		k := gen_string(993)
		vx := gen_float64(1093)
		v, ok := src.MapStringDouble[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[176] {
		k := gen_string(994)
		vx := gen_float64(1094)
		v, ok := src.MapStringDouble[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[176] {
		k := gen_string(995)
		vx := gen_float64(1095)
		v, ok := src.MapStringDouble[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[176] {
		k := gen_string(996)
		vx := gen_float64(1096)
		v, ok := src.MapStringDouble[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[176] {
		k := gen_string(997)
		vx := gen_float64(1097)
		v, ok := src.MapStringDouble[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[176] {
		k := gen_string(998)
		vx := gen_float64(1098)
		v, ok := src.MapStringDouble[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[177] {
		k := gen_string(999)
		vx := gen_float32(1099)
		v, ok := src.MapStringFloat[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[177] {
		k := gen_string(1000)
		vx := gen_float32(1100)
		v, ok := src.MapStringFloat[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[177] {
		k := gen_string(1001)
		vx := gen_float32(1101)
		v, ok := src.MapStringFloat[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[177] {
		k := gen_string(1002)
		vx := gen_float32(1102)
		v, ok := src.MapStringFloat[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[177] {
		k := gen_string(1003)
		vx := gen_float32(1103)
		v, ok := src.MapStringFloat[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[177] {
		k := gen_string(1004)
		vx := gen_float32(1104)
		v, ok := src.MapStringFloat[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[178] {
		k := gen_string(1005)
		vx := gen_int32(1105)
		v, ok := src.MapStringInt32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[178] {
		k := gen_string(1006)
		vx := gen_int32(1106)
		v, ok := src.MapStringInt32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[178] {
		k := gen_string(1007)
		vx := gen_int32(1107)
		v, ok := src.MapStringInt32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[178] {
		k := gen_string(1008)
		vx := gen_int32(1108)
		v, ok := src.MapStringInt32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[178] {
		k := gen_string(1009)
		vx := gen_int32(1109)
		v, ok := src.MapStringInt32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[178] {
		k := gen_string(1010)
		vx := gen_int32(1110)
		v, ok := src.MapStringInt32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[179] {
		k := gen_string(1011)
		vx := gen_int64(1111)
		v, ok := src.MapStringInt64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[179] {
		k := gen_string(1012)
		vx := gen_int64(1112)
		v, ok := src.MapStringInt64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[179] {
		k := gen_string(1013)
		vx := gen_int64(1113)
		v, ok := src.MapStringInt64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[179] {
		k := gen_string(1014)
		vx := gen_int64(1114)
		v, ok := src.MapStringInt64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[179] {
		k := gen_string(1015)
		vx := gen_int64(1115)
		v, ok := src.MapStringInt64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[179] {
		k := gen_string(1016)
		vx := gen_int64(1116)
		v, ok := src.MapStringInt64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[180] {
		k := gen_string(1017)
		vx := gen_uint32(1117)
		v, ok := src.MapStringUint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[180] {
		k := gen_string(1018)
		vx := gen_uint32(1118)
		v, ok := src.MapStringUint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[180] {
		k := gen_string(1019)
		vx := gen_uint32(1119)
		v, ok := src.MapStringUint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[180] {
		k := gen_string(1020)
		vx := gen_uint32(1120)
		v, ok := src.MapStringUint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[180] {
		k := gen_string(1021)
		vx := gen_uint32(1121)
		v, ok := src.MapStringUint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[180] {
		k := gen_string(1022)
		vx := gen_uint32(1122)
		v, ok := src.MapStringUint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[181] {
		k := gen_string(1023)
		vx := gen_uint64(1123)
		v, ok := src.MapStringUint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[181] {
		k := gen_string(1024)
		vx := gen_uint64(1124)
		v, ok := src.MapStringUint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[181] {
		k := gen_string(1025)
		vx := gen_uint64(1125)
		v, ok := src.MapStringUint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[181] {
		k := gen_string(1026)
		vx := gen_uint64(1126)
		v, ok := src.MapStringUint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[181] {
		k := gen_string(1027)
		vx := gen_uint64(1127)
		v, ok := src.MapStringUint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[181] {
		k := gen_string(1028)
		vx := gen_uint64(1128)
		v, ok := src.MapStringUint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[182] {
		k := gen_string(1029)
		vx := gen_int32(1129)
		v, ok := src.MapStringSint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[182] {
		k := gen_string(1030)
		vx := gen_int32(1130)
		v, ok := src.MapStringSint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[182] {
		k := gen_string(1031)
		vx := gen_int32(1131)
		v, ok := src.MapStringSint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[182] {
		k := gen_string(1032)
		vx := gen_int32(1132)
		v, ok := src.MapStringSint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[182] {
		k := gen_string(1033)
		vx := gen_int32(1133)
		v, ok := src.MapStringSint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[182] {
		k := gen_string(1034)
		vx := gen_int32(1134)
		v, ok := src.MapStringSint32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[183] {
		k := gen_string(1035)
		vx := gen_int64(1135)
		v, ok := src.MapStringSint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[183] {
		k := gen_string(1036)
		vx := gen_int64(1136)
		v, ok := src.MapStringSint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[183] {
		k := gen_string(1037)
		vx := gen_int64(1137)
		v, ok := src.MapStringSint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[183] {
		k := gen_string(1038)
		vx := gen_int64(1138)
		v, ok := src.MapStringSint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[183] {
		k := gen_string(1039)
		vx := gen_int64(1139)
		v, ok := src.MapStringSint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[183] {
		k := gen_string(1040)
		vx := gen_int64(1140)
		v, ok := src.MapStringSint64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[184] {
		k := gen_string(1041)
		vx := gen_uint32(1141)
		v, ok := src.MapStringFixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[184] {
		k := gen_string(1042)
		vx := gen_uint32(1142)
		v, ok := src.MapStringFixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[184] {
		k := gen_string(1043)
		vx := gen_uint32(1143)
		v, ok := src.MapStringFixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[184] {
		k := gen_string(1044)
		vx := gen_uint32(1144)
		v, ok := src.MapStringFixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[184] {
		k := gen_string(1045)
		vx := gen_uint32(1145)
		v, ok := src.MapStringFixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[184] {
		k := gen_string(1046)
		vx := gen_uint32(1146)
		v, ok := src.MapStringFixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[185] {
		k := gen_string(1047)
		vx := gen_uint64(1147)
		v, ok := src.MapStringFixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[185] {
		k := gen_string(1048)
		vx := gen_uint64(1148)
		v, ok := src.MapStringFixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[185] {
		k := gen_string(1049)
		vx := gen_uint64(1149)
		v, ok := src.MapStringFixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[185] {
		k := gen_string(1050)
		vx := gen_uint64(1150)
		v, ok := src.MapStringFixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[185] {
		k := gen_string(1051)
		vx := gen_uint64(1151)
		v, ok := src.MapStringFixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[185] {
		k := gen_string(1052)
		vx := gen_uint64(1152)
		v, ok := src.MapStringFixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[186] {
		k := gen_string(1053)
		vx := gen_int32(1153)
		v, ok := src.MapStringSfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[186] {
		k := gen_string(1054)
		vx := gen_int32(1154)
		v, ok := src.MapStringSfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[186] {
		k := gen_string(1055)
		vx := gen_int32(1155)
		v, ok := src.MapStringSfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[186] {
		k := gen_string(1056)
		vx := gen_int32(1156)
		v, ok := src.MapStringSfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[186] {
		k := gen_string(1057)
		vx := gen_int32(1157)
		v, ok := src.MapStringSfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[186] {
		k := gen_string(1058)
		vx := gen_int32(1158)
		v, ok := src.MapStringSfixed32[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[187] {
		k := gen_string(1059)
		vx := gen_int64(1159)
		v, ok := src.MapStringSfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[187] {
		k := gen_string(1060)
		vx := gen_int64(1160)
		v, ok := src.MapStringSfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[187] {
		k := gen_string(1061)
		vx := gen_int64(1161)
		v, ok := src.MapStringSfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[187] {
		k := gen_string(1062)
		vx := gen_int64(1162)
		v, ok := src.MapStringSfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[187] {
		k := gen_string(1063)
		vx := gen_int64(1163)
		v, ok := src.MapStringSfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[187] {
		k := gen_string(1064)
		vx := gen_int64(1164)
		v, ok := src.MapStringSfixed64[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[188] {
		k := gen_string(1065)
		vx := gen_bool(1165)
		v, ok := src.MapStringBool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[188] {
		k := gen_string(1066)
		vx := gen_bool(1166)
		v, ok := src.MapStringBool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[188] {
		k := gen_string(1067)
		vx := gen_bool(1167)
		v, ok := src.MapStringBool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[188] {
		k := gen_string(1068)
		vx := gen_bool(1168)
		v, ok := src.MapStringBool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[188] {
		k := gen_string(1069)
		vx := gen_bool(1169)
		v, ok := src.MapStringBool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[188] {
		k := gen_string(1070)
		vx := gen_bool(1170)
		v, ok := src.MapStringBool[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[189] {
		k := gen_string(1071)
		vx := gen_string(1171)
		v, ok := src.MapStringString[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[189] {
		k := gen_string(1072)
		vx := gen_string(1172)
		v, ok := src.MapStringString[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[189] {
		k := gen_string(1073)
		vx := gen_string(1173)
		v, ok := src.MapStringString[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[189] {
		k := gen_string(1074)
		vx := gen_string(1174)
		v, ok := src.MapStringString[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[189] {
		k := gen_string(1075)
		vx := gen_string(1175)
		v, ok := src.MapStringString[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[189] {
		k := gen_string(1076)
		vx := gen_string(1176)
		v, ok := src.MapStringString[k]
		if ok {
			eq := v == vx
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[190] {
		k := gen_string(1077)
		vx := gen_bytes(1177)
		v, ok := src.MapStringBytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[190] {
		k := gen_string(1078)
		vx := gen_bytes(1178)
		v, ok := src.MapStringBytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[190] {
		k := gen_string(1079)
		vx := gen_bytes(1179)
		v, ok := src.MapStringBytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[190] {
		k := gen_string(1080)
		vx := gen_bytes(1180)
		v, ok := src.MapStringBytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[190] {
		k := gen_string(1081)
		vx := gen_bytes(1181)
		v, ok := src.MapStringBytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[190] {
		k := gen_string(1082)
		vx := gen_bytes(1182)
		v, ok := src.MapStringBytes[k]
		if ok {
			eq := bytes.Equal(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[191] {
		k := gen_string(1083)
		vx := gen_Item(1183)
		v, ok := src.MapString_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[191] {
		k := gen_string(1084)
		vx := gen_Item(1184)
		v, ok := src.MapString_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[191] {
		k := gen_string(1085)
		vx := gen_Item(1185)
		v, ok := src.MapString_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[191] {
		k := gen_string(1086)
		vx := gen_Item(1186)
		v, ok := src.MapString_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[191] {
		k := gen_string(1087)
		vx := gen_Item(1187)
		v, ok := src.MapString_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
	if include[191] {
		k := gen_string(1088)
		vx := gen_Item(1188)
		v, ok := src.MapString_Item[k]
		if ok {
			eq := itemEqual(v, vx)
			if !eq {
				t.Errorf("value %#v in map is incorrect, %#v != %#v", k, v, vx)
			}
		} else {
			t.Errorf("value %#v is missing from map", k)
		}
	}
}

package codec_test

import (
	"bytes"
	"context"
	"fmt"
	"testing"

	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/sealed"
)

func allhot() []bool {
	buf := make([]bool, 192)
	for i := range buf {
		buf[i] = true
	}
	return buf
}

func onehot(k int) []bool {
	buf := make([]bool, 192)
	buf[k] = true
	return buf
}

func TestSealedEncoding(t *testing.T) {
	var out Maps

	fillMaps(&out, allhot())

	enc := sealed.NewInsecure()
	ctx := context.TODO()

	_, err := enc.Encode(ctx, nil, nil, &out, nil)
	if err != nil {
		t.Fatal(err)
	}

	// fmt.Printf("----------\n%x\n", buf)
}

func TestSealedRoundTripLimited(t *testing.T) {
	for k := 0; k < 192; k++ {
		t.Run(fmt.Sprintf("hot_%d", k), func(t *testing.T) {
			t.Run(hotnames[k], func(t *testing.T) {
				defer func() {
					if p := recover(); p != nil {
						t.Fatalf("panicked: %v", p)
					}
				}()

				sealedRoundTripSingle(t, k)
			})
		})
	}
}

func TestSealedRoundTripSingle(t *testing.T) {
	sealedRoundTripSingle(t, 191)
}

func sealedRoundTripSingle(t *testing.T, k int) {
	var out Maps
	fillMaps(&out, onehot(k))

	//	nbuf, _ := proto.Marshal(&out)
	//fmt.Printf("native |%x|\n", nbuf)

	enc := sealed.NewInsecure()
	ctx := context.TODO()

	tp := ptr.String("a-type")

	buf, err := enc.Encode(ctx, nil, &tp, &out, nil)
	if err != nil {
		t.Fatal(err)
	}

	var ret Maps

	ld := sealed.ExpectedTypeLoader(&ret, tp)
	enc.Decode(ctx, ld, buf)

	checkMaps(t, &ret, onehot(k))
}

func TestSealedRoundTrip(t *testing.T) {
	var out Maps

	fillMaps(&out, allhot())

	enc := sealed.NewInsecure()
	ctx := context.TODO()

	tp := ptr.String("a-type")

	buf, err := enc.Encode(ctx, nil, &tp, &out, nil)
	if err != nil {
		t.Fatal(err)
	}

	var ret Maps

	ld := sealed.ExpectedTypeLoader(&ret, tp)

	enc.Decode(ctx, ld, buf)
}

// this test is needed because out codec handles maps differently from
// object fields for some reason
func TestKitchenRoundTrip(t *testing.T) {
	var kit KitchenSink

	kit.F64 = 17.125
	kit.F32 = 2.5
	kit.I32 = 3
	kit.I64 = 4
	kit.U32 = 5
	kit.U64 = 6
	kit.S32 = 7
	kit.S64 = 8
	kit.Fix32 = 0xcafffff3
	kit.Fix64 = 0xcafffffffffffff3
	kit.Sfix32 = -0x7ffffff3
	kit.Sfix64 = -0x6ffffffffffffff3
	kit.B = true
	kit.S = "hello, world"
	kit.Buf = []byte{0, 1, 2, 3, 4, 5, 6, 7}
	kit.M = &Maps{}
	fillMaps(kit.M, allhot())

	enc := sealed.NewInsecure()
	ctx := context.TODO()

	tp := ptr.String("k-type")

	buf, err := enc.Encode(ctx, nil, &tp, &kit, nil)
	if err != nil {
		t.Fatal(err)
	}

	var ret KitchenSink

	ld := sealed.ExpectedTypeLoader(&ret, tp)
	enc.Decode(ctx, ld, buf)

	checkMaps(t, ret.M, allhot())

	if kit.F64 != ret.F64 {
		t.Errorf("expected %s to come back as %v but got %v", "F64", kit.F64, ret.F64)
	}
	if kit.F32 != ret.F32 {
		t.Errorf("expected %s to come back as %v but got %v", "F32", kit.F32, ret.F32)
	}
	if kit.I32 != ret.I32 {
		t.Errorf("expected %s to come back as %v but got %v", "I32", kit.I32, ret.I32)
	}
	if kit.I64 != ret.I64 {
		t.Errorf("expected %s to come back as %v but got %v", "I64", kit.I64, ret.I64)
	}
	if kit.U32 != ret.U32 {
		t.Errorf("expected %s to come back as %v but got %v", "U32", kit.U32, ret.U32)
	}
	if kit.U64 != ret.U64 {
		t.Errorf("expected %s to come back as %v but got %v", "U64", kit.U64, ret.U64)
	}
	if kit.S32 != ret.S32 {
		t.Errorf("expected %s to come back as %v but got %v", "S32", kit.S32, ret.S32)
	}
	if kit.S64 != ret.S64 {
		t.Errorf("expected %s to come back as %v but got %v", "S64", kit.S64, ret.S64)
	}
	if kit.Fix32 != ret.Fix32 {
		t.Errorf("expected %s to come back as %v but got %v", "Fix32", kit.Fix32, ret.Fix32)
	}
	if kit.Fix64 != ret.Fix64 {
		t.Errorf("expected %s to come back as %v but got %v", "Fix64", kit.Fix64, ret.Fix64)
	}
	if kit.Sfix32 != ret.Sfix32 {
		t.Errorf("expected %s to come back as %v but got %v", "Sfix32", kit.Sfix32, ret.Sfix32)
	}
	if kit.Sfix64 != ret.Sfix64 {
		t.Errorf("expected %s to come back as %v but got %v", "Sfix64", kit.Sfix64, ret.Sfix64)
	}
	if kit.B != ret.B {
		t.Errorf("expected %s to come back as %v but got %v", "B", kit.B, ret.B)
	}
	if kit.S != ret.S {
		t.Errorf("expected %s to come back as %v but got %v", "S", kit.S, ret.S)
	}

	if !bytes.Equal(kit.Buf, ret.Buf) {
		t.Errorf("expected %s to come back with %#x but got %#x", "Buf", kit.Buf, ret.Buf)
	}
}

package sealed

import (
	"testing"
)

func TestZigZag32(t *testing.T) {
	for _, val := range []int32{0, 1, -1, 2, -2, 3, -3, 4, -4} {
		enc := zigzagEncode32(val)
		dec := zigzagDecode32(enc)
		if val != dec {
			t.Errorf("value %d encoded as <%b> but decoded as %d", val, enc, dec)
		}
	}
}

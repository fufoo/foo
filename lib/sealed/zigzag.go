package sealed

func zigzagEncode32(k int32) uint32 {
	if k < 0 {
		return 1 + (uint32(-k-1) << 1)
	} else {
		return uint32(k) << 1
	}
}

func zigzagDecode32(k uint32) int32 {
	if k&1 == 0 {
		return int32(k >> 1)
	} else {
		return -(int32(k>>1) + 1)
	}
}

func zigzagEncode64(k int64) uint64 {
	if k < 0 {
		return 1 + (uint64(-k-1) << 1)
	} else {
		return uint64(k) << 1
	}
}

func zigzagDecode64(k uint64) int64 {
	if k&1 == 0 {
		return int64(k >> 1)
	} else {
		return -(int64(k>>1) + 1)
	}
}

package sealed

import (
	"bufio"
	"bytes"
	"compress/flate"
	"context"
	"crypto/cipher"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"math"

	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/services/core"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/reflect/protoreflect"
)

// a Loader is responsible for unpacking the decrypted (and
// decompressed) data into an appropriate object.  It is given the
// type pointer, the vector of external references, and a reader which
// is the body payload.
type Loader func(context.Context, *ptr.Ptr, []ptr.Ptr, *bufio.Reader) (interface{}, error)

type Decoder interface {
	Decode(ctx context.Context, fn Loader, src []byte) (interface{}, error)
}

var ErrInvalidMessage = errors.New("invalid object")
var ErrUnexpectedMessage = errors.New("unexpected object")

type Preamble struct {
	Version    int
	Mode       CipherMode
	Size       int
	KeyID      string
	InitVector []byte
}

func ParsePreamble(ctx context.Context, src []byte) (*Preamble, error) {
	var pre Preamble

	i := 0
	vp, n := binary.Uvarint(src[i:])
	if n <= 0 || vp > 0xffff {
		return nil, ErrInvalidMessage
	}
	i += n
	versionPreamble := uint32(vp)

	if trace {
		log.Tracef(ctx, "version preamble mode %d version %d",
			versionPreamble>>4,
			versionPreamble&0xf)
	}

	if versionPreamble&0xf != formatVersion {
		panic("bad format version")
		return nil, ErrInvalidMessage
	}

	pre.Mode = CipherMode(versionPreamble >> 4)

	if pre.Mode != CipherNONE {

		kidlen, n := binary.Uvarint(src[i:])
		if n <= 0 || kidlen > 1024 {
			panic("bad kid")
			return nil, ErrInvalidMessage
		}
		i += n

		pre.KeyID = string(src[i : i+int(kidlen)])
		i += int(kidlen)
		if trace {
			log.Tracef(ctx, "key id %q", pre.KeyID)
		}

		bs := pre.Mode.BlockSize()
		pre.InitVector = src[i : i+bs]
		i += bs
		if trace {
			log.Tracef(ctx, "init vector [%x] (%d) mode %s", pre.InitVector, bs, pre.Mode)
		}
	}
	pre.Size = i
	return &pre, nil
}

// Extract just the type part
func (s *SecureEncoding) DecodeType(ctx context.Context, src []byte) (*ptr.Ptr, error) {
	tp, _, err := s.decode(ctx, nil, src, false)
	return tp, err
}

func (s *SecureEncoding) Decode(ctx context.Context, fn Loader, src []byte) (interface{}, error) {
	_, obj, err := s.decode(ctx, fn, src, true)
	return obj, err
}

func (s *SecureEncoding) decode(ctx context.Context, fn Loader, src []byte, obj bool) (*ptr.Ptr, interface{}, error) {
	var in encoded

	i := 0
	vp, n := binary.Uvarint(src[i:])
	if n <= 0 || vp > 0xffff {
		return nil, nil, ErrInvalidMessage
	}
	i += n
	in.versionPreamble = uint32(vp)

	if trace {
		log.Tracef(ctx, "version preamble mode %d version %d",
			in.versionPreamble>>4,
			in.versionPreamble&0xf)
	}

	if in.versionPreamble&0xf != formatVersion {
		panic("bad format version")
		return nil, nil, ErrInvalidMessage
	}

	in.mode = CipherMode(in.versionPreamble >> 4)

	if in.mode != CipherNONE {
		in.blockSize = in.mode.BlockSize()

		kidlen, n := binary.Uvarint(src[i:])
		if n <= 0 || kidlen > 1024 {
			panic("bad kid")
			return nil, nil, ErrInvalidMessage
		}
		i += n

		in.keyId = string(src[i : i+int(kidlen)])
		i += int(kidlen)
		if trace {
			log.Tracef(ctx, "key id %q", in.keyId)
		}

		in.initVector = src[i : i+in.blockSize]
		i += in.blockSize
		if trace {
			log.Tracef(ctx, "init vector [%x] (%d) mode %s", in.initVector, in.blockSize, in.mode)
		}

	}

	numx, n := binary.Uvarint(src[i:])
	if n <= 0 || numx > uint64(len(src))/ptr.Size {
		return nil, nil, ErrInvalidMessage
	}
	i += n
	if trace {
		log.Tracef(ctx, "+%d  there are %d pointers to read", i, numx)
	}

	in.xrefs = decodeIndex(int(numx), src[i:])
	i += ptr.Size * int(numx)

	if trace {
		log.Tracef(ctx, "loaded %d ptrs", len(in.xrefs))
	}

	lockup := src[i:]

	index := in.xrefs

	// decrypt the body
	var stream cipher.Stream

	var decrypted *bufio.Reader

	if s.mode == CipherNONE {
		decrypted = bufio.NewReader(bytes.NewReader(lockup))
	} else {
		switch {
		case s.mode.IsCFB():
			stream = cipher.NewCFBDecrypter(s.secure, in.initVector)
		case s.mode.IsOFB():
			stream = cipher.NewOFB(s.secure, in.initVector)
		case s.mode.IsCTR():
			stream = cipher.NewCTR(s.secure, in.initVector)
		default:
			panic("invalid mode")
		}
		if trace {
			log.Tracef(ctx, "index has %d", len(index))
		}

		decrypted = bufio.NewReader(cipher.StreamReader{
			R: bytes.NewBuffer(lockup),
			S: stream,
		})
	}

	ti, err := binary.ReadUvarint(decrypted)
	if err != nil {
		panic("rats")
		return nil, nil, ErrInvalidMessage
	}

	var tp *ptr.Ptr
	if ti == 0 {
		// typeless; decoding won't happen
	} else if ti == 1 {
		// not a loadable type, it's inline in the body
		tp = new(ptr.Ptr)
		io.ReadFull(decrypted, tp.Bits[:])
		if trace {
			log.Tracef(ctx, "type is immediate %s", tp)
		}
	} else if (ti & 0xf) == 2 {
		tp = &index[ti>>4]
		if trace {
			log.Tracef(ctx, "type is loadable %s", tp)
		}
	} else {
		return nil, nil, ErrInvalidMessage
	}
	if !obj {
		// short circuit... we only want the type, so stop here
		return tp, nil, nil
	}

	sizeEnc, err := binary.ReadUvarint(decrypted)
	if err != nil {
		panic("rats")
		return nil, nil, ErrInvalidMessage
	}
	if trace {
		log.Tracef(ctx, "size %d encoding %d", sizeEnc>>4, sizeEnc&0xf)
	}

	encodedSize := sizeEnc >> 4

	switch sizeEnc & 0xf {
	case EncodingNone:
		panic("NONE-NONE")
	case EncodingProtobuf:
		// nothing else to do here?
	case EncodingFlateProtobuf, EncodingFlate, EncodingFlateCustom:
		if trace {
			log.Tracef(ctx, "setting up deflation reader over %d", encodedSize)
		}
		decrypted = bufio.NewReader(
			flate.NewReader(&loggingReader{
				&io.LimitedReader{
					R: decrypted,
					N: int64(encodedSize),
				},
				0,
			}))
	default:
		panic("todo")
	}
	if tp == nil {
		// don't decode, but we do need to unpack the encrypted
		// compressed stream
		buf, err := io.ReadAll(decrypted)
		return tp, buf, err
	} else {
		obj, err := fn(ctx, tp, index, decrypted)
		return tp, obj, err
	}
}

type MessageMaker func(*ptr.Ptr) (proto.Message, error)

func ExpectedTypeLoader(dst proto.Message, t ptr.Ptr) Loader {
	return func(ctx context.Context, tp *ptr.Ptr, index []ptr.Ptr, src *bufio.Reader) (interface{}, error) {
		// don't allow blobs; we're not expecting that!
		if tp == nil || *tp != t {
			return nil, ErrUnexpectedMessage
		}

		buf, err := io.ReadAll(src)
		if err != nil {
			return nil, err
		}
		err = decodeContent(ctx, dst, index, buf)
		if err != nil {
			return nil, err
		}
		return dst, nil
	}
}

func ProtobufLoader(maker MessageMaker) Loader {
	return func(ctx context.Context, tp *ptr.Ptr, index []ptr.Ptr, src *bufio.Reader) (interface{}, error) {
		buf, err := io.ReadAll(src)
		if err != nil {
			return nil, err
		}
		if tp == nil {
			// allow blobs even here
			if len(index) > 0 {
				// if there is no type, there should not be any fanout
				return nil, ErrInvalidMessage
			}
			return buf, nil
		}
		msg, err := maker(tp)
		if err != nil {
			return nil, err
		}

		err = decodeContent(ctx, msg, index, buf)
		if err != nil {
			return nil, err
		}
		return msg, nil
	}
}

func decodeIndex(n int, src []byte) []ptr.Ptr {
	index := make([]ptr.Ptr, n)
	for i := range index {
		copy(index[i].Bits[:], src[i*ptr.Size:])
	}
	return index
}

type vallocer func() protoreflect.Value

func decodeContent(ctx context.Context, dst proto.Message, index []ptr.Ptr, src []byte) error {

	var walk func(v protoreflect.Message, i int, src []byte) error

	scanValue := func(valloc vallocer, f protoreflect.FieldDescriptor, wire int, src []byte) (protoreflect.Value, []byte) {
		if trace {
			log.Tracef(ctx, "scan (%s) %s wire %d: len %d |%x|",
				f.Name(), f.Kind(), wire, len(src), src)
		}
		var val uint64
		var skip int
		var contained []byte

		switch wire & 7 {
		case wireVarint:
			val, skip = binary.Uvarint(src)
			if skip <= 0 {
				panic(ErrInvalidMessage)
			}
			if trace {
				log.Tracef(ctx, "wireVarint value %d skip %d", val, skip)
			}

		case wire64:
			val = binary.LittleEndian.Uint64(src)
			skip = 8

		case wire32:
			val = uint64(binary.LittleEndian.Uint32(src))
			skip = 4

		case wireLengthDelimited:
			length, n := binary.Uvarint(src)
			if n <= 0 || length > 1024*1024 {
				panic(ErrInvalidMessage)
			}
			skip = n + int(length)
			if n+int(length) > len(src) {
				// Go will let us reslice to be bigger
				// than the slice's length as long as
				// it is within the cap(), but we want
				// to respect the given length
				panic("overrun")
			}
			contained = src[n : n+int(length)]
			if trace {
				log.Tracef(ctx, "contained len %d %x", length, contained)
			}

		default:
			panic(fmt.Sprintf("TODO unimplemented wire type %d", wire&7))
		}

		var x protoreflect.Value

		if contained != nil {
			if f.IsList() {
				panic("cannot have a list as a value here")
			} else if f.IsMap() {
				panic("cannot have a map as a value here")
			}
			switch f.Kind() {
			case protoreflect.MessageKind:
				if trace {
					log.Tracef(ctx, "a message: %s", f.Name())
				}
				x = valloc()
				err := walk(x.Message(), 0, contained)
				if err != nil {
					panic(err)
				}

			case protoreflect.StringKind:
				x = protoreflect.ValueOfString(string(contained))

			case protoreflect.BytesKind:
				x = protoreflect.ValueOfBytes(contained)

			default:
				panic(fmt.Sprintf("TODO unsupported contained kind %s", f.Kind()))
			}
		} else {
			switch f.Kind() {
			case protoreflect.BoolKind:
				b := true
				if val == 0 {
					b = false
				}
				x = protoreflect.ValueOfBool(b)

			case protoreflect.Uint32Kind, protoreflect.Fixed32Kind:
				x = protoreflect.ValueOfUint32(uint32(val))
			case protoreflect.Sint32Kind:
				x = protoreflect.ValueOfInt32(zigzagDecode32(uint32(val)))
			case protoreflect.Int32Kind, protoreflect.Sfixed32Kind:
				x = protoreflect.ValueOfInt32(int32(val))

			case protoreflect.Uint64Kind, protoreflect.Fixed64Kind:
				x = protoreflect.ValueOfUint64(uint64(val))
			case protoreflect.Sint64Kind:
				x = protoreflect.ValueOfInt64(zigzagDecode64(uint64(val)))
			case protoreflect.Int64Kind, protoreflect.Sfixed64Kind:
				x = protoreflect.ValueOfInt64(int64(val))

			case protoreflect.DoubleKind:
				x = protoreflect.ValueOfFloat64(math.Float64frombits(val))
			case protoreflect.FloatKind:
				x = protoreflect.ValueOfFloat32(math.Float32frombits(uint32(val)))
			default:
				panic(fmt.Sprintf("TODO ... cannot decode map value type yet %s", f.Kind()))
			}
		}
		return x, src[skip:]
	}

	scanKey := func(f protoreflect.FieldDescriptor, wire int, src []byte) (protoreflect.MapKey, []byte) {
		// key cannot be a message, so won't need vallocer
		value, rest := scanValue(nil, f, wire, src)
		return protoreflect.MapKey(value), rest
	}

	scanMapEntry := func(into protoreflect.Map, f protoreflect.FieldDescriptor, valloc vallocer, src []byte) error {

		if trace {
			log.Tracef(ctx, "scanMapEntry |%x|", src)
		}

		keyType := f.MapKey()
		valueType := f.MapValue()

		var keyValue protoreflect.MapKey
		var valueValue protoreflect.Value

		//i := 0
		hasKey := false
		hasValue := false
		for len(src) > 0 {
			if trace {
				log.Tracef(ctx, "scanMapEntry constituent |%x|", src)
			}

			ktag, n := binary.Uvarint(src)
			if n <= 0 {
				return ErrInvalidMessage
			}
			if trace {
				log.Tracef(ctx, "ktag %#x => %d:%d", ktag, ktag>>3, ktag&7)
			}

			//i += n
			src = src[n:]

			fnum := ktag >> 3
			if fnum == 1 {
				keyValue, src = scanKey(keyType, int(ktag&7), src)
				if trace {
					log.Tracef(ctx, "key is: {%s}", keyValue)
				}
				hasKey = true
			} else if fnum == 2 {
				valueValue, src = scanValue(valloc, valueType, int(ktag&7), src)
				hasValue = true
			}
		}
		if hasKey && hasValue {
			into.Set(keyValue, valueValue)
			return nil
		} else {
			return ErrInvalidMessage
		}
	}

	walk = func(v protoreflect.Message, i int, src []byte) error {
		//t := v.Type().(protoreflect.MessageFieldTypes)

		for len(src) > 0 {
			// parse a field
			i0 := i
			ktag, n := binary.Uvarint(src)
			if n <= 0 {
				return ErrInvalidMessage
			}
			if trace {
				log.Tracef(ctx, "@%04x tag %d wiretype %d",
					i0,
					ktag/8,
					ktag&7)
			}
			i += n
			src = src[n:]

			tag := protoreflect.FieldNumber(ktag / 8)
			field := v.Descriptor().Fields().ByNumber(tag)

			var skip int
			var val uint64
			var contained []byte

			switch ktag & 7 {
			case wireVarint:
				val, skip = binary.Uvarint(src)
				if skip <= 0 {
					return ErrInvalidMessage
				}

			case wire64:
				val = binary.LittleEndian.Uint64(src)
				skip = 8

			case wire32:
				val = uint64(binary.LittleEndian.Uint32(src))
				skip = 4

			case wireLengthDelimited:
				length, n := binary.Uvarint(src)
				if n <= 0 || length > 1024*1024 {
					return ErrInvalidMessage
				}
				skip = n + int(length)
				contained = src[n : n+int(length)]

			default:
				panic(fmt.Sprintf("TODO unimplemented wire type %d", ktag&7))
			}

			i += skip
			src = src[skip:]

			if field == nil {
				if trace {
					log.Tracef(ctx, "unknown field, skipping %d", skip)
				}
				continue
			}
			if trace {
				log.Tracef(ctx, "@%04x field %q", i0, field.Name())
			}

			if contained != nil {
				if trace {
					log.Tracef(ctx, "%d contained bytes (kind: %s)", len(contained), field.Kind())
					log.Tracef(ctx, "|%x|", contained)
				}
				if field.IsList() {
					theList := v.Mutable(field).List()
					if trace {
						log.Tracef(ctx, "it's a member of a list")
					}

					switch field.Kind() {

					case protoreflect.MessageKind:
						walk(theList.AppendMutable().Message(), 0, contained)

					case protoreflect.StringKind:
						x := protoreflect.ValueOfString(string(contained))
						theList.Append(x)
					}
				} else if field.IsMap() {
					m := v.Mutable(field).Map()
					/*					valloc := func(f protoreflect.FieldDescriptor) protoreflect.Value {
										return m.NewValue()
									}*/

					err := scanMapEntry(m, field, m.NewValue, contained)
					if err != nil {
						return err
					}
				} else {

					switch field.Kind() {

					case protoreflect.MessageKind:
						if trace {
							log.Tracef(ctx, "a message: %s", v.Descriptor().Name())
						}
						walk(v.Mutable(field).Message(), 0, contained)

					case protoreflect.StringKind:
						x := protoreflect.ValueOfString(string(contained))
						v.Set(field, x)

					case protoreflect.BytesKind:
						x := protoreflect.ValueOfBytes(contained)
						v.Set(field, x)

					default:
						panic("unsupported kind " + field.Kind().String())
					}
				}

			} else {
				// check for a ptr
				if field.Kind() == protoreflect.MessageKind {
					typeName := field.Message().FullName()
					if trace {
						log.Tracef(ctx, "message %q [%d]", typeName, val)
					}
					if typeName == "foo.services.core.Ptr" {
						if val >= uint64(len(index)) {
							panic("out of range")
							return ErrInvalidMessage
						}
						p := index[val]
						if trace {
							log.Tracef(ctx, "  = %s", p)
						}
						pp := core.From(p)
						msg := protoreflect.ValueOfMessage(pp.ProtoReflect())
						if field.IsList() {
							theList := v.Mutable(field).List()
							theList.Append(msg)
						} else {
							v.Set(field, msg)
						}
					} else {
						panic("um, what?")
					}
				} else {
					if trace {
						log.Tracef(ctx, "value is %d", val)
					}

					var value protoreflect.Value
					switch field.Kind() {
					case protoreflect.DoubleKind:
						f := math.Float64frombits(val)
						value = protoreflect.ValueOfFloat64(f)
					case protoreflect.FloatKind:
						f := math.Float32frombits(uint32(val))
						value = protoreflect.ValueOfFloat32(f)
					case protoreflect.BoolKind:
						b := true
						if val == 0 {
							b = false
						}
						value = protoreflect.ValueOfBool(b)
					case protoreflect.Uint64Kind, protoreflect.Fixed64Kind:
						value = protoreflect.ValueOfUint64(val)
					case protoreflect.Uint32Kind, protoreflect.Fixed32Kind:
						value = protoreflect.ValueOfUint32(uint32(val))
					case protoreflect.Int64Kind, protoreflect.Sfixed64Kind:
						value = protoreflect.ValueOfInt64(int64(val))
					case protoreflect.Int32Kind, protoreflect.Sfixed32Kind:
						value = protoreflect.ValueOfInt32(int32(val))
					case protoreflect.EnumKind:
						value = protoreflect.ValueOfEnum(protoreflect.EnumNumber(val))
					case protoreflect.Sint64Kind:
						value = protoreflect.ValueOfInt64(zigzagDecode64(val))
					case protoreflect.Sint32Kind:
						value = protoreflect.ValueOfInt32(zigzagDecode32(uint32(val)))
					default:
						panic(fmt.Sprintf("TODO %d %s", field.Kind(), field.Kind()))
					}

					if field.IsList() {
						v.Mutable(field).List().Append(value)
					} else {
						v.Set(field, value)
					}
				}
			}

		}
		return nil
	}

	return walk(dst.ProtoReflect(), 0, src)
}

type loggingReader struct {
	R     io.Reader
	total int
}

func (l *loggingReader) Read(dst []byte) (int, error) {
	n, err := l.R.Read(dst)
	if trace {
		log.Tracef(context.TODO(), "* +%d did read %d of %d", l.total, n, len(dst))
	}
	l.total += n
	return n, err
}

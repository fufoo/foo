package sealed

import (
	"bytes"
	"compress/flate"
	"context"
	"crypto/cipher"
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"io"
	"math"
	"sort"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/ptr"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/reflect/protoreflect"
)

const trace = false        // traces actual protobuf encoding
const tracePacking = false // only traces packing layer

type CipherMode int

const (
	CipherNONE = CipherMode(iota)
	CipherAES256CBC
	CipherAES192CBC
	CipherAES128CBC
	CipherAES256CFB
	CipherAES192CFB
	CipherAES128CFB
	CipherAES256CTR
	CipherAES192CTR
	CipherAES128CTR
	CipherAES256OFB
	CipherAES192OFB
	CipherAES128OFB
)

func (m CipherMode) String() string {
	switch m {
	case CipherNONE:
		return "CipherNONE"
	case CipherAES256CBC:
		return "CipherAES256CBC"
	case CipherAES192CBC:
		return "CipherAES192CBC"
	case CipherAES128CBC:
		return "CipherAES128CBC"
	case CipherAES256CFB:
		return "CipherAES256CFB"
	case CipherAES192CFB:
		return "CipherAES192CFB"
	case CipherAES128CFB:
		return "CipherAES128CFB"
	case CipherAES256CTR:
		return "CipherAES256CTR"
	case CipherAES192CTR:
		return "CipherAES192CTR"
	case CipherAES128CTR:
		return "CipherAES128CTR"
	case CipherAES256OFB:
		return "CipherAES256OFB"
	case CipherAES192OFB:
		return "CipherAES192OFB"
	case CipherAES128OFB:
		return "CipherAES128OFB"
	default:
		panic("bad CipherMode")
	}
}

const (
	EncodingNone = iota
	EncodingFlate
	EncodingProtobuf
	EncodingFlateProtobuf
	EncodingCustom
	EncodingFlateCustom
)

func (c CipherMode) BlockSize() int {
	switch c {
	case CipherNONE:
		return 0
	// AES block size is 16 (even though its key length varies)
	case CipherAES256CBC,
		CipherAES192CBC,
		CipherAES128CBC,
		CipherAES256CFB,
		CipherAES192CFB,
		CipherAES128CFB,
		CipherAES256CTR,
		CipherAES192CTR,
		CipherAES128CTR,
		CipherAES256OFB,
		CipherAES192OFB,
		CipherAES128OFB:

		return 16
	default:
		panic("unsupported mode")
	}
}

func (c CipherMode) IsBlock() bool {
	return c.IsCBC()
}

func (c CipherMode) IsCBC() bool {
	switch c {
	case CipherAES256CBC, CipherAES192CBC, CipherAES128CBC:
		return true
	default:
		return false
	}
}

func (c CipherMode) IsCFB() bool {
	switch c {
	case CipherAES256CFB, CipherAES192CFB, CipherAES128CFB:
		return true
	default:
		return false
	}
}

func (c CipherMode) IsOFB() bool {
	switch c {
	case CipherAES256OFB, CipherAES192OFB, CipherAES128OFB:
		return true
	default:
		return false
	}
}

func (c CipherMode) IsCTR() bool {
	switch c {
	case CipherAES256CTR, CipherAES192CTR, CipherAES128CTR:
		return true
	default:
		return false
	}
}

var log = logging.New("sealed")

const formatVersion = 1

type Encoder interface {
	Encode(ctx context.Context, dst []byte, tp *ptr.Ptr, src interface{}, iv []byte) ([]byte, error)
}

type Encoding interface {
	Encoder
	Decoder
	MakeInitializationVector([]byte) []byte
}

type SecureEncoding struct {
	keyId  string
	mode   CipherMode
	secure cipher.Block
}

func hashToIV(n int, data [][]byte) []byte {
	iv := make([]byte, n)
	h := sha256.New()
	for _, frag := range data {
		h.Write(frag)
	}
	hsum := h.Sum(nil)[:]
	copy(iv, hsum)
	if n > len(hsum) {
		panic("todo need to pad this")
	}
	return iv
}

func (s *SecureEncoding) MakeInitializationVector(data []byte) []byte {
	return hashToIV(s.secure.BlockSize(), [][]byte{data})
}

type encoded struct {
	mode               CipherMode
	blockSize          int
	versionPreamble    uint32
	keyId              string
	initVector         []byte
	xrefs              []ptr.Ptr
	xrefIndex          map[ptr.Ptr]uint16
	typeIndex          uint32
	typePtr            *ptr.Ptr
	sizeEncodingFormat uint64
	marshaled          [][]byte
}

func AssignPointerIndices(index map[ptr.Ptr]uint16) []ptr.Ptr {
	lst := make([]ptr.Ptr, len(index))
	i := 0
	for p := range index {
		lst[i] = p
		i++
	}
	sort.Sort(ptrlist(lst))
	for i, p := range lst {
		index[p] = uint16(i)
	}
	return lst
}

func (e *encoded) prepareXrefs(ctx context.Context) {
	lst := make([]ptr.Ptr, 0, len(e.xrefIndex))
	for k := range e.xrefIndex {
		lst = append(lst, k)
	}
	sort.Sort(ptrlist(lst))
	for i, p := range lst {
		e.xrefIndex[p] = uint16(i)
		if trace {
			log.Tracef(ctx, "  %s -> [%d]", p, i)
		}
	}
	e.xrefs = lst
}

func (e *encoded) setType(tp *ptr.Ptr) {
	if tp == nil {
		e.typePtr = nil
		e.typeIndex = 0
	} else if !tp.IsLoadable() {
		e.typePtr = tp
		e.typeIndex = 1
	} else {
		e.typePtr = tp
		k, ok := e.xrefIndex[*tp]
		if !ok {
			panic("type is not in index")
		}
		e.typeIndex = 2 + uint32(k)
	}
}

func (e *encoded) size() int {
	n := uintSize(uint64(e.versionPreamble))
	if e.mode != CipherNONE {
		n += uintSize(uint64(len(e.keyId))) + len(e.keyId) + len(e.initVector)
	}
	n += uintSize(uint64(len(e.xrefs)))
	n += ptr.Size * len(e.xrefs)

	// now to account for the encrypted portion
	unencryptedSize := n
	n += uintSize(uint64(e.typeIndex))
	if e.typeIndex == 1 {
		n += ptr.Size
	}
	n += uintSize(e.sizeEncodingFormat)
	for _, frag := range e.marshaled {
		n += len(frag)
	}
	// round up to a block size
	if e.mode.IsBlock() {
		if trace {
			fmt.Printf("pre rounding n=%d\n", n)
		}
		encryptedSize := n - unencryptedSize
		if trace {
			fmt.Printf("encrypted size=%d\n", encryptedSize)
		}

		blockSize := e.mode.BlockSize()
		if trace {
			fmt.Printf("block size=%d\n", blockSize)
		}

		n = unencryptedSize + blockSize*((encryptedSize+blockSize-1)/blockSize)
		if trace {
			fmt.Printf("post rounding n=%d\n", n)
		}

	}
	return n
}

func (e *encoded) synthesizeIV() {
	if e.initVector == nil {
		// synthesize an IV if needed
		iv := hashToIV(e.blockSize, e.marshaled)
		if trace {
			fmt.Printf("synthesized iv [%x] mode %s\n", iv, e.mode)
		}
		e.initVector = iv
	}
}

func (e *encoded) appendOnto(prefix []byte) ([]byte, []byte) {
	n := e.size()
	// grow the capacity if needed, and reserve the entire length we need
	if trace {
		fmt.Printf("len(prefix)=%d cap(prefix)=%d n=%d\n",
			len(prefix),
			cap(prefix),
			n)
	}
	output := grow(prefix, n)[:len(prefix)+n]
	if trace {
		fmt.Printf("len(output)=%d cap(output)=%d\n",
			len(output),
			cap(output))
	}

	i := len(prefix)

	// VersionAndEncryption
	i += binary.PutUvarint(output[i:], uint64(e.versionPreamble))

	if e.mode != CipherNONE {
		// KeyId
		i += binary.PutUvarint(output[i:], uint64(len(e.keyId)))
		copy(output[i:], e.keyId)
		i += len(e.keyId)

		// InitVector
		if trace {
			fmt.Printf("i=%d of %d initVector [%x]\n", i, len(output), e.initVector)
		}
		copy(output[i:], e.initVector)
		i += len(e.initVector)
	}

	// NumXrefs
	i += binary.PutUvarint(output[i:], uint64(len(e.xrefs)))

	// Xrefs
	for _, xref := range e.xrefs {
		copy(output[i:], xref.Bits[:])
		i += len(xref.Bits)
	}
	if trace {
		fmt.Printf("header [%x]\n", output[len(prefix):i])
	}

	encryptedStart := i

	// TypeIndex
	i += binary.PutUvarint(output[i:], uint64(e.typeIndex))
	if e.typeIndex == 1 {
		// LiteralType
		copy(output[i:], e.typePtr.Bits[:])
		i += len(e.typePtr.Bits)
	}
	i += binary.PutUvarint(output[i:], e.sizeEncodingFormat)
	for _, frag := range e.marshaled {
		copy(output[i:], frag)
		i += len(frag)
	}

	// if we want block encryption, pad with an approximation of noise
	if e.mode.IsBlock() {
		encryptedSize := i - encryptedStart
		blockSize := e.mode.BlockSize()
		roundedUp := blockSize * ((encryptedSize + blockSize - 1) / blockSize)
		iv := e.initVector
		for j := encryptedSize; j < roundedUp; j++ {
			output[i] = iv[j%len(iv)]
			i++
		}
	}

	if i != len(output) {
		panic(fmt.Sprintf("miscalculated, expected %d but actually %d", len(output), i))
	}
	if trace {
		fmt.Printf("%d bytes of output, of which %d is encrypted\n",
			i-len(prefix), i-encryptedStart)
	}
	return output, output[encryptedStart:]
}

func deflate(src [][]byte) []byte {
	var tmp bytes.Buffer
	dst, _ := flate.NewWriter(&tmp, flate.BestCompression)
	for _, chunk := range src {
		dst.Write(chunk)
	}
	dst.Close()
	return tmp.Bytes()
}

type MarshalFlag uint

const (
	DeflateCompression MarshalFlag = 1 << iota
)

// Marshaled stores the marshaled representation of an object.  The
// index should be sorted, and if the type pointer of the object is
// itself Loadable, it should appear somewhere in the index (the
// payload is not responsible for representing *which* pointer
// represents the type, that is handled by the final serdes phase)
type Marshaled struct {
	Payload []byte
	Index   []ptr.Ptr
	Flags   MarshalFlag
}

// MarshalCAM implements CAMMarshaler.  A Marshaled object can
// be trivially marshaled.
func (m Marshaled) MarshalCAM(*ptr.Ptr) (Marshaled, error) {
	return m, nil
}

type CAMMarshaler interface {
	// MarshalCAM is the generic protocol for marshaling an object
	// for storage in the CAM; it should return a byte slice of
	// marshaled pointer-free data together with a slice of
	// pointers, and some flags to control how the marshaled data
	// is finally serialized
	MarshalCAM(typePtr *ptr.Ptr) (Marshaled, error)
}

func (s *SecureEncoding) Encode(ctx context.Context, onto []byte, typePtr *ptr.Ptr, src interface{}, iv []byte) ([]byte, error) {

	out := encoded{
		mode:            s.mode,
		versionPreamble: uint32(s.mode)<<4 + formatVersion,
		keyId:           s.keyId,
		initVector:      iv,
		xrefIndex:       make(map[ptr.Ptr]uint16),
	}
	if s.secure != nil {
		out.blockSize = s.secure.BlockSize()

		if out.blockSize != s.mode.BlockSize() {
			panic(fmt.Sprintf("mode/crypter block size mismatch (mode %s expected %d, crypter %d)",
				s.mode,
				s.mode.BlockSize(),
				s.secure.BlockSize()))
		}
	}
	if typePtr != nil && typePtr.IsLoadable() {
		out.xrefIndex[*typePtr] = 0
	}

	if blob, ok := src.([]byte); ok {
		// TODO check to see if we should bother compressing
		out.sizeEncodingFormat = EncodingFlate
		zblob := deflate([][]byte{blob})
		out.marshaled = [][]byte{zblob}
		out.sizeEncodingFormat += uint64(len(zblob)) << 4
		out.prepareXrefs(ctx)
	} else if m, ok := src.(CAMMarshaler); ok {
		// if we have a custom CAM marshaler, use it
		marsh, err := m.MarshalCAM(typePtr)
		if err != nil {
			return nil, err
		}
		payload := [][]byte{marsh.Payload}
		index := marsh.Index
		compress := (marsh.Flags & DeflateCompression) != 0
		if tracePacking {
			log.Debugf(ctx, "cam marshaled | %x", payload)
		}

		if compress {
			out.marshaled = [][]byte{deflate(payload)}
			out.sizeEncodingFormat = EncodingFlateCustom
		} else {
			out.marshaled = payload
			out.sizeEncodingFormat = EncodingCustom
		}
		for _, chunk := range out.marshaled {
			out.sizeEncodingFormat += uint64(len(chunk)) << 4
		}
		out.xrefs = index
	} else {
		out.sizeEncodingFormat = EncodingFlateProtobuf
		err := out.sealUsingReflection(ctx, src.(proto.Message))
		if err != nil {
			return nil, err
		}
	}

	if tracePacking {
		for _, m := range out.marshaled {
			log.Tracef(ctx, "  marshaled |%x|", m)
		}
	}

	out.synthesizeIV()
	out.setType(typePtr)
	if tracePacking {
		log.Tracef(ctx, "size %d encoding format %d...",
			out.sizeEncodingFormat>>4,
			out.sizeEncodingFormat&0xf)
	}
	packed, lockup := out.appendOnto(onto)

	if tracePacking {
		log.Tracef(ctx, "packed | %x", packed)
	}
	if s.mode == CipherNONE {
		return packed, nil
	}
	if trace {
		log.Tracef(ctx, "lockup | %x", lockup)
	}

	// encrypt the body
	var stream cipher.Stream
	var block cipher.BlockMode

	switch {

	case s.mode.IsCBC():
		block = cipher.NewCBCEncrypter(s.secure, out.initVector)
	case s.mode.IsCFB():
		stream = cipher.NewCFBEncrypter(s.secure, out.initVector)
	case s.mode.IsOFB():
		stream = cipher.NewOFB(s.secure, out.initVector)
	case s.mode.IsCTR():
		stream = cipher.NewCTR(s.secure, out.initVector)
	default:
		panic("invalid mode")
	}

	if stream != nil {
		stream.XORKeyStream(lockup, lockup)
	} else {
		block.CryptBlocks(lockup, lockup)
	}
	return packed, nil
}

func uintSize(n uint64) int {
	// TODO make this faster
	if n < 0x80 {
		return 1
	}
	var waste [10]byte
	return binary.PutUvarint(waste[:], n)
}

func (out *encoded) sealUsingReflection(ctx context.Context, src proto.Message) error {

	index := out.xrefIndex

	foundPtr := func(v protoreflect.Message) {
		var p ptr.Ptr

		bitsField := v.Descriptor().Fields().ByNumber(1)
		bits := v.Get(bitsField).Interface()
		copy(p.Bits[:], bits.([]uint8))
		index[p] = 0 // this will get assigned later
	}

	// PASS 1 is about finding all the pointers

	var walk func(v protoreflect.Message)
	var visit func(f protoreflect.FieldDescriptor, v protoreflect.Value)

	visit = func(f protoreflect.FieldDescriptor, v protoreflect.Value) {

		tag := f.Number()
		name := f.FullName()

		switch v := v.Interface().(type) {
		case protoreflect.List:
			n := v.Len()
			if trace {
				log.Tracef(ctx, "field %d (%s) is a list (len %d)",
					tag, name, n)
			}
			// TODO we could short circuit this scan if we
			// know there are no pointers in the element type
			for i := 0; i < n; i++ {
				visit(f, v.Get(i))
			}

		case protoreflect.Map:
			// TODO we could short circuit this scan if we
			// know there are no pointers in either type.
			// Sort of a "hasAnyPointers" pre-preflight

			if trace {
				log.Tracef(ctx, "field %d (%s) is a map (len %d)",
					tag, name, v.Len())
			}
			v.Range(func(k protoreflect.MapKey, v protoreflect.Value) bool {
				visit(f.MapKey(), k.Value())
				visit(f.MapValue(), v)
				return true
			})

		case protoreflect.Message:
			// check to see if it is a message of type foo_api.Ptr

			typeName := v.Descriptor().FullName()
			if typeName == "foo_api.Ptr" {
				// there can be at most 32K pointers in an
				// object (because of the 1MB object limit),
				// so at most it will require 3 bytes to encode
				// (2 bytes can encode up to 16K, so close!)
				if trace {
					log.Tracef(ctx, "field %d (%s) is a message of type PTR (+~3)",
						tag, name)
				}
				foundPtr(v)
			} else {
				// similarly, we will need to encode the length
				// of this message, but we don't know exactly
				// how big the length field is yet
				if trace {
					log.Tracef(ctx, "field %d (%s) is a message of type %s",
						tag, name,
						typeName)
				}
				walk(v)
			}

		case protoreflect.EnumNumber:
			if trace {
				log.Tracef(ctx, "field %d (%s) is an enumerand %d",
					tag, name, int32(v))
			}

		case uint64, uint32, int64, int32, bool, string, float64, float32:
			if trace {
				log.Tracef(ctx, "field %d (%s) is a %s scalar %T",
					tag, name, f.Kind(), v)
			}
		case []byte:
			if trace {
				log.Tracef(ctx, "field %d (%s) is a %s blob",
					tag, name, f.Kind())
			}

		default:
			if trace {
				log.Tracef(ctx, "field %d (%s) is unsupported %T",
					f.Number(),
					f.FullName(),
					v)
			}
			panic(fmt.Sprintf("Unsupported field %d (%s) %T",
				f.Number(),
				f.FullName(),
				v))
		}
	}

	walk = func(v protoreflect.Message) {
		v.Range(func(f protoreflect.FieldDescriptor, v protoreflect.Value) bool {
			visit(f, v)
			return true
		})
	}

	walk(src.ProtoReflect())

	if trace {
		log.Tracef(ctx, "done with pass 1, found %d pointers", len(index))
	}

	toc := make([]ptr.Ptr, 0, len(index))
	for k := range index {
		toc = append(toc, k)
	}
	sort.Sort(ptrlist(toc))
	for i, p := range toc {
		index[p] = uint16(i)
		if trace {
			log.Tracef(ctx, "  %s -> [%d]", p, i)
		}
	}

	// pass 2 - actually encode the data

	// organized the xrefs so we can properly encode their references
	out.prepareXrefs(ctx)

	var buf bytes.Buffer
	if false {
		dst, err := flate.NewWriter(&buf, flate.BestCompression)
		if err != nil {
			panic(err)
		}
		out.sizeEncodingFormat = EncodingFlateProtobuf
		out.encodeContent(ctx, logwriter(dst), src.ProtoReflect())
		dst.Close()
	} else {
		out.sizeEncodingFormat = EncodingProtobuf
		out.encodeContent(ctx, &buf, src.ProtoReflect())
	}

	data := buf.Bytes()
	out.marshaled = [][]byte{data}
	out.sizeEncodingFormat += uint64(len(data)) << 4
	return nil
}

var short [128][]byte

func init() {
	for i := 0; i < 128; i++ {
		short[i] = []byte{uint8(i)}
	}
}

const (
	wireVarint = iota
	wire64
	wireLengthDelimited
	wireStartGroup
	wireEndGroup
	wire32
)

func kindToWire(k protoreflect.Kind) int {
	switch k {
	case protoreflect.BoolKind,
		protoreflect.EnumKind,
		protoreflect.Int32Kind,
		protoreflect.Sint32Kind,
		protoreflect.Uint32Kind,
		protoreflect.Int64Kind,
		protoreflect.Sint64Kind,
		protoreflect.Uint64Kind:
		return wireVarint

	case protoreflect.Sfixed32Kind,
		protoreflect.Fixed32Kind,
		protoreflect.FloatKind:
		return wire32

	case protoreflect.Sfixed64Kind,
		protoreflect.Fixed64Kind,
		protoreflect.DoubleKind:
		return wire64

	case protoreflect.StringKind,
		protoreflect.BytesKind,
		protoreflect.MessageKind:
		return wireLengthDelimited

	default:
		panic("unsupported kind")
	}
}

func (out *encoded) encodeContent(ctx context.Context, dst io.Writer, root protoreflect.Message) {

	index := out.xrefIndex
	encodeVarint := func(n uint64) []byte {
		if n < 128 {
			return short[n]
		}
		var tmp [10]byte
		return tmp[:binary.PutUvarint(tmp[:], n)]
	}
	encodes64 := func(n int64) []byte {
		var tmp [8]byte
		binary.LittleEndian.PutUint64(tmp[:], uint64(n))
		return tmp[:]
	}
	encodes32 := func(n int32) []byte {
		var tmp [4]byte
		binary.LittleEndian.PutUint32(tmp[:], uint32(n))
		return tmp[:]
	}
	encode64 := func(n uint64) []byte {
		var tmp [8]byte
		binary.LittleEndian.PutUint64(tmp[:], n)
		return tmp[:]
	}
	encode32 := func(n uint32) []byte {
		var tmp [4]byte
		binary.LittleEndian.PutUint32(tmp[:], n)
		return tmp[:]
	}

	totalSize := func(lst [][]byte) (size int) {
		for _, frag := range lst {
			size += len(frag)
		}
		return size
	}

	pack := func(lst [][]byte) []byte {
		out := make([]byte, totalSize(lst))
		i := 0
		for _, frag := range lst {
			i += copy(out[i:], frag)
		}
		return out
	}

	putPtrRef := func(v protoreflect.Message) []byte {
		var p ptr.Ptr

		bitsField := v.Descriptor().Fields().ByNumber(1)
		bits := v.Get(bitsField).Interface()
		copy(p.Bits[:], bits.([]uint8))
		return encodeVarint(uint64(index[p]))
	}

	var walk func(v protoreflect.Message) [][]byte
	var visit func(f protoreflect.FieldDescriptor, v protoreflect.Value) [][]byte

	fieldTag := func(num protoreflect.FieldNumber, wireType int) []byte {
		return encodeVarint((uint64(num) << 3) + uint64(wireType))
	}

	visit = func(f protoreflect.FieldDescriptor, v protoreflect.Value) [][]byte {
		tag := f.Number()
		name := f.FullName()

		// serialize the field tag

		switch v := v.Interface().(type) {
		case protoreflect.List:
			n := v.Len()
			var lst [][]byte
			// nb there are different encoding of this as well,
			// but only for certain types ("packed repeated
			// primitive numeric" types like []uint8?)
			for i := 0; i < n; i++ {
				lst = append(lst, visit(f, v.Get(i))...)
			}
			if trace {
				log.Tracef(ctx, "field %d (%s) is a list (len %d)",
					tag, name, totalSize(lst))
			}
			return lst

		case protoreflect.Map:
			var lst [][]byte
			//keyWire := kindToWire(f.MapKey().Kind())
			//valueWire := kindToWire(f.MapValue().Kind())

			v.Range(func(k protoreflect.MapKey, v protoreflect.Value) bool {
				var inner [][]byte

				//inner = append(inner, fieldTag(1, keyWire))
				inner = append(inner, visit(f.MapKey(), k.Value())...)
				//lst = append(lst, fieldTag(2, valueWire))
				inner = append(inner, visit(f.MapValue(), v)...)

				lst = append(lst,
					fieldTag(tag, wireLengthDelimited),
					encodeVarint(uint64(totalSize(inner))),
					pack(inner))

				return true
			})
			if trace {
				log.Tracef(ctx, "field %d (%s) is a map (len %d)",
					tag, name, totalSize(lst))
				log.Tracef(ctx, "%x", lst)
			}
			return lst

		case protoreflect.EnumNumber:
			if f.Kind() != protoreflect.EnumKind {
				panic("we should handle EnumNumber w/o EnumKind encoding")
			}
			return [][]byte{
				fieldTag(tag, wireVarint),
				encodeVarint(uint64(v)),
			}

		case int32:
			// note that there are different encodings
			// (Int32Kind, Sint32Kind, Sfixed32Kind)
			var lst [][]byte
			switch f.Kind() {
			case protoreflect.Sfixed32Kind:
				lst = [][]byte{
					fieldTag(tag, wire32),
					encodes32(v),
				}
			case protoreflect.Int32Kind:
				// this is the "plain" kind, which is always 10 bytes
				// for negative values
				lst = [][]byte{
					fieldTag(tag, wireVarint),
					encodeVarint(uint64(v)),
				}
			case protoreflect.Sint32Kind:
				// this is the zig-zag encoded one
				lst = [][]byte{
					fieldTag(tag, wireVarint),
					encodeVarint(uint64(zigzagEncode32(v))),
				}
			default:
				panic("invalid field kind for int32")
			}
			if trace {
				log.Tracef(ctx, "field %d (%s) is a int32 kind %s (+%d)",
					tag, name, f.Kind(), totalSize(lst))
			}
			return lst

		case int64:
			// note that there are different encodings
			// (Int64Kind, Sint64Kind, Sfixed64Kind)
			var lst [][]byte
			switch f.Kind() {
			case protoreflect.Sfixed64Kind:
				lst = [][]byte{
					fieldTag(tag, wire64),
					encodes64(v),
				}
			case protoreflect.Int64Kind:
				// this is the "plain" kind, which is always 10 bytes
				// for negative values
				lst = [][]byte{
					fieldTag(tag, wireVarint),
					encodeVarint(uint64(v)),
				}
			case protoreflect.Sint64Kind:
				// this is the zig-zag encoded one
				lst = [][]byte{
					fieldTag(tag, wireVarint),
					encodeVarint(zigzagEncode64(v)),
				}
			default:
				panic("invalid field kind for uint64")
			}
			if trace {
				log.Tracef(ctx, "field %d (%s) is a uint64 kind %s (+%d)",
					tag, name, f.Kind(), totalSize(lst))
			}
			return lst

		case uint64:
			// note that there are different encodings
			// (Uint64Kind, Fixed64Kind)

			var lst [][]byte
			switch f.Kind() {
			case protoreflect.Fixed64Kind:
				lst = [][]byte{
					fieldTag(tag, wire64),
					encode64(v),
				}
			case protoreflect.Uint64Kind:
				lst = [][]byte{
					fieldTag(tag, wireVarint),
					encodeVarint(v),
				}
			default:
				panic("invalid field kind for uint64")
			}
			if trace {
				log.Tracef(ctx, "field %d (%s) is a uint64 kind %s (+%d)",
					tag, name, f.Kind(), totalSize(lst))
			}
			return lst

		case uint32:
			// note that there are different encodings
			// (Uint32Kind, Fixed32Kind)

			var lst [][]byte
			switch f.Kind() {
			case protoreflect.Fixed32Kind:
				lst = [][]byte{
					fieldTag(tag, wire32),
					encode32(v),
				}
			case protoreflect.Uint32Kind:
				lst = [][]byte{
					fieldTag(tag, wireVarint),
					encodeVarint(uint64(v)),
				}
			default:
				panic("invalid field kind for uint32")
			}
			if trace {
				log.Tracef(ctx, "field %d (%s) is a uint32 kind %s (+%d)",
					tag, name, f.Kind(), totalSize(lst))
			}
			return lst

		case bool:
			var x []byte
			if v {
				x = encodeVarint(1)
			} else {
				x = encodeVarint(0)
			}

			return [][]byte{fieldTag(tag, wireVarint), x}

		case float64:
			return [][]byte{
				fieldTag(tag, wire64),
				encode64(math.Float64bits(v)),
			}

		case float32:
			return [][]byte{
				fieldTag(tag, wire32),
				encode32(math.Float32bits(v)),
			}

		case protoreflect.Message:
			// check to see if it is a message of type foo_api.Ptr

			typeName := v.Descriptor().FullName()
			if typeName == "foo_api.Ptr" {
				lst := [][]byte{
					fieldTag(tag, wireVarint),
					putPtrRef(v),
				}
				if trace {
					log.Tracef(ctx, "field %d (%s) is a message of type PTR (+%d)",
						tag, name, totalSize(lst))
				}
				return lst
			} else {
				lst := [][]byte{
					fieldTag(tag, wireLengthDelimited),
				}
				lst = append(lst, walk(v)...)
				if trace {
					log.Tracef(ctx, "field %d (%s) is a message of type %s (+%d)",
						tag, name,
						typeName,
						totalSize(lst))
				}
				return lst
			}

		case string:
			lst := [][]byte{
				fieldTag(tag, wireLengthDelimited),
				encodeVarint(uint64(len(v))),
				[]byte(v),
			}
			if trace {
				log.Tracef(ctx, "field %d (%s) is a string (+%d)",
					tag, name, totalSize(lst))
			}
			return lst

		case []byte:
			lst := [][]byte{
				fieldTag(tag, wireLengthDelimited),
				encodeVarint(uint64(len(v))),
				v,
			}
			if trace {
				log.Tracef(ctx, "field %d (%s) is a []byte (+%d)",
					tag, name, totalSize(lst))
			}
			return lst

		default:
			if trace {
				log.Tracef(ctx, "field %d (%s) is unsupported %T",
					f.Number(),
					f.FullName(),
					v)
			}
			panic(fmt.Sprintf("Unsupported field %d (%s) %T",
				f.Number(),
				f.FullName(),
				v))
		}
	}

	// walk *includes* the length prefix of its children
	walk = func(v protoreflect.Message) [][]byte {
		lst := [][]byte{nil}
		v.Range(func(f protoreflect.FieldDescriptor, v protoreflect.Value) bool {
			lst = append(lst, visit(f, v)...)
			return true
		})

		lst[0] = encodeVarint(uint64(totalSize(lst[1:])))
		return lst
	}

	walk0 := func(v protoreflect.Message) [][]byte {
		var lst [][]byte

		v.Range(func(f protoreflect.FieldDescriptor, v protoreflect.Value) bool {
			lst = append(lst, visit(f, v)...)
			return true
		})
		return lst
	}

	lst := walk0(root)

	if trace {
		log.Tracef(ctx, "total size is %d", totalSize(lst))
	}
	i := 0
	for j, frag := range lst {
		if trace {
			log.Tracef(ctx, "(+%04x) frag[%d] len %d : %x", i, j, len(frag), frag)
		}
		i += len(frag)
		dst.Write(frag)
	}
}

type ptrlist []ptr.Ptr

func (l ptrlist) Len() int           { return len(l) }
func (l ptrlist) Less(i, j int) bool { return l[i].Less(l[j]) }
func (l ptrlist) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }

func NewSecureEncoding(mode CipherMode, keyId string, secure cipher.Block) *SecureEncoding {
	return &SecureEncoding{
		keyId:  keyId, //"171:foo/393",
		mode:   mode,
		secure: secure,
	}
}

func NewInsecure() *SecureEncoding {
	return &SecureEncoding{
		mode: CipherNONE,
	}
}

func grow(src []byte, need int) []byte {
	if cap(src) < len(src)+need {
		dst := make([]byte, len(src), len(src)+need)
		copy(dst, src)
		return dst
	}
	return src
}

type loggingWriter struct {
	W     io.Writer
	total int
}

func logwriter(w io.Writer) io.Writer {
	return &loggingWriter{W: w}
}

func (l *loggingWriter) Write(src []byte) (int, error) {
	n, err := l.W.Write(src)
	if trace {
		log.Tracef(context.TODO(), "* +%d did write %d of %d", l.total, n, len(src))
	}
	l.total += n
	return n, err
}

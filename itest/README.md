# Integration Test

## Setup

### Users and Domains

User `alice` is the paving user

User `alice` and `bob` are root admins (have "*" permissions)


User    | Perms for RED Domains | Perms for BLUE Domains | Perms for GREEN Domains
--------|-----------------------|------------------------|-------------------------
`carol` |     red:admin         |    blue:admin          |     green:admin
`david` |     red:user          |    blue:admin          |     green:admin
`ellen` |                       |    blue:user           |     green:admin
`fred`  |                       |    blue:user           |     green:admin
`grace` |                       |                        |     green:user

### Permission Sets

Permission Set    |   Operation    | Prefix       |  Permission Needed
------------------|----------------|--------------|----------------------
RED               |   GET          |  none        |  red:admin
RED               |   SCAN         |  none        |  red:admin
RED               |   FOLLOW       |  none        |  red:admin
RED               |   UPDATE       |  none        |  red:admin
RED               |   GET          |  public/     |  red:user
RED               |   GET          |  shared/     |  red:user
RED               |   FOLLOW       |  shared/     |  red:user


### Cluster

#### Paving

The cluster is started on `foo-0.itest.fufoo.net:6201` with:


```
{
  "admin": {
    "kid": "Vsi",
    "publicKey": "LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUhZd0VBWUhLb1pJemowQ0FRWUZLNEVFQUNJRFlnQUU0dFFCNXI2Ly9kaUNuaHRYdmJIM0JGZ1lLTW9wNGc0bwpWbGdORUF6MUs5SHV3QWxLaVpHN0EvOENjN3ovUWdDZkNjU1h6dnV0aHhtVGFJUTJ4UkVqcVRmeExOWFNpc0lPCndaMURodUEzV2xjdm5RcXhGR0FFYVppWjVhQWxCVURQCi0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLQo=",
    "name": "admin@itest.fufoo.net",
    "permissions": [
      "*"
    ]
  },
  "cluster": {
    "name": "urn:foo-cluster:foo-dev.callisto.axis.rscheme.org"
  }
}
```




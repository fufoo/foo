package main

import (
	"bytes"
	"context"
	"fmt"
	"github.com/joho/godotenv"
	"io"
	"io/ioutil"
	"os/exec"
	"strings"
	"sync"
	"time"
)

func main() {
	err := godotenv.Load("integration_test.env")
	if err != nil {
		panic(err)
	}
	tmpdir, err := ioutil.TempDir("", "foo-test-*.d")
	if err != nil {
		panic(err)
	}
	fmt.Printf("**************** %s\n", tmpdir)
	r := &testRun{
		tmpdir: tmpdir,
	}
	rootcase := &testCase{}
	ctx := context.WithValue(context.Background(), contextCaseKey, rootcase)
	r.root(ctx)

	for _, clean := range rootcase.cleanups {
		clean()
	}

}

type testRun struct {
	tmpdir string
}

type testCase struct {
	parent   *testCase
	name     string
	cleanups []func()
}

func (tc *testCase) Cleanup(thunk func()) {
	tc.cleanups = append(tc.cleanups, thunk)
}

type contextKey int

const (
	contextCaseKey = contextKey(iota)
)

func contextCase(ctx context.Context) *testCase {
	v := ctx.Value(contextCaseKey)
	return v.(*testCase)
}

func (c *testCase) Root() *testCase {
	if c.parent == nil {
		return c
	}
	return c.parent.Root()
}

func (c *testCase) Depth() int {
	if c.parent != nil {
		return 1 + c.parent.Depth()
	}
	return 0
}

func (r *testRun) Subtest(ctx context.Context, name string, fn func(context.Context)) {
	sub := &testCase{
		parent: contextCase(ctx),
		name:   name,
	}
	ctx = context.WithValue(ctx, contextCaseKey, sub)

	indent := 4 * sub.Depth()
	fmt.Printf("%*sSTART %s\n", indent, "", name)
	t0 := time.Now()
	fn(ctx)
	dt := time.Since(t0)
	fmt.Printf("%*sEND %s [%s]\n", indent, "", name, dt)
	// run cleanups
	for _, clean := range sub.cleanups {
		clean()
	}
}

func (r *testRun) Infof(ctx context.Context, msg string, args ...interface{}) {
	this := contextCase(ctx)
	indent := 4 * (1 + this.Depth())
	fmt.Printf("%*sINFO %s\n", indent, "", fmt.Sprintf(msg, args...))
}

func (r *testRun) Fatalf(msg string, args ...interface{}) {
	panic(fmt.Sprintf(msg, args...))
}

func (r *testRun) Errorf(msg string, args ...interface{}) {
	panic(fmt.Sprintf(msg, args...))
}

func (r *testRun) Fatal(why interface{}) {
	panic(why)
}

func (r *testRun) shell(ctx context.Context, line string, input []byte) (int, []byte, []byte) {
	words := strings.Split(line, " ")
	cmd := exec.Command(words[0], words[1:]...)

	stderr, err := cmd.StderrPipe()
	if err != nil {
		r.Fatal(err)
	}

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		r.Fatal(err)
	}

	if len(input) > 0 {
		stdin, err := cmd.StdinPipe()
		if err != nil {
			r.Fatal(err)
		}
		go func() {
			defer stdin.Close()
			io.Copy(stdin, bytes.NewReader(input))
		}()
	}

	var outbuf, errbuf []byte

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		defer wg.Done()
		outbuf, _ = ioutil.ReadAll(stdout)
	}()
	go func() {
		defer wg.Done()
		errbuf, _ = ioutil.ReadAll(stderr)
	}()

	err = cmd.Run()
	wg.Wait() // finish slurping up the output

	if err != nil {
		if exits, ok := err.(*exec.ExitError); ok {
			return exits.ExitCode(), outbuf, errbuf
		}
		r.Fatal(err)
	}
	return 0, outbuf, errbuf
}

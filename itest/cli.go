package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path/filepath"
)

func (r *testRun) createUsers(ctx context.Context) {
	r.Subtest(ctx, "Alice", r.createUserTester("alice"))
	r.Subtest(ctx, "Bob", r.createUserTester("bob"))
}

func (r *testRun) userKeygener(name string) func(ctx context.Context) {
	return func(ctx context.Context) {
		cmd := fmt.Sprintf("fooctl subject keygen -o %s -p %s",
			filepath.Join(r.tmpdir, name+".creds"),
			filepath.Join(r.tmpdir, name+".pub"))
		rc, stdout, stderr := r.shell(ctx, cmd, nil)
		if rc != 0 {
			r.Fatalf("FIT-1000 Expected exit status 0 got %d", rc)
		}
		if len(stdout) > 0 {
			r.Errorf("FIT-1001 Expected no stdout, got %d bytes", len(stdout))
		}
		if len(stderr) > 0 {
			r.Errorf("FIT-1002 Expected no stderr, got %d bytes", len(stderr))
		}
	}
}

func (r *testRun) createUserTester(name string) func(ctx context.Context) {
	return func(ctx context.Context) {
		r.userKeygener(name)(ctx)
	}
}

func (r *testRun) updateSubjectId(ctx context.Context, name string, id string) *Creds {
	c := r.creds(name)
	c.set("subjectId", id)
	c.writeback()
	return c
}

type Creds struct {
	src  string
	run  *testRun
	data map[string]interface{}
}

func (c *Creds) writeback() {
	buf, err := json.MarshalIndent(c.data, "", "  ")
	if err != nil {
		c.run.Fatal(err)
	}
	err = ioutil.WriteFile(c.src, append(buf, '\n'), 0600)
	if err != nil {
		c.run.Fatal(err)
	}
}

func (c *Creds) get(key string) interface{} {
	return c.data[key]
}

func (c *Creds) set(key string, value interface{}) {
	c.data[key] = value
}

func (r *testRun) creds(name string) *Creds {

	c := &Creds{
		run: r,
		src: filepath.Join(r.tmpdir, name+".creds"),
	}

	buf, err := ioutil.ReadFile(c.src)
	if err != nil {
		r.Fatal(err)
	}

	err = json.Unmarshal(buf, &c.data)
	if err != nil {
		r.Fatal(err)
	}
	return c
}

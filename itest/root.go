package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"os/exec"
	"path/filepath"
	"time"
)

func (r *testRun) root(ctx context.Context) {

	r.Subtest(ctx, "CreateRootUser", r.createRootUser)
	r.Subtest(ctx, "StartServer", r.startServer)
}

func (r *testRun) startServer(ctx context.Context) {

	port := "6699"

	c := exec.Command("fooctl", "serve",
		"--rest",
		"--port", port,
		"--pave", filepath.Join(r.tmpdir, "pave.json"),
		"--insecure")
	serverLog, err := os.Create(filepath.Join(r.tmpdir, "srv-0.log"))
	if err != nil {
		r.Fatal(err)
	}

	c.Stdout = serverLog
	c.Stderr = os.Stderr
	c.Env = append(os.Environ(), fmt.Sprintf("FOO_MICRO=%s", filepath.Join(r.tmpdir, "srv-0.db")))
	err = c.Start()
	if err != nil {
		r.Fatal(err)
	}

	// wait for it to come up
	addr, err := net.ResolveTCPAddr("tcp", fmt.Sprintf("localhost:%s", port))
	if err != nil {
		r.Fatal(err)
	}

	halt := make(chan struct{})

	go func() {
		defer close(halt)
		c.Wait()
	}()

	t0 := time.Now()
	for i := 0; ; i++ {
		select {
		case <-halt:
			r.Fatalf("server died before it came up")
		default:
		}

		time.Sleep(100 * time.Millisecond)
		conn, err := net.DialTCP("tcp", nil, addr)
		if err == nil {
			dt := time.Since(t0)
			r.Infof(ctx, "FIT-1100 server is up after %s", dt)
			conn.Close()
			break
		} else if i > 50 {
			fmt.Printf("server not yet up after %s: %s\n",
				time.Since(t0).Round(time.Second),
				err)
		} else if i > 70 {
			c.Process.Kill()
			r.Fatal("timed out waiting for server to come up")
		}
	}

	contextCase(ctx).Root().Cleanup(func() {
		fmt.Printf("(Shutdown server)\n")
		c.Process.Kill()
	})
}

// create the root user and set up the paving file
func (r *testRun) createRootUser(ctx context.Context) {
	r.Subtest(ctx, "Keygen", r.userKeygener("root"))

	// the root user always has id 100
	creds := r.updateSubjectId(ctx, "root", "100")

	// build a paving json

	pub, err := ioutil.ReadFile(filepath.Join(r.tmpdir, "root.pub"))
	if err != nil {
		r.Fatal(err)
	}

	var paver struct {
		KeyID       string   `json:"kid"`
		PublicKey   []byte   `json:"publicKey"`
		Name        string   `json:"name"`
		Permissions []string `json:"permissions"`
		IsApp       bool     `json:"isApp"`
	}
	paver.KeyID = creds.get("kid").(string)
	paver.PublicKey = pub
	paver.Name = "root"
	paver.Permissions = []string{"*"}

	paverbuf, err := json.MarshalIndent(&paver, "", "  ")
	if err != nil {
		r.Fatal(err)
	}
	err = ioutil.WriteFile(
		filepath.Join(r.tmpdir, "pave.json"),
		append(paverbuf, '\n'),
		0666)
	if err != nil {
		r.Fatal(err)
	}
}

# Flexible Observable Objects (Foo) Database

## (Simplified) Running Locally

Create a storage area
```
mkdir /tmp/foo-cluster.d
```

Provision an admin client API key
```
/tmp/fooctl client keygen -o /tmp/foo-cluster.d/admin.key
```

The first time launching the cluster, the `-clustername` argument
should be specified with the distinguished name of the cluster.  This
will be the certificate name of the CA used to sign SSL certificates
for the cluster.  The `-hostname` may also be specified if the name by
which the nodes of the cluster are addressed is not `localhost`.

The root client key should also be enrolled by specifying the root key
file with the `-rootclient` option.

```
/tmp/run-foo-dev-cluster -clustername "c=US, st=Texas, l=Austin, o=FuFoo Network, ou=Development, cn=$HOSTNAME" -hostname localhost -rootclient /tmp/foo-cluster.d/admin.key
```

After that, the cluster name may be omitted as the root certificate
will already exist.

Accessing the cluster is done using a foo client config file and
setting the `FOO_CONFIG` environment variable to point to that file.
For admin access, specify the path to the admin key in the `auth.file`
property like so
```
{
  "secret": "my-secret",
  "server": "localhost:2230",
  "caTrustFile": "/tmp/foo-cluster.d/ca/root.cert",
  "debug": "trace",
  "auth": {
    "mechanism": "client-key",
    "file": "/tmp/foo-cluster.d/admin.key"
  }
}
```

## References

What we are doing is very similar to "Prolly" Trees from dolt:

https://docs.dolthub.com/architecture/storage-engine/prolly-tree

see also
https://github.com/attic-labs/noms/blob/master/doc/intro.md#prolly-trees-probabilistic-b-trees

in particular:

> A database has two responsibilities: it provides storage of
> content-addressed chunks of data, and it keeps track of zero or more
> datasets.

sounds a lot like foo (dataset = observable)

and noting that Noms dates back to 2015, predating my earliest work on
omen :nice:

```
commit 68c3ac02058e559367534aeeb7d9f8f483a4db1b
Author: Aaron Boodman <aaron@aaronboodman.com>
Date:   Tue Jun 2 20:45:33 2015 -0700

    first commit
```


## Account Management

### Create a client account and enroll it

Note that the account id must be a PTR of type STRING, obtained as by:

```
$ /tmp/fooctl literal donovan
nmGm0IIdhMdgrmvcRePuDkb0Sz6BPS3UJ1ui2gYhpLTs
```

### Generate the credentials and enroll it

1. generate key (`fooctl client keygen`)
2. enroll key (`fooctl client enroll`)

for example:

```
fooctl client keygen -o myclient.key
fooctl client enroll -i /tmp/myclient.key --owner nmGm0IIdhMdgrmvcRePuDkb0Sz6BPS3UJ1ui2gYhpLTs
```

### Configure a config file

config.json
```
{
  "server": "localhost:2230",
  "caTrustFile": "/test/donovan/foo-cluster.d/db/ca/root.cert",
  "auth": {
    "mechanism": "client-key",
    "file": "/tmp/myclient.key"
  }
}
```

### Now you can acquire domains

```
FOO_CONFIG=config.json fooctl admin acquire --name test123 --region dev/us.nm.0
```

### And you can run grpc

This provides a bridge from (regular) gRPC to a qrpc-based foo cluster

```
FOO_CONFIG=/test/donovan/foo-cluster.d/db/foo.config.json /tmp/foo-grpc
```

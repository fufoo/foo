# Foo Authorization Policies

Access policy for a domain is defined in terms of two
parts, first an *observable groups* map which reduces the domain 
of observables names to something policy-relevant.
The second is a *rule* defined in OPA (Open Policy Agent)
terms.

Here is how policy checking works:

1. The token is checked for authentication.  If not authenticated
   (with claim validation, modulo expiration), the request is declined with
   a 401.
2. The claim is checked for expiry.  If expired, the request is declined
   with a 401.
3. The policy is loaded from the domain being accessed.
4. The transaction is turned into a set of observable groups
   with READ or WRITE access annotations.
5. The authenticated claims and the observable group access request is
   run through the policy rule.  If policy check fails, then the
   request is declined with a 403.

The first step is memoized with respect to the token.

The last step is memoized with respect to the observable groups, the
claims, the rule, and any external data accessed by the rule itself.

**NOTE** External data access by the rule has to be carefully
controlled to avoid a rogue policy from leaking information.  The
policy should be able to access anything in the domain, but accessing
anything outside the domain is dangerous (e.g., ldap group
information).  It may be reasonable (especially for locality reasons,
since we are sharding by domain) to *require* any accessed data to be
in the target domain.  If you need something like LDAP group lookups,
then it needs to get replicated from the source of truth into the
domain.  That would give foo a seam along which to manage
intra-enterprise but cross-domain authorization.  e.g., there must be
a policy somewhere that says an RScheme domain can access RScheme user
groups but Atlassian domains can't.  This could be lightweight, since
the data itself doesn't need to be copied provided they are in the
same DAR (BYOK?) domain.

(* DAR = Data At Rest [encryption])
(* BYOK = Bring Your Own Key)

## Observable Groups

```
{
  "observableGroups": [
    {
      "prefix": "u/",
      "group": "user"
    },
    {
      "prefix": "p/",
      "group": "play"
    },
    {
      "group": "other"
    }
  ]
}
```

## OPA Policy Input

```
{
  "user": {
    "claims": {
      ...
    }
  }
  "read": ["user"],
  "write": ["play"]
}
```

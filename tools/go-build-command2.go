package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
	"time"

	"golang.org/x/mod/modfile"
)

// walk up until we find a go.mod

func main() {
	top := findClosestGoModule()
	if top == "." {
		fmt.Fprintf(os.Stderr, "Could not find a go.mod file\n")
		os.Exit(1)
	}

	mf := readModule(top)
	modName := mf.Module.Mod.Path

	buf := &bytes.Buffer{}
	fmt.Fprintf(buf, "cd %s ;", top)

	// all the named arguments should be either relative package paths
	// or relative to a cmd/ subpackage

	testmode := false
	runmode := false
	any := false
	outputdir := os.TempDir()
	outputfile := ""
	readonly := false
	vendormode := false
	tags := ""

	version := makeVersionInfo(top)
	if version.Version != "" {
		fmt.Printf("# %s\n", version.Version)
	}
	for _, arg := range os.Args[1:] {
		if arg == "-test" {
			testmode = true
			continue
		}
		if strings.HasPrefix(arg, "-tags=") {
			tags = strings.TrimPrefix(arg, "-tags=")
			continue
		}
		if arg == "-ro" {
			// pass '-mod=readonly' to avoid side-effects
			// note that a build might fail with "go:
			// updates to go.sum needed, disabled by
			// -mod=readonly" which probably indicates
			// that target has not been built without -ro
			// mode yet
			readonly = true
			continue
		}
		if arg == "-vendor" {
			// pass '-mod=vendor' to allow what we're building
			// to build with vendored packages
			vendormode = true
			continue
		}
		if arg == "-run" {
			runmode = true
			continue
		}
		if strings.HasPrefix(arg, "-output=") {
			outputdir = strings.TrimPrefix(arg, "-output=")
			continue
		}
		if strings.HasPrefix(arg, "-outputfile=") {
			outputfile = strings.TrimPrefix(arg, "-outputfile=")
			continue
		}

		base := path.Base(arg)
		v := version
		v.Name = base

		target := identifyTarget(modName, top, arg)

		if target == "" {
			fmt.Fprintf(os.Stderr, "Could not identify target package for %q\n", arg)
			os.Exit(1)
		}

		if any {
			fmt.Fprintf(buf, " &&")
		}
		if testmode {
			fmt.Fprintf(buf, " go test -v %s",
				target)
		} else {
			// note that output respects both TMPDIR and
			// also should work on Windows, not that we care
			if base == "." {
				// fmt.Fprintf(os.Stderr, "# %q\n", modName)
				base = filepath.Base(modName)
			}

			output := outputfile
			if output == "" {
				output = filepath.Join(outputdir, base)
			}
			shargs := []string{
				"go",
				"build",
				"-o",
				output,
			}
			if tags != "" {
				shargs = append(shargs, "-tags", tags)
			}
			if readonly {
				shargs = append(shargs, "-mod=readonly")
			}
			if vendormode {
				shargs = append(shargs, "-mod=vendor")
			}
			shargs = append(shargs, v.arg()...)
			shargs = append(shargs, target)
			buf.WriteByte(' ')
			buf.WriteString(strings.Join(shargs, " "))
		}
		any = true
	}
	buf.WriteByte('\n')
	os.Stdout.Write(buf.Bytes())

	if runmode {
		x := exec.Command("sh", "-c", buf.String())
		x.Stderr = os.Stderr
		x.Stdout = os.Stdout
		err := x.Run()
		if err != nil {
			fmt.Fprintf(os.Stderr, "command failed: %s\n", err)
			os.Exit(1)
		}
	}
}

func identifyTarget(mod, top, name string) string {
	isdir := func(rel string) bool {
		f := filepath.Join(top, filepath.FromSlash(rel))
		sb, err := os.Stat(f)
		if err != nil {
			return false
		}
		return sb.IsDir()
	}
	if isdir(name) {
		return path.Join(mod, name)
	}
	if isdir(path.Join("cmd", name)) {
		return path.Join(mod, "cmd", name)
	}
	return ""
}

func findClosestGoModule() string {

	here, err := filepath.Abs(".")
	if err != nil {
		panic(err)
	}

	for {
		_, err := os.Stat(filepath.Join(here, "go.mod"))
		if err == nil {
			return here
		}
		if here == "/" {
			return "."
		}
		here = filepath.Join(here, "..")
	}
}

func readModule(top string) *modfile.File {
	f := filepath.Join(top, "go.mod")
	buf, err := ioutil.ReadFile(f)
	if err != nil {
		panic(err)
	}

	mf, err := modfile.ParseLax(f, buf, nil)
	if err != nil {
		panic(err)
	}
	return mf
}

type Version struct {
	Version   string    `json:"version"`
	BuildTime time.Time `json:"build_time"`
	Commit    string    `json:"commit,omitempty"`
	Branch    string    `json:"branch,omitempty"`
	Tag       string    `json:"tag,omitempty"`
	Name      string    `json:"name"`
}

func (v *Version) arg() []string {
	if v == nil {
		return nil
	}
	vbuf, _ := json.Marshal(v)
	encv := base64.StdEncoding.EncodeToString(vbuf)

	return []string{
		"-ldflags",
		fmt.Sprintf("'-X main.BuildTicket=%s'", encv),
	}
}

func makeVersionInfo(d string) Version {
	t := time.Now()
	return Version{
		Version:   versionTag(d, t),
		Commit:    headCommit(d),
		Branch:    branch(d),
		BuildTime: time.Now(),
		Tag:       headTag(d),
	}
}

func git(dir string, args ...string) string {

	cmd := exec.Command("git", args...)
	cmd.Dir = dir
	buf, err := cmd.Output()
	if err != nil {
		fmt.Fprintf(os.Stderr, "# error running: git %s\n",
			strings.Join(args, " "))
		return ""
	}
	return strings.TrimSpace(string(buf))
}

func versionTag(dir string, t time.Time) string {
	dirtyFlag := fmt.Sprintf("--dirty=%s",
		strings.ToLower(t.Format("-m2Jan1504")))
	return git(dir, "describe", dirtyFlag)
}

func headCommit(dir string) string {
	return git(dir, "rev-parse", "HEAD")
}

func headTag(dir string) string {
	return git(dir, "tag", "--points-at")
}

func branch(dir string) string {
	return git(dir, "rev-parse", "--abbrev-ref", "HEAD")
}

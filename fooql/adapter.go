package fooql

import (
	"bitbucket.org/fufoo/core/ptr"
)

type Ptr = ptr.Ptr
type blah struct {
	x []Ptr
}

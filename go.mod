module bitbucket.org/fufoo/foo

go 1.22

//replace bitbucket.org/fufoo/fulang => ../fulang
//replace bitbucket.org/fufoo/core => ../core

// replace bitbucket.org/fufoo/qrpc => ../qrpc

//replace bitbucket.org/fufoo/fulang => ../fulang

//replace bitbucket.org/dkolbly/logging => ../../dkolbly/logging
//replace bitbucket.org/dkolbly/earley3 => ../../dkolbly/earley3

require (
	bitbucket.org/dkolbly/logging v0.9.6
	bitbucket.org/fufoo/core v0.0.6
	bitbucket.org/fufoo/fulang v0.0.2
	bitbucket.org/fufoo/qrpc v0.1.7
	github.com/dgraph-io/badger/v3 v3.2103.1
	github.com/dkolbly/cli v2.0.0+incompatible
	github.com/go-chi/chi v1.5.4
	github.com/golang-jwt/jwt/v4 v4.2.0
	github.com/golang/protobuf v1.5.3
	github.com/google/uuid v1.6.0
	github.com/hashicorp/memberlist v0.5.0
	github.com/joho/godotenv v1.3.0
	github.com/kelindar/bitmap v1.2.1
	github.com/quic-go/quic-go v0.41.0
	github.com/spaolacci/murmur3 v1.1.0
	go.etcd.io/bbolt v1.3.5
	golang.org/x/mod v0.15.0
	google.golang.org/grpc v1.62.0
	google.golang.org/protobuf v1.32.0
)

require (
	bitbucket.org/dkolbly/earley3 v1.0.3 // indirect
	bitbucket.org/fufoo/cli v0.0.4 // indirect
	bitbucket.org/fufoo/slogging v0.0.3 // indirect
	github.com/armon/go-metrics v0.0.0-20180917152333-f0300d1749da // indirect
	github.com/boltdb/bolt v1.3.1 // indirect
	github.com/cespare/xxhash v1.1.0 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgraph-io/ristretto v0.1.0 // indirect
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/go-task/slim-sprig v0.0.0-20230315185526-52ccab3ef572 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/golang/glog v1.2.0 // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/golang/snappy v0.0.3 // indirect
	github.com/google/btree v0.0.0-20180813153112-4030bb1f1f0c // indirect
	github.com/google/flatbuffers v1.12.0 // indirect
	github.com/google/pprof v0.0.0-20240227163752-401108e1b7e7 // indirect
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-immutable-radix v1.0.0 // indirect
	github.com/hashicorp/go-msgpack v0.5.3 // indirect
	github.com/hashicorp/go-multierror v1.0.0 // indirect
	github.com/hashicorp/go-sockaddr v1.0.0 // indirect
	github.com/hashicorp/golang-lru v0.5.0 // indirect
	github.com/klauspost/compress v1.12.3 // indirect
	github.com/klauspost/cpuid/v2 v2.0.9 // indirect
	github.com/liamg/tml v0.6.0 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/miekg/dns v1.1.26 // indirect
	github.com/onsi/ginkgo/v2 v2.15.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/quic-go/qtls-go1-20 v0.4.1 // indirect
	github.com/sean-/seed v0.0.0-20170313163322-e2103e2c3529 // indirect
	go.opencensus.io v0.22.5 // indirect
	go.uber.org/mock v0.4.0 // indirect
	golang.org/x/crypto v0.20.0 // indirect
	golang.org/x/exp v0.0.0-20240222234643-814bf88cf225 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	golang.org/x/tools v0.18.0 // indirect
	google.golang.org/genproto v0.0.0-20240123012728-ef4313101c80 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240123012728-ef4313101c80 // indirect
)

//replace bitbucket.org/fufoo/core => ../core
//replace bitbucket.org/fufoo/qrpc => ../qrpc

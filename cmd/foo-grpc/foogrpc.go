// foo-grpc provides a single GRPC API to talk to a foo cluster;
// it is basically a bridge from GRPC to QRPC
package main

import (
	"bytes"
	"context"
	"crypto"
	"fmt"
	"io"
	"log/slog"
	"math/rand"
	"net"
	"os"
	"time"

	"bitbucket.org/fufoo/core/errlib"

	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/services/cam"
	"bitbucket.org/fufoo/foo/services/state"
	"bitbucket.org/fufoo/slogging"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	//"google.golang.org/grpc/credentials/tls/certprovider/pemfile"
	"google.golang.org/grpc/credentials/insecure"

	"bitbucket.org/fufoo/foo/cmd/internal/mint"
)

var log = slogging.New("foogrpc")

type Server struct {
	conn   *foo.Conn
	issuer *mint.Issuer
	ipub   map[string]crypto.PublicKey
}

type stateBridge struct {
	s *Server
	state.UnimplementedStateServer
}

type camBridge struct {
	s *Server
	cam.UnimplementedStorageServer
}

func main() {

	ctx := context.Background()

	cert := os.Getenv("TLS_CERTIFICATE")
	key := os.Getenv("TLS_KEY")

	/*
		cp, err := pemfile.NewProvider(pemfile.Options{
			CertFile: cert,
			KeyFile: key,
			RefreshDuration: time.Minute,
		})
	*/

	var cp *CertProvider

	if cert != "" && key != "" {
		var err error
		cp, err = newCertProvider(cert, key)
		if err != nil {
			log.Error("failed to load",
				slog.String("msgid", "FOA-9908"),
				slog.String("error", err.Error()))
			os.Exit(1)
		}
	}

	listen := os.Getenv("LISTEN_ON")
	if listen == "" {
		listen = ":7777"
	}

	lis, err := net.Listen("tcp", listen)
	if err != nil {
		panic(err)
	}

	f, err := initFoo(ctx)
	if err != nil {
		panic(err)
	}

	srv := &Server{
		conn: f,
		ipub: make(map[string]crypto.PublicKey),
	}

	srv.setupAuth(ctx)

	var opts []grpc.ServerOption

	if cp == nil {
		opts = append(opts, grpc.Creds(insecure.NewCredentials()))
	} else {
		opts = append(opts, grpc.Creds(cp))
	}

	opts = append(opts,
		grpc.ChainUnaryInterceptor(srv.unary),
		grpc.ChainStreamInterceptor(srv.stream))

	grpcServer := grpc.NewServer(opts...)

	state.RegisterStateServer(grpcServer, stateBridge{s: srv})
	cam.RegisterStorageServer(grpcServer, camBridge{s: srv})

	grpcServer.Serve(lis)
}

func (me *Server) stream(srv any, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
	ctx, claims, err := me.authMiddleware(stream.Context())
	if err != nil {
		return err
	}
	log.Info("starting stream",
		slog.String("method", info.FullMethod),
		slog.String("subject", claims.Subject))

	return handler(srv, recontextual{
		ServerStream: stream,
		ctx:          ctx,
	})
}

// recontextual implements grpc.ServerStream, and is a means by which we can
// insert our own context into the flow
type recontextual struct {
	grpc.ServerStream
	ctx context.Context
}

func (r recontextual) Context() context.Context {
	return r.ctx
}

func (srv *Server) authMiddleware(ctx context.Context) (context.Context, *authClaims, error) {
	var authz string

	if md, ok := metadata.FromIncomingContext(ctx); ok {
		for k, vs := range md {
			for _, v := range vs {
				log.Info("incoming metadata", slog.String("key", k), slog.String("value", v))
			}
		}
		if a := md["authorization"]; len(a) == 1 {
			authz = a[0]
		}
	}

	if authz == "" {
		n := 50 + rand.Intn(50)
		time.Sleep(time.Duration(n) * time.Millisecond)
		return nil, nil, errlib.Newf("FOA-9970 no authorization supplied")
	}

	claims := srv.VerifyToken(ctx, authz)
	if claims == nil {
		n := 50 + rand.Intn(50)
		time.Sleep(time.Duration(n) * time.Millisecond)
		return nil, nil, errlib.Newf("FOA-9971 invalid authorization supplied")
	}

	return context.WithValue(
		ctx,
		authContextKey,
		&Auth{
			claims: claims,
		},
	), claims, nil
}

func (srv *Server) unary(ctx context.Context, req any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (any, error) {
	ctx, claims, err := srv.authMiddleware(ctx)
	if err != nil {
		return nil, err
	}
	log.Info("unary call",
		slog.String("method", info.FullMethod),
		slog.String("subject", claims.Subject))
	return handler(ctx, req)
}

type contextKey int

const (
	authContextKey contextKey = iota
)

type Auth struct {
	claims *authClaims
}

func AuthFromContext(ctx context.Context) *Auth {
	v := ctx.Value(authContextKey)
	if v == nil {
		return nil
	}
	return v.(*Auth)
}

// these are easier... the trickier bridge is for streaming data
func (b stateBridge) Get(ctx context.Context, x *state.GetRequest) (*state.GetResponse, error) {
	auth := AuthFromContext(ctx)

	if !auth.claims.HasScope("state:read") {
		return nil, errlib.Newf("FOA-8101 token does not include scope for state:read")
	}
	if !auth.claims.AllowDomain(x.Domain) {
		return nil, errlib.Newf("FOA-8102 token does not permit access to domain #%d", x.Domain)
	}

	var buf bytes.Buffer
	fmt.Fprintf(&buf, "%s | ", time.Now().Format(time.RFC3339Nano))
	fmt.Fprintf(&buf, "GET #%d", x.Domain)
	for _, s := range x.Subjects {
		fmt.Fprintf(&buf, " %q", s)
	}
	buf.WriteByte('\n')
	os.Stdout.Write(buf.Bytes())
	//
	return b.s.conn.State().Get(ctx, x)
}

func (b stateBridge) Apply(ctx context.Context, x *state.TxnRequest) (*state.TxnRecord, error) {
	auth := AuthFromContext(ctx)

	if !auth.claims.HasScope("state:write") {
		return nil, errlib.Newf("FOA-8103 token does not include scope for state:write")
	}
	if !auth.claims.AllowDomain(x.Domain) {
		return nil, errlib.Newf("FOA-8104 token does not permit access to domain #%d", x.Domain)
	}

	var buf bytes.Buffer
	fmt.Fprintf(&buf, "%s | ", time.Now().Format(time.RFC3339Nano))
	fmt.Fprintf(&buf, "APPLY #%d", x.Domain)
	for _, s := range x.Subjects {
		fmt.Fprintf(&buf, " %q", s)
	}
	buf.WriteByte('\n')
	os.Stdout.Write(buf.Bytes())
	//
	return b.s.conn.State().Apply(ctx, x)
}

func (b camBridge) Load(ctx context.Context, x *cam.LoadRequest) (*cam.LoadResponse, error) {
	auth := AuthFromContext(ctx)

	if !auth.claims.HasScope("cam:read") {
		return nil, errlib.Newf("FOA-8105 token does not include scope for cam:read")
	}

	var buf bytes.Buffer
	fmt.Fprintf(&buf, "%s | ", time.Now().Format(time.RFC3339Nano))
	fmt.Fprintf(&buf, "LOAD")
	for _, s := range x.Refs {
		fmt.Fprintf(&buf, " {%s}", s.Must().String())
	}
	buf.WriteByte('\n')
	os.Stdout.Write(buf.Bytes())
	//
	return b.s.conn.Storage().Load(ctx, x)
}

func (b camBridge) Store(ctx context.Context, x *cam.StoreRequest) (*cam.StoreResponse, error) {
	auth := AuthFromContext(ctx)

	if !auth.claims.HasScope("cam:write") {
		return nil, errlib.Newf("FOA-8106 token does not include scope for cam:write")
	}
	if !auth.claims.AllowDomain(x.Domain) {
		return nil, errlib.Newf("FOA-8107 token does not permit access to domain #%d", x.Domain)
	}

	var buf bytes.Buffer
	fmt.Fprintf(&buf, "%s | ", time.Now().Format(time.RFC3339Nano))
	fmt.Fprintf(&buf, "STORE")

	ret, err := b.s.conn.Storage().Store(ctx, x)

	if err != nil {
		fmt.Fprintf(&buf, " *ERR")
	} else {
		for _, s := range ret.Refs {
			fmt.Fprintf(&buf, " {%s}", s.Must().String())
		}
	}

	buf.WriteByte('\n')
	os.Stdout.Write(buf.Bytes())
	return ret, err
}

func (b stateBridge) Follow(x *state.FollowRequest, srv state.State_FollowServer) error {
	auth := AuthFromContext(srv.Context())

	if !auth.claims.HasScope("state:watch") {
		return errlib.Newf("FOA-8108 token does not include scope for state:watch")
	}
	if !auth.claims.AllowDomain(x.Domain) {
		return errlib.Newf("FOA-8109 token does not permit access to domain #%d", x.Domain)
	}

	var buf bytes.Buffer
	fmt.Fprintf(&buf, "%s | ", time.Now().Format(time.RFC3339Nano))
	fmt.Fprintf(&buf, "FOLLOW #%d", x.Domain)
	for _, s := range x.Subjects {
		fmt.Fprintf(&buf, " %q", s)
	}
	buf.WriteByte('\n')
	os.Stdout.Write(buf.Bytes())
	//
	ctx := srv.Context()

	resp, err := b.s.conn.State().Follow(ctx, x)
	if err != nil {
		return err
	}
	t0 := time.Now()
	n := 0
	for {
		event, err := resp.Recv()
		if err != nil {
			dt := time.Since(t0)

			buf.Reset()
			fmt.Fprintf(&buf, "%s | ", time.Now().Format(time.RFC3339Nano))
			fmt.Fprintf(&buf, "FOLLOW #%d", x.Domain)
			for _, s := range x.Subjects {
				fmt.Fprintf(&buf, " %q", s)
			}
			fmt.Fprintf(&buf, " complete, sent %d events in %s",
				n,
				dt.Round(time.Second))
			buf.WriteByte('\n')
			os.Stdout.Write(buf.Bytes())
			//
			if err == io.EOF {
				return nil
			}
			return err
		}
		srv.Send(event)
		n++
	}
}

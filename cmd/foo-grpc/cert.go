package main

import (
	"context"
	"crypto/sha256"
	"crypto/tls"
	"crypto/x509"
	"encoding/hex"
	//"encoding/pem"
	//"fmt"
	"io"
	"log/slog"
	"net"
	"os"
	"sync"
	"time"

	"bitbucket.org/fufoo/core/errlib"
	"google.golang.org/grpc/credentials"
)

// this is designed to operate in an environment where `certbot` (see
// https://certbot.eff.org/instructions?ws=other&os=ubuntufocal) is
// keeping a file (i.e.,
// /etc/letsencrypt/live/foo-dev.fufoo.net/privkey.pem) up to date
// out-of-band to us.  We are running in a k8s pod with that directory
// mounted on /cert or something like that, so we want to keep an eye
// on the file.
//
// tls.Config has a field called `GetCertificate` that we will implement

type CertProvider struct {
	lock    sync.Mutex
	cert    *tls.Certificate
	public  string
	private string
	last    time.Time
	hash    [32]byte
}

func newCertProvider(pub, priv string) (*CertProvider, error) {
	cp := &CertProvider{
		public:  pub,
		private: priv,
	}
	// first fill
	err := cp.refill()
	if err != nil {
		log.Error("cannot obtain initial certificates for TLS",
			slog.String("msgid", "FOA-9901"),
			slog.String("error", err.Error()))
		return nil, err
	}
	return cp, nil
}

func (cp *CertProvider) getCertificate(hello *tls.ClientHelloInfo) (*tls.Certificate, error) {
	if hello != nil {
		log.Info("got a client hello")
	} else {
		log.Info("no client hello")
	}
	cp.lock.Lock()
	defer cp.lock.Unlock()

	if cp.cert == nil {
		return nil, errlib.Newf("FOA-9918 no certificate available")
	}
	return cp.cert, nil
}

func (cp *CertProvider) refill() error {

	fd, err := os.Open(cp.public)
	if err != nil {
		return errlib.Wrap("FOA-9913", err)
	}
	defer fd.Close()

	certbuf, err := io.ReadAll(fd)
	if err != nil {
		return errlib.Wrap("FOA-9911", err)
	}
	h := sha256.Sum256(certbuf)

	if h == cp.hash {
		log.Debug("no change to certificate")
		return nil
	}

	sb, err := fd.Stat()
	if err != nil {
		return errlib.Wrap("FOA-9912", err)
	}

	// read the corresponding private key
	keybuf, err := os.ReadFile(cp.private)
	if err != nil {
		return errlib.Wrap("FOA-9914", err)
	}

	cert, err := tls.X509KeyPair(certbuf, keybuf)
	if err != nil {
		return errlib.Wrap("FOA-9915", err)
	}

	pcert, err := x509.ParseCertificate(cert.Certificate[0])
	if err != nil {
		return err
	}

	log.Info("obtained certificate data",
		slog.String("msgid", "FOA-9902"),
		slog.String("mtime", sb.ModTime().Format(time.RFC3339Nano)),
		slog.String("hash", hex.EncodeToString(h[:])),
		slog.String("subject", pcert.Subject.String()),
	)

	cp.lock.Lock()
	defer cp.lock.Unlock()

	cp.hash = h
	cp.last = sb.ModTime()
	cp.cert = &cert

	return nil
}

/*
   don't you hate it when you write a function and then discover there's
   something that already does it, and better :(

   --> replaced by use of tls.X509KeyPair() <--

func parseCertificates(data []byte) (lst []*x509.Certificate, _ error) {
	for {
		block, rest := pem.Decode(data)
		if block == nil {
			return
		}
		switch block.Type {
		case "CERTIFICATE":
			cert, err := x509.ParseCertificate(block.Bytes)
			if err != nil {
				return nil, err
			}
			lst = append(lst, cert)

		default:
			return nil, fmt.Errorf("expected only CERTIFICATE PEM data, but found %q", block.Type)
		}
		data = rest
	}

}
*/

// ClientHandshake implements grpc.TransportCredentials
func (cp *CertProvider) ClientHandshake(context.Context, string, net.Conn) (net.Conn, credentials.AuthInfo, error) {
	panic("TODO")
}

// ServerHandshake implements grpc.TransportCredentials
func (cp *CertProvider) ServerHandshake(conn net.Conn) (net.Conn, credentials.AuthInfo, error) {
	server := tls.Server(conn, &tls.Config{
		GetCertificate: cp.getCertificate,
	})
	err := server.Handshake()
	if err != nil {
		log.Error("handshake failed",
			slog.String("msgid", "FOA-9710"),
			slog.String("error", err.Error()))
		return conn, nil, err
	}
	ai := cpAuthInfo{
		CommonAuthInfo: credentials.CommonAuthInfo{
			SecurityLevel: credentials.PrivacyAndIntegrity,
		},
	}

	return server, ai, nil
}

type cpAuthInfo struct {
	credentials.CommonAuthInfo
}

func (cp cpAuthInfo) AuthType() string {
	return "tls"
}

// Info implements grpc.TransportCredentials
func (cp *CertProvider) Info() credentials.ProtocolInfo {
	panic("TODO")
}

// Clone implements grpc.TransportCredentials
func (cp *CertProvider) Clone() credentials.TransportCredentials {
	return cp
}

// OverrideServerName implements grpc.TransportCredentials
func (cp *CertProvider) OverrideServerName(name string) error {
	// deprecated
	panic("why are we here?")
}

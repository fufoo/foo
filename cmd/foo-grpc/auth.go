package main

import (
	"bufio"
	"context"
	"crypto/rand"
	"crypto/x509"
	"encoding/base64"
	"fmt"
	"io"
	"log/slog"
	"math/big"
	"os"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v4"

	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/cmd/internal/mint"
)

type authClaims struct {
	jwt.RegisteredClaims
	Scopes  []string `json:"scopes"`
	Domains []uint64 `json:"domains"`
	Org     string   `json:"org,omitempty"`
}

func (a *authClaims) HasScope(scope string) bool {
	for _, s := range a.Scopes {
		if s == scope {
			return true
		}
	}
	return false
}

func (a *authClaims) AllowDomain(id uint64) bool {
	for _, allow := range a.Domains {
		if allow == id {
			return true
		}
	}
	return false
}

func (a *authClaims) Expires() *time.Time {
	if a.ExpiresAt == nil {
		return nil
	}
	return &a.ExpiresAt.Time
}

func issuer(claims jwt.Claims) string {
	if ac, ok := claims.(*authClaims); ok {
		return ac.Issuer
	} else {
		panic("not what we expected")
	}
}

func uid() string {
	var nonce [14]byte
	n, err := io.ReadFull(rand.Reader, nonce[:])
	if err != nil {
		return ""
	}
	if n != len(nonce) {
		return ""
	}
	var z big.Int
	z.SetBytes(nonce[:])
	return z.Text(62)
}

func (s *Server) setupAuth(ctx context.Context) error {
	iss, err := mint.NewIssuer(ctx)
	if err != nil {
		return err
	}
	s.issuer = iss

	// write the new one
	fd, err := os.OpenFile("/tmp/public.dat", os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(fd, "%s,%s\n", iss.ID(), iss.PublicKey())
	fd.Close()

	// read the entire list

	fd, err = os.Open("/tmp/public.dat")
	if err != nil {
		panic(err)
	}
	buf := bufio.NewReader(fd)
	for {
		lineb, err := buf.ReadBytes('\n')
		if err != nil {
			break
		}
		line := strings.TrimSpace(string(lineb))
		k := strings.IndexByte(line, ',')
		id := line[:k]
		pub, err := base64.StdEncoding.DecodeString(line[k+1:])
		if err != nil {
			panic(err)
		}

		pubk, err := x509.ParsePKIXPublicKey(pub)
		if err != nil {
			panic(err)
		}
		s.ipub[id] = pubk
	}

	s.ipub[iss.ID()] = iss.Public()

	s.issueAdHocTokens()
	return nil
}

func (s *Server) VerifyToken(ctx context.Context, token string) *authClaims {
	var jwtparser = jwt.NewParser(jwt.WithValidMethods([]string{"ES384"}))

	var claims authClaims

	fn := func(t *jwt.Token) (any, error) {
		iss := issuer(t.Claims)
		if pub, ok := s.ipub[iss]; ok {
			return pub, nil
		} else {
			return nil, errlib.Newf("FOA-9074 no pub key for issuer %q", iss)
		}
	}

	_, err := jwtparser.ParseWithClaims(token, &claims, fn)
	if err != nil {
		// for now
		log.Error("failed to parse claims",
			slog.String("error", err.Error()))
		return nil
	}
	log.Info("parsed claims",
		slog.String("subject", claims.Subject))
	return &claims
}

func (s *Server) issueAdHocTokens() {
	fmt.Printf(">>>>>>>>>>>>>\n\n")
	s.issueAdHocToken("READ access", []string{"cam:read", "state:read", "state:watch"},
		[]uint64{1001, 1017})
	s.issueAdHocToken(
		"WRITE access",
		[]string{
			"cam:read",
			"state:read",
			"cam:write",
			"state:write",
		},
		[]uint64{1001})
	fmt.Printf("\n<<<<<<<<<<<<<<<<<\n")
}

func (s *Server) issueAdHocToken(label string, scopes []string, domains []uint64) {

	now := time.Now()

	you := ptr.HashAddress([]byte("donovan@rscheme.org"))
	org := ptr.String("rscheme.org")

	forever := false

	claims := &authClaims{
		RegisteredClaims: jwt.RegisteredClaims{
			Issuer:  s.issuer.ID(),
			Subject: you.String(),
			Audience: []string{
				"foo-dev.fufoo.net",
			},
			IssuedAt: &jwt.NumericDate{
				Time: now,
			},
			NotBefore: &jwt.NumericDate{
				Time: now,
			},
			ID: uid(),
		},
		Scopes:  scopes,
		Domains: domains,
		Org:     org.String(),
	}

	if !forever {
		claims.RegisteredClaims.ExpiresAt = &jwt.NumericDate{
			Time: now.Add(7 * 24 * time.Hour),
		}
	}

	str, exp, err := s.issuer.Mint(claims)
	if err != nil {
		panic(err)
	}

	fmt.Printf("# %s:\n", label)
	fmt.Printf("JWT_TOKEN=%s\n", str)
	fmt.Printf("\n")

	if exp == nil {
		log.Info("minted",
			slog.String("jwt", str),
		)
	} else {
		log.Info("minted",
			slog.String("jwt", str),
			slog.String("expires", exp.Format(time.RFC3339Nano)),
		)
	}
}

package main

import (
	"bufio"
	"context"
	"encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/lib/minica"
)

var log = logging.New("runcluster")

var verbose *bool

func main() {
	verbose = flag.Bool("verbose", false, "show commands being run")
	baseport := flag.Int("port", 2200, "base port number")
	numcam := flag.Int("numcam", 5, "number of CAM nodes")
	numstate := flag.Int("numstate", 3, "number of State nodes")
	datadir := flag.String("dir", "/tmp/foo-cluster.d", "base directory for storage")
	secret := flag.String("secret", "PLUGH", "secret for cluster level config")
	clusname := flag.String("clustername", "", "name of cluster (required if initializing CA)")
	altname := flag.String("san", "", "subject alternate name (SAN) of cluster (required if initializing CA)")
	hostname := flag.String("hostname", "localhost", "our own host name")
	rootclient := flag.String("rootclient", "", "root client key for bootstrapping")

	flag.Parse()

	logging.Init(logging.LowestLevel("trace"))

	ctx := context.Background()
	r, err := minica.New(ctx, filepath.Join(*datadir, "ca"), *clusname, *altname)
	if err != nil {
		panic(err)
	}

	localCert, localKey, err := r.GetCertAndKeyForHost(ctx, *hostname)
	if err != nil {
		panic(err)
	}

	cc := &ClusterConfig{
		Secret:          *secret,
		DataDir:         *datadir,
		NumCAMServers:   *numcam,
		NumStateServers: *numstate,
		BasePort:        *baseport,
	}
	clus := &Cluster{
		Config:        cc,
		caFile:        r.CAFile(),
		serverCert:    localCert,
		serverKey:     localKey,
		hostname:      *hostname, // has to match the cert
		rootClientKey: *rootclient,
	}

	if *rootclient != "" {
		auth, err := foo.ReadClientAuth(*rootclient)
		if err != nil {
			panic(err)
		}
		log.Debugf(ctx, "enrollment ticket: %s", auth.Enrollment())
		clus.bootRootClient = auth.Enrollment()
		_, _, err = foo.ParseEnrollment(clus.bootRootClient)
		if err != nil {
			panic(err)
		}

	}

	clus.Start(ctx)

	for _, n := range clus.running {
		if n.cmd.Process == nil {
			fmt.Printf("  TBD  : %s (port %d)\n", n.title, n.port)
		} else {
			fmt.Printf("%-6d : %s (port %d)\n", n.cmd.Process.Pid, n.title, n.port)
		}
	}

	clus.Wait()
}

func findExecutable(name string) string {
	path, err := exec.LookPath(name)
	if err == nil {
		return path
	}
	path = filepath.Join("/tmp", name)
	_, err = os.Stat(path)
	if err == nil {
		return path
	}
	log.Errorf(context.TODO(), "could not find %q executable", name)
	os.Exit(1)
	return ""
}

type ClusterConfig struct {
	Secret          string
	DataDir         string
	NumCAMServers   int
	NumStateServers int
	BasePort        int
}

type Cluster struct {
	Config         *ClusterConfig
	caFile         string
	serverCert     string // since we are running everything on one machine,
	serverKey      string // there is only one cert
	hostname       string // the hostname we are on (matches the cert)
	bootRootClient string // enrollment ticket for root client
	running        []*Node
	rootClientKey  string // file for root client key
}

func (clus *Cluster) Start(ctx context.Context) {
	clus.StartZero(ctx)
	for i := 0; i < clus.Config.NumCAMServers; i++ {
		clus.StartCAM(ctx, i)
	}
	for i := 0; i < clus.Config.NumStateServers; i++ {
		clus.StartState(ctx, i)
	}
	// give a moment for those to start up
	time.Sleep(time.Second)

	clus.StartAuth(ctx)
	clus.StartGateway(ctx, clus.WriteConfig(ctx))
	clus.StartAdmin(ctx)
}

type FooConfig struct {
	Server      string `json:"server"`
	CATrustFile string `json:"caTrustFile"`
	Auth        struct {
		Mechanism string `json:"mechanism"`
		File      string `json:"file"`
	} `json:"auth"`
}

func (clus *Cluster) WriteConfig(ctx context.Context) string {
	p := filepath.Join(clus.Config.DataDir, "foo.config.json")

	fooconf := &FooConfig{
		Server:      fmt.Sprintf("%s:%d", clus.hostname, clus.Config.BasePort+30),
		CATrustFile: clus.caFile,
	}
	fooconf.Auth.Mechanism = "client-key"
	fooconf.Auth.File = clus.rootClientKey
	buf, _ := json.MarshalIndent(fooconf, "", "  ")
	ioutil.WriteFile(p, buf, 0600)

	return p
}

func (clus *Cluster) StartGateway(ctx context.Context, fooConfigFile string) {
	cmd := exec.Command(findExecutable("foo-gate"))
	port := clus.Config.BasePort + 40

	cmd.Env = []string{
		fmt.Sprintf("PORT=%d", port),
		fmt.Sprintf("FOO_CONFIG=%s", fooConfigFile),
	}

	time.Sleep(time.Second) // what a hack!
	clus.Add(
		ctx,
		&Node{
			title: "gateway",
			port:  port,
			cmd:   cmd,
		},
		5*time.Second)
}

func (clus *Cluster) StartAuth(ctx context.Context) {
	cmd := exec.Command(findExecutable("foo-authd"))
	port := clus.Config.BasePort + 30

	cmd.Env = []string{
		fmt.Sprintf("FOO_XAUTH_PORT=%d", port),
		fmt.Sprintf("FOO_NODE_ADVERTISE=%s:%d", clus.hostname, port),
		fmt.Sprintf("FOO_NODE_NAME=auth"),
		fmt.Sprintf("FOO_NODE_ID=auth00"),
		fmt.Sprintf("FOO_SERVER=%s:%d", clus.hostname, clus.Config.BasePort), //point to zero
	}

	if clus.bootRootClient != "" {
		bootenv := "FOO_XAUTH_BOOTSTRAP=" + clus.bootRootClient
		cmd.Env = append(cmd.Env, bootenv)
	}

	clus.Add(ctx, &Node{
		title: "auth",
		port:  port,
		cmd:   cmd,
	}, 0)

}

func (clus *Cluster) StartAdmin(ctx context.Context) {
	cmd := exec.Command(findExecutable("foo-admind"))
	port := clus.Config.BasePort + 50

	cmd.Env = []string{
		fmt.Sprintf("FOO_ADMIN_PORT=%d", port),
		fmt.Sprintf("FOO_NODE_ADVERTISE=%s:%d", clus.hostname, port),
		fmt.Sprintf("FOO_NODE_NAME=admin"),
		fmt.Sprintf("FOO_NODE_ID=admin00"),
		fmt.Sprintf("FOO_SERVER=%s:%d", clus.hostname, clus.Config.BasePort), //point to zero
	}

	time.Sleep(time.Second) // what a hack!
	clus.Add(ctx, &Node{
		title: "admin",
		port:  port,
		cmd:   cmd,
	}, time.Second)

}

func (clus *Cluster) StartCAM(ctx context.Context, i int) {
	cmd := exec.Command(findExecutable("foo-camd"))
	port := clus.Config.BasePort + 10 + i

	dir := filepath.Join(clus.Config.DataDir, fmt.Sprintf("cam-%d.d", i))
	os.Mkdir(dir, 0700)

	x := fmt.Sprintf("CAM%d", i)
	cmd.Env = []string{
		fmt.Sprintf("FOO_CAM_PORT=%d", port),
		fmt.Sprintf("FOO_CAM_DATAFILE=%s", dir),
		fmt.Sprintf("FOO_NODE_ADVERTISE=%s:%d", clus.hostname, port),
		fmt.Sprintf("FOO_NODE_NAME=cam-%d", i),
		fmt.Sprintf("FOO_SERVER=%s:%d", clus.hostname, clus.Config.BasePort), //point to zero
		fmt.Sprintf("FOO_NODE_ID=%s", base64.RawURLEncoding.EncodeToString([]byte(x))),
	}

	clus.Add(ctx, &Node{
		title: fmt.Sprintf("cam-%d", i),
		port:  port,
		cmd:   cmd,
	}, 0)
}

func (clus *Cluster) StartState(ctx context.Context, i int) {
	cmd := exec.Command(findExecutable("foo-stated"))
	port := clus.Config.BasePort + 20 + i

	dir := filepath.Join(clus.Config.DataDir, fmt.Sprintf("state-%d.db", i))

	x := fmt.Sprintf("STATE%d", i)
	cmd.Env = []string{
		fmt.Sprintf("FOO_STATE_PORT=%d", port),
		fmt.Sprintf("FOO_STATE_DATAFILE=%s", dir),
		fmt.Sprintf("FOO_NODE_ADVERTISE=%s:%d", clus.hostname, port),
		fmt.Sprintf("FOO_NODE_NAME=state-%d", i),
		fmt.Sprintf("FOO_SERVER=%s:%d", clus.hostname, clus.Config.BasePort), //point to zero
		fmt.Sprintf("FOO_NODE_ID=%s", base64.RawURLEncoding.EncodeToString([]byte(x))),
	}

	clus.Add(
		ctx,
		&Node{
			title: fmt.Sprintf("state-%d", i),
			port:  port,
			cmd:   cmd,
		},
		time.Second,
	)
}

func (clus *Cluster) StartZero(ctx context.Context) {
	cmd := exec.Command(findExecutable("foo-zero"))

	cmd.Env = []string{
		fmt.Sprintf("FOO_ZERO_PORT=%d", clus.Config.BasePort),
	}

	clus.Add(ctx, &Node{title: "zero", port: clus.Config.BasePort, cmd: cmd}, 0)
}

type Node struct {
	title    string
	logdir   string
	cmd      *exec.Cmd
	port     int
	logs     [8]uint64
	precheck func() bool
}

func (clus *Cluster) Add(ctx context.Context, n *Node, delay time.Duration) {

	if n.logdir == "" {
		n.logdir = filepath.Join(clus.Config.DataDir, "log.d")
		os.Mkdir(n.logdir, 0700)
	}

	r, w, err := os.Pipe()
	if err != nil {
		panic(err)
	}

	n.cmd.Stdout = w
	n.cmd.Stderr = os.Stderr

	go n.pipelogs(r)

	common := os.Environ()
	env := make([]string, 0, len(common)+len(n.cmd.Env)+4)

	env = append(env, common...)
	env = append(env,
		fmt.Sprintf("FOO_CA_TRUST=%s", clus.caFile),
		fmt.Sprintf("FOO_NODE_CERT=%s", clus.serverCert),
		fmt.Sprintf("FOO_NODE_KEY=%s", clus.serverKey),
		fmt.Sprintf("FOO_DEBUG=trace"),
	)
	env = append(env, n.cmd.Env...)

	if true {
		name := fmt.Sprintf("run-%s.sh", n.title)
		runscript, err := os.Create(filepath.Join(n.logdir, name))
		if err != nil {
			panic(err)
		}
		defer runscript.Close()

		fmt.Fprintf(runscript, "#! /bin/sh\n")

		for _, e := range env[len(common):] {
			fmt.Fprintf(runscript, "export %s\n", e)
		}
		fmt.Fprintf(runscript, "exec %s", n.cmd.Path)
		for _, arg := range n.cmd.Args[1:] {
			fmt.Fprintf(runscript, " %s", arg)
		}
		fmt.Fprintf(runscript, "\n")
	}

	n.cmd.Env = env

	if *verbose {
		for _, env := range n.cmd.Env[len(common):] {
			fmt.Printf("%s ", env)
		}
		fmt.Printf("%s", n.cmd.Path)
		for _, arg := range n.cmd.Args {
			fmt.Printf(" %s", arg)
		}
		fmt.Printf("\n")
	}

	if delay != 0 {
		go func() {
			time.Sleep(delay)
			err = n.cmd.Start()
			if err != nil {
				log.Errorf(ctx, "failed to start: %s", err)
			}
		}()
	} else {
		err = n.cmd.Start()
		if err != nil {
			log.Errorf(ctx, "failed to start: %s", err)
		}
	}
	clus.running = append(clus.running, n)
}

func (clus *Cluster) Wait() {
	for _, p := range clus.running {
		p.cmd.Wait()
	}
}

func (n *Node) pipelogs(r io.ReadCloser) {
	defer r.Close()
	ctx := context.Background()

	t0 := time.Now().Format("2006-01-02")

	lf := filepath.Join(n.logdir, fmt.Sprintf("%s.%s.log", n.title, t0))
	ctx = logging.Set(ctx, "logfile", lf)

	out, err := os.OpenFile(
		lf,
		os.O_CREATE|os.O_WRONLY|os.O_APPEND,
		0600)
	if err != nil {
		panic(err)
	}

	//dst := bufio.NewWriter(out)
	src := bufio.NewReader(r)
	for {
		line, err := src.ReadBytes('\n')
		if err != nil {
			return
		}
		_, err = out.Write(line)
		if err != nil {
			log.Errorf(ctx, "failed to write: %s", err)
		}

		var rec Record
		json.Unmarshal(line, &rec)
		if pri, ok := logging.PriorityFromString(rec.Level); ok {
			n.logs[pri]++
			if pri >= logging.PriorityWarning {
				var b []string
				if k := n.logs[logging.PriorityCritical]; k > 0 {
					b = append(b, fmt.Sprintf("%d CRIT", k))
				}
				if k := n.logs[logging.PriorityError]; k > 0 {
					b = append(b, fmt.Sprintf("%d ERR", k))
				}
				if k := n.logs[logging.PriorityWarning]; k > 0 {
					b = append(b, fmt.Sprintf("%d WARN", k))
				}
				if k := n.logs[logging.PriorityInfo]; k > 0 {
					b = append(b, fmt.Sprintf("%d INFO", k))
				}
				if k := n.logs[logging.PriorityAudit]; k > 0 {
					b = append(b, fmt.Sprintf("%d AUDIT", k))
				}
				log.Debugf(ctx, "%s has reported: +1 %s ==> %s",
					n.title,
					rec.Level,
					strings.Join(b, ", "))
			}
		}
	}
}

type Record struct {
	Timestamp time.Time `json:"@timestamp"`
	Level     string    `json:"level"`
	Loc       string    `json:"loc"`
	LocFile   string    `json:"loc_file"`
	Module    string    `json:"module"`
	Message   string    `json:"message"`
}

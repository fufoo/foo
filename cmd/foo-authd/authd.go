package main

import (
	"context"
	"os"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/foo/cmd/internal/authimpl"
	"bitbucket.org/fufoo/foo/internal/services/platform"
	"bitbucket.org/fufoo/foo/internal/tls"
	"bitbucket.org/fufoo/foo/internal/zeroclient"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/qrpc"
	"bitbucket.org/fufoo/qrpc/tracer"
	quic "github.com/quic-go/quic-go"
)

var log = logging.New("authd")

func main() {
	lvl := os.Getenv("FOO_DEBUG")
	if lvl == "" {
		lvl = "trace"
	}
	logging.Init(logging.LowestLevel(lvl))

	ctx := context.Background()
	defer func() {
		log.Warningf(ctx, "xauthd stopping")
	}()

	cfg := ConfigFromEnv(ctx)
	if cfg == nil {
		os.Exit(2)
	}

	//buf, _ := json.MarshalIndent(cfg, "", "  ")
	//log.Infof(ctx, "config--\n%s", buf)

	zc, err := zeroclient.New(ctx, cfg.Zero)
	if err != nil {
		log.Errorf(ctx, "could not connect to zero: %s", err)
		os.Exit(1)
	}

	sys := zeroclient.Bootstrap(ctx, "xauth", zc, cfg.Zero)

	self, err := authimpl.New(ctx, zc, sys)
	if err != nil {
		log.Errorf(ctx, "could not start: %s", err)
		os.Exit(1)
	}

	log.Debugf(ctx, "waiting for initial sync before starting our server")
	err = self.WaitForReady()
	if err != nil {
		log.Errorf(ctx, "initial sync failed: %s", err)
		time.Sleep(time.Second)
		os.Exit(1)
	}

	log.Debugf(ctx, "ready to roll")

	server := qrpc.NewServer()
	platform.RegisterAuthQServer(server, self)

	t, err := tls.PlatformServer(cfg.Zero.CertFile, cfg.Zero.KeyFile, tls.Trust(cfg.Zero.TrustFile))
	if err != nil {
		panic(err)
	}

	qc := &quic.Config{
		Versions: []quic.VersionNumber{1},
		Tracer:   tracer.New(1),
	}

	listener, err := quic.ListenAddr(":"+cfg.Listen, t, qc)
	if err != nil {
		panic(err)
	}

	log.Infof(ctx, "xauthd started on port %s", cfg.Listen)

	if boot := os.Getenv("FOO_XAUTH_BOOTSTRAP"); boot != "" {
		go bootstrap(ctx, self, boot)
	}

	server.Serve(ctx, listener)
}

type Config struct {
	Zero   *zeroclient.Config
	Listen string
}

func ConfigFromEnv(ctx context.Context) *Config {
	listen := os.Getenv("FOO_XAUTH_PORT")
	if listen == "" {
		listen = "2200"
	}

	z := zeroclient.ConfigFromEnv(ctx)
	if z == nil {
		return nil
	}

	return &Config{
		Zero:   z,
		Listen: listen,
	}
}

func bootstrap(ctx context.Context, in *authimpl.Server, using string) {
	id, pub, err := foo.ParseEnrollment(using)
	if err != nil {
		log.Errorf(ctx, "invalid bootstrap enrollment: %s", err)
		return
	}
	// how can turn "WaitForReady" into "WaitForStable"... do we
	// even have a way of knowing if the cluster has stabilized??
	time.Sleep(2 * time.Second)
	log.Infof(ctx, "bootstrapping enrollment for {%s}", id)
	for {
		if in.EnrollBootstrapClient(ctx, id, pub) {
			break
		}
		time.Sleep(10 * time.Second)
	}
}

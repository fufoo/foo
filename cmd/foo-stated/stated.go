package main

import (
	"context"
	"encoding/json"
	"os"

	"bitbucket.org/dkolbly/logging"
	//"bitbucket.org/fufoo/foo/cmd/internal/micro"
	"bitbucket.org/fufoo/foo/internal/tls"
	"bitbucket.org/fufoo/foo/internal/zeroclient"
	"bitbucket.org/fufoo/foo/lib/state/badgerstate"
	"bitbucket.org/fufoo/foo/services/state"
	"bitbucket.org/fufoo/qrpc"
	quic "github.com/quic-go/quic-go"
)

var log = logging.New("stated")

func main() {
	lvl := os.Getenv("FOO_DEBUG")
	if lvl == "" {
		lvl = "trace"
	}
	logging.Init(logging.LowestLevel(lvl))
	qrpc.SetTrace(false)

	ctx := context.Background()

	cfg := ConfigFromEnv(ctx)
	if cfg == nil {
		os.Exit(2)
	}

	if false {
		buf, _ := json.MarshalIndent(cfg, "", "  ")
		log.Infof(ctx, "config--\n%s", buf)
	}

	zc, err := zeroclient.New(ctx, cfg.Zero)
	if err != nil {
		log.Errorf(ctx, "could not connect to zero: %s", err)
		os.Exit(1)
	}

	mic, err := badgerstate.Open(cfg.DataFile)
	if err != nil {
		log.Errorf(ctx, "FCL-1801 could not open: %v", err)
		os.Exit(1)
	}

	// micro itself implements the state server api
	server := qrpc.NewServer( /*qrpc.SessionMiddleware(loggingMiddleware)*/ )

	state.RegisterStateQServer(server, mic)

	sys := zeroclient.Bootstrap(ctx, "state", zc, cfg.Zero)

	go func() {
		var state *zeroclient.State
		conn := false
		for {
			state = sys.Next(state)
			if !state.Connected() {
				log.Debugf(ctx, "zero not connected: %s", state.Error())
				conn = false
			} else {
				if !conn {
					log.Debugf(ctx, "zero (re-)connected")
					conn = true
				}
			}
		}
	}()

	t, err := tls.PlatformServer(cfg.Zero.CertFile, cfg.Zero.KeyFile, tls.Trust(cfg.Zero.TrustFile))
	if err != nil {
		panic(err)
	}

	listener, err := quic.ListenAddr(":"+cfg.Listen, t, nil)
	if err != nil {
		panic(err)
	}

	server.Serve(ctx, listener)

	log.Infof(ctx, "running")
}

type Config struct {
	Zero     *zeroclient.Config
	Listen   string
	DataFile string
}

func ConfigFromEnv(ctx context.Context) *Config {
	listen := os.Getenv("FOO_STATE_PORT")
	if listen == "" {
		listen = "2200"
	}

	ok := true

	dataFile := os.Getenv("FOO_STATE_DATAFILE")
	if dataFile == "" {
		log.Errorf(ctx, "No data file specified (FOO_STATE_DATAFILE)")
		ok = false
	}

	z := zeroclient.ConfigFromEnv(ctx)
	if !ok || z == nil {
		return nil
	}

	return &Config{
		Zero:     z,
		Listen:   listen,
		DataFile: dataFile,
	}
}

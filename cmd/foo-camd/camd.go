package main

import (
	"context"
	"encoding/json"
	"os"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/foo/cmd/internal/camimpl"
	"bitbucket.org/fufoo/foo/cmd/internal/persist"
	"bitbucket.org/fufoo/foo/internal/tls"
	"bitbucket.org/fufoo/foo/internal/zeroclient"
	"bitbucket.org/fufoo/foo/services/cam"
	"bitbucket.org/fufoo/qrpc"
	quic "github.com/quic-go/quic-go"
)

const NumBuckets = 256 // TODO load this from deployment config

var log = logging.New("camd")

func main() {
	lvl := os.Getenv("FOO_DEBUG")
	if lvl == "" {
		lvl = "trace"
	}
	logging.Init(logging.LowestLevel(lvl))

	ctx := context.Background()

	cfg := ConfigFromEnv(ctx)
	if cfg == nil {
		os.Exit(2)
	}

	buf, _ := json.MarshalIndent(cfg, "", "  ")
	log.Infof(ctx, "config--\n%s", buf)

	zc, err := zeroclient.New(ctx, cfg.Zero)
	if err != nil {
		log.Errorf(ctx, "could not connect to zero: %s", err)
		os.Exit(1)
	}

	sys := zeroclient.Bootstrap(ctx, "cam", zc, cfg.Zero)

	go func() {
		var state *zeroclient.State
		conn := false
		for {
			state = sys.Next(state)
			if !state.Connected() {
				log.Debugf(ctx, "zero not connected: %s", state.Error())
				conn = false
			} else {
				if !conn {
					log.Debugf(ctx, "zero (re-)connected")
					conn = true
				}
			}
		}
	}()

	sto, err := persist.Open(ctx, cfg.DataFile, NumBuckets)
	if err != nil {
		panic(err)
	}

	server := qrpc.NewServer(qrpc.SessionMiddleware(loggingMiddleware))

	cam.RegisterStorageQServer(server, camimpl.New(sto))

	port := os.Getenv("FOO_CAM_PORT")
	if port == "" {
		port = "44001"
	}

	listener, err := quic.ListenAddr(":"+port, tls.NewTLSConfig(true), nil)
	if err != nil {
		panic(err)
	}

	server.Serve(ctx, listener)

	log.Infof(ctx, "running")
}

func loggingMiddleware(ctx context.Context, sess qrpc.Session, next qrpc.SessionHandler) {
	t0 := time.Now()
	ctx = logging.Set(ctx, "clientAddr", sess.RemoteAddr().String())
	next(ctx, sess)
	dt := time.Since(t0)
	log.Debugf(ctx, "connection lasted %s", dt)
}

type Config struct {
	Zero     *zeroclient.Config
	Listen   string
	DataFile string
}

func ConfigFromEnv(ctx context.Context) *Config {
	listen := os.Getenv("FOO_CAM_PORT")
	if listen == "" {
		listen = "2200"
	}

	ok := true

	dataFile := os.Getenv("FOO_CAM_DATAFILE")
	if dataFile == "" {
		log.Errorf(ctx, "No data file specified (FOO_STATE_DATAFILE)")
		ok = false
	}

	z := zeroclient.ConfigFromEnv(ctx)
	if !ok || z == nil {
		return nil
	}

	return &Config{
		Zero:     z,
		Listen:   listen,
		DataFile: dataFile,
	}
}

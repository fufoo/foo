// this micro implementation is an initial version for the state server;
// it needs to be vastly improved, because it is based on the micro
// service which is not recommended for production use :badpokerface:

package micro

import (
	"bytes"
	"context"
	"encoding/binary"
	"fmt"
	"io"
	"regexp"
	"sync"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
	//"bitbucket.org/fufoo/core/version"
	"bitbucket.org/fufoo/foo/lib/followfilter"
	"bitbucket.org/fufoo/foo/lib/special"
	"bitbucket.org/fufoo/foo/services/core"
	"bitbucket.org/fufoo/foo/services/state"
	bolt "go.etcd.io/bbolt"
	"google.golang.org/protobuf/encoding/protowire"
	proto "google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var log = logging.New("imicro")

const markIncrement = 1

type dname struct {
	domain uint64
	name   string
}

// TODO: FOO-2 the key notation we use here is not very suitable for
// prefix scans should use an 8-byte fixed prefix in big-endian then
// the string itself, then Scan() can seek right to where it needs to
// go if a domain or even a prefix is specified i.e., scan domain 258
// foo/* can seek to
//
//      [00 00 00 00 00 00 01 02 f o o /]

func unkey(k []byte) dname {

	d, l := protowire.ConsumeVarint(k)
	if l <= 0 {
		panic("invalid key encoding (domain)")
	}
	k = k[l:]

	n, l := protowire.ConsumeString(k)
	if l <= 0 {
		panic("invalid key encoding (name)")
	}
	k = k[l:]
	if len(k) > 0 {
		panic("invalid key encoding (leftover)")
	}
	return dname{
		domain: d,
		name:   n,
	}
}

func (d dname) key() []byte {
	var tmp [128]byte
	buf := tmp[:0]

	buf = protowire.AppendVarint(buf, d.domain)
	buf = protowire.AppendString(buf, d.name)
	return buf
}

type Foo struct {
	cache         map[dname][]byte
	db            *bolt.DB
	logseq        uint64
	lock          sync.Mutex
	followNotify  chan<- uint64
	followStalls  chan<- *follower
	followCancels chan<- *follower
}

// this is an all-in-one service, providing both the State API as well
// as the CAM API.  It is designed to operate within a single process;
// although it persists data to disk (in the named bolt file), it is
// not designed to interoperate with other processes sharing a disk
// file.
func Open(store string) (*Foo, error) {
	opts := bolt.Options{
		Timeout:      time.Second,
		FreelistType: bolt.FreelistMapType,
	}
	db, err := bolt.Open(store, 0600, &opts)
	if err != nil {
		return nil, err
	}
	f := &Foo{
		db:    db,
		cache: make(map[dname][]byte),
	}
	//f.cond = sync.NewCond(&f.lock)

	db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("obs"))
		if err != nil {
			return err
		}

		_, err = tx.CreateBucketIfNotExists([]byte("cam"))
		if err != nil {
			return err
		}

		b, err := tx.CreateBucketIfNotExists([]byte("log"))

		key, _ := b.Cursor().Last()
		if key != nil {
			f.logseq = binary.BigEndian.Uint64(key)
		}
		return err
	})
	log.Tracef(context.Background(), "%s : log seq is %d", store, f.logseq)

	stalls := make(chan *follower)
	cancels := make(chan *follower)
	notifies := make(chan uint64)
	go f.followStaller(stalls, cancels, notifies)

	f.followNotify = notifies
	f.followStalls = stalls
	f.followCancels = cancels

	return f, nil
}

func (f *Foo) getobs(b *bolt.Bucket, dn dname) *state.Obs {

	var data []byte

	if obs, ok := f.cache[dn]; ok {
		data = obs
	} else {
		data = b.Get(dn.key())
	}
	o := new(state.Obs)
	if data == nil {
		o.Domain = dn.domain
		o.Name = dn.name
	} else {
		proto.Unmarshal(data, o)
		if o.Domain != dn.domain {
			panic("corrupt, domain mismatch")
		}
		if o.Name != dn.name {
			panic("corrupt, name mismatch")
		}
	}
	return o
}

func (f *Foo) Get(ctx context.Context, req *state.GetRequest) (*state.GetResponse, error) {
	f.lock.Lock()
	defer f.lock.Unlock()

	ret := new(state.GetResponse)

	// we want to return the mark that is *after* the most recent
	// log message
	ret.Mark = f.logseq + markIncrement

	err := f.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("obs"))

		for _, s := range req.Subjects {
			dn := dname{
				domain: req.Domain,
				name:   s,
			}
			ret.States = append(ret.States, f.getobs(b, dn))
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return ret, nil
}

func equal(current *state.Obs, precond *core.Ptr) bool {
	if current.Value == nil {
		return precond == nil
	}
	if precond == nil {
		return false
	}
	// no need to convert to a ptr.Ptr; just compare the bytes
	return bytes.Equal(current.Value.Bits, precond.Bits)
}

func (f *Foo) Apply(ctx context.Context, req *state.TxnRequest) (*state.TxnRecord, error) {
	log.Debugf(ctx, "FOA-7519 apply txn #%d", req.Audit)

	if req.Domain < 1 {
		log.Warningf(ctx, "FOA-7516 invalid zero domain in Apply")
		return nil, errlib.Newf("FOA-7517 invalid zero domain")
	}

	// compile the specials

	var specials []special.Handler

	for _, s := range req.Specials {
		h, err := special.Compile(ctx, s, req)
		if err != nil {
			// could not compile a special
			return nil, errlib.Wrap("FOA-7503", err)
		}
		specials = append(specials, h)
	}
	f.lock.Lock()
	defer f.lock.Unlock()

	result := &state.TxnRecord{
		Request: req,
	}
	updates := make(map[dname][]byte)

	err := f.db.Update(func(t *bolt.Tx) error {
		obs := t.Bucket([]byte("obs"))
		logs := t.Bucket([]byte("log"))

		// apply all the changes at the DB level (we could
		// rely on bolt's ACID here :noice:)

		var states []*state.Obs
		for _, s := range req.Subjects {
			dn := dname{
				domain: req.Domain,
				name:   s,
			}
			states = append(states, f.getobs(obs, dn))
		}
		result.States = states
		result.Disposition = make([]state.Disposition, len(req.Subjects))

		// check preconds
		success := true
		for _, pre := range req.Preconds {
			if !equal(states[pre.Slot], pre.Value) {
				log.Debugf(ctx, "precondition slot[%d] %q...", pre.Slot, req.Subjects[pre.Slot])
				if states[pre.Slot] == nil {
					log.Debugf(ctx, "no state")
				} else {
					log.Debugf(ctx, "state = %s", states[pre.Slot].Short())
				}
				if pre.Value == nil {
					log.Debugf(ctx, "no value")
				} else {
					log.Debugf(ctx, "value = {%s}", pre.Value.Must())
				}
				result.Disposition[pre.Slot] = state.Disposition_CONFLICT
				success = false
			} else {
				// being read... if this subject appears in the Effects, will
				// be updated to either CHANGED or TOUCHED
				result.Disposition[pre.Slot] = state.Disposition_SILENT
			}
		}
		if !success {
			result.Conflict = true
			return nil
		}

		// preconditions passed, we are going to try to commit
		result.Time = timestamppb.Now()

		edits := false
		change := make(map[dname]*state.Obs)

		postchange := func(s *state.Obs) {
			dn := dname{
				domain: s.Domain,
				name:   s.Name,
			}

			if _, ok := change[dn]; ok {
				// already in the change set
			}

			s.Mtime = result.Time
			if s.Seq == 0 {
				s.Seq = 100
			} else {
				s.Seq = s.Seq + 1
			}
			s.Audit = req.Audit
			change[dn] = s
		}

		for _, eff := range req.Effects {
			//log.Tracef(ctx, "applying effect %s", eff)
			differ := !equal(states[eff.Slot], eff.Value)
			if eff.Touch || differ {
				s := states[eff.Slot]

				if s.Value == nil && eff.Value != nil {
					log.Tracef(ctx, "setting new value for %s", s.Name)
				} else if s.Value != nil && eff.Value == nil {
					log.Tracef(ctx, "deleting value for %s", s.Name)
				}

				s.Value = eff.Value
				if differ {
					result.Disposition[eff.Slot] = state.Disposition_CHANGED
				} else {
					result.Disposition[eff.Slot] = state.Disposition_TOUCHED
				}
				postchange(s)
			}
		}

		// apply the specials
		for _, spec := range specials {
			changed, err := spec.Apply(states)
			if err != nil {
				// rather than reporting this as a
				// conflict, we report an error.
				// Otherwise, we'd want to roll back
				// the states so the client can see
				// the state of things before the txn
				// started to apply.  If we find that
				// it's important to give the client
				// information, other than the error
				// code, about why the special failed
				// (i.e., the state of the database)
				// then we might change this protocol
				// later.
				return err
			}
			if len(changed) > 0 {
				log.Tracef(ctx, "applied special effect to: %v", changed)
				for _, k := range changed {
					postchange(states[k])
				}
				edits = true
			} else {
				log.Tracef(ctx, "special effect did nothing")
			}
		}

		for dn, s := range change {
			obuf, err := proto.Marshal(s)
			if err != nil {
				panic(err)
			}
			updates[dn] = obuf
			err = obs.Put(dn.key(), obuf)
			if err != nil {
				return err
			}
			edits = true
		}

		if !edits {
			log.Tracef(ctx, "there were no edits, actually")
			return nil
		} else {
			// all done; encode the txnrecord and store it
			// in the log
			result.Id = f.logseq + markIncrement
			buf, err := proto.Marshal(result)
			if err != nil {
				return err
			}
			var tmp [8]byte
			binary.BigEndian.PutUint64(tmp[:], result.Id)
			return logs.Put(tmp[:], buf)
		}
	})
	if err != nil {
		return nil, err
	}

	if result.Conflict {
		// there was a conflict... don't update our cache or
		// bump the log seq
		log.Tracef(ctx, "FOA-7518 conflict")
		return result, nil
	}

	// update the cache
	for k, v := range updates {
		f.cache[k] = v
	}

	pbuf, _ := proto.Marshal(result)
	log.Tracef(ctx, "[%d] TXN record is %d bytes", f.logseq, len(pbuf))

	f.logseq++
	f.followNotify <- f.logseq

	return result, nil
}

func (f *Foo) Follow(ctx context.Context, req *state.FollowRequest) (state.State_FollowClient, error) {
	log.Debugf(ctx, "Follow {%s}", req)

	sub, err := followfilter.New(ctx, req.Subjects, req.AllowGlob)
	if err != nil {
		return nil, err
	}

	follow := &follower{
		owner:  f,
		index:  req.AfterMark,
		domain: req.Domain,
		sub:    sub,
	}
	follow.ctx = logging.Set(ctx, "follower", fmt.Sprintf("%p", follow))

	if req.AtEnd {
		if req.AfterMark != 0 {
			return nil, errlib.Newf("FOA-7514 cannot specify after_mark if at_end is set")
		}
		log.Tracef(ctx, "request at end = %d", f.logseq)
		follow.index = f.logseq + 1
	}
	log.Tracef(ctx, "D#%d after mark [%d] subscription is %s",
		follow.domain,
		follow.index,
		sub.String())
	return follow, nil
}

type follower struct {
	owner  *Foo
	ctx    context.Context
	index  uint64
	domain uint64
	sub    followfilter.Subscription
	cb     chan<- uint64
}

func (f *follower) match(ctx context.Context, tr *state.TxnRecord) bool {
	pat := followfilter.Compile(ctx, tr.Request.Subjects)
	return pat.Match(ctx, &f.sub)
}

func (f *follower) trynext() (uint64, *state.TxnRecord) {
	ctx := f.ctx
	db := f.owner.db

	item := new(state.TxnRecord)
	var posn uint64

	err := db.View(func(t *bolt.Tx) error {
		b := t.Bucket([]byte("log"))
		cur := b.Cursor()

		log.Tracef(ctx, "seeking to log[%d]", f.index)

		var tmp [8]byte
		binary.BigEndian.PutUint64(tmp[:], f.index)

		key, data := cur.Seek(tmp[:])

		for {
			if key == nil {
				log.Tracef(ctx, "EOF")
				return io.EOF
			}
			f.index = binary.BigEndian.Uint64(key)
			log.Tracef(ctx, "actually at log[%d]", f.index)
			posn = f.index
			f.index++

			item.Reset()
			err := proto.Unmarshal(data, item)
			if err != nil {
				log.Warningf(ctx, "FOA-7518 corrupt data at log[%d]", posn)
				return err
			}

			// see if there's any overlap with what we care about
			if (f.domain == 0 || item.Request.Domain == f.domain) && f.match(ctx, item) {
				return nil
			}
			log.Tracef(ctx, "no match, trying next one")
			key, data = cur.Next()
		}
	})
	if err != nil {
		log.Tracef(ctx, "view returned: %s", err)
		return 0, nil
	}
	return posn, item
}

func (f *Foo) followStaller(stall, cancel <-chan *follower, seq <-chan uint64) {
	stalled := make(map[*follower]struct{})

	ctx := context.Background()
	for {
		select {
		case f := <-stall:
			log.Tracef(ctx, "%p stalling D#%d at index %d",
				f, f.domain, f.index)
			stalled[f] = struct{}{}

		case c := <-cancel:
			log.Tracef(ctx, "%p canceled", c)
			delete(stalled, c)

		case n := <-seq:
			log.Tracef(ctx, "new logseq %d", n)

			var pop []*follower
			for k := range stalled {
				if n >= k.index {
					log.Tracef(ctx, "pop %p because %d>=%d",
						k,
						n,
						k.index)
					pop = append(pop, k)
				} else {
					log.Tracef(ctx, "%p oh well %d<%d", k, n, k.index)
				}
			}
			for _, k := range pop {
				delete(stalled, k)
				k.cb <- n
			}
		}
	}
}

func (f *follower) stall() bool {
	cb := make(chan uint64, 1)
	f.cb = cb

	f.owner.followStalls <- f
	select {
	case <-cb:
		return true
	case <-f.ctx.Done():
		f.owner.followCancels <- f
		return false
	}
}

func (f *follower) Recv() (*state.TxnRecord, error) {
	for {
		at, ret := f.trynext()
		if ret != nil {
			log.Tracef(f.ctx, "found a match at log[%d]", at)
			// include the mark in the TxnRecord itself
			// (but the mark *after* the record we just read)
			ret.NextMark = at + markIncrement
			return ret, nil
		}
		if !f.stall() {
			return nil, context.Canceled
		}
	}
}

func compile(ctx context.Context, globs []string) (*regexp.Regexp, error) {
	return followfilter.CompileGlobs(ctx, globs)
}

type scanclient struct {
	ch <-chan *state.Obs
}

func (sc *scanclient) Recv() (*state.Obs, error) {
	item, ok := <-sc.ch
	if ok {
		return item, nil
	}
	return nil, io.EOF
}

func (f *Foo) Scan(ctx context.Context, req *state.GetRequest) (state.State_ScanClient, error) {
	var pat *regexp.Regexp
	if len(req.Subjects) > 0 {
		var err error
		pat, err = compile(ctx, req.Subjects)
		if err != nil {
			return nil, err
		}
	}

	ch := make(chan *state.Obs, 10)
	sc := &scanclient{ch}

	go func() {
		defer close(ch)
		n := 0
		f.db.View(func(t *bolt.Tx) error {
			b := t.Bucket([]byte("obs"))
			cur := b.Cursor()

			key, value := cur.Seek([]byte{})

			for key != nil {
				k := unkey(key)
				match := (req.Domain == 0 || k.domain == req.Domain) &&
					(pat == nil || pat.MatchString(k.name))
				log.Tracef(ctx, "scanned %d [%s] match=%t", k.domain, k.name, match)

				if match {
					o := new(state.Obs)
					proto.Unmarshal(value, o)
					ch <- o
				}
				key, value = cur.Next()
				n++
			}
			return nil
		})
		log.Tracef(ctx, "scanned %d", n)
	}()

	return sc, nil
}

package camimpl

import (
	"context"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/foo/cmd/internal/persist"
	"bitbucket.org/fufoo/foo/services/cam"
	"bitbucket.org/fufoo/foo/services/core"
)

var log = logging.New("camimpl")

type Impl struct {
	persistence *persist.Store
}

func New(store *persist.Store) *Impl {
	return &Impl{store}
}

func (srv *Impl) Store(ctx context.Context, req *cam.StoreRequest) (*cam.StoreResponse, error) {
	ret := &cam.StoreResponse{}

	for _, d := range req.Data {
		p, err := srv.persistence.Store(ctx, d)
		if err != nil {
			return nil, err
		}
		log.Debugf(ctx, "stored {%s} %d bytes", p, len(d.Payload))
		ret.Refs = append(ret.Refs, core.From(p))
	}
	return ret, nil
}

func (srv *Impl) Load(ctx context.Context, req *cam.LoadRequest) (*cam.LoadResponse, error) {
	ret := &cam.LoadResponse{}

	for _, r := range req.Refs {
		p := r.Must()
		d, err := srv.persistence.Load(ctx, &p)
		if err != nil {
			return nil, err
		}
		ret.Data = append(ret.Data, d)
	}
	return ret, nil
}

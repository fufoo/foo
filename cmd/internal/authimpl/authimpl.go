package authimpl

import (
	"context"
	"fmt"
	"sync"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
	//"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/cmd/internal/mint"
	"bitbucket.org/fufoo/foo/internal/services/platform"
	"bitbucket.org/fufoo/foo/internal/zeroclient"
	"bitbucket.org/fufoo/foo/lib/foo"
	//"bitbucket.org/fufoo/foo/lib/bighash"
	"bitbucket.org/fufoo/foo/lib/keycreds"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var log = logging.New("authimpl")

type Server struct {
	iss        *mint.Issuer
	lock       sync.Mutex
	etag       string
	dep        *platform.Deployment
	last       *zeroclient.State
	uninit     *sync.Cond
	secure     foo.Credentials
	rootDomain uint64
}

// New builds a new auth server.  It creates a new key pair for
// signing authentication requests (see the Exchange API) and
// registers the public key with the platform via the Zero
// AddExternalAuthKeyRequest API.
func New(ctx context.Context, zc platform.ZeroQClient, sys *zeroclient.SystemModel) (*Server, error) {
	iss, err := mint.NewIssuer(ctx)
	if err != nil {
		return nil, err
	}
	exp := time.Now().Add(time.Hour)

	req := &platform.AddExternalAuthKeyRequest{
		Key: &platform.PublicKey{
			Authority: platform.PublicKey_XAUTH,
			Issuer:    iss.ID(),
			Public:    iss.PublicKey(),
			Expires:   timestamppb.New(exp),
		},
	}
	zc.AddExternalAuthKey(ctx, req)

	s := &Server{
		iss: iss,
		// TODO these should be parameters:
		secure:     keycreds.KeyFromString("00userbase"),
		rootDomain: 1,
	}
	s.uninit = sync.NewCond(&s.lock)
	go s.keepupdated(sys)

	return s, nil
}

func (s *Server) update1(ctx context.Context, state *zeroclient.State) {
	if !state.Connected() {
		log.Debugf(ctx, "zero not connected: %s", state.Error())
	} else {
		log.Debugf(ctx, "zero (re-)connected")
		s.lock.Lock()
		defer s.lock.Unlock()
		s.etag = fmt.Sprintf("t%d", time.Now().UnixNano())
		s.dep = state.Deployment()
		s.last = state
		s.uninit.Broadcast()
	}
}

func (s *Server) keepupdated(sys *zeroclient.SystemModel) {
	var state *zeroclient.State
	for {
		state = sys.Next(state)
		s.update1(context.Background(), state)
	}
}

func (s *Server) WaitForReady() error {
	s.lock.Lock()
	defer s.lock.Unlock()
	for s.last == nil {
		s.uninit.Wait()
	}
	return s.last.Error()
}

func (s *Server) Salt(ctx context.Context, req *platform.SaltRequest) (*platform.SaltResponse, error) {
	panic("TODO")
}

func (s *Server) current() (*platform.Deployment, string) {
	s.lock.Lock()
	defer s.lock.Unlock()

	return s.dep, s.etag
}

func (s *Server) Exchange(ctx context.Context, req *platform.ExchangeRequest) (*platform.ExchangeResponse, error) {
	log.Debugf(ctx, "identifier %q is attempting auth mechanism %s",
		req.Identifier,
		req.Mechanism)
	switch req.Mechanism {
	case "basic":
		return s.exchangeBasic(ctx, req)
	case "client-key":
		return s.exchangeJWT(ctx, req)
	}

	log.Warningf(ctx, "FPL-3491 unsupported auth mechanism %q", req.Mechanism)
	return nil, errlib.Newf("FPL-3401 unsupported auth mechanism")
}

func (s *Server) exchangeBasic(ctx context.Context, req *platform.ExchangeRequest) (*platform.ExchangeResponse, error) {

	dep, tag := s.current()
	if tag == "" {
		return nil, errlib.Newf("FPL-3450 no userbase available")
	}
	/*
		f := foo.DialDeployment(ctx, dep)

		d := f.Access(s.rootDomain, s.secure)
		err := d.View(ctx, func(ctx context.Context, rd foo.ReadTxn) error {
			p, err := rd.Get(ctx, "ix/clients")
			if err != nil {
				return err
			}
			if p.IsNull() {
				// this should not happen; we must have failed
				// in bootstrap or something is misconfigured
				log.Errorf(ctx, "FPL-3458 no client index")
			} else {
				log.Debugf(ctx, "client index {%s}", p)
			}
			return nil
		})
		if err != nil {
			return nil, errlib.Wrap("FPL-3451", err)
		}
	*/

	pw, ok := userbase[req.Identifier]
	if !ok {
		log.Warningf(ctx, "FPL-3492 no user %q", req.Identifier)
		return nil, errlib.Newf("FPL-3402 auth denied")
	}
	if req.Credentials != pw {
		log.Warningf(ctx, "FPL-3493 invalid credentials for user %q", req.Identifier)
		return nil, errlib.Newf("FPL-3403 auth denied")
	}
	token, exp, err := s.iss.Mint(s.iss.InternalClaims(req.Identifier))
	if err != nil {
		return nil, err
	}

	if req.DeploymentEtag == tag {
		// don't return the deployment info (it's relatively
		// large and unchanging) if the client already has
		// that tag
		dep = nil
	}
	resp := platform.ExchangeResponse{
		Token: &platform.IssuedToken{
			Token:   token,
			Expires: timestamppb.New(*exp),
		},
		DeploymentEtag: tag,
		Deployment:     dep,
	}
	return &resp, nil
}

var userbase = map[string]string{
	"alice":   "p0xme",
	"plays2":  "c8eabc62-cdbc-48ac-8f00-386b04cdb0e2",
	"graphex": "6dd18edf-c798-4b1f-accd-28a24dc31ad6",
}

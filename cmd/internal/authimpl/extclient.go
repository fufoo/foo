package authimpl

import (
	"bytes"
	"context"
	"crypto/ed25519"
	"crypto/x509"
	"errors"
	"time"

	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/internal/services/platform"
	"bitbucket.org/fufoo/foo/lib/bighash"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/services/core"
	"bitbucket.org/fufoo/foo/services/core/ptrpb"
	"github.com/golang-jwt/jwt/v4"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (s *Server) EnrollBootstrapClient(ctx context.Context, id *ptr.Ptr, pub any) bool {

	pubbuf, err := x509.MarshalPKIXPublicKey(pub)
	if err != nil {
		log.Errorf(ctx, "FPL-3418 failed to enroll bootstrap: %s", err)
		return false
	}

	xc := &platform.ExternalClient{
		Id:        core.From(*id),
		Created:   timestamppb.Now(),
		PublicKey: pubbuf,
	}

	err = s.enroll(ctx, xc, true)
	if err != nil {
		log.Errorf(ctx, "FPL-3419 failed to enroll bootstrap: %s", err)
		return false
	}
	return true
}

func (s *Server) Enroll(ctx context.Context, req *platform.EnrollRequest) (*platform.EnrollResponse, error) {

	log.Debugf(ctx, "Enroll {%s}", req)
	if req.Id == nil {
		return nil, errlib.Newf("FPL-3415 no id during client enrollment")
	}

	if req.Owner == nil {
		return nil, errlib.Newf("FPL-3416 no owner during client enrollment")
	}
	id := req.Id.AsPtr()
	owner := req.Owner.AsPtr()

	pub, err := x509.ParsePKIXPublicKey(req.PublicKey)
	if err != nil {
		return nil, errlib.Wrap("FPL-3417", err)
	}

	if _, ok := pub.(ed25519.PublicKey); !ok {
		return nil, errlib.Newf("FPL-3415 unsupported public key for client enrollment")
	}

	pubser, err := x509.MarshalPKIXPublicKey(pub)
	if err != nil {
		return nil, err
	}

	xc := &platform.ExternalClient{
		Owner:     ptrpb.New(owner),
		Id:        ptrpb.New(id),
		PublicKey: pubser,
		Created:   timestamppb.Now(),
	}
	err = s.enroll(ctx, xc, false)
	if err != nil {
		log.Errorf(ctx, "FPL-3416 failed to enroll: %s", err)
		return nil, err
	}
	log.Auditf(ctx, "FPL-3410 enrolled {%s} for {%s}", id, owner)
	return &platform.EnrollResponse{}, nil
}

func (s *Server) enroll(ctx context.Context, xc *platform.ExternalClient, bootstrap bool) error {
	dep, tag := s.current()
	if tag == "" {
		return errlib.Newf("FPL-3417 platform is not available")
	}

	f := foo.DialDeployment(ctx, dep)

	id := xc.Id.AsPtr()

	d := f.Access(s.rootDomain, s.secure)
	return d.Update(ctx, func(ctx context.Context, wr foo.WriteTxn) error {
		p, err := wr.Get(ctx, "ix/clients")
		if err != nil {
			return err
		}
		var h *bighash.Hash

		if p.IsNull() {
			h = bighash.New(wr)
			log.Infof(ctx, "creating new ix/clients table")
		} else {
			h = bighash.Load(wr, p)
			log.Infof(ctx, "loading existing ix/clients table")
		}

		c0, err := h.Get(ctx, id)
		if err == nil {
			var tmp platform.ExternalClient
			err = wr.Fill(ctx, c0, &tmp)
			if err != nil {
				return err
			}

			if ptrEqual(tmp.Owner, xc.Owner) &&
				bytes.Equal(tmp.PublicKey, xc.PublicKey) {
				log.Infof(ctx, "no change to client entry {%s}", id)
				return nil
			}
			log.Infof(ctx, "overriding client entry for {%s}", id)
		} else if errors.Is(err, bighash.ErrKeyNotFound) {
			log.Infof(ctx, "new client entry for {%s}", id)
		} else {
			return err
		}

		entry, err := wr.Store(ctx, xc)
		if err != nil {
			return err
		}
		log.Debugf(ctx, "stored client entry {%s}", entry)

		err = h.Insert(ctx, id, entry)

		if err != nil {
			return err
		}
		p, err = h.Store(ctx, wr)
		if err != nil {
			return err
		}

		err = wr.Set(ctx, "ix/clients", p)
		if err != nil {
			return err
		}
		if bootstrap {
			log.Infof(ctx, "bootstrapping ix/clients to {%s}", p)
		}
		return nil
	})
}

func ptrEqual(a, b *core.Ptr) bool {
	if a == nil && b == nil {
		return true
	}
	if a == nil || b == nil {
		return false
	}
	return a.Must() == b.Must()
}

var jwtparser = jwt.NewParser(jwt.WithValidMethods([]string{"EdDSA"}))

func (s *Server) exchangeJWT(ctx context.Context, req *platform.ExchangeRequest) (*platform.ExchangeResponse, error) {

	idp, ok := ptr.DecodeStringPtr(req.Identifier)
	if !ok {
		return nil, errlib.Newf("FPL-3413 invalid client identifier")
	}

	dep, tag := s.current()
	if tag == "" {
		return nil, errlib.Newf("FPL-3417 platform is not available")
	}

	f := foo.DialDeployment(ctx, dep)

	d := f.Access(s.rootDomain, s.secure)
	err := d.View(ctx, func(ctx context.Context, rd foo.ReadTxn) error {

		p, err := rd.Get(ctx, "ix/clients")
		if err != nil {
			return err
		}
		if p.IsNull() {
			return errlib.Newf("FPL-3416 no clients")
		}
		h := bighash.Load(rd, p)

		var kid string
		keyfn := func(tok *jwt.Token) (interface{}, error) {
			k, ok := tok.Header["kid"]
			if !ok {
				return nil, invalidTokenErr("missing key id")
			}
			kid, ok = k.(string)
			if !ok {
				return nil, invalidTokenErr("bad key id form")
			}

			kidp, ok := ptr.DecodeStringPtr(kid)
			if !ok {
				return nil, invalidTokenErr("invalid key id")
			}
			if kidp != idp {
				return nil, invalidTokenErr("incorrect key id")
			}

			val, err := h.Get(ctx, kidp)
			if err != nil {
				return nil, err
			}

			var xc platform.ExternalClient
			err = rd.Fill(ctx, val, &xc)
			if err != nil {
				return nil, err
			}
			pub, err := x509.ParsePKIXPublicKey(xc.PublicKey)
			if err != nil {
				return nil, err
			}
			dt := time.Since(xc.Created.AsTime())

			if xc.Owner != nil {
				log.Debugf(ctx, "loaded {%s} age %s owner {%s}",
					kidp, dt, xc.Owner.AsPtr())
			} else {
				log.Debugf(ctx, "loaded {%s} age %s", kidp, dt)
			}
			return pub, nil
		}

		var reg jwt.RegisteredClaims
		_, err = jwtparser.ParseWithClaims(req.Credentials, &reg, keyfn)
		if err != nil {
			return errlib.Wrap("FPL-3421", err)
		}
		if reg.Subject != kid {
			return invalidTokenErr("key id mismatch")
		}

		forme := false
		for _, aud := range reg.Audience {
			if aud == "foo-xauth" {
				forme = true
				break
			}
		}
		if !forme {
			return invalidTokenErr("incorrect audience")
		}

		return nil
	})
	if err != nil {
		return nil, errlib.Wrap("FPL-3429", err)
	}

	// validated the client; mint the exchange token

	token, exp, err := s.iss.Mint(s.iss.InternalClaims(req.Identifier))
	if err != nil {
		return nil, err
	}

	if req.DeploymentEtag == tag {
		// don't return the deployment info (it's relatively
		// large and unchanging) if the client already has
		// that tag
		dep = nil
	}
	resp := platform.ExchangeResponse{
		Token: &platform.IssuedToken{
			Token:   token,
			Expires: timestamppb.New(*exp),
		},
		DeploymentEtag: tag,
		Deployment:     dep,
	}
	return &resp, nil
}

func invalidTokenErr(why string) error {
	return errlib.Newf("FPL-3422 invalid token (%s)", why)
}

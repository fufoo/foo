package mint

import (
	"context"
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"encoding/base64"
	"fmt"
	"io"
	"math/big"
	"sync/atomic"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
	"github.com/golang-jwt/jwt/v4"
)

var log = logging.New("mint")

type Issuer struct {
	id      string
	key     *ecdsa.PrivateKey
	expires time.Time
	seq     uint64
}

func (iss *Issuer) ID() string {
	return iss.id
}

func (iss *Issuer) Expires() time.Time {
	return iss.expires
}

func (iss *Issuer) Public() crypto.PublicKey {
	return iss.key.Public()
}

func (iss *Issuer) PublicKey() string {
	public, err := x509.MarshalPKIXPublicKey(iss.key.Public())
	if err != nil {
		return ""
	}

	return base64.StdEncoding.EncodeToString(public)
	/*
		buf := pem.EncodeToMemory(&pem.Block{
			Type:  "PUBLIC KEY",
			Bytes: public,
		})
		return string(buf)*/
}

func NewIssuer(ctx context.Context) (*Issuer, error) {

	var nonce [14]byte
	n, err := io.ReadFull(rand.Reader, nonce[:])
	if err != nil {
		return nil, err
	}
	if n != len(nonce) {
		return nil, errlib.Newf("FPL-3371 could not read a nonce for issuer, only got %d", n)
	}
	var z big.Int
	z.SetBytes(nonce[:])
	id := z.Text(62)

	priv, err := ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
	if err != nil {
		return nil, err
	}

	/*
		private, err := x509.MarshalPKCS8PrivateKey(priv)
		if err != nil {
			panic(err)
		}
		buf0 := pem.EncodeToMemory(&pem.Block{
			Type:  "EC PRIVATE KEY",
			Bytes: private,
		})
		log.Infof(ctx, "Private Key is\n%s", buf0)
	*/
	/*

		public, err := x509.MarshalPKIXPublicKey(priv.Public())
		if err != nil {
			return nil, err
		}

		buf := pem.EncodeToMemory(&pem.Block{
			Type:  "PUBLIC KEY",
			Bytes: public,
		})
	*/
	iss := &Issuer{
		id:      id,
		key:     priv,
		expires: time.Now().Add(4 * time.Hour),
	}
	log.Infof(ctx, "issuer %s public key:\n%s", id, iss.PublicKey())
	return iss, nil
}

type Expirer interface {
	Expires() *time.Time
}

func expires(claims jwt.Claims) *time.Time {
	if c, ok := claims.(*jwt.RegisteredClaims); ok {
		if c.ExpiresAt == nil {
			return nil
		}
		return &c.ExpiresAt.Time
	} else if xt, ok := claims.(Expirer); ok {
		return xt.Expires()
	} else {
		panic(fmt.Sprintf("claims %T is neither *jwt.RegisteredClaims nor implements Expirer", claims))
	}
}

func (iss *Issuer) Mint(claims jwt.Claims) (string, *time.Time, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodES384, claims)

	str, err := token.SignedString(iss.key)
	if err != nil {
		return "", nil, err
	}
	return str, expires(claims), nil
}

func (iss *Issuer) ExternalClaims(subject string) jwt.Claims {
	now := time.Now().Round(time.Second)
	exp := &jwt.NumericDate{now.Add(time.Hour)}
	iat := &jwt.NumericDate{now.Add(-2 * time.Second)}

	return &jwt.RegisteredClaims{
		Audience:  []string{"foo-platform"},
		ExpiresAt: exp,
		IssuedAt:  iat,
		ID:        fmt.Sprintf("x%d", atomic.AddUint64(&iss.seq, 1)),
		Issuer:    iss.id,
		NotBefore: iat,
		Subject:   subject,
	}
}

func (iss *Issuer) InternalClaims(subject string) jwt.Claims {
	now := time.Now().Round(time.Second)
	exp := &jwt.NumericDate{now.Add(time.Hour)}
	iat := &jwt.NumericDate{now.Add(-2 * time.Second)}

	return &jwt.RegisteredClaims{
		Audience:  []string{"foo-platform"},
		ExpiresAt: exp,
		IssuedAt:  iat,
		ID:        fmt.Sprintf("i%d", atomic.AddUint64(&iss.seq, 1)),
		Issuer:    iss.id,
		NotBefore: iat,
		Subject:   subject,
	}
}

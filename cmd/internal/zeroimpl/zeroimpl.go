package zeroimpl

import (
	"context"
	"fmt"
	"sync"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/foo/cmd/internal/mint"
	"bitbucket.org/fufoo/foo/internal/services/platform"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var log = logging.New("zeroimpl")

func New(ctx context.Context, id uint64, name string) (platform.ZeroQServer, error) {
	work := make(chan zop, 100)
	iss, err := mint.NewIssuer(ctx)
	if err != nil {
		return nil, err
	}

	z := &zero{
		work: work,
		seq:  100,
		keys: &platform.KeySet{
			Index: make(map[string]*platform.PublicKey),
		},
		nodes:  make(map[nodeKey]*node),
		issuer: iss,
		skeleton: &platform.Deployment{
			Id:   id,
			Name: name,
		},
	}

	zerokey := &platform.PublicKey{
		Authority: platform.PublicKey_ZERO,
		Issuer:    iss.ID(),
		Public:    iss.PublicKey(),
		Expires:   timestamppb.New(iss.Expires()),
	}

	z.keys.Index[iss.ID()] = zerokey

	go z.consumeWork(work)
	return z, nil
}

type nodeKey struct {
	kind string // e.g. "cam", "state", or "xauth"
	seed string // an instance id for the node
}

type node struct {
	feed *bootstream
	info *platform.Node
}

type zero struct {
	lock     sync.Mutex
	work     chan<- zop
	seq      uint64
	keys     *platform.KeySet
	nodes    map[nodeKey]*node
	push     <-chan time.Time
	issuer   *mint.Issuer
	skeleton *platform.Deployment
}

type zop interface {
	apply(*zero)
}

func (z *zero) consumeWork(ops <-chan zop) {
	for op := range ops {
		op.apply(z)
	}
}

func (z *zero) schedulePush() {
	if z.push == nil {
		t := time.After(time.Second)
		z.push = t
		go func() {
			ctx := context.Background()
			log.Tracef(ctx, "waiting to fire push")
			<-t
			log.Tracef(ctx, "firing push")
			z.work <- push{}
		}()
	}
}

type push struct {
}

func (z *zero) theDeployment() *platform.Deployment {
	services := make(map[string]*platform.ServiceDeployment)

	for k, node := range z.nodes {
		dep := services[k.kind]
		if dep == nil {
			dep = &platform.ServiceDeployment{
				Kind:    k.kind,
				Current: &platform.Cluster{},
			}
			services[k.kind] = dep
		}
		dep.Current.Nodes = append(dep.Current.Nodes, node.info)
	}
	if s, ok := services["state"]; ok {
		s.State = &platform.StateConfig{
			Buckets: 1024,
		}
	}
	if s, ok := services["cam"]; ok {
		s.Cam = &platform.CAMConfig{
			Replicas:  3,
			Buckets:   1024,
			CdnFormat: "http://dev-donovan-local.callisto.axis.rscheme.org:8022/cam/{{.Addr}}/{{.Base}}",
		}
	}

	d := &platform.Deployment{
		Id:   z.skeleton.Id,
		Name: z.skeleton.Name,
	}
	for _, v := range services {
		d.Services = append(d.Services, v)
	}
	return d
}

func (z *zero) copyOfKeys() *platform.KeySet {
	src := z.keys.Index
	dst := make(map[string]*platform.PublicKey, len(src))
	for k, v := range src {
		dst[k] = v
	}
	return &platform.KeySet{
		Index: dst,
	}
}

func (push) apply(z *zero) {
	ctx := context.Background()
	log.Tracef(ctx, "pushing")
	z.push = nil

	keys := z.copyOfKeys()
	dep := z.theDeployment()

	for k, v := range z.nodes {
		log.Tracef(ctx, "  pushing to %s node %q with seed %q", k.kind, v.info.Name, k.seed)
		v.feed.send(z.seq, &platform.BootstrapNodeResponse{
			Seq:        z.seq,
			Keys:       keys,
			Deployment: dep,
		})
	}
}

type addxauth struct {
	done     chan<- error
	request  *platform.AddExternalAuthKeyRequest
	response *platform.AddExternalAuthKeyResponse
}

func (x *addxauth) apply(z *zero) {

	if _, ok := z.keys.Index[x.request.Key.Issuer]; ok {
		x.done <- errlib.Newf("FPL-1003 key %q already registered", x.request.Key.Issuer)
		return
	}

	z.keys.Index[x.request.Key.Issuer] = x.request.Key

	// scan for expired ones
	now := time.Now()
	for k, v := range z.keys.Index {
		if v.Expires.AsTime().Before(now) {
			delete(z.keys.Index, k)
		}
	}

	z.seq++
	x.response = &platform.AddExternalAuthKeyResponse{}
	x.done <- nil
}

func (z *zero) AddExternalAuthKey(ctx context.Context, req *platform.AddExternalAuthKeyRequest) (*platform.AddExternalAuthKeyResponse, error) {
	done := make(chan error, 1)

	x := &addxauth{
		done:    done,
		request: req,
	}
	z.work <- x
	err := <-done
	return x.response, err
}

type bootstrap struct {
	ctx      context.Context
	done     chan<- error
	request  *platform.BootstrapNodeRequest
	response *bootstream
}

type canceled struct {
	b *bootstrap
}

func (b *bootstrap) apply(z *zero) {
	ctx := b.ctx

	log.Tracef(ctx, "boot {%s}", b.request)

	// issue their first token

	//

	nk := nodeKey{
		kind: b.request.Kind,
		seed: b.request.Node.Seed,
	}

	if n0, ok := z.nodes[nk]; ok {
		if b.request.Node.Name != n0.info.Name {
			log.Warningf(ctx, "FPL-1014 node seed %s already registered to node %q, disallowing %q",
				nk.seed,
				n0.info.Name,
				b.request.Node.Name)
			b.done <- errlib.Newf("FPL-1013 node seed %q already registered", nk.seed)
			return
		}
		log.Infof(ctx, "FPL-1013 node %q with seed %s has re-booted",
			n0.info.Name,
			nk.seed)
		if b.request.Node.Addr != n0.info.Addr {
			log.Warningf(ctx, "FPL-1014 address changed from %q to %q", n0.info.Addr, b.request.Node.Addr)
		}
		if b.request.Node.Weight != n0.info.Weight {
			log.Infof(ctx, "FPL-1015 weight changed from %d to %d", n0.info.Weight, b.request.Node.Weight)
		}
		n0.feed.replaced()
	}
	z.nodes[nk] = &node{
		feed: b.response,
		info: b.request.Node,
	}
	z.seq++ // config has changed, bump the seq

	subject := fmt.Sprintf("%s|%s|%s", b.request.Kind, b.request.Node.Name, b.request.Node.Seed)

	token, expires, err := z.issuer.Mint(z.issuer.InternalClaims(subject))
	if err != nil {
		panic(err)
	}

	log.Debugf(ctx, "jwt expires in %s is %s", time.Until(*expires), token)
	log.Debugf(ctx, "%s", token)

	entry := &platform.BootstrapNodeResponse{
		Seq:        z.seq,
		Keys:       z.copyOfKeys(),
		Deployment: z.theDeployment(),
		Token: &platform.IssuedToken{
			Token:   token,
			Expires: timestamppb.New(*expires),
		},
	}
	b.response.send(z.seq, entry)
	b.done <- nil
	z.schedulePush()
}

func (c canceled) apply(z *zero) {
	panic("TODO")
}

func (z *zero) BootstrapNode(ctx context.Context, req *platform.BootstrapNodeRequest) (platform.Zero_BootstrapNodeQClient, error) {

	if req.Node == nil || req.Kind == "" || req.Node.Seed == "" || req.Node.Addr == "" {
		return nil, errlib.Newf("FPL-1015 missing node information")
	}

	done := make(chan error, 1)

	bridge := &bootstream{}
	bridge.ready = sync.NewCond(&bridge.lock)

	b := &bootstrap{
		ctx:      ctx,
		done:     done,
		request:  req,
		response: bridge,
	}

	z.work <- b

	select {
	case <-ctx.Done():
		z.work <- canceled{b}
		return nil, context.Canceled
	case err := <-done:
		if err != nil {
			return nil, err
		} else {
			return bridge, nil
		}
	}
}

type bootstream struct {
	ready *sync.Cond
	lock  sync.Mutex
	seq   uint64
	datum *platform.BootstrapNodeResponse
	err   error
}

func (b *bootstream) Recv() (*platform.BootstrapNodeResponse, error) {
	b.lock.Lock()
	defer b.lock.Unlock()

	for b.datum == nil && b.err == nil {
		b.ready.Wait()
	}
	// clear any datum as we return it, but leave errors around
	d := b.datum
	if d != nil {
		b.datum = nil
	}
	return d, b.err
}

func (b *bootstream) replaced() {
	b.lock.Lock()
	defer b.lock.Unlock()
	b.err = errlib.Newf("FPL-1016 node replaced")
	b.ready.Broadcast()
}

func (b *bootstream) send(seq uint64, item *platform.BootstrapNodeResponse) {
	b.lock.Lock()
	defer b.lock.Unlock()

	if b.seq != seq {
		b.datum = item
		b.seq = seq
		b.ready.Broadcast()
	}
}

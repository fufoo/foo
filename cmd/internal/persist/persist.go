package persist

import (
	"context"
	"crypto/sha256"
	"encoding/binary"
	"encoding/json"
	"hash"
	//"math/bits"
	"os"
	"sync"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/internal/netmux"
	"bitbucket.org/fufoo/foo/lib/errors"
	"bitbucket.org/fufoo/foo/services/cam"
	"bitbucket.org/fufoo/foo/services/core"
	"github.com/dgraph-io/badger/v3"
	"github.com/google/uuid"
)

var log = logging.New("persist")

type Datum = cam.Datum

/*type Datum struct {
	Kind    ptr.Kind // something for which IsLoadable() is true
	Payload []byte
}*/

type activeIndirect struct {
	count    uint64
	fd       *os.File
	opened   time.Time
	accessed time.Time
}

type bucketStats struct {
	count uint64
	size  uint64 // might be approximate; see docs for ValueSize()
}

type storeMeta struct {
	Format      int       `json:"format"`
	Serial      string    `json:"uuid"`
	CreatedTime time.Time `json:"ctime"`
	NumBuckets  int       `json:"numBuckets"`
}

type Store struct {
	meta     storeMeta
	db       *badger.DB
	lock     sync.Mutex
	indirect map[string]*activeIndirect
	buckets  []bucketStats
}

func Open(ctx context.Context, file string, buckets int) (*Store, error) {

	opts := badger.DefaultOptions(file)

	// in SMALL mode
	// opts.ValueLogFileSize = 2000000

	db, err := badger.Open(opts)
	if err != nil {
		return nil, err
	}

	s := &Store{
		db:       db,
		indirect: make(map[string]*activeIndirect),
		buckets:  make([]bucketStats, buckets),
	}
	s.fetchBucketData(ctx)

	return s, nil
}

type StoreStats interface {
	Len() int
	Counts() []uint64
	Sizes() []uint64
}

type statSnap []bucketStats

func (s statSnap) Len() int {
	return len(s)
}

func (s statSnap) Counts() []uint64 {
	ret := make([]uint64, len(s))
	for i, bs := range s {
		ret[i] = bs.count
	}
	return ret
}

func (s statSnap) Sizes() []uint64 {
	ret := make([]uint64, len(s))
	for i, bs := range s {
		ret[i] = bs.size
	}
	return ret
}

func (s *Store) Stats() StoreStats {
	b := make([]bucketStats, len(s.buckets))
	copy(b, s.buckets)
	return statSnap(b)
}

func (s *Store) fetchBucketData(ctx context.Context) error {
	needReset := false

	log.Tracef(ctx, "fetching bucket metadata")

	if false {
		// dump the entire store

		s.db.View(func(txn *badger.Txn) error {
			it := txn.NewIterator(badger.IteratorOptions{})
			defer it.Close()

			count := 0
			for it.Rewind(); it.Valid(); it.Next() {
				item := it.Item()
				if item.IsDeletedOrExpired() {
					log.Tracef(ctx, "[%x] DEL", item.Key())
				} else {
					item.Value(func(v []byte) error {
						log.Tracef(ctx, "[%x] VALID (%d bytes) [%.32x]", item.Key(), len(v), v)
						count++
						return nil
					})
				}
			}
			log.Tracef(ctx, "%d valid", count)
			return nil
		})
	}
	err := s.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte{configInfo})
		if err != nil {
			if err == badger.ErrKeyNotFound {
				needReset = true
				return nil
			}
			return err
		}

		var meta storeMeta

		err = item.Value(func(val []byte) error {
			return json.Unmarshal(val, &meta)
		})

		if err != nil {
			log.Tracef(ctx, "could not read meta")
			needReset = true
			return nil
		}

		if meta.Format != 1 {
			log.Tracef(ctx, "stored format is %d", meta.Format)
			needReset = true
			return nil
		}
		s.meta = meta

		if meta.NumBuckets != len(s.buckets) {
			log.Tracef(ctx, "change is number of buckets; stored %d now have %d",
				meta.NumBuckets, len(s.buckets))
			needReset = true
			return nil
		}

		it := txn.NewIterator(badger.IteratorOptions{
			PrefetchSize:   100,
			PrefetchValues: true,
			Prefix:         []byte{bucketInfo},
		})
		defer it.Close()

		b := s.buckets
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			if item.IsDeletedOrExpired() {
				continue
			}
			var bs bucketStats
			bucketNumber := binary.BigEndian.Uint32(item.Key()[1:])
			err := item.Value(func(val []byte) error {
				bs.count = binary.BigEndian.Uint64(val[:8])
				bs.size = binary.BigEndian.Uint64(val[8:16])
				return nil
			})
			if err != nil {
				panic(err)
			}
			if int(bucketNumber) >= len(b) {
				log.Errorf(ctx, "found bucket %d [%x] which is >= %d", bucketNumber, item.Key(), len(b))
				needReset = true
				return nil
			}
			b[bucketNumber] = bs
		}
		return nil
	})
	if err == nil && needReset {
		log.Infof(ctx, "need to reset")
		err = s.resetBucketData(ctx)
	}
	if err != nil {
		log.Tracef(ctx, "failed: %s", err)
		return err
	}
	log.Infof(ctx, "store %q (created on %s) has %d buckets",
		s.meta.Serial,
		s.meta.CreatedTime.Format("2006-01-02"),
		len(s.buckets))

	return nil
}

func (s *Store) resetBucketData(ctx context.Context) error {
	if s.meta.Serial == "" {
		s.meta.Serial = uuid.New().String()
		s.meta.CreatedTime = time.Now()
	}
	s.meta.Format = 1
	s.meta.NumBuckets = len(s.buckets)
	metabuf, err := json.Marshal(&s.meta)
	if err != nil {
		panic(err)
	}

	// rebuild the bucket metadata
	b := make([]bucketStats, s.meta.NumBuckets)

	err = s.db.View(func(txn *badger.Txn) error {
		it := txn.NewIterator(badger.IteratorOptions{
			PrefetchSize:   10,
			PrefetchValues: true,
			Prefix:         []byte{datumKey},
		})
		defer it.Close()

		for it.Rewind(); it.Valid(); it.Next() {
			var p core.Ptr
			p.Bits = it.Item().Key()[1:]
			i := netmux.BucketFor(&p, len(b))
			b[i].count++
			size := uint64(it.Item().ValueSize())
			b[i].size += size
			//log.Tracef(ctx, "bucket[%d] has +1 +%d", i, size)
		}
		return nil
	})

	err = s.db.Update(func(txn *badger.Txn) error {
		// delete all the existing bucket info, it is invalid
		it := txn.NewIterator(badger.IteratorOptions{
			PrefetchSize: 100,
			Prefix:       []byte{bucketInfo},
		})
		defer it.Close()

		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			if item.IsDeletedOrExpired() {
				continue
			}
			k := it.Item().KeyCopy(nil)
			//log.Tracef(ctx, "delete [%x]", k)
			err := txn.Delete(k)
			if err != nil {
				panic(err)
			}
		}

		// write the new bucket info
		for i, bs := range b {
			if bs.count > 0 {
				var key [5]byte
				var value [16]byte

				key[0] = bucketInfo
				binary.BigEndian.PutUint32(key[1:], uint32(i))
				binary.BigEndian.PutUint64(value[:8], bs.count)
				binary.BigEndian.PutUint64(value[8:], bs.size)
				err = txn.Set(key[:], value[:])
				if err != nil {
					panic(err)
				}
				//log.Tracef(ctx, "[%x] bucket %d: count %d size %d", key[:], i, bs.count, bs.size)
			}
		}
		// write the metadata
		log.Tracef(ctx, "setting metadata: %s", metabuf)
		txn.Set([]byte{configInfo}, metabuf)
		return nil
	})
	if err != nil {
		panic(err)
		return err
	}

	s.buckets = b
	return nil
}

type scanResult struct {
	hash   ptr.Ptr
	values []ptr.Ptr
	count  uint64
	size   uint64
}

type Scanner struct {
	in      *Store
	prefix  ptr.Ptr
	bits    int
	fanout  int
	results []scanResult
	mask    uint64
	value   uint64
}

func (s *Store) NewScan(prefix *ptr.Ptr, bits, fanout int) *Scanner {
	if bits > 64 {
		panic("can't handle that many bits")
	}
	value := binary.BigEndian.Uint64(prefix.Bits[:])
	mask := ^(^uint64(0) >> bits)

	return &Scanner{
		in:      s,
		prefix:  *prefix,
		bits:    bits,
		fanout:  fanout,
		results: make([]scanResult, 1<<fanout),
		value:   value,
		mask:    mask,
	}
}

const maxValues = 16

type ReportVisitor func(fanout int, count, size uint64, hash *ptr.Ptr, literal []ptr.Ptr)

func (s *Scanner) Report(fn ReportVisitor) {
	for i, sr := range s.results {
		if !sr.hash.IsNull() {
			fn(i, sr.count, sr.size, &sr.hash, sr.values)
		}
	}
}

func (s *Scanner) Run(ctx context.Context) error {
	var prefix [9]byte
	prefix[0] = datumKey
	copy(prefix[1:], s.prefix.Bits[:])
	prefixBytes := 1 + (s.bits / 8)
	log.Tracef(ctx, "running scan, db prefix [%x] ie mask %016x value %016x",
		prefix[:prefixBytes],
		s.mask,
		s.value,
	)
	fanoutShift := 64 - (s.bits + s.fanout)
	fanoutMask := (uint64(1) << s.fanout) - 1
	log.Tracef(ctx, "fanout shift %d mask %016x", fanoutShift, fanoutMask)

	return s.in.db.View(func(txn *badger.Txn) error {

		it := txn.NewIterator(badger.IteratorOptions{
			Prefix: prefix[:prefixBytes],
		})
		defer it.Close()

		started := false

		var h hash.Hash
		hfor := -1

		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			addr := item.Key()[1:] // this is just the slice version of the ptr
			if len(addr) != ptr.Size {
				panic("bad addr?!?!")
			}
			k := binary.BigEndian.Uint64(addr)
			if k&s.mask == s.value {
				fanout := int((k >> fanoutShift) & fanoutMask)
				log.Tracef(ctx, "[%x] is of interest, fanout %#x", item.Key(), fanout)
				started = true

				if fanout != hfor {
					if h != nil {
						s.results[hfor].hash = ptr.FromHash(ptr.Hash, h.Sum(nil))
					}

					h = sha256.New()
					hfor = fanout
				}
				count := s.results[hfor].count
				vals := s.results[hfor].values
				if count < maxValues {
					var p ptr.Ptr
					copy(p.Bits[:], addr)
					s.results[hfor].values = append(vals, p)
				} else if count == maxValues {
					s.results[hfor].values = nil
				}
				s.results[hfor].count = count + 1
				s.results[hfor].size += uint64(item.EstimatedSize())
				h.Write(addr)
			} else if started {
				log.Tracef(ctx, "[%x] passed the region of interest", item.Key())
				break
			} else {
				log.Tracef(ctx, "[%x] not yet in the region of interest", item.Key())
			}
		}
		if h != nil {
			s.results[hfor].hash = ptr.FromHash(ptr.Hash, h.Sum(nil))
		}

		return nil
	})
}

func (s *Store) Load(ctx context.Context, p *ptr.Ptr) (result *Datum, err error) {
	var key [1 + ptr.Size]byte
	key[0] = 'H'
	copy(key[1:], p.Bits[:])
	var value []byte

	err = s.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get(key[:])
		if err != nil {
			if err == badger.ErrKeyNotFound {
				return errors.ErrNoSuchObject{p}
			}
			log.Errorf(ctx, "failed: %s", err)
			return err
		}
		value, err = item.ValueCopy(nil)
		return err
	})
	if err == nil {
		if value[0] == directValue {
			result = &Datum{
				Ref:     core.From(*p),
				Payload: value[1:],
			}
		} else {
			value, err = s.readIndirect(ctx, value[1:])
			if err == nil {
				result = &Datum{
					Ref:     core.From(*p),
					Payload: value,
				}
			}
		}
	}
	return
}

func (s *Store) openIndirect(src string) (*activeIndirect, error) {
	now := time.Now()
	s.lock.Lock()
	defer s.lock.Unlock()

	if ai, ok := s.indirect[src]; ok {
		ai.count++
		ai.accessed = now
		return ai, nil
	}
	fd, err := os.Open(src)
	if err != nil {
		return nil, err
	}
	ai := &activeIndirect{
		fd:       fd,
		opened:   now,
		accessed: now,
		count:    1,
	}
	s.indirect[src] = ai
	return ai, nil
}

func (s *Store) readIndirect(ctx context.Context, spec []byte) ([]byte, error) {
	log.Debugf(ctx, "spec %x", spec)
	file, offset, length := parseIndirectValue(spec)
	/*log.Debugf(ctx, "reading from indirect %q (offset %d length %d)",
	file,
	offset,
	length)*/
	ai, err := s.openIndirect(file)
	if err != nil {
		return nil, err
	}
	chunk := make([]byte, length)
	n, err := ai.fd.ReadAt(chunk, offset)
	if err != nil {
		return nil, err
	}
	if n != length {
		return nil, errlib.Newf("CAM-9999 %s partial read %d/%d at offset %d",
			file,
			n,
			length,
			offset)
	}
	return chunk, nil
}

func encodeIndirectValue(file string, offset int64, length int) []byte {
	var buf [1024]byte
	i := 0
	i += binary.PutUvarint(buf[i:], uint64(offset))
	i += binary.PutUvarint(buf[i:], uint64(length))
	copy(buf[i:], []byte(file))
	i += len(file)
	return buf[:i]
}

func parseIndirectValue(data []byte) (string, int64, int) {
	offset, n := binary.Uvarint(data)
	if n <= 0 {
		return "", 0, 0
	}
	data = data[n:]

	length, n := binary.Uvarint(data)
	if n <= 0 {
		return "", 0, 0
	}
	return string(data[n:]), int64(offset), int(length)
}

const (
	bucketInfo    = 0x42 // 'B'
	configInfo    = 0x43 // 'C'
	datumKey      = 0x48 // 'H'
	directValue   = 0x44 // 'D'
	indirectValue = 0x49 // 'I'
)

func (s *Store) StoreIndirect(ctx context.Context, p *ptr.Ptr, file string, offset uint64, length int) error {

	indir := encodeIndirectValue(file, int64(offset), length)

	val := append([]byte{indirectValue}, indir...)

	var key [1 + ptr.Size]byte
	key[0] = datumKey
	copy(key[1:], p.Bits[:])

	return s.db.Update(func(txn *badger.Txn) error {
		return txn.Set(key[:], val)
	})
}

func RecomputePtr(k ptr.Kind, d *Datum) (ptr.Ptr, error) {

	if !k.IsLoadable() {
		// this should have been caught earlier
		return ptr.Null, errlib.Newf("FLO-1313 {%s} is not loadable")
	}

	h := sha256.Sum256(d.Payload)
	p := ptr.FromHash(k, h[:])
	return p, nil
}

func (s *Store) Store(ctx context.Context, d *Datum) (ptr.Ptr, error) {
	var expected ptr.Ptr
	var k ptr.Kind
	if d.Ref == nil {
		k = ptr.Untyped
	} else {
		expected = d.Ref.Must()
		k = expected.Kind()
	}

	p, err := RecomputePtr(k, d)
	if err != nil {
		return ptr.Null, err
	}

	if d.Ref != nil && p != expected {
		return ptr.Null, errlib.Newf("FLO-1315 expected {%s} but payload is {%s}",
			expected,
			p)
	}

	log.Debugf(ctx, "store %d bytes {%s}", len(d.Payload), p)
	var key [1 + ptr.Size]byte
	key[0] = datumKey
	copy(key[1:], p.Bits[:])

	tmp := make([]byte, len(d.Payload)+1)
	copy(tmp[1:], d.Payload)
	tmp[0] = directValue

	err = s.db.Update(func(txn *badger.Txn) error {
		return txn.Set(key[:], tmp)
	})
	if err != nil {
		return ptr.Null, err
	}
	return p, nil
}

/*
func (s *Store) Write(ctx context.Context, key *ptr.Ptr, value []byte) error {
	return s.db.Update(func(txn *badger.Txn) error {
		return txn.Set([]byte("Q:xyzzy"), []byte(`THE ANSWER IS 42`))
	})
}

func (s *Store) Read(ctx context.Context, key *ptr.Ptr) (result []byte, err error) {

	err = s.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte("Q:xyzzy"))
		if err != nil {
			log.Errorf(ctx, "failed: %s", err)
			return err
		}
		log.Infof(ctx, "success: %q", item)
		p, err := item.ValueCopy(nil)
		if err != nil {
			return err
		}
		result = p
		return nil
	})
	return
}
*/

package adminimpl

import (
	"context"
	"errors"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/internal/services/platform"
	"bitbucket.org/fufoo/foo/lib/bighash"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/lib/sequence"
	"bitbucket.org/fufoo/foo/services/admin"
	"bitbucket.org/fufoo/foo/services/core/ptrpb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var log = logging.New("adminimpl")

type Server struct {
	conn  *foo.Conn
	use   *foo.Domain
	creds foo.Credentials
	tv    TokenVerifier
}

func (s *Server) resolveExternalClient(ctx context.Context, subject string) (*platform.ExternalClient, error) {
	var xc *platform.ExternalClient

	id, ok := ptr.DecodeStringPtr(subject)
	if !ok {
		return nil, errlib.Newf("FAD-7103 invalid subject {%s}", subject)
	}

	err := s.use.View(ctx, func(ctx context.Context, rd foo.ReadTxn) error {

		p, err := rd.Get(ctx, "ix/clients")
		if err != nil {
			return err
		}
		var h *bighash.Hash

		if p.IsNull() {
			h = bighash.New(rd)
			log.Infof(ctx, "creating new ix/clients table")
		} else {
			h = bighash.Load(rd, p)
			log.Infof(ctx, "loading existing ix/clients table")
		}

		c0, err := h.Get(ctx, id)
		if err != nil {
			if errors.Is(err, bighash.ErrKeyNotFound) {
				return errlib.Newf("FAD-7104 unknown subject {%s}", id)
			}
			return errlib.Wrap("FAD-7105", err)
		}

		xc = new(platform.ExternalClient)
		err = rd.Fill(ctx, c0, xc)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return xc, nil
}

// this is typically implemented by *zeroclient.SystemModel
type TokenVerifier interface {
	VerifyToken(ctx context.Context, token string) (string, error)
}

func New(conn *foo.Conn, creds foo.Credentials, use *foo.Domain, v TokenVerifier) *Server {
	return &Server{
		conn:  conn,
		creds: creds,
		use:   use,
		tv:    v,
	}
}

func (s *Server) AcquireDomain(ctx context.Context, req *admin.AcquireDomainRequest) (*admin.AcquireDomainResponse, error) {

	if req.Authz == nil {
		return nil, errlib.Newf("FAD-7101 missing authorization")
	}
	t := time.Now()

	// validate token
	subject, err := s.tv.VerifyToken(ctx, req.Authz.Token)
	if err != nil {
		return nil, errlib.Wrap("FAD-7102", err)
	}
	log.Debugf(ctx, "token from {%s} is OK", subject)

	xc, err := s.resolveExternalClient(ctx, subject)

	if err != nil {
		return nil, err
	}

	if xc.Owner == nil {
		// the external client (i.e., the entity authenticating
		// to the cluster) does not have an actual owner.  This
		// can happen, for example, if the authenticating entity
		// is the cluster itself.  Create an external user first using
		// `fooctl client enroll`
		return nil, errlib.Newf("FAD-7106 no owner for {%s}", subject)
	}

	owner := xc.Owner.AsPtr()
	log.Debugf(ctx, "owner is {%s}", owner)

	var resp *admin.AcquireDomainResponse

	err = s.use.Update(ctx, func(ctx context.Context, wr foo.WriteTxn) error {

		d, err := sequence.Next(ctx, wr, "seq/domain", 1000)
		if err != nil {
			return err
		}

		resp = &admin.AcquireDomainResponse{
			DomainId: d,
		}
		return nil
	})
	if err != nil {
		return nil, errlib.Wrap("FAD-7109", err)
	}

	// the first transaction in the new domain is to store the
	// metadata for the domain
	fresh := s.conn.Access(resp.DomainId, s.creds)
	err = fresh.Update(ctx, func(ctx context.Context, wr foo.WriteTxn) error {
		d := platform.Domain{
			Id:      resp.DomainId,
			Name:    req.Name,
			Label:   req.Label,
			Owner:   ptrpb.New(owner),
			Created: timestamppb.New(t),
		}
		p, err := wr.Store(ctx, &d)
		if err != nil {
			return err
		}
		return wr.Set(ctx, "foo/domain", p)
	})
	if err != nil {
		return nil, errlib.Wrap("FAD-7108", err)
	}
	log.Auditf(ctx, "FAD-7100 domain #%d acquired by {%s}",
		resp.DomainId,
		owner,
		logging.F("domain", resp.DomainId),
	)
	return resp, nil
}

func (s *Server) DescribeDomain(ctx context.Context, req *admin.DescribeDomainRequest) (*admin.DescribeDomainResponse, error) {
	var resp admin.DescribeDomainResponse

	for _, id := range req.DomainId {
		use := s.conn.Access(id, s.creds)
		err := use.View(ctx, func(ctx context.Context, rd foo.ReadTxn) error {
			p, err := rd.Get(ctx, "foo/domain")
			if err != nil {
				return err
			}
			var d platform.Domain
			err = rd.Fill(ctx, p, &d)
			if err != nil {
				return nil
			}
			resp.Descriptions = append(resp.Descriptions,
				&admin.DomainDescription{
					Id:      id,
					Owner:   d.Owner,
					Name:    d.Name,
					Label:   d.Label,
					Region:  "us-atx-1", // TODO figure this out
					Created: d.Created,
				},
			)
			return nil
		})
		if err != nil {
			return nil, err
		}
	}
	return &resp, nil
}

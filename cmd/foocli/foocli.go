// foocli is a client of the (public) foo protocol
package main

import (
	"context"
	"os"

	"bitbucket.org/fufoo/cli"
	"bitbucket.org/fufoo/slogging"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"bitbucket.org/fufoo/foo/services/cam"
	"bitbucket.org/fufoo/foo/services/state"
)

var log = slogging.New("foocli")

var app = &cli.Command{
	Name:  "foocli",
	Usage: "command for managing a tenant of foo",
	Subcommands: []*cli.Command{
		accountCmd,
	},
}

func middleware(fn func(ctx context.Context, c *cli.Request, up *Uplink) error) func(c *cli.Request) error {
	return func(c *cli.Request) error {

		var opts []grpc.DialOption

		cred, err := credentials.NewClientTLSFromFile(
			"/test/donovan/foo-cluster.d/db/ca/root.cert",
			"callisto")
		if err != nil {
			panic(err)
		}
		opts = append(opts, grpc.WithTransportCredentials(cred))

		if token := os.Getenv("JWT_TOKEN"); token != "" {
			perRPC := staticCreds(token)
			opts = append(opts, grpc.WithPerRPCCredentials(perRPC))
		}

		conn, err := grpc.Dial("localhost:7777", opts...)
		if err != nil {
			panic(err)
		}
		defer conn.Close()

		up := &Uplink{
			cam:   cam.NewStorageClient(conn),
			state: state.NewStateClient(conn),
		}

		return fn(c.Context, c, up)
	}
}

func main() {
	ctx := context.Background()
	app.Run(ctx, os.Args[1:])
}

type Uplink struct {
	cam   cam.StorageClient
	state state.StateClient
}

type staticCreds string

func (s staticCreds) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{
		"authorization": string(s),
	}, nil
}

func (s staticCreds) RequireTransportSecurity() bool {
	return true
}

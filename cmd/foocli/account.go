package main

import (
	"context"
	"fmt"

	"bitbucket.org/fufoo/cli"
	//"bitbucket.org/fufoo/core/ptr"

	//"bitbucket.org/fufoo/foo/services/core"
	"bitbucket.org/fufoo/foo/services/state"
)

var accountCmd = &cli.Command{
	Name:  "account",
	Usage: "administer account",
	Subcommands: []*cli.Command{
		&cli.Command{
			Name:   "show",
			Usage:  "show status of account",
			Action: middleware(accountShowRun),
		},
	},
}

func accountShowRun(ctx context.Context, c *cli.Request, up *Uplink) error {
	ret, err := up.state.Get(ctx, &state.GetRequest{
		Domain:   1001,
		Subjects: []string{"MasterDirectory"},
	})
	if err != nil {
		panic(err)
	}
	fmt.Printf("{%s}\n", ret)
	if ret.States[0].Value != nil {
		ptr := ret.States[0].Value.Must()
		fmt.Printf(" ==> {%s}\n", ptr)
	}
	return nil
}

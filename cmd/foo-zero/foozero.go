package main

import (
	"context"
	"os"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/foo/cmd/internal/zeroimpl"
	"bitbucket.org/fufoo/foo/internal/services/platform"
	"bitbucket.org/fufoo/foo/internal/tls"
	"bitbucket.org/fufoo/qrpc"
	"bitbucket.org/fufoo/qrpc/tracer"
	quic "github.com/quic-go/quic-go"
)

var log = logging.New("zerod")

func main() {
	lvl := os.Getenv("FOO_DEBUG")
	if lvl == "" {
		lvl = "trace"
	}
	logging.Init(logging.LowestLevel(lvl))

	ctx := context.Background()
	defer func() {
		log.Warningf(ctx, "zero stopping")
	}()

	self, err := zeroimpl.New(ctx, 1, "local-dev")
	if err != nil {
		log.Errorf(ctx, "could not start: %s", err)
		os.Exit(1)
	}

	server := qrpc.NewServer( /*qrpc.SessionMiddleware(loggingMiddleware)*/ )

	platform.RegisterZeroQServer(server, self)

	port := os.Getenv("FOO_ZERO_PORT")
	if port == "" {
		port = "2200"
	}

	certfile := os.Getenv("FOO_NODE_CERT")
	keyfile := os.Getenv("FOO_NODE_KEY")
	trust := os.Getenv("FOO_CA_TRUST")

	cfg, err := tls.PlatformServerMutual(certfile, keyfile, tls.Trust(trust))
	if err != nil {
		panic(err)
	}

	qc := &quic.Config{
		Versions: []quic.VersionNumber{1},
		Tracer:   tracer.New(1),
	}

	listener, err := quic.ListenAddr(":"+port, cfg, qc)
	if err != nil {
		panic(err)
	}

	log.Infof(ctx, "zero started on port %s", port)
	server.Serve(ctx, listener)
}

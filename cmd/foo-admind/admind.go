// the admin service is not part of the core platform; it consumes the
// usual foo cam+state API and participates in auth exchange.  It acts
// merely as a highly privileged client of the foo platform.

package main

import (
	"context"
	"os"
	"strings"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/foo/cmd/internal/adminimpl"
	"bitbucket.org/fufoo/foo/internal/tls"
	"bitbucket.org/fufoo/foo/internal/zeroclient"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/lib/keycreds"
	"bitbucket.org/fufoo/foo/lib/minica"
	"bitbucket.org/fufoo/foo/services/admin"
	"bitbucket.org/fufoo/qrpc"
	quic "github.com/quic-go/quic-go"
)

var log = logging.New("admind")

func main() {
	lvl := os.Getenv("FOO_DEBUG")
	if lvl == "" {
		lvl = "trace"
	}
	logging.Init(logging.LowestLevel(lvl))

	ctx := context.Background()

	aic, config, listenPort := ConfigFromEnv(ctx)

	zc, err := zeroclient.New(ctx, aic.Zero)
	if err != nil {
		log.Errorf(ctx, "could not connect to zero: %s", err)
		os.Exit(1)
	}
	sys := zeroclient.Bootstrap(ctx, "admin", zc, aic.Zero)

	conn, err := foo.DialRemote(ctx, config)
	if err != nil {
		panic(err)
	}

	server := qrpc.NewServer(qrpc.SessionMiddleware(loggingMiddleware))

	srv := adminimpl.New(
		conn,
		aic.Secrets,
		conn.Access(aic.Domain, aic.Secrets),
		sys,
	)

	admin.RegisterAdminQServer(server, srv)

	caDir := "/test/donovan/foo-cluster.d/db/ca"

	r, err := minica.New(ctx, caDir, "", "")
	if err != nil {
		panic(err)
	}

	hostname := strings.Split(listenPort, ":")[0]
	myCertFile, myKeyFile, err := r.GetCertAndKeyForHost(ctx, hostname)
	if err != nil {
		panic(err)
	}

	t, err := tls.PlatformServer(
		myCertFile,
		myKeyFile,
		tls.Trust(config.TrustFile))
	if err != nil {
		panic(err)
	}

	listener, err := quic.ListenAddr(listenPort, t, nil)
	if err != nil {
		panic(err)
	}

	server.Serve(ctx, listener)

	log.Infof(ctx, "running")
}

func loggingMiddleware(ctx context.Context, sess qrpc.Session, next qrpc.SessionHandler) {
	t0 := time.Now()
	ctx = logging.Set(ctx, "clientAddr", sess.RemoteAddr().String())
	next(ctx, sess)
	dt := time.Since(t0)
	log.Debugf(ctx, "connection lasted %s", dt)
}

func ConfigFromEnv(ctx context.Context) (Config, *foo.RemoteConfig, string) {

	rc := &foo.RemoteConfig{
		TrustFile: "/test/donovan/foo-cluster.d/db/ca/root.cert",
		Server:    "localhost:2230", // this is the auth node
	}
	err := rc.UseClientAuthFromFile("/test/donovan/foo-cluster.d/admin.key")
	if err != nil {
		panic(err)
	}

	listen := os.Getenv("FOO_ADMIN_PORT")
	if listen == "" {
		listen = "2240"
	}

	z := zeroclient.ConfigFromEnv(ctx)

	aic := Config{
		Zero: z,
		// TODO these should be parameters; note they are the same
		// as in xauth because TODO we should distinguish between
		// the admin domain and the xauth domain that stores client
		// enrollments (ix/clients)
		Domain:  1,
		Secrets: keycreds.KeyFromString("00userbase"),
	}

	return aic, rc, "localhost:" + listen
}

type Config struct {
	Zero    *zeroclient.Config
	Domain  uint64
	Secrets foo.Credentials
}

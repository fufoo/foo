package main

import (
	"context"
	"io/ioutil"

	"bitbucket.org/fufoo/foo/internal/scripting"
	"bitbucket.org/fufoo/foo/lib/foo"
	"github.com/dkolbly/cli"
)

func fuScriptMain(ctx context.Context, cc *cli.Context, conn *foo.Conn, creds foo.Credentials) error {

	var text []byte
	name := "cmdline"

	if cc.IsSet("file") {
		name = cc.String("file")
		buf, err := ioutil.ReadFile(name)
		if err != nil {
			return err
		}
		text = buf
	} else {
		text = []byte(cc.String("eval"))
	}

	script, err := scripting.Compile(ctx, name, text)
	if err != nil {
		return err
	}
	domain := uint64(cc.Int("domain"))
	d := conn.Access(domain, creds)
	return script.Run(ctx, d)
}

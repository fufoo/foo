package main

import (
	"context"
	"io/ioutil"
	"os"
	"path/filepath"
	"plugin"
	"strings"
)

func loadPlugins(ctx context.Context) {
	dirs := os.Getenv("FOO_PLUGINS")
	if dirs == "" {
		return
	}
	loaded := 0
	for _, dir := range strings.Split(dirs, string(filepath.ListSeparator)) {
		lst, err := ioutil.ReadDir(dir)
		if err != nil {
			log.Warningf(ctx, "could not scan plugin directory: %s", err)
			continue
		}
		for _, f := range lst {
			name := f.Name()
			if strings.HasPrefix(name, ".") {
				continue
			}
			switch filepath.Ext(name) {
			case ".plu", ".plugin", ".foo-plugin":
				fq := filepath.Join(dir, name)
				_, err := plugin.Open(fq)
				if err != nil {
					log.Warningf(ctx, "failed to load %s: %s",
						fq, err)
				}
				log.Debugf(ctx, "loaded plugin: %s", fq)
				loaded++
			}

		}
	}
	log.Debugf(ctx, "loaded %d plugins", loaded)
}

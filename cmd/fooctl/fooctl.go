package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"os"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/version"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/lib/keycreds"
	"github.com/dkolbly/cli"
)

var log = logging.New("fooctl")

var app = &cli.App{
	Name:    "fooctl",
	Version: "TODO",
	Commands: []*cli.Command{
		loadCmd,
		storeCmd,
		scriptCmd,
		scanCmd,
		followCmd,
		zeroCmd,
		clientCmd,
		literalCmd,
		adminCmd,
	},
}

var configFlag = &cli.StringFlag{
	Name:    "config",
	Usage:   "the configuration file for connecting to the platform",
	EnvVars: []string{"FOO_CONFIG"},
}

var authServerFlag = &cli.StringFlag{
	Name:    "server",
	Usage:   "the auth server to contact",
	EnvVars: []string{"FOO_AUTH_SERVER"},
}

var authMechanismFlag = &cli.StringFlag{
	Name:    "auth-mechanism",
	Usage:   "the auth mechanism to use to get authenticated",
	EnvVars: []string{"FOO_AUTH_MECHANISM"},
}

var authUserFlag = &cli.StringFlag{
	Name:    "auth-user",
	Usage:   "the auth user to authenticate as",
	EnvVars: []string{"FOO_AUTH_USER"},
}

var authPasswordFlag = &cli.StringFlag{
	Name:    "auth-password",
	Usage:   "the password to authenticate using",
	EnvVars: []string{"FOO_AUTH_PASSWORD"},
}

var secretFlag = &cli.StringFlag{
	Name:    "secret",
	Aliases: []string{"s"},
	Usage:   "secret value to encrypt data",
	EnvVars: []string{"FOO_SECRET"},
}

var scanCmd = &cli.Command{
	Name:   "scan",
	Usage:  "scan observables",
	Action: callsServer(false, scanMain),
	Flags: []cli.Flag{
		&cli.IntFlag{
			Name:     "domain",
			Aliases:  []string{"d"},
			Usage:    "the domain to scan",
			Required: true,
		},
		configFlag,
		authServerFlag,
		authMechanismFlag,
		authUserFlag,
		authPasswordFlag,
	},
}

var followCmd = &cli.Command{
	Name:   "follow",
	Usage:  "follow a set of observables",
	Action: callsServer(false, followMain),
	Flags: []cli.Flag{
		&cli.IntFlag{
			Name:     "domain",
			Aliases:  []string{"d"},
			Usage:    "the domain to follow",
			Required: true,
		},
		&cli.StringFlag{
			Name:    "wait",
			Aliases: []string{"w"},
			Usage:   "the maximum amount of time to wait",
		},
		configFlag,
		authServerFlag,
		authMechanismFlag,
		authUserFlag,
		authPasswordFlag,
	},
}

var storeCmd = &cli.Command{
	Name:   "store",
	Usage:  "store a datum in the CAM",
	Action: callsServer(true, storeMain),
	Flags: []cli.Flag{
		&cli.IntFlag{
			Name:    "domain",
			Aliases: []string{"d"},
			Usage:   "the domain to store object for",
		},
		&cli.StringFlag{
			Name:    "type",
			Aliases: []string{"t"},
			Usage:   "mime or PTR data type",
		},
		secretFlag,
		configFlag,
		authServerFlag,
		authMechanismFlag,
		authUserFlag,
		authPasswordFlag,
	},
}

var loadCmd = &cli.Command{
	Name:   "load",
	Usage:  "load a datum from the CAM",
	Action: callsServer(true, loadMain),
	Flags: []cli.Flag{
		secretFlag,
		configFlag,
		authServerFlag,
		authMechanismFlag,
		authUserFlag,
		authPasswordFlag,
		&cli.StringFlag{
			Name:    "output",
			Aliases: []string{"o"},
			Usage:   "where to write the loaded data",
		},
	},
}

var literalCmd = &cli.Command{
	Name:   "literal",
	Usage:  "convert to and from Ptr literals",
	Action: literalMain,
	Flags: []cli.Flag{
		&cli.BoolFlag{
			Name:    "reverse",
			Aliases: []string{"r"},
			Usage:   "convert from ptr to literal (we autodetect most cases)",
		},
		&cli.BoolFlag{
			Name:    "hash",
			Aliases: []string{"H"},
			Usage:   "compute hash values of string args",
		},
		&cli.BoolFlag{
			Name:    "number",
			Aliases: []string{"N"},
			Usage:   "interpret argument as rational number",
		},
		&cli.BoolFlag{
			Name:    "uuid",
			Aliases: []string{"U"},
			Usage:   "interpret argument as uuid",
		},
	},
}

var scriptCmd = &cli.Command{
	Name:   "script",
	Usage:  "run a script",
	Action: callsServer(true, scriptMain),
	Flags: []cli.Flag{
		&cli.BoolFlag{
			Name:  "stack",
			Usage: "use simple postfix stack language",
		},
		&cli.IntFlag{
			Name:    "domain",
			Aliases: []string{"d"},
			Usage:   "the domain to run the script against",
		},
		&cli.StringFlag{
			Name:    "file",
			Aliases: []string{"f"},
			Usage:   "the path of the script to run",
		},
		&cli.StringFlag{
			Name:    "eval",
			Aliases: []string{"e"},
			Usage:   "the literal script to run",
		},
		secretFlag,
		configFlag,
		authServerFlag,
		authMechanismFlag,
		authUserFlag,
		authPasswordFlag,
	},
}

var BuildTicket string

func main() {
	version.Init(BuildTicket)
	logging.Init(logging.LowestLevel(os.Getenv("FOO_DEBUG")))

	if version.V.Version != "" {
		app.Version = version.V.Version
	}

	ctx := context.Background()

	err := app.Run(os.Args)
	if err != nil {
		log.Error(ctx, err)
		os.Exit(1)
	}
	return
}

type externalClientConfig struct {
	Domain      uint64 `json:"domain"`
	Secret      string `json:"secret"`
	CATrustFile string `json:"caTrustFile"`
	Debug       string `json:"debug"`
	Server      string `json:"server"`
	Auth        struct {
		Mechanism string `json:"mechanism"`
		User      string `json:"user"`
		Password  string `json:"password"`
		File      string `json:"file"`
	} `json:"auth"`
}

func readConfig(src string) (*externalClientConfig, error) {
	buf, err := ioutil.ReadFile(src)
	if err != nil {
		return nil, err
	}

	xc := new(externalClientConfig)
	err = json.Unmarshal(buf, xc)
	if err != nil {
		return nil, err
	}
	return xc, nil
}

func callsServer(needcreds bool, fn func(context.Context, *cli.Context, *foo.Conn, foo.Credentials) error) func(*cli.Context) error {
	return func(cc *cli.Context) error {
		ctx := context.Background()

		var rc *foo.RemoteConfig
		var creds foo.Credentials

		if cfile := cc.String("config"); cfile != "" {
			cfg, err := readConfig(cfile)
			if err != nil {
				return err
			}

			// let environment override things
			if x := os.Getenv("FOO_SERVER"); x != "" {
				cfg.Server = x
			}
			if x := os.Getenv("FOO_DEBUG"); x != "" {
				cfg.Debug = x
			}
			if x := os.Getenv("FOO_CA_TRUST"); x != "" {
				cfg.CATrustFile = x
			}

			rc = &foo.RemoteConfig{
				TrustFile: cfg.CATrustFile,
				Server:    cfg.Server,
			}

			if cfg.Auth.Mechanism == "client-key" {
				err = rc.UseClientAuthFromFile(cfg.Auth.File)
				if err != nil {
					return err
				}
			} else {
				rc.AuthMechanism = cfg.Auth.Mechanism
				rc.AuthIdentity = cfg.Auth.User
				rc.AuthCredentials = cfg.Auth.Password
			}

			if cfg.Secret != "" {
				creds = keycreds.KeyFromString(cfg.Secret)
			} else if s := cc.String("secret"); s != "" {
				creds = keycreds.KeyFromString(s)
			}
			if cfg.Debug != "" {
				logging.Init(logging.LowestLevel(cfg.Debug))
			}
		} else {
			rc = &foo.RemoteConfig{
				TrustFile: os.Getenv("FOO_CA_TRUST"),
				Server:    cc.String("server"),
			}

			if cc.String("auth-mechanism") == "client-key" {
				err := rc.UseClientAuthFromFile(cc.String("auth-file"))
				if err != nil {
					return err
				}
			} else {
				rc.AuthMechanism = cc.String("auth-mechanism")
				rc.AuthIdentity = cc.String("auth-user")
				rc.AuthCredentials = cc.String("auth-password")
			}
			if secret := cc.String("secret"); secret != "" {
				creds = keycreds.KeyFromString(secret)
			}
		}

		if creds == nil && needcreds {
			return errlib.Newf("FCL-1440 no credentials specified")
		}

		conn, err := foo.DialRemote(ctx, rc)
		if err != nil {
			return err
		}
		return fn(ctx, cc, conn, creds)
	}
}

package main

import (
	"context"
	"errors"
	"fmt"
	"io"

	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/services/core"
	"github.com/dkolbly/cli"
	"google.golang.org/grpc/status"
)

func scanMain(ctx context.Context, cc *cli.Context, conn *foo.Conn, _ foo.Credentials) error {
	domain := uint64(cc.Int("domain"))
	d := conn.Access(domain, nil)

	for rec := range d.Scan(ctx, cc.Args().Slice()) {
		fmt.Printf("%s%s\n", rec.Short(), valuestr(valueornull(rec.Value)))
	}
	return nil
}

func isEOF(err error) bool {
	if errors.Is(err, io.EOF) {
		return true
	}
	conv := status.Convert(err)
	if conv == nil {
		return false
	}
	return false
}

func valueornull(p *core.Ptr) ptr.Ptr {
	if p == nil {
		return ptr.Null
	}
	return p.Must()
}

func valuestr(p ptr.Ptr) string {
	if p.IsNull() {
		return ""
	}
	if !p.IsLiteral() {
		return ""
	}
	return " ; " + p.LiteralString()

}

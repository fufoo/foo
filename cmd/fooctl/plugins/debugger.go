package plugins

import (
	//"io"
	"context"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/lib/sealed"
)

var log = logging.New("plugins")

// a protocol for plugins

var probers []ObjectLoadProber

type ObjectLoadProber interface {
	Name() string
	Probe(ctx context.Context, rd foo.ReadTxn, t ptr.Ptr) (sealed.Loader, error)
}

func RegisterObjectLoadProber(p ObjectLoadProber) {
	probers = append(probers, p)
}

func GetObjectLoader(ctx context.Context, rd foo.ReadTxn, t ptr.Ptr) (sealed.Loader, error) {
	for _, prober := range probers {
		ld, err := prober.Probe(ctx, rd, t)
		if ld != nil {
			return ld, nil
		}
		if err != nil {
			log.Warningf(ctx, "load probe %s failed for {%s}: %s",
				prober.Name(),
				t,
				err)
		}
	}
	return nil, nil
}

/*

type DebugHandlerFunc func(dst io.Writer, src ptr.Ptr) sealed.Loader

var debuggers = make(map[ptr.Ptr]DebugHandlerFunc)

// RegisterDebugHandler registers a debug handler for the given
// type
func RegisterDebugHandler(forType ptr.Ptr, handler DebugHandlerFunc) {
	debuggers[forType] = handler
}

*/

package main

import (
	"errors"
	"fmt"
	"math/big"
	"os"

	"bitbucket.org/fufoo/core/ptr"
	"github.com/dkolbly/cli"
	"github.com/google/uuid"
)

func literalMain(cc *cli.Context) error {
	for _, arg := range cc.Args().Slice() {
		p, ok := ptr.DecodeStringPtr(arg)
		if ok || cc.Bool("reverse") {
			if !ok {
				fmt.Fprintf(os.Stderr, "%s is not a valid ptr\n", arg)
				return errors.New("invalid ptr")
			}
			if !p.IsLiteral() {
				fmt.Fprintf(os.Stderr, "%s is not a literal; metatype is %s\n",
					p,
					p.Kind())
				return errors.New("not a literal")
			}
			decodeLiteral(p)
		} else if cc.Bool("number") {
			var n big.Rat
			_, ok := n.SetString(arg)
			if !ok {
				var i big.Int
				if _, ok := i.SetString(arg, 0); ok {
					n.SetInt(&i)
				} else {
					fmt.Fprintf(os.Stderr, "%q is not a rational number\n",
						arg)
					return errors.New("not a number")
				}
			}
			fmt.Printf("%s\n", ptr.Rat(&n))
		} else if cc.Bool("uuid") {
			u, err := uuid.Parse(arg)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%s\n", err)
				return err
			}
			fmt.Printf("%s\n", ptr.UUID(u))
		} else if cc.Bool("hash") {
			fmt.Printf("%s\n", ptr.HashAddress([]byte(arg)))
		} else {
			fmt.Printf("%s\n", ptr.String(arg))
		}
	}
	return nil
}

func decodeLiteral(p ptr.Ptr) {
	switch p.Kind() {
	case ptr.LiteralString:
		fmt.Printf("%s\n", p.StringValue())
	case ptr.LiteralRat:
		fmt.Printf("%s\n", p.LiteralString())
	case ptr.LiteralUUID:
		fmt.Printf("%s\n", uuid.UUID(p.UUIDValue()))
	case ptr.LiteralBytes:
		os.Stdout.Write(p.BytesValue())
	default:
		fmt.Printf("other type: %s\n", p.Kind())
	}
}

package main

import (
	"context"
	"os"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
	//"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/admin"
	"bitbucket.org/fufoo/foo/lib/foo"
	pb "bitbucket.org/fufoo/foo/services/admin"
	//"bitbucket.org/fufoo/foo/services/core/ptrpb"
	"github.com/dkolbly/cli"
)

var adminCmd = &cli.Command{
	Name:  "admin",
	Usage: "perform user admin operations",
	Subcommands: []*cli.Command{
		adminAquireCmd,
		adminDescribeCmd,
	},
}

var adminDescribeCmd = &cli.Command{
	Name:  "describe",
	Usage: "describe a domain",
	Flags: []cli.Flag{
		&cli.IntFlag{
			Name:     "domain",
			Aliases:  []string{"d"},
			Required: true,
		},
		configFlag,
		authServerFlag,
		authMechanismFlag,
		authUserFlag,
		authPasswordFlag,
	},
	Action: adminDescribeMain,
}

var adminAquireCmd = &cli.Command{
	Name:  "acquire",
	Usage: "acquire a domain",
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:     "name",
			Aliases:  []string{"n"},
			Required: true,
		},
		&cli.StringFlag{
			Name:    "label",
			Aliases: []string{"l"},
		},
		&cli.StringFlag{
			Name:     "region",
			Aliases:  []string{"r"},
			Required: true,
		},
		configFlag,
		authServerFlag,
		authMechanismFlag,
		authUserFlag,
		authPasswordFlag,
	},
	Action: adminAquireMain,
}

func callsAdmin(ctx context.Context, cc *cli.Context) (*admin.Conn, error) {
	cfile := cc.String("config")
	if cfile == "" {
		return nil, errlib.Newf("FCL-2321 need config")
	}

	cfg, err := readConfig(cfile)
	if err != nil {
		return nil, err
	}

	// let environment override things
	if x := os.Getenv("FOO_DEBUG"); x != "" {
		cfg.Debug = x
	}
	if x := os.Getenv("FOO_CA_TRUST"); x != "" {
		cfg.CATrustFile = x
	}

	logging.Init(logging.LowestLevel(cfg.Debug))

	if cfg.Auth.Mechanism != "client-key" {
		panic("unsupported auth mechanism")
	}

	auth, err := foo.ReadClientAuth(cfg.Auth.File)
	if err != nil {
		return nil, err
	}

	token, err := auth.NewToken("foo-xauth")

	log.Debugf(ctx, "token %s", token)
	log.Debugf(ctx, "trust file %s", cfg.CATrustFile)

	acfg := admin.Config{
		TrustFile:       cfg.CATrustFile,
		AuthServer:      "localhost:2230",
		Server:          "localhost:2250",
		AuthMechanism:   "client-key",
		AuthIdentity:    auth.ID.String(),
		AuthCredentials: token,
	}

	return admin.Dial(ctx, &acfg)
}

func adminAquireMain(cc *cli.Context) error {

	ctx := context.Background()
	conn, err := callsAdmin(ctx, cc)
	if err != nil {
		return err
	}

	resp, err := conn.AcquireDomain(ctx, &pb.AcquireDomainRequest{
		Region: cc.String("region"),
		Name:   cc.String("name"),
		Label:  cc.String("label"),
	})
	if err != nil {
		return err
	}
	log.Infof(ctx, "acquired domain %d", resp.DomainId)
	return nil
}

func adminDescribeMain(cc *cli.Context) error {
	ctx := context.Background()
	conn, err := callsAdmin(ctx, cc)
	if err != nil {
		return err
	}

	resp, err := conn.DescribeDomain(ctx, &pb.DescribeDomainRequest{
		DomainId: []uint64{
			uint64(cc.Int("domain")),
		},
	})
	if err != nil {
		return err
	}
	for _, desc := range resp.Descriptions {
		t := desc.Created.AsTime()
		log.Infof(ctx, "domain: %d\nowner: {%s}\nname: %q\nlabel: %q\ncreated: %s (%s ago)",
			desc.Id,
			desc.Owner.AsPtr(),
			desc.Name,
			desc.Label,
			t.Local().Format(time.RFC3339),
			time.Since(t),
		)
	}
	return nil

}

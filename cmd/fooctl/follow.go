package main

import (
	"context"
	"fmt"
	"time"

	"bitbucket.org/fufoo/foo/lib/foo"
	"github.com/dkolbly/cli"
)

func followMain(ctx context.Context, cc *cli.Context, conn *foo.Conn, _ foo.Credentials) error {
	domain := uint64(cc.Int("domain"))
	d := conn.Access(domain, nil)

	if cc.IsSet("wait") {
		dt, err := time.ParseDuration(cc.String("wait"))
		if err != nil {
			return err
		}
		ctx, _ = context.WithTimeout(ctx, dt)
		log.Debugf(ctx, "will follow for %s", dt)
	}

	args := cc.Args().Slice()

	watch := make(map[string]struct{})
	for _, arg := range args {
		watch[arg] = struct{}{}
	}

	return d.View(ctx, func(ctx context.Context, rd foo.ReadTxn) error {
		for _, arg := range args {
			p, err := rd.Get(ctx, arg)
			if err != nil {
				return err
			}
			if p.IsNull() {
				fmt.Printf("%s : NULL\n", arg)
			} else {
				fmt.Printf("%s : {%s}%s\n", arg, p, valuestr(p))
			}
		}
		for {
			changes, err := rd.WaitForChanges(ctx)
			if err != nil {
				return err
			}
			for _, changed := range changes {
				_, ok := watch[changed]
				if !ok {
					continue
				}
				p, err := rd.Get(ctx, changed)
				if err != nil {
					return err
				}
				if p.IsNull() {
					fmt.Printf("%s : NULL\n", changed)
				} else {
					fmt.Printf("%s : {%s}%s\n", changed, p, valuestr(p))
				}
			}
		}
		return nil
	})
}

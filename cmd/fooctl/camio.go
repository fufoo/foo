package main

import (
	"bufio"
	"context"
	"io"
	"io/ioutil"
	"os"

	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/cmd/fooctl/plugins"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/lib/keycreds"
	"bitbucket.org/fufoo/foo/lib/mime"
	"bitbucket.org/fufoo/foo/lib/sealed"
	"github.com/dkolbly/cli"
)

func storeMain(ctx context.Context, cc *cli.Context, conn *foo.Conn, creds foo.Credentials) error {
	if cc.IsSet("secret") {
		creds = keycreds.KeyFromString(cc.String("secret"))
	}

	var tx *ptr.Ptr
	if cc.IsSet("type") {
		m, ok := mime.PtrForType[cc.String("type")]
		if !ok {
			t, ok := ptr.DecodeStringPtr(cc.String("type"))
			if ok {
				tx = &t
			} else {
				return errlib.Newf("FCL-1310 cannot interpret type %q", cc.String("type"))
			}
		} else {
			tx = &m
		}
	}

	thingifier := func(s string) (interface{}, error) {
		var blob []byte
		var err error

		if s[0] == '@' {
			blob, err = ioutil.ReadFile(s[1:])
			if err != nil {
				return nil, err
			}

		} else {
			blob = []byte(s)
		}

		if tx != nil {
			return &TypedBlob{
				Type: *tx,
				Data: blob,
			}, nil
		} else {
			return blob, nil
		}
	}

	d := conn.Access(conn.Root(), creds)
	return d.Update(ctx, func(ctx context.Context, wr foo.WriteTxn) error {
		for _, arg := range cc.Args().Slice() {
			thing, err := thingifier(arg)
			if err != nil {
				return err
			}
			p, err := wr.Store(ctx, thing)
			if err != nil {
				return err
			}
			log.Debugf(ctx, "considering {%s} stored", p)
		}
		return nil
	})
}

// TODO this is now available as foo.TypeBlob
type TypedBlob struct {
	Type ptr.Ptr
	Data []byte
}

func (m *TypedBlob) MarshalCAM(typePtr *ptr.Ptr) (sealed.Marshaled, error) {
	return sealed.Marshaled{
		Payload: m.Data,
		Flags:   sealed.DeflateCompression,
	}, nil
	//	return [][]byte{m.Data}, nil, true, nil
}

func (m *TypedBlob) IssueType(context.Context, foo.ReadTxn) (ptr.Ptr, error) {
	return m.Type, nil
}

func loadMain(ctx context.Context, cc *cli.Context, conn *foo.Conn, creds foo.Credentials) error {
	if cc.IsSet("secret") {
		creds = keycreds.KeyFromString(cc.String("secret"))
	}

	loadPlugins(ctx)

	save := func(buf []byte) error {
		if cc.IsSet("output") {
			fd, err := os.Create(cc.String("output"))
			if err != nil {
				return err
			}
			_, err = fd.Write(buf)
			return err
		}
		return nil
	}

	d := conn.Access(conn.Root(), creds)
	return d.View(ctx, func(ctx context.Context, rd foo.ReadTxn) error {
		for _, arg := range cc.Args().Slice() {
			p, ok := ptr.DecodeStringPtr(arg)
			if !ok {
				return errlib.Newf("FCL-1401 invalid pointer %q", arg)
			}

			item, err := rd.Load(ctx, p, cliloader(rd))
			if err != nil {
				return err
			}
			log.Debugf(ctx, "loaded {%s} -> %T", p, item)
			if buf, ok := item.([]byte); ok {
				if !cc.IsSet("output") {
					log.Debugf(ctx, "%x = %q", buf, buf)
				}
				return save(buf)
			} else if tb, ok := item.(*TypedBlob); ok {
				if !cc.IsSet("output") {
					log.Debugf(ctx, "[%#v] %x = %q", tb.Type, tb.Data, tb.Data)
				}
				return save(tb.Data)
			}
		}
		return nil
	})
}

func cliloader(rd foo.ReadTxn) func(ctx context.Context, tp *ptr.Ptr, ix []ptr.Ptr, src *bufio.Reader) (interface{}, error) {

	return func(ctx context.Context, tp *ptr.Ptr, ix []ptr.Ptr, src *bufio.Reader) (interface{}, error) {
		if tp != nil {
			ld, _ := plugins.GetObjectLoader(ctx, rd, *tp)
			if ld != nil {
				return ld(ctx, tp, ix, src)
			}
		}

		content, err := io.ReadAll(src)
		if err != nil {
			return nil, err
		}
		if tp == nil {
			return content, nil
		}
		return &TypedBlob{
			Type: *tp,
			Data: content,
		}, nil
	}
}

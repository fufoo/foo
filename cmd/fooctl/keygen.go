package main

import (
	"context"
	"crypto/ed25519"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"os"

	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/internal/services/platform"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/services/core/ptrpb"
	"github.com/dkolbly/cli"
	"github.com/google/uuid"
)

var clientCmd = &cli.Command{
	Name:  "client",
	Usage: "manage client credentials",
	Subcommands: []*cli.Command{
		clientKeygenCmd,
		clientEnrollCmd,
		clientUnenrollCmd,
	},
}

var clientEnrollCmd = &cli.Command{
	Name:   "enroll",
	Usage:  "enroll a key pair as an authorized client",
	Action: callsServer(false, clientEnrollMain),
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:    "input",
			Usage:   "where to read the key pair file from",
			Aliases: []string{"i"},
			Value:   "client.key",
		},
		&cli.StringFlag{
			Name:     "owner",
			Usage:    "the account owning the client",
			Required: true,
		},
		configFlag,
		authServerFlag,
		authMechanismFlag,
		authUserFlag,
		authPasswordFlag,
	},
}

var clientUnenrollCmd = &cli.Command{
	Name:   "unenroll",
	Usage:  "unenroll a key pair as an authorized client",
	Action: callsServer(false, clientUnenrollMain),
}

var clientKeygenCmd = &cli.Command{
	Name:   "keygen",
	Usage:  "generate a key pair for client enrollment",
	Action: clientKeygenMain,
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:    "output",
			Usage:   "where to write the key pair file",
			Aliases: []string{"o"},
			Value:   "client.key",
		},
	},
}

func clientUnenrollMain(ctx context.Context, cc *cli.Context, conn *foo.Conn, _ foo.Credentials) error {
	panic("TODO")
	return nil
}

func clientKeygenMain(cc *cli.Context) error {
	_, priv, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		return err
	}

	privbuf, err := x509.MarshalPKCS8PrivateKey(priv)
	if err != nil {
		return err
	}

	cid, err := uuid.NewRandom()
	if err != nil {
		return err
	}

	b := &pem.Block{
		Type:  "PRIVATE KEY",
		Bytes: privbuf,
		Headers: map[string]string{
			"Foo-KeyId": ptr.UUID(cid).String(),
		},
	}

	fd, err := os.Create(cc.String("output"))
	if err != nil {
		return err
	}
	defer fd.Close()

	err = pem.Encode(fd, b)
	if err != nil {
		return err
	}

	return nil
}

/*func readClientInfo(src string) (*ClientInfo, error) {

}*/

func clientEnrollMain(ctx context.Context, cc *cli.Context, conn *foo.Conn, _ foo.Credentials) error {

	owner, ok := ptr.DecodeStringPtr(cc.String("owner"))
	if !ok {
		return errlib.Newf("FCL-2370 invalid owner id")
	}

	auth, err := foo.ReadClientAuth(cc.String("input"))
	if err != nil {
		return err
	}
	log.Debugf(ctx, "id is {%s}", auth.ID.String())

	pub, err := x509.MarshalPKIXPublicKey(auth.Key.Public())
	if err != nil {
		return err
	}

	req := &platform.EnrollRequest{
		Id:        ptrpb.New(auth.ID),
		PublicKey: pub,
		Owner:     ptrpb.New(owner),
	}

	return conn.EnrollExternalClient(ctx, req)
}

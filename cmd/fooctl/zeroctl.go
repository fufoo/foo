package main

import (
	"context"

	"bitbucket.org/fufoo/foo/internal/services/platform"
	"bitbucket.org/fufoo/foo/internal/tls"
	"bitbucket.org/fufoo/qrpc"
	"github.com/dkolbly/cli"
)

var zeroCmd = &cli.Command{
	Name:  "zero",
	Usage: "interact with zero server",
	Subcommands: []*cli.Command{
		zeroAddCmd,
		zeroBootCmd,
	},
}

var serverFlag = &cli.StringFlag{
	Name:     "server",
	Usage:    "the zero server to talk to",
	Required: true,
	EnvVars:  []string{"FOO_SERVER"},
}

var nodeCertFlag = &cli.StringFlag{
	Name:     "node-cert",
	Usage:    "the client certificate",
	Required: true,
	EnvVars:  []string{"FOO_NODE_CERT"},
}
var nodeKeyFlag = &cli.StringFlag{
	Name:     "node-key",
	Usage:    "the client private key",
	Required: true,
	EnvVars:  []string{"FOO_NODE_KEY"},
}
var trustFlag = &cli.StringFlag{
	Name:     "ca-trust",
	Usage:    "the certificate(s) to trust to identify the server",
	Required: true,
	EnvVars:  []string{"FOO_CA_TRUST"},
}

var zeroBootCmd = &cli.Command{
	Name:   "boot",
	Usage:  "obtain bootstrap information",
	Action: callsZeroServer(zeroBootMain),
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:    "kind",
			Aliases: []string{"k"},
			Usage:   "the kind of service we are",
		},
		&cli.StringFlag{
			Name:    "name",
			Aliases: []string{"n"},
			Usage:   "the friendly name of this node",
		},
		&cli.StringFlag{
			Name:    "seed",
			Aliases: []string{"s"},
			Usage:   "the seed static identifier for this node",
		},
		&cli.IntFlag{
			Name:    "weight",
			Aliases: []string{"w"},
			Usage:   "the weighting (+/- percentage; 0 = normal)",
		},
		&cli.StringFlag{
			Name:    "addr",
			Aliases: []string{"a"},
			Usage:   "the advertised address of this node",
		},
		&cli.BoolFlag{
			Name:  "no-watch",
			Usage: "don't watch the config, just report the first once",
		},
		serverFlag,
		nodeCertFlag,
		nodeKeyFlag,
		trustFlag,
	},
}

var zeroAddCmd = &cli.Command{
	Name:   "addkey",
	Usage:  "add an external auth key to the server",
	Action: callsZeroServer(zeroAddKeyMain),
	Flags: []cli.Flag{
		serverFlag,
		nodeCertFlag,
		nodeKeyFlag,
		trustFlag,
	},
}

func callsZeroServer(fn func(context.Context, *cli.Context, platform.ZeroQClient) error) func(*cli.Context) error {
	return func(cc *cli.Context) error {

		ctx := context.Background()
		cert := cc.String("node-cert")
		key := cc.String("node-key")
		server := cc.String("server")
		cfg, err := tls.PlatformClientMutual(cert, key, tls.Trust(cc.String("ca-trust")))
		if err != nil {
			return err
		}
		c, err := qrpc.Dial(ctx, server, qrpc.DialWithTLS(cfg))
		if err != nil {
			return err
		}
		return fn(ctx, cc, platform.NewZeroQClient(c))
	}
}

func zeroAddKeyMain(ctx context.Context, cc *cli.Context, zero platform.ZeroQClient) error {
	_, err := zero.AddExternalAuthKey(ctx, &platform.AddExternalAuthKeyRequest{
		Key: &platform.PublicKey{
			Issuer:    "x",
			Authority: platform.PublicKey_XAUTH,
		},
	})
	if err != nil {
		return err
	}
	log.Infof(ctx, "success")
	return nil
}

func zeroBootMain(ctx context.Context, cc *cli.Context, zero platform.ZeroQClient) error {
	info, err := zero.BootstrapNode(ctx, &platform.BootstrapNodeRequest{
		Kind: cc.String("kind"),
		Node: &platform.Node{
			Name:   cc.String("name"),
			Addr:   cc.String("addr"),
			Seed:   cc.String("seed"),
			Weight: int32(cc.Int("weight")),
		},
	})
	if err != nil {
		return err
	}
	for {
		item, err := info.Recv()
		if err != nil {
			log.Warningf(ctx, "halt: %s", err)
			return err
		}
		log.Infof(ctx, "config: {%s}", item)
	}
}

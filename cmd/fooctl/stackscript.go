package main

import (
	"context"
	"fmt"
	"math/big"
	"strconv"

	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/bighash"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/fulang/ast"
	"github.com/dkolbly/cli"
	"go/token"
)

// a simple stack language, since Fu isn't quite ready
//
//  example program to create a new hash table, add some stuff to it, and store it as the state of an obs
//
//     newhash /a <-
//     a "hello" literal "world" literal hashinsert
//     a "fox" literal "baz" literal hashinsert
//     a hashstore dup "test/124" set "test/123" set
//
//  retrieve a hash table and set an entry in it
//
//     "test/123" get hashload /a <-
//       a "count" literal 17 literal hashinsert
//     a hashstore "test/123" set
//
//  convert a literal string ptr to its string value
//
//     "kBM0TRQb7Iyscgg7TQR16ysnPfgp1aIHr39Tq3sA5S1s" toptr stringvalue print
//
//  delete the state of an observable
//
//     null "ix/app" set
//
//  e.g.,
//
//     fooctl script --stack --domain 153 'null "index/wu" set'

func scriptMain(ctx context.Context, cc *cli.Context, conn *foo.Conn, creds foo.Credentials) error {
	if cc.Bool("stack") {
		return stackScriptMain(ctx, cc, conn, creds)
	} else {
		return fuScriptMain(ctx, cc, conn, creds)
	}
}

func stackScriptMain(ctx context.Context, cc *cli.Context, conn *foo.Conn, creds foo.Credentials) error {

	domain := conn.Access(uint64(cc.Int("domain")), creds)

	for _, arg := range cc.Args().Slice() {

		tokens, err := ast.ScanText([]byte(arg))
		if err != nil {
			return err
		}

		eval := func(ctx context.Context, tx foo.WriteTxn) error {
			ops := make(map[string]func(context.Context))
			in := &Interp{
				ops:  ops,
				vars: make(map[string]interface{}),
				tx:   tx,
			}
			ops["dup"] = in.dupop
			ops["exch"] = in.exchop
			ops["print"] = in.printop

			ops["get"] = in.getop
			ops["set"] = in.setop
			ops["hashload"] = in.hashloadop
			ops["hashstore"] = in.hashstoreop
			ops["newhash"] = in.newhashop
			ops["literal"] = in.literalop
			ops["hashinsert"] = in.hashinsertop
			ops["hashget"] = in.hashgetop
			ops["add"] = in.addop
			ops["sub"] = in.subop
			ops["mul"] = in.mulop
			ops["div"] = in.divop
			ops["toptr"] = in.toptrop
			ops["null"] = in.nullop
			ops["stringvalue"] = in.stringvalueop

			err := in.run(ctx, tokens)
			if err != nil {
				return err
			}
			if len(in.stack) != 0 {
				return errlib.Newf("FCL-4410 incomplete program (%d left on stack) %v", len(in.stack), in.stack)
			}
			return nil
		}

		err = domain.Update(ctx, eval)
		if err != nil {
			return err
		}
	}
	return nil
}

type Interp struct {
	tx    foo.WriteTxn
	stack []interface{}
	ops   map[string]func(context.Context)
	vars  map[string]interface{}
}

func balanced(lst []*ast.Lexeme) ([]*ast.Lexeme, []*ast.Lexeme) {
	depth := 1

	for i, t := range lst {
		if t.Category() == token.RBRACE {
			depth--
			if depth == 0 {
				return lst[:i], lst[i+1:]
			}
		} else if t.Category() == token.LBRACE {
			depth++
		}
	}
	panic("missing closing brace")
}

type Block struct {
	content []*ast.Lexeme
}

func (in *Interp) run(ctx context.Context, tokens []*ast.Lexeme) error {
	for len(tokens) > 0 {
		next := tokens[0]
		tokens = tokens[1:]

		switch next.Category() {
		case token.IDENT:
			if v, ok := in.vars[next.Literal()]; ok {
				if block, ok := v.(Block); ok {
					in.run(ctx, block.content)
				} else {
					in.push(v)
				}
			} else if fn, ok := in.ops[next.Literal()]; ok {
				fn(ctx)
			} else {
				return errlib.Newf("FCL-4412 undefined operator %q", next.Literal())
			}

		case token.QUO:
			// special form
			if len(tokens) == 0 {
				return errlib.Newf("FCL-4443 '/' not followed by identifier")
			}
			next = tokens[0]
			tokens = tokens[1:]
			if next.Category() != token.IDENT {
				return errlib.Newf("FCL-4444 '/' not followed by identifier")
			}
			in.push(next)

		case token.ARROW:
			in.arrowop()

		case token.SEMICOLON:

		case token.LBRACE:
			var block []*ast.Lexeme
			block, tokens = balanced(tokens)
			log.Debugf(ctx, "block has %d tokens", len(block))
			for i, t := range block {
				log.Debugf(ctx, "   %d. %s (%s)", i, t.Category(), t.Literal())
			}
			in.stack = append(in.stack, Block{block})

		case token.INT:
			n, err := strconv.ParseInt(next.Literal(), 0, 64)
			if err != nil {
				panic(err)
			}
			in.stack = append(in.stack, n)

		case token.STRING:
			str, err := strconv.Unquote(next.Literal())
			if err != nil {
				panic(err) // should be unquotable as a parsed lexeme
			}
			in.stack = append(in.stack, str)
		default:
			panic(fmt.Sprintf("unexpected token %s", next.Category()))
		}
	}
	return nil
}

func (in *Interp) mutation(ctx context.Context) foo.WriteTxn {
	return in.tx
}

func (in *Interp) push(item interface{}) {
	in.stack = append(in.stack, item)
}

func (in *Interp) pop() interface{} {
	n := len(in.stack)
	top := in.stack[n-1]
	in.stack = in.stack[:n-1]
	return top
}

func (in *Interp) popnum() *big.Rat {
	n := len(in.stack)
	top := in.stack[n-1]
	in.stack = in.stack[:n-1]

	if i, ok := top.(int64); ok {
		return big.NewRat(i, 1)
	}
	if p, ok := top.(ptr.Ptr); ok {
		return p.RatValue()
	}
	if r, ok := top.(*big.Rat); ok {
		return r
	}
	panic("not a number")
}

func (in *Interp) popstring() string {
	n := len(in.stack)
	top := in.stack[n-1]
	in.stack = in.stack[:n-1]
	return top.(string)
}

func (in *Interp) popptr() ptr.Ptr {
	n := len(in.stack)
	top := in.stack[n-1]
	in.stack = in.stack[:n-1]
	return top.(ptr.Ptr)
}

func (in *Interp) setop(ctx context.Context) {
	top := in.popstring()
	val := in.popptr()
	in.mutation(ctx).Set(ctx, top, val)
}

func (in *Interp) getop(ctx context.Context) {
	top := in.popstring()
	p, err := in.tx.Get(ctx, top)
	if err != nil {
		panic(err)
	}
	log.Debugf(ctx, "get %q => {%s}", top, p)
	in.push(p)
}

func (in *Interp) hashloadop(ctx context.Context) {
	top := in.popptr()
	log.Debugf(ctx, "hashload {%s}", top)
	in.push(bighash.Load(in.tx, top))
}

func (in *Interp) newhashop(ctx context.Context) {
	in.push(bighash.New(in.tx))
}

func (in *Interp) hashstoreop(ctx context.Context) {
	h := in.pop().(*bighash.Hash)
	p, err := h.Store(ctx, in.mutation(ctx))
	if err != nil {
		panic(err)
	}
	in.push(p)
}

// turn a literal value into a ptr
func (in *Interp) literalop(ctx context.Context) {
	top := in.pop()
	switch top := top.(type) {
	case string:
		in.push(ptr.String(top))
	case int:
		in.push(ptr.Int64(int64(top)))
	case int64:
		in.push(ptr.Int64(top))
	case *big.Rat:
		in.push(ptr.Rat(top))
	default:
		panic(fmt.Sprintf("literal: unsupported type %T", top))
	}
}

func (in *Interp) printop(ctx context.Context) {
	top := in.pop()
	if rat, ok := top.(*big.Rat); ok {
		log.Infof(ctx, "PRINT %s", rat)
	} else {
		log.Infof(ctx, "PRINT %#v", top)
	}
}

func (in *Interp) exchop(ctx context.Context) {
	n := len(in.stack)
	in.stack[n-2], in.stack[n-1] = in.stack[n-1], in.stack[n-2]
}

func (in *Interp) dupop(ctx context.Context) {
	top := in.pop()
	in.push(top)
	in.push(top)
}

func (in *Interp) hashinsertop(ctx context.Context) {
	value := in.popptr()
	key := in.popptr()
	h := in.pop().(*bighash.Hash)

	h.Insert(ctx, key, value)
}

func (in *Interp) hashgetop(ctx context.Context) {
	key := in.popptr()
	h := in.pop().(*bighash.Hash)

	p, _ := h.Get(ctx, key)
	in.push(p)
}

func (in *Interp) arrowop() {
	name := in.pop().(*ast.Lexeme)
	if name.Category() != token.IDENT {
		panic("arrow target is not an identifier")
	}
	in.vars[name.Literal()] = in.pop()
}

func (in *Interp) addop(ctx context.Context) {
	a := in.popnum()
	b := in.popnum()
	in.push(b.Add(b, a))
}

func (in *Interp) mulop(ctx context.Context) {
	a := in.popnum()
	b := in.popnum()
	in.push(b.Mul(b, a))
}

func (in *Interp) divop(ctx context.Context) {
	a := in.popnum()
	b := in.popnum()
	in.push(b.Quo(b, a))
}

func (in *Interp) subop(ctx context.Context) {
	a := in.popnum()
	b := in.popnum()
	in.push(b.Sub(b, a))
}

func (in *Interp) nullop(ctx context.Context) {
	in.push(ptr.Null)
}

func (in *Interp) toptrop(ctx context.Context) {
	a := in.popstring()
	p, ok := ptr.DecodeStringPtr(a)
	if !ok {
		panic("not a valid encoded ptr: {" + a + "}")
	}
	in.push(p)
}

func (in *Interp) stringvalueop(ctx context.Context) {
	a := in.popptr()
	in.push(a.StringValue())
}

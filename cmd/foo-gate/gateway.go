package main

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/ptr"
	"bitbucket.org/fufoo/foo/lib/camasset"
	"bitbucket.org/fufoo/foo/lib/foo"
	"bitbucket.org/fufoo/foo/lib/keycreds"
	"bitbucket.org/fufoo/foo/services/cam"
	"github.com/go-chi/chi"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var log = logging.New("foo-gate")

func main() {

	ctx := context.Background()
	db, err := initFoo(ctx)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to boot: %s\n", err)
		os.Exit(1)
	}

	// static and hardcoded for now; TODO make it dynamic so that
	// key shredding can delete assets
	s := &server{
		db:  db,
		cdn: keycreds.KeyFromString("some static stuff"),
	}

	mux := chi.NewMux()
	mux.Get("/cam/{ptr}/{name}", s.get)
	mux.Post("/cam/{name}", s.post)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	http.ListenAndServe(":"+port, mux)
}

type server struct {
	db  *foo.Conn
	cdn foo.Credentials
}

func (s *server) get(w http.ResponseWriter, r *http.Request) {
	p, ok := ptr.DecodeStringPtr(chi.URLParam(r, "ptr"))
	if !ok {
		w.Header().Set("Content-Type", "text/plain")
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid ptr\n"))
		return
	}
	if !p.IsLoadable() {
		w.Header().Set("Content-Type", "text/plain")
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("ptr is not loadable\n"))
		return
	}

	camasset.Serve(w, r, s.db, s.cdn, p)
}

func (s *server) post(w http.ResponseWriter, r *http.Request) {
	domainStr := r.Header.Get("X-Foo-Domain")
	if domainStr == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("missing X-Foo-Domain header\n"))
		return
	}
	domain, err := strconv.ParseUint(domainStr, 10, 64)
	if err != nil || domain < 2 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "invalid X-Foo-Domain header value %q\n", domain)
		return
	}
	ticket := r.Header.Get("X-Foo-Cam-Upload-Ticket")
	if ticket == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("missing X-Foo-Cam-Upload-Ticket header\n"))
		return
	}

	ctx := r.Context()

	name := chi.URLParam(r, "name")

	asset := cam.Asset{
		Name:    name,
		Mime:    r.Header.Get("Content-Type"),
		ModTime: timestamppb.Now(),
	}

	if mtime := r.Header.Get("X-Foo-Cam-Modified-Time"); mtime != "" {
		if t, err := time.Parse("2006-01-02 15:04:05.999999999 Z0700", mtime); err == nil {
			asset.ModTime = timestamppb.New(t)
		} else if t, err := time.Parse(time.RFC3339Nano, mtime); err == nil {
			asset.ModTime = timestamppb.New(t)
		} else {
			_, err := time.Parse("2006-01-02 15:04:05.999999999 Z0700", mtime)

			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, "invalid X-Foo-Cam-Modified-Time header %q\n%s\n", mtime, err)
			//w.Write([]byte("invalid X-Foo-Cam-Modified-Time header\n"))
			return
		}
	}

	h := sha256.New()

	// small for now; TODO handle large uploads
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	// shared secret for now... TODO fix this and get real
	uploadSecret := os.Getenv("UPLOAD_SECRET")
	h.Write([]byte(uploadSecret))
	h.Write(buf)
	if hex.EncodeToString(h.Sum(nil)) != ticket {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("mismatching X-Foo-Cam-Upload-Ticket\n"))
		return
	}

	asset.Frags = append(asset.Frags, &cam.Fragment{
		Size:   uint32(len(buf)),
		Inline: buf,
	})

	var p ptr.Ptr

	err = s.db.Access(domain, s.cdn).
		Update(ctx, func(ctx context.Context, wr foo.WriteTxn) (err error) {
			p, err = wr.Store(ctx, &asset)
			return
		})

	if err != nil {
		log.Warningf(ctx, "failed to write asset: %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("problem storing asset\n"))
		return
	}

	w.Header().Set("Location", fmt.Sprintf("/cam/%s/%s", p, name))
}

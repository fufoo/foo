package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"os"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
	"bitbucket.org/fufoo/foo/lib/foo"
)

type externalClientConfig struct {
	Domain      uint64 `json:"domain"`
	CATrustFile string `json:"caTrustFile"`
	Debug       string `json:"debug"`
	Server      string `json:"server"`
	Auth        struct {
		Mechanism string `json:"mechanism"`
		User      string `json:"user"`
		Password  string `json:"password"`
		File      string `json:"file"`
	} `json:"auth"`
}

func readConfig(src string) (*externalClientConfig, error) {
	buf, err := ioutil.ReadFile(src)
	if err != nil {
		return nil, err
	}

	xc := new(externalClientConfig)
	err = json.Unmarshal(buf, xc)
	if err != nil {
		return nil, err
	}
	return xc, nil
}

func initFoo(ctx context.Context) (*foo.Conn, error) {

	configFile := os.Getenv("FOO_CONFIG")

	if configFile == "" {
		return nil, errlib.New("FCS-1099", "No FOO_CONFIG specified in the environment")
	}

	xc, err := readConfig(configFile)
	if err != nil {
		return nil, err
	}
	if xc.Debug == "" {
		xc.Debug = "info"
	}
	logging.Init(logging.LowestLevel(xc.Debug))

	cfg := foo.RemoteConfig{
		TrustFile: xc.CATrustFile,
		Server:    xc.Server,
	}
	if xc.Auth.Mechanism != "client-key" {
		return nil, errlib.Newf("CAP-1902 Only \"client-key\" auth mechanism supported by caps, not %q", xc.Auth.Mechanism)
	}
	cfg.UseClientAuthFromFile(xc.Auth.File)

	conn, err := foo.DialRemote(ctx, &cfg)
	if err != nil {
		return nil, err
	}

	return conn, nil
}


# Foo V2

```
foo/
  cmd/
    foo/                - command line interface for the public client API
    fooctl/             - command line interface for the private internal cluster API
    foo-zerod/          - bootstrap server
    foo-authd/          - authentication exchange service
    foo-stated/         - state server
    foo-camd/           - storage server
    foo-gated/          - gateway server (REST and gRPC API)
    foo-ird/            - inter-regional cross-connect server
    foo-gcd/            - garbage collection and accounting server
    foo-sidecar/        - sidecar gateway
    foo-indexer/        - indexing service
    foo-scriptd/        - foo fu script engine
    internal/           - libraries to support servers
    
  internal/             - internal libraries
  lib/                  - public libraries
    foo/                - main library used by clients
    bighash/            - big hash tables
    biglist/            - big lists
    
```


```go
import "bitbucket.org/fufoo/foo/lib/foo"

func runit() {
  f, _ := foo.Dial("http://foo-sidecar")
  p, err := f.Store(ctx, &Object{...})
  
}
```

## Data Ownership and Accounting

For CAM storage, accounting is tied to key ids.  Since key ids
naturally partition the CAM space, and key shredding is the way to
delete an account, this is a natural fit.

Domain accounting for state management is handled more explicitly,
with first class accounts and ownership.

## Cross-region Replication

### Failover

Failover is achieved by replicating both the state of a domain and the
data reachable from it.

The participating regions have a model of the state of failover
replication in terms of how far "behind" the replica is.  If the
replica is "nearly current", then it is suitable for read operations.
When a replica is first created, it is too far behind to make read
operations sensible.

"Nearly current" means that *a* consistent snapshot has been
replicated.  (Consistent snapshot meaning a complete set of state plus
all the data reachable from that state)

### Latency

Data can be replicated across regions for latency enhancement reasons.
This can be driven by push operations, directed pull operations, or
scanned pull operations.

#### Push Operations

## Deployment Stacks

### Staging Deployment

us-central1 (Iowa)
australia-southeast1 (Sydney)
europe-west3 (Frankfurt)

### Worldwide Production Deployment

us-east4 (Northern Virginia)
us-central1 (Iowa)
us-west2 (Los Angeles)
europe-central2 (Warsaw)
europe-west3 (Frankfurt)
asia-east1 (Taiwan)
asia-south2 (Delhi)
australia-southeast1 (Sydney)

## Authentication

Principals can configure their own credentials using any of several
authentication mechanisms that are supported by the auth service.
However, they must contact the auth service (`foo-authd`) to exchange
their credentials for a short-lived JWT that is trusted by all the
services.

## Inter-Regional Transit

The inter-regional transfer daemon is responsible for following the
transaction log of a remote region and transferring the data (state
and CAM).

Some self-pacing is possible in which if the region gets far behind it
can stop transferring data that is no longer reachable from any
current state pointer.  This could be a bit tricky to set up because
it's not always obvious which data pending transfer is reachable or
not from the current state graph.  It is also tricky because the
transfer process inherently violates the constraint that if we have a
datum X then we also have everything reachable from X (an invariant we
maintain for GC purposes).  This suggests we need some kind of staging
area to hold data that has been transferred but no everything
reachable has been transferred.

## Local Dev Stack

### Hostnames and Certificates

```
cd ~/p/axis-net-ca
./create-certificate-for-domain.sh 1411 dev-donovan-local.callisto.axis.rscheme.org
export FOO_NODE_CERT=$(readlink -f ca/1411.pem)
export FOO_NODE_KEY=$(readlink -f ca/1411.key)
export FOO_CA_TRUST=$(readlink -f root-ca.pem)
```

(the root CA is already trusted)

### Ports

```
FOO_ZERO_PORT=2200
FOO_STATE1_PORT=2211
FOO_STATE2_PORT=2212
FOO_STATE3_PORT=2213
FOO_CAM1_PORT=2221
FOO_CAM2_PORT=2222
FOO_CAM3_PORT=2223
FOO_CAM4_PORT=2224
FOO_CAM5_PORT=2225
FOO_XAUTH1_PORT=2231
```

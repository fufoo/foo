FROM golang:1.18.2-alpine3.15 AS build

RUN apk add --no-cache git gnupg
WORKDIR /go/foo
ADD . /go/foo
ENV CGO_ENABLED=0
RUN go run tools/go-build-command2.go -vendor -run -output=/go/bin cmd/fooctl
RUN go run tools/go-build-command2.go -vendor -run -output=/go/bin cmd/foo-zero
RUN go run tools/go-build-command2.go -vendor -run -output=/go/bin cmd/foo-authd
RUN go run tools/go-build-command2.go -vendor -run -output=/go/bin cmd/foo-stated
RUN go run tools/go-build-command2.go -vendor -run -output=/go/bin cmd/foo-camd
RUN go run tools/go-build-command2.go -vendor -run -output=/go/bin cmd/foo-gate
RUN go run tools/go-build-command2.go -vendor -run -output=/go/bin cmd/run-foo-dev-cluster

FROM alpine:3.15
# RUN apk --no-cache add ca-certificates
COPY --from=build /go/bin/* /usr/local/bin/
ADD tools/launch-dev.sh /root/launch-dev.sh
